
-- This Script is only compatible with HSQL, not MySQL.

INSERT INTO Products (id, description, style, finish, price, productCode, quantity, archived, stockPosition, previousVersion, primaryPictureId)
VALUES 1, 'A silver ring with engraving', 'Ring', 'Silver', 122.75, 'SR0011', '43', false, 'A3', 0, 1;

INSERT INTO Products (id, description, style, finish, price, productCode, quantity, archived, stockPosition, previousVersion, primaryPictureId)
VALUES 2,  'A gold ring with engraving', 'Ring', 'Gold', 355.25, 'GR0011', '2', false, 'A1', 0, 2;

INSERT INTO Products (id, description, style, finish, price, productCode, quantity, archived, stockPosition, previousVersion)
VALUES 3, 'A silver bracelet with attachments', 'Bracelet', 'Silver', 76.15, 'SB0017', '8', false, 'B2', 0;