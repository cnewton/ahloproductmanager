
-- This Script is only compatible with HSQL, not MySQL.

INSERT INTO Quotations (id, name, buyerId, price, status, discount, revisionNo, remarks, lastModified, invoiceId)
VALUES 1, 'Quotation for Mains Jewellers', 2, 0.00, 'REVIEW', 0.00, 0, 'No remarks', to_date('2000-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), 1;

INSERT INTO Quotations (id, name, buyerId, price, status, discount, revisionNo, remarks, lastModified, invoiceId)
VALUES 2, 'Quotation for Promenade Jewellery', 1, 0.00, 'REVIEW', 0.00, 0, 'No remarks', to_date('2000-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), 1;

INSERT INTO Quotations (id, name, buyerId, price, status, discount, revisionNo, remarks, lastModified, invoiceId)
VALUES 3, 'Quotation for Jewellery One HK', 1, 0.00, 'REVIEW', 0.00, 0, 'No remarks', to_date('2000-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'), 1;