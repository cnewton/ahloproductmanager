
-- This Script is only compatible with HSQL, not MySQL.

-- All passwords are the BCrypt hashed version of "password"

INSERT INTO Users (id, username, password, enabled, name, email)
VALUES (1, 'Test User', '$2a$10$jjxpHYEL9BW5nAdxZcl0GuWlt9bvFUQOtW/XuJAZg4DpphtEghPD2', true, 'John Tester', 'test@test.com');

INSERT INTO Users (id, username, password, enabled, name, email)
VALUES (2, 'Test Admin', '$2a$10$jjxpHYEL9BW5nAdxZcl0GuWlt9bvFUQOtW/XuJAZg4DpphtEghPD2', true, 'John Tester', 'test@test.com');

INSERT INTO Users (id, username, password, enabled, name, email)
VALUES (3, 'Test Disabled', '$2a$10$jjxpHYEL9BW5nAdxZcl0GuWlt9bvFUQOtW/XuJAZg4DpphtEghPD2', false, 'John Tester', 'test@test.com');

INSERT INTO UserRoles(id, userId, role)
VALUES (1, '1', 'ROLE_USER');

INSERT INTO UserRoles(id, userId, role)
VALUES (2, '2', 'ROLE_USER');

INSERT INTO UserRoles(id, userId, role)
VALUES (3, '2', 'ROLE_ADMIN');