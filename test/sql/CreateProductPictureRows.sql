
-- This Script is only compatible with HSQL, not MySQL.

INSERT INTO ProductPictures (id, encodeType, productId)
VALUES 1, 'jpg', 1;
INSERT INTO ProductPictures (id, encodeType, productId)
VALUES 2, 'jpg', 1;
INSERT INTO ProductPictures (id, encodeType, productId)
VALUES 3, 'jpg', 1;

INSERT INTO S3ProductPictures (id, s3Bucket, s3FileKey)
VALUES 1, 'testBucket1', 'testFileKey1';

INSERT INTO DatabaseProductPictures (id, picture)
VALUES (2, 'CAFEBABE');
