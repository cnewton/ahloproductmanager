
-- This Script is only compatible with HSQL, not MySQL.

INSERT INTO SerializedQuotations (id, quotationId, status, quotation)
VALUES 1, 1, 'ACCEPTED', '{"id":1,"name":"famous","buyer":{"id":1,"name":"bossman of test","phone":"+85211112222","email":"test@me.com","address":"123 Fake St, Simpsonville","company":"NewCross Inc","previousVersion":0,"visible":true},"products":[],"price":18.43209999999999837427822058089077472686767578125,"status":"ACCEPTED","discount":1.399999999999999911182158029987476766109466552734375,"revisionNo":0,"remarks":"No Remarks","lastModified":[2016,1,23,19,53,11,398000000]}';

INSERT INTO SerializedQuotations (id, quotationId, status, quotation)
VALUES 2, 1, 'ACCEPTED', '{"id":1,"name":"famous","buyer":{"id":1,"name":"bossman of test","phone":"+85211112222","email":"test@me.com","address":"123 Fake St, Simpsonville","company":"NewCross Inc","previousVersion":0,"visible":true},"products":[],"price":18.43209999999999837427822058089077472686767578125,"status":"ACCEPTED","discount":1.399999999999999911182158029987476766109466552734375,"revisionNo":0,"remarks":"No Remarks","lastModified":[2016,1,23,19,53,11,398000000]}';

INSERT INTO SerializedQuotations (id, quotationId, status, quotation)
VALUES 3, 1, 'ACCEPTED', '{"id":1,"name":"famous","buyer":{"id":1,"name":"bossman of test","phone":"+85211112222","email":"test@me.com","address":"123 Fake St, Simpsonville","company":"NewCross Inc","previousVersion":0,"visible":true},"products":[],"price":18.43209999999999837427822058089077472686767578125,"status":"ACCEPTED","discount":1.399999999999999911182158029987476766109466552734375,"revisionNo":0,"remarks":"No Remarks","lastModified":[2016,1,23,19,53,11,398000000]}';
