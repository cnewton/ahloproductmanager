
-- This Script is only compatible with HSQL, not MySQL.

INSERT INTO Buyers (id, name, phone, email, address, company, visible, previousVersion)
VALUES 1, 'Ching Kwun Yau', '+85211112222', 'CKY@yahoo.com.hk', '123 Kowloon St', 'Promenade Jewellery', true, 0;

INSERT INTO Buyers (id, name, phone, email, address, company, visible, previousVersion)
VALUES 2, 'Chan Ah Dong', '+85222223333', 'CAD@yahoo.com.hk', '125 Kowloon St', 'Mains Jewellers', true, 0;

INSERT INTO Buyers (id, name, phone, email, address, company, visible, previousVersion)
VALUES 3, 'Fai Hung Lim', '+85288888888', 'FHL@yahoo.com.hk', '99 Ran Dum Rd', 'Jewellery One HK', true, 0;