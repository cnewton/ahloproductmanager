package hk.com.ahlopm.productmanager.test.dao.productpicture;

import static org.junit.Assert.assertEquals;
import hk.com.ahlopm.productmanager.dao.productpicture.ProductPictureDAO;
import hk.com.ahlopm.productmanager.domainobject.productpicture.ProductPicture;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;
import hk.com.ahlopm.productmanager.test.dao.DomainObjectDAOTest;
import hk.com.ahlopm.productmanager.test.dao.DomainObjectDAOTester;

import org.hibernate.HibernateException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class ProductPictureDAOTest extends SpringTest implements DomainObjectDAOTest {

    @Autowired
    private DomainObjectDAOTester<ProductPicture> tester;

    @Autowired
    private ProductPictureDAO productPictureDAO;

    private ProductPicture testProductPicture;

    @Before
    public void setUp() throws Exception {

        testProductPicture = DomainObjectFactory.createProductPicture();
        tester.setDAO(productPictureDAO);
        tester.setTestObject(testProductPicture);
    }

    @After
    public void tearDown() throws Exception {

        // DB gets cleaned up automatically, no work to do here.
    }

    @Test
    @Transactional
    public void testFind() {

        tester.testFind();
    }

    @Test
    @Transactional
    public void testFindNonExistant() {

        tester.testFindNonExistant();
    }

    @Test
    @Transactional
    public void testFindAll() {

        tester.testFindAll();
    }

    @Test
    @Transactional
    public void testCreate() {

        testProductPicture.setId(0);
        tester.testCreate();
    }

    @Test
    @Transactional
    public void testUpdate() {

        testProductPicture.setEncodeType("bmp");

        tester.testUpdate();

        assertEquals("Name was not updated correctly", "bmp",
                productPictureDAO.find(testProductPicture.getId()).getEncodeType());

    }

    @Test(expected = HibernateException.class)
    @Transactional
    public void testUpdateNonExistant() {

        tester.testUpdateNonExistant();
    }

    @Test
    @Transactional
    public void testDeleteByObject() {

        testProductPicture.setId(0);
        productPictureDAO.createOrUpdate(testProductPicture); // Create the
                                                              // object so that
                                                              // it may be
                                                              // deleted.
        tester.testDeleteByObject();
    }

    @Test(expected = HibernateException.class)
    @Transactional
    public void testDeleteNonExistantByObject() {

        tester.testDeleteNonExistantByObject();
    }

    @Test
    @Transactional
    public void testDeleteById() {

        tester.testDeleteById();
    }

    @Test(expected = IllegalArgumentException.class)
    @Transactional
    public void testDeleteNonExistantById() {

        tester.testDeleteNonExistantById();
    }

    @Test
    @Transactional
    public void testCount() {

        tester.testCount();
    }
}
