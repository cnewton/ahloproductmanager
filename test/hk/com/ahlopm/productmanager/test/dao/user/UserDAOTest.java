package hk.com.ahlopm.productmanager.test.dao.user;

import static org.junit.Assert.assertEquals;
import hk.com.ahlopm.productmanager.dao.user.UserDAO;
import hk.com.ahlopm.productmanager.domainobject.user.User;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;
import hk.com.ahlopm.productmanager.test.dao.DomainObjectDAOTest;
import hk.com.ahlopm.productmanager.test.dao.DomainObjectDAOTester;

import org.hibernate.HibernateException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class UserDAOTest extends SpringTest implements DomainObjectDAOTest {

    @Autowired
    private DomainObjectDAOTester<User> tester;

    @Autowired
    private UserDAO userDAO;

    private User testUser;

    @Before
    public void setUp() throws Exception {

        testUser = DomainObjectFactory.createUser();
        tester.setDAO(userDAO);
        tester.setTestObject(testUser);
    }

    @After
    public void tearDown() throws Exception {

        // DB gets cleaned up automatically, no work to do here.
    }

    @Test
    @Transactional
    public void testFind() {

        tester.testFind();
    }

    @Test
    @Transactional
    public void testFindNonExistant() {

        tester.testFindNonExistant();
    }

    @Test
    @Transactional
    public void testFindAll() {

        tester.testFindAll();
    }

    @Test
    @Transactional
    public void testCreate() {

        testUser.setId(0);
        tester.testCreate();
    }

    @Test
    @Transactional
    public void testUpdate() {

        testUser.setName("updated");

        tester.testUpdate();

        assertEquals("Name was not updated correctly", "updated", userDAO.find(testUser.getId()).getName());

    }

    @Test(expected = HibernateException.class)
    @Transactional
    public void testUpdateNonExistant() {

        tester.testUpdateNonExistant();
    }

    @Test
    @Transactional
    public void testDeleteByObject() {

        testUser.setId(0);
        userDAO.createOrUpdate(testUser); // Create the object so that it may
                                          // be deleted.
        tester.testDeleteByObject();
    }

    @Test(expected = HibernateException.class)
    @Transactional
    public void testDeleteNonExistantByObject() {

        tester.testDeleteNonExistantByObject();
    }

    @Test
    @Transactional
    public void testDeleteById() {

        tester.testDeleteById();
    }

    @Test(expected = IllegalArgumentException.class)
    @Transactional
    public void testDeleteNonExistantById() {

        tester.testDeleteNonExistantById();
    }

    @Test
    @Transactional
    public void testCount() {

        tester.testCount();
    }
}
