package hk.com.ahlopm.productmanager.test.dao;

public interface DomainObjectDAOTest {

    public void testFind();

    public void testFindNonExistant();

    public void testFindAll();

    public void testCreate();

    public void testUpdate();

    public void testUpdateNonExistant();

    public void testDeleteByObject();

    public void testDeleteNonExistantByObject();

    public void testDeleteById();

    public void testDeleteNonExistantById();

    public void testCount();

}
