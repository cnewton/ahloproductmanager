package hk.com.ahlopm.productmanager.test.dao.serializedquotation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import hk.com.ahlopm.productmanager.dao.quotation.QuotationDAO;
import hk.com.ahlopm.productmanager.dao.serializedquotation.SerializedQuotationDAO;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.quotation.QuotationStatus;
import hk.com.ahlopm.productmanager.domainobject.serializedquotation.SerializedQuotation;
import hk.com.ahlopm.productmanager.exception.SerializedQuotationPersistenceException;
import hk.com.ahlopm.productmanager.service.serializedquotation.SerializedQuotationService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;
import hk.com.ahlopm.productmanager.test.dao.DomainObjectDAOTest;
import hk.com.ahlopm.productmanager.test.dao.DomainObjectDAOTester;

import org.hibernate.HibernateException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class SerializedQuotationDAOTest extends SpringTest implements DomainObjectDAOTest {

    @Autowired
    private DomainObjectDAOTester<SerializedQuotation> tester;

    @Autowired
    private SerializedQuotationDAO serializedQuotationDAO;

    @Autowired
    private SerializedQuotationService serializedQuotationService;

    @Autowired
    private QuotationDAO quotationDAO;

    private SerializedQuotation testQuotation;

    @Before
    public void setUp() throws Exception {

        testQuotation = DomainObjectFactory.createSerializedQuotation();
        tester.setDAO(serializedQuotationDAO);
        tester.setTestObject(testQuotation);
    }

    @After
    public void tearDown() throws Exception {

        // DB gets cleaned up automatically, no work to do here.
    }

    @Test
    @Transactional
    public void testFind() {

        tester.testFind();
    }

    @Test
    @Transactional
    public void testFindNonExistant() {

        tester.testFindNonExistant();
    }

    @Test
    @Transactional
    public void testFindAll() {

        tester.testFindAll();
    }

    @Test
    @Transactional
    public void testCreate() {

        testQuotation.setId(0);
        tester.testCreate();
    }

    @Test
    @Transactional
    public void testUpdate() {

        testQuotation.setQuotationId(111);

        tester.testUpdate();

        assertEquals("QuotationId was not updated correctly", 111,
                serializedQuotationDAO.find(testQuotation.getId()).getQuotationId());

    }

    @Test(expected = HibernateException.class)
    @Transactional
    public void testUpdateNonExistant() {

        tester.testUpdateNonExistant();
    }

    @Test
    @Transactional
    public void testDeleteByObject() {

        testQuotation.setId(0);
        serializedQuotationDAO.createOrUpdate(testQuotation); // Create the
                                                              // object so that
        // it may
        // be deleted.
        tester.testDeleteByObject();
    }

    @Test(expected = HibernateException.class)
    @Transactional
    public void testDeleteNonExistantByObject() {

        tester.testDeleteNonExistantByObject();
    }

    @Test
    @Transactional
    public void testDeleteById() {

        tester.testDeleteById();
    }

    @Test(expected = IllegalArgumentException.class)
    @Transactional
    public void testDeleteNonExistantById() {

        tester.testDeleteNonExistantById();
    }

    @Test
    @Transactional
    public void testCount() {

        tester.testCount();
    }

    @Test
    @Transactional
    public void testFindAllByQuotation() throws SerializedQuotationPersistenceException {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);
        quotationDAO.createOrUpdate(testQuotation);

        serializedQuotationService.serializeQuotation(testQuotation.getId());

        assertEquals("The method should have returned a list of size 1.", 1,
                serializedQuotationDAO.findByQuotationId(testQuotation.getId()).size());

        assertEquals("The method should have returned a serial entry with the id of the quotation",
                testQuotation.getId(),
                serializedQuotationDAO.findByQuotationId(testQuotation.getId()).iterator().next().getQuotationId());
    }

    @Test
    @Transactional
    public void testFindPresentingByQuotation() throws SerializedQuotationPersistenceException {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.PRESENTING);
        quotationDAO.createOrUpdate(testQuotation);

        Quotation testQuotation2 = DomainObjectFactory.createQuotation();
        testQuotation2.setId(0);
        testQuotation2.setStatus(QuotationStatus.DECLINED);
        quotationDAO.createOrUpdate(testQuotation2);

        serializedQuotationService.serializeQuotation(testQuotation.getId());
        serializedQuotationService.serializeQuotation(testQuotation2.getId());

        assertNotNull(serializedQuotationDAO.findPresentingByQuotationId(testQuotation.getId()));
        assertNull(serializedQuotationDAO.findPresentingByQuotationId(testQuotation2.getId()));
    }
}
