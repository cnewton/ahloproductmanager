package hk.com.ahlopm.productmanager.test.dao.quotation;

import static org.junit.Assert.assertEquals;
import hk.com.ahlopm.productmanager.dao.quotation.QuotationDAO;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.quotation.QuotationStatus;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;
import hk.com.ahlopm.productmanager.test.dao.DomainObjectDAOTest;
import hk.com.ahlopm.productmanager.test.dao.DomainObjectDAOTester;

import org.hibernate.HibernateException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class QuotationDAOTest extends SpringTest implements DomainObjectDAOTest {

    @Autowired
    private DomainObjectDAOTester<Quotation> tester;

    @Autowired
    private QuotationDAO quotationDAO;

    private Quotation testQuotation;

    @Before
    public void setUp() throws Exception {

        testQuotation = DomainObjectFactory.createQuotation();
        tester.setDAO(quotationDAO);
        tester.setTestObject(testQuotation);
    }

    @After
    public void tearDown() throws Exception {

        // DB gets cleaned up automatically, no work to do here.
    }

    @Test
    @Transactional
    public void testFind() {

        tester.testFind();
    }

    @Test
    @Transactional
    public void testFindNonExistant() {

        tester.testFindNonExistant();
    }

    @Test
    @Transactional
    public void testFindAll() {

        tester.testFindAll();
    }

    @Test
    @Transactional
    public void testCreate() {

        testQuotation.setId(0);
        tester.testCreate();
    }

    @Test
    @Transactional
    public void testUpdate() {

        testQuotation.setName("updated");

        tester.testUpdate();

        assertEquals("Name was not updated correctly", "updated", quotationDAO.find(testQuotation.getId()).getName());

    }

    @Test(expected = HibernateException.class)
    @Transactional
    public void testUpdateNonExistant() {

        tester.testUpdateNonExistant();
    }

    @Test
    @Transactional
    public void testDeleteByObject() {

        testQuotation.setId(0);
        quotationDAO.createOrUpdate(testQuotation); // Create the object so that
                                                    // it may
        // be deleted.
        tester.testDeleteByObject();
    }

    @Test(expected = HibernateException.class)
    @Transactional
    public void testDeleteNonExistantByObject() {

        tester.testDeleteNonExistantByObject();
    }

    @Test
    @Transactional
    public void testDeleteById() {

        tester.testDeleteById();
    }

    @Test(expected = IllegalArgumentException.class)
    @Transactional
    public void testDeleteNonExistantById() {

        tester.testDeleteNonExistantById();
    }

    @Test
    @Transactional
    public void testCount() {

        tester.testCount();
    }

    @Test
    @Transactional
    public void testFindAllInReview() {

        long countBefore = quotationDAO.findAllInReview().size();

        Quotation quote1 = DomainObjectFactory.createQuotation();
        quote1.setId(0);
        quote1.setStatus(QuotationStatus.REVIEW);

        Quotation quote2 = DomainObjectFactory.createQuotation();
        quote2.setId(0);
        quote2.setStatus(QuotationStatus.ACCEPTED);

        quotationDAO.createOrUpdate(quote1);
        quotationDAO.createOrUpdate(quote2);

        long countAfter = quotationDAO.findAllInReview().size();

        assertEquals("The size of the returned list should only increase by 1.", countBefore + 1, countAfter);
        assertEquals("The returned list should contain the quote of the correct state.", true,
                quotationDAO.findAllInReview().contains(quote1));
        assertEquals("The returned list should not contain the quote of the incorrect state.", false,
                quotationDAO.findAllInReview().contains(quote2));
    }

    @Test
    @Transactional
    public void testFindAllInManagement() {

        long countBefore = quotationDAO.findAllInManagement().size();

        Quotation quote1 = DomainObjectFactory.createQuotation();
        quote1.setId(0);
        quote1.setStatus(QuotationStatus.REVIEW);

        Quotation quote2 = DomainObjectFactory.createQuotation();
        quote2.setId(0);
        quote2.setStatus(QuotationStatus.ACCEPTED);

        quotationDAO.createOrUpdate(quote1);
        quotationDAO.createOrUpdate(quote2);

        long countAfter = quotationDAO.findAllInManagement().size();

        assertEquals("The size of the returned list should only increase by 1.", countBefore + 1, countAfter);
        assertEquals("The returned list should contain the quote of the correct state.", true,
                quotationDAO.findAllInManagement().contains(quote2));
        assertEquals("The returned list should not contain the quote of the incorrect state.", false,
                quotationDAO.findAllInManagement().contains(quote1));
    }
}
