package hk.com.ahlopm.productmanager.test.dao.quoteproduct;

import static org.junit.Assert.assertEquals;
import hk.com.ahlopm.productmanager.dao.quoteproduct.QuoteProductDAO;
import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;
import hk.com.ahlopm.productmanager.test.dao.DomainObjectDAOTest;
import hk.com.ahlopm.productmanager.test.dao.DomainObjectDAOTester;

import org.hibernate.HibernateException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class QuoteProductDAOTest extends SpringTest implements DomainObjectDAOTest {

    @Autowired
    private DomainObjectDAOTester<QuoteProduct> tester;

    @Autowired
    private QuoteProductDAO quoteProductDAO;

    private QuoteProduct testQuoteProduct;

    @Before
    public void setUp() throws Exception {

        testQuoteProduct = DomainObjectFactory.createQuoteProduct();
        tester.setDAO(quoteProductDAO);
        tester.setTestObject(testQuoteProduct);
    }

    @After
    public void tearDown() throws Exception {

        // DB gets cleaned up automatically, no work to do here.
    }

    @Test
    @Transactional
    public void testFind() {

        tester.testFind();
    }

    @Test
    @Transactional
    public void testFindNonExistant() {

        tester.testFindNonExistant();
    }

    @Test
    @Transactional
    public void testFindAll() {

        tester.testFindAll();
    }

    @Test
    @Transactional
    public void testCreate() {

        testQuoteProduct.setId(0);
        tester.testCreate();
    }

    @Test
    @Transactional
    public void testUpdate() {

        testQuoteProduct.setQuantity(111);

        tester.testUpdate();

        assertEquals("Quantity was not updated correctly", 111,
                quoteProductDAO.find(testQuoteProduct.getId()).getQuantity());

    }

    @Test(expected = HibernateException.class)
    @Transactional
    public void testUpdateNonExistant() {

        tester.testUpdateNonExistant();
    }

    @Test
    @Transactional
    public void testDeleteByObject() {

        testQuoteProduct.setId(0);
        quoteProductDAO.createOrUpdate(testQuoteProduct); // Create the object
                                                          // so that it may
        // be deleted.
        tester.testDeleteByObject();
    }

    @Test(expected = HibernateException.class)
    @Transactional
    public void testDeleteNonExistantByObject() {

        tester.testDeleteNonExistantByObject();
    }

    @Test
    @Transactional
    public void testDeleteById() {

        tester.testDeleteById();
    }

    @Test(expected = IllegalArgumentException.class)
    @Transactional
    public void testDeleteNonExistantById() {

        tester.testDeleteNonExistantById();
    }

    @Test
    @Transactional
    public void testCount() {

        tester.testCount();
    }
}
