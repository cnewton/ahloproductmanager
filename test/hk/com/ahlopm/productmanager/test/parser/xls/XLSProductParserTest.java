package hk.com.ahlopm.productmanager.test.parser.xls;

import static org.junit.Assert.assertEquals;
import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.parser.xls.XLSProductParser;
import hk.com.ahlopm.productmanager.service.product.ProductService;
import hk.com.ahlopm.productmanager.test.common.SpringTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.transaction.annotation.Transactional;

import com.amazonaws.util.IOUtils;

public class XLSProductParserTest extends SpringTest {

    private XLSProductParser parser;

    private File testFile;

    @Autowired
    private ProductService productService;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

        testFile = null;
        parser = null;
    }

    @Test
    @Transactional
    public void testValid1Product0Image() throws IOException {

        testFile = new ClassPathResource("xls/valid1Product0Image.xlsx").getFile();
        InputStream stream = new FileInputStream(testFile);
        parser = new XLSProductParser(IOUtils.toByteArray(stream));
        Product product = parser.parseProductRow();

        assertEquals("There should be 0 images returned.", 0, parser.parseProductPictures().size());

        assertEquals("The parser should now be EOF.", true, parser.eof());
        assertEquals("The product should be creatable.", product, productService.create(product));
    }

    @Test
    @Transactional
    public void testValid1Product1Image() throws IOException {

        testFile = new ClassPathResource("xls/valid1Product1Image.xlsx").getFile();
        InputStream stream = new FileInputStream(testFile);
        parser = new XLSProductParser(IOUtils.toByteArray(stream));
        Product product = parser.parseProductRow();

        assertEquals("There should be 1 images returned.", 1, parser.parseProductPictures().size());

        assertEquals("The parser should now be EOF.", true, parser.eof());
        assertEquals("The product should be creatable.", product, productService.create(product));
    }

    @Test
    @Transactional
    public void testValid2Product2Image() throws IOException {

        testFile = new ClassPathResource("xls/valid2Product2Image.xlsx").getFile();
        InputStream stream = new FileInputStream(testFile);
        parser = new XLSProductParser(IOUtils.toByteArray(stream));

        Product product = parser.parseProductRow();
        assertEquals("There should be 1 images returned.", 1, parser.parseProductPictures().size());
        assertEquals("The parser should not be EOF.", false, parser.eof());
        assertEquals("The product should be creatable.", product, productService.create(product));

        Product product2 = parser.parseProductRow();
        assertEquals("There should be 1 images returned.", 1, parser.parseProductPictures().size());
        assertEquals("The parser should now be EOF.", true, parser.eof());
        assertEquals("The product should be creatable.", product2, productService.create(product2));
    }

    @Test(expected = IllegalStateException.class)
    @Transactional
    public void testInvalid1ProductMissingImage() throws IOException {

        testFile = new ClassPathResource("xls/invalid1ProductMissingImage.xlsx").getFile();
        InputStream stream = new FileInputStream(testFile);
        parser = new XLSProductParser(IOUtils.toByteArray(stream));
    }

    @Test(expected = IllegalStateException.class)
    @Transactional
    public void testInvalid1ProductMoreColumns() throws IOException {

        testFile = new ClassPathResource("xls/invalid1ProductMoreColumns.xlsx").getFile();
        InputStream stream = new FileInputStream(testFile);
        parser = new XLSProductParser(IOUtils.toByteArray(stream));
    }

    @Test(expected = IllegalStateException.class)
    @Transactional
    public void testInvalid1ProductFewColumns() throws IOException {

        testFile = new ClassPathResource("xls/invalid1ProductFewColumns.xlsx").getFile();
        InputStream stream = new FileInputStream(testFile);
        parser = new XLSProductParser(IOUtils.toByteArray(stream));
    }

    @Test(expected = IllegalStateException.class)
    @Transactional
    public void testInvalid1ProductManyImage() throws IOException {

        testFile = new ClassPathResource("xls/invalid1ProductManyImage.xlsx").getFile();
        InputStream stream = new FileInputStream(testFile);
        parser = new XLSProductParser(IOUtils.toByteArray(stream));
    }

    @Test(expected = IllegalStateException.class)
    @Transactional
    public void testInvalid1ProductFewImage() throws IOException {

        testFile = new ClassPathResource("xls/invalid1ProductFewImage.xlsx").getFile();
        InputStream stream = new FileInputStream(testFile);
        parser = new XLSProductParser(IOUtils.toByteArray(stream));
    }
}
