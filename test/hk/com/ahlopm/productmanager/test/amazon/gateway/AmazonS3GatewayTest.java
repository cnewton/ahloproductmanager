package hk.com.ahlopm.productmanager.test.amazon.gateway;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import hk.com.ahlopm.productmanager.amazons3.AmazonS3Gateway;
import hk.com.ahlopm.productmanager.domainobject.productpicture.S3ProductPicture;
import hk.com.ahlopm.productmanager.exception.S3ResolutionFailureException;
import hk.com.ahlopm.productmanager.service.configuration.ConfigurationService;
import hk.com.ahlopm.productmanager.test.common.ComplexObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import com.amazonaws.AmazonServiceException;

public class AmazonS3GatewayTest extends SpringTest {

    @Autowired
    private AmazonS3Gateway awsGateway;

    @Autowired
    private ConfigurationService configService;

    @Rule
    public TestName testName = new TestName();

    private String filePrefix;

    @Before
    public void setUp() throws Exception {

        filePrefix = this.getClass().getSimpleName() + "/" + testName.getMethodName();

        ReflectionTestUtils.setField(awsGateway, "s3Client", null);
    }

    @After
    public void tearDown() throws Exception {

        ReflectionTestUtils.setField(awsGateway, "s3Client", null);
    }

    @Test
    @Transactional
    public void testValidPictureRequest() throws S3ResolutionFailureException {

        S3ProductPicture pic = ComplexObjectFactory.createExternS3ProductPicture(filePrefix);
        byte[] expectedData = pic.getObjectData();

        pic.resolveImage();

        assertEquals("The returned data should match", true, Arrays.equals(expectedData, pic.getPicture()));
    }

    @Test
    @Transactional
    public void testRequestNonExistantBucket() throws S3ResolutionFailureException {

        S3ProductPicture pic = ComplexObjectFactory.createExternS3ProductPicture(filePrefix);
        pic.setS3Bucket("invaliddatainput");
        try {
            pic.resolveImage();
            fail("Did not throw expected exception");
        } catch (S3ResolutionFailureException e) {
            AmazonServiceException ase = (AmazonServiceException) e.getCause();
            assertTrue(ase.getErrorCode().equals("NoSuchBucket"));
        }
    }

    @Test
    @Transactional
    public void testRequestNonExistantFile() throws S3ResolutionFailureException {

        S3ProductPicture pic = ComplexObjectFactory.createExternS3ProductPicture(filePrefix);
        pic.setS3FileKey("invaliddatainput");

        try {
            pic.resolveImage();
            fail("Did not throw expected exception");
        } catch (S3ResolutionFailureException e) {
            AmazonServiceException ase = (AmazonServiceException) e.getCause();
            assertTrue(ase.getErrorCode().equals("NoSuchKey"));
        }
    }

    @Test
    @Transactional
    public void testRequestIncorrectAccessKey() throws S3ResolutionFailureException {

        configService.set_AWS_S3_ACCESS_KEY("invaliddatainput");

        S3ProductPicture pic = ComplexObjectFactory.createExternS3ProductPicture(filePrefix);

        try {
            pic.resolveImage();
            fail("Did not throw expected exception");
        } catch (S3ResolutionFailureException e) {
            // Cleanup
            AmazonServiceException ase = (AmazonServiceException) e.getCause();
            assertTrue(ase.getErrorCode().equals("InvalidAccessKeyId"));
            configService.set_AWS_S3_ACCESS_KEY(null);
        }
    }

    @Test
    @Transactional
    public void testRequestIncorrectSecretKey() throws S3ResolutionFailureException {

        configService.set_AWS_S3_SECRET_KEY("invaliddatainput");

        S3ProductPicture pic = ComplexObjectFactory.createExternS3ProductPicture(filePrefix);

        try {
            pic.resolveImage();
            fail("Did not throw expected exception");
        } catch (S3ResolutionFailureException e) {
            // Cleanup
            AmazonServiceException ase = (AmazonServiceException) e.getCause();
            assertTrue(ase.getErrorCode().equals("SignatureDoesNotMatch"));
            configService.set_AWS_S3_SECRET_KEY(null);
        }
    }

    @Test
    @Transactional
    public void testValidPost() throws S3ResolutionFailureException {

        S3ProductPicture pic = ComplexObjectFactory.createExternS3ProductPicture(filePrefix);

        // Get current file contents, if any.
        byte[] beforeData = null;
        try {
            pic.resolveImage();
            beforeData = pic.getObjectData();
        } catch (Exception e) {
            // Irrelevant
        }

        // Reset data value
        pic = ComplexObjectFactory.createExternS3ProductPicture(filePrefix);
        // Post to (and overwrite) file
        awsGateway.createS3Object(pic);

        // Expect resolve result to be the same as what was posted.
        byte[] expectedData = pic.getObjectData();
        pic.resolveImage();

        assertEquals("The content should match.", true, Arrays.equals(expectedData, pic.getObjectData()));

        // To ensure that the post has not gone unexpectedly, ensure that it was
        // an overwrite.
        assertEquals("The data before original resolution should not match.", false,
                Arrays.equals(beforeData, pic.getObjectData()));

        // Cleanup (ensure future tests do not contain data that Already matches
        // expectation)
        pic.setObjectData(new byte[] { 1, 1, 1, 1 });
        awsGateway.createS3Object(pic);
    }

    @Test
    @Transactional
    public void testPostNonExistantBucket() throws S3ResolutionFailureException {

        S3ProductPicture pic = ComplexObjectFactory.createExternS3ProductPicture(filePrefix);
        pic.setS3Bucket("invaliddatainput");

        try {
            awsGateway.createS3Object(pic);
            fail("Did not throw expected exception");
        } catch (S3ResolutionFailureException e) {
            AmazonServiceException ase = (AmazonServiceException) e.getCause();
            assertTrue(ase.getErrorCode().equals("NoSuchBucket"));
        }
    }

    @Test
    @Transactional
    public void testPostNonExistantFileData() throws S3ResolutionFailureException {

        S3ProductPicture pic = ComplexObjectFactory.createExternS3ProductPicture(filePrefix);
        pic.setObjectData(null);

        try {
            awsGateway.createS3Object(pic);
            fail("Did not throw expected exception");
        } catch (S3ResolutionFailureException e) {
            assertTrue(e.getMessage().contains("No object data"));
        }
    }

    @Test
    @Transactional
    public void testPostIncorrectAccessKey() throws S3ResolutionFailureException {

        configService.set_AWS_S3_ACCESS_KEY("invaliddatainput");

        S3ProductPicture pic = ComplexObjectFactory.createExternS3ProductPicture(filePrefix);

        try {
            awsGateway.createS3Object(pic);
            fail("Did not throw expected exception");
        } catch (S3ResolutionFailureException e) {
            // Cleanup
            AmazonServiceException ase = (AmazonServiceException) e.getCause();
            assertTrue(ase.getErrorCode().equals("InvalidAccessKeyId"));
            configService.set_AWS_S3_ACCESS_KEY(null);
        }
    }

    @Test
    @Transactional
    public void testPostIncorrectSecretKey() throws S3ResolutionFailureException {

        configService.set_AWS_S3_SECRET_KEY("invaliddatainput");

        S3ProductPicture pic = ComplexObjectFactory.createExternS3ProductPicture(filePrefix);

        try {
            awsGateway.createS3Object(pic);
            fail("Did not throw expected exception");
        } catch (S3ResolutionFailureException e) {
            // Cleanup
            AmazonServiceException ase = (AmazonServiceException) e.getCause();
            assertTrue(ase.getErrorCode().equals("SignatureDoesNotMatch"));
            configService.set_AWS_S3_SECRET_KEY(null);
        }
    }

}
