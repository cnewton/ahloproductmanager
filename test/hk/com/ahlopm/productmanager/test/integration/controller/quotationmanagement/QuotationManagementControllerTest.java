package hk.com.ahlopm.productmanager.test.integration.controller.quotationmanagement;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import hk.com.ahlopm.productmanager.domainobject.buyer.Buyer;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.quotation.QuotationStatus;
import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;
import hk.com.ahlopm.productmanager.domainobject.serializedquotation.SerializedQuotation;
import hk.com.ahlopm.productmanager.service.buyer.BuyerService;
import hk.com.ahlopm.productmanager.service.quotation.QuotationService;
import hk.com.ahlopm.productmanager.service.quotationmanagement.QuotationManagementService;
import hk.com.ahlopm.productmanager.service.serializedquotation.SerializedQuotationService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;

public class QuotationManagementControllerTest extends SpringTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private QuotationService quotationService;

    @Autowired
    private SerializedQuotationService serializedQuotationService;

    @Autowired
    private BuyerService buyerService;

    @Autowired
    private QuotationManagementService quotationManagementService;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testFinalizeQuotation() throws Exception {

        Buyer buyer = DomainObjectFactory.createBuyer();
        buyer.setId(0);
        buyerService.create(buyer);

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setBuyer(buyer);
        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.REVIEW);

        QuoteProduct qProduct = DomainObjectFactory.createQuoteProduct();
        qProduct.setQuotation(testQuotation);
        qProduct.setId(0);
        testQuotation.getProducts().add(qProduct);

        quotationService.create(testQuotation);

        mockMvc.perform(put("/quotation/" + testQuotation.getId() + "/finalize")).andExpect(status().isOk());

        testQuotation = quotationService.find(testQuotation.getId());

        assertEquals("The quotation should now be in the finalizing state.", QuotationStatus.FINALIZING,
                testQuotation.getStatus());

        // Cleanup
        quotationService.delete(testQuotation);
        buyerService.delete(buyer);
    }

    @Test
    public void testFinalizeNonExistantQuotation() throws Exception {

        mockMvc.perform(put("/quotation/-1/finalize")).andExpect(status().isNotFound());
    }

    @Test
    public void testFinalizeQuotationWrongState() throws Exception {

        Buyer buyer = DomainObjectFactory.createBuyer();
        buyer.setId(0);
        buyerService.create(buyer);

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setBuyer(buyer);
        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.FINALIZING);

        QuoteProduct qProduct = DomainObjectFactory.createQuoteProduct();
        qProduct.setQuotation(testQuotation);
        qProduct.setId(0);
        testQuotation.getProducts().add(qProduct);

        quotationService.create(testQuotation);

        mockMvc.perform(put("/quotation/" + testQuotation.getId() + "/finalize")).andExpect(status().isBadRequest());

        // Cleanup
        quotationService.delete(testQuotation);
        buyerService.delete(buyer);
    }

    @Test
    public void testFinalizeQuotationNoBuyer() throws Exception {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setBuyer(null);
        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.REVIEW);

        QuoteProduct qProduct = DomainObjectFactory.createQuoteProduct();
        qProduct.setQuotation(testQuotation);
        qProduct.setId(0);
        testQuotation.getProducts().add(qProduct);

        quotationService.create(testQuotation);

        mockMvc.perform(put("/quotation/" + testQuotation.getId() + "/finalize")).andExpect(status().isBadRequest());

        // Cleanup
        quotationService.delete(testQuotation);
    }

    @Test
    public void testFinalizeQuotationNoProduct() throws Exception {

        Buyer buyer = DomainObjectFactory.createBuyer();
        buyer.setId(0);
        buyerService.create(buyer);

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setBuyer(buyer);
        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.REVIEW);

        quotationService.create(testQuotation);

        mockMvc.perform(put("/quotation/" + testQuotation.getId() + "/finalize")).andExpect(status().isBadRequest());

        // Cleanup
        quotationService.delete(testQuotation);
        buyerService.delete(buyer);
    }

    @Test
    public void testReviewQuotation() throws Exception {

        Buyer buyer = DomainObjectFactory.createBuyer();
        buyer.setId(0);
        buyerService.create(buyer);

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setBuyer(buyer);
        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.FINALIZING);

        QuoteProduct qProduct = DomainObjectFactory.createQuoteProduct();
        qProduct.setQuotation(testQuotation);
        qProduct.setId(0);
        testQuotation.getProducts().add(qProduct);

        quotationService.create(testQuotation);

        int revision = testQuotation.getRevisionNo();

        mockMvc.perform(put("/quotation/" + testQuotation.getId() + "/review")).andExpect(status().isOk());

        testQuotation = quotationService.find(testQuotation.getId());
        assertEquals("The quotation should now be in the review state.", QuotationStatus.REVIEW,
                testQuotation.getStatus());
        assertEquals("The quotation revision number should have incremented.", revision + 1,
                testQuotation.getRevisionNo());
        // Cleanup
        quotationService.delete(testQuotation);
        buyerService.delete(buyer);
    }

    @Test
    public void testReviewNonExistantQuotation() throws Exception {

        mockMvc.perform(put("/quotation/-1/review")).andExpect(status().isNotFound());
    }

    @Test
    public void testReviewQuotationWrongState() throws Exception {

        Buyer buyer = DomainObjectFactory.createBuyer();
        buyer.setId(0);
        buyerService.create(buyer);

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setBuyer(buyer);
        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.REVIEW);

        QuoteProduct qProduct = DomainObjectFactory.createQuoteProduct();
        qProduct.setQuotation(testQuotation);
        qProduct.setId(0);
        testQuotation.getProducts().add(qProduct);

        quotationService.create(testQuotation);

        mockMvc.perform(put("/quotation/" + testQuotation.getId() + "/review")).andExpect(status().isBadRequest());

        // Cleanup
        quotationService.delete(testQuotation);
        buyerService.delete(buyer);
    }

    @Test
    public void testBeginPresentingQuotation() throws Exception {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.FINALIZING);
        quotationService.create(testQuotation);

        mockMvc.perform(put("/quotation/" + testQuotation.getId() + "/present")).andExpect(status().isOk());

        testQuotation = quotationService.find(testQuotation.getId());
        assertEquals("The quotation should now be in the presenting state.", QuotationStatus.PRESENTING,
                testQuotation.getStatus());

        SerializedQuotation sq = serializedQuotationService.findPresentingByQuotation(testQuotation.getId());

        // Cleanup
        quotationService.delete(testQuotation);
        serializedQuotationService.delete(sq);

    }

    @Test
    public void testBeginPresentingNonExistantQuotation() throws Exception {

        mockMvc.perform(put("/quotation/-1/present")).andExpect(status().isNotFound());
    }

    @Test
    public void testBeginPresentingQuotationWrongState() throws Exception {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.REVIEW);
        quotationService.create(testQuotation);

        mockMvc.perform(put("/quotation/" + testQuotation.getId() + "/present")).andExpect(status().isBadRequest());

        // Cleanup
        quotationService.delete(testQuotation);
    }

    @Test
    public void testAcceptQuotation() throws Exception {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.FINALIZING);
        quotationService.create(testQuotation);

        quotationManagementService.beginPresentingQuotation(testQuotation.getId());

        mockMvc.perform(put("/quotation/" + testQuotation.getId() + "/accept")).andExpect(status().isOk());

        testQuotation = quotationService.find(testQuotation.getId());
        assertEquals("The quotation should now be in the presenting state.", QuotationStatus.ACCEPTED,
                testQuotation.getStatus());

        SerializedQuotation sq = serializedQuotationService.findAllByQuotation(testQuotation.getId()).iterator().next();
        assertEquals("The serialized Quotation should be in the accepted state.", QuotationStatus.ACCEPTED,
                sq.getStatus());

        // Cleanup
        quotationService.delete(testQuotation);
        serializedQuotationService.delete(sq);

    }

    @Test
    public void testAcceptNonExistantQuotation() throws Exception {

        mockMvc.perform(put("/quotation/-1/accept")).andExpect(status().isNotFound());
    }

    @Test
    public void testAcceptQuotationWrongState() throws Exception {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.REVIEW);
        quotationService.create(testQuotation);

        mockMvc.perform(put("/quotation/" + testQuotation.getId() + "/accept")).andExpect(status().isBadRequest());

        // Cleanup
        quotationService.delete(testQuotation);
    }

    @Test
    public void testDeclineQuotation() throws Exception {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.FINALIZING);
        quotationService.create(testQuotation);

        quotationManagementService.beginPresentingQuotation(testQuotation.getId());

        mockMvc.perform(put("/quotation/" + testQuotation.getId() + "/decline")).andExpect(status().isOk());

        testQuotation = quotationService.find(testQuotation.getId());
        assertEquals("The quotation should now be in the presenting state.", QuotationStatus.DECLINED,
                testQuotation.getStatus());

        SerializedQuotation sq = serializedQuotationService.findAllByQuotation(testQuotation.getId()).iterator().next();
        assertEquals("The serialized Quotation should be in the declined state.", QuotationStatus.DECLINED,
                sq.getStatus());

        // Cleanup
        quotationService.delete(testQuotation);
        serializedQuotationService.delete(sq);

    }

    @Test
    public void testDeclineNonExistantQuotation() throws Exception {

        mockMvc.perform(put("/quotation/-1/decline")).andExpect(status().isNotFound());
    }

    @Test
    public void testDeclineQuotationWrongState() throws Exception {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.REVIEW);
        quotationService.create(testQuotation);

        mockMvc.perform(put("/quotation/" + testQuotation.getId() + "/decline")).andExpect(status().isBadRequest());

        // Cleanup
        quotationService.delete(testQuotation);
    }

    @Test
    public void testCancelQuotation() throws Exception {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.FINALIZING);
        quotationService.create(testQuotation);

        quotationManagementService.beginPresentingQuotation(testQuotation.getId());

        mockMvc.perform(put("/quotation/" + testQuotation.getId() + "/cancel")).andExpect(status().isOk());

        testQuotation = quotationService.find(testQuotation.getId());
        assertEquals("The quotation should now be in the presenting state.", QuotationStatus.CANCELLED,
                testQuotation.getStatus());

        assertEquals("There should be no attached Serialized quotation.", 0,
                serializedQuotationService.findAllByQuotation(testQuotation.getId()).size());

        // Cleanup
        quotationService.delete(testQuotation);

    }

    @Test
    public void testCancelNonExistantQuotation() throws Exception {

        mockMvc.perform(put("/quotation/-1/cancel")).andExpect(status().isNotFound());
    }

    @Test
    public void testCancelQuotationWrongState() throws Exception {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.REVIEW);
        quotationService.create(testQuotation);

        mockMvc.perform(put("/quotation/" + testQuotation.getId() + "/cancel")).andExpect(status().isBadRequest());

        // Cleanup
        quotationService.delete(testQuotation);
    }
}
