package hk.com.ahlopm.productmanager.test.integration.controller.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import hk.com.ahlopm.productmanager.domainobject.user.User;
import hk.com.ahlopm.productmanager.exception.ObjectNotFoundException;
import hk.com.ahlopm.productmanager.service.user.UserService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;
import hk.com.ahlopm.productmanager.test.integration.controller.DomainObjectControllerTest;
import hk.com.ahlopm.productmanager.test.integration.controller.DomainObjectControllerTester;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

public class UserControllerTest extends SpringTest implements DomainObjectControllerTest {

    private MockMvc mockMvc;

    private User testUser;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private UserService userService;

    @Autowired
    private DomainObjectControllerTester<User> tester;

    @Autowired
    private ObjectMapper jsonMapper;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        tester.setMockMvc(mockMvc);
        tester.setService(userService);
        testUser = DomainObjectFactory.createUser();
        tester.setTestObject(testUser);
        tester.setURL("/user");
    }

    @After
    public void tearDown() throws Exception {

    }

    @Override
    @Test
    public void testGET() {

        try {
            tester.testGET();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testGETNonExistant() {

        try {
            tester.testGETNonExistant();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testGETAll() {

        try {
            tester.testGETAll();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testPOST() {

        try {
            tester.testPOST();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testPOSTNotCreate() {

        try {
            tester.testPOSTNotCreate();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testPUT() {

        testUser.setId(0);
        testUser.setName("updated");
        testUser = userService.create(testUser);

        try {
            tester.testPUT();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testPUTNotUpdate() {

        try {
            tester.testPUTNotUpdate();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testPUTNonExistant() {

        try {
            tester.testPUTNonExistant();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testDELETE() {

        testUser.setId(0);
        userService.create(testUser);

        try {
            tester.testDELETE();

            // This fail should not be reached as the object not found exception
            // should be thrown by delete.
            fail("Failed to throw object not found exception after deletion");
        } catch (ObjectNotFoundException e) {
            // Success

        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testDELETENonExistant() {

        try {
            tester.testDELETENonExistant();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testCount() {

        try {
            tester.testCount();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testUpdateUserPassword() throws Exception {

        testUser.setId(0);
        testUser.setPassword("1");
        testUser = userService.create(testUser);

        String[] passwords = { "1", "2" };

        mockMvc.perform(put("/user/" + testUser.getId() + "/password").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(passwords))).andExpect(status().isOk());

        testUser = userService.find(testUser.getId());
        assertEquals("The users password should be updated.", true,
                passwordEncoder.matches("2", testUser.getPassword()));

        // Cleanup
        userService.delete(testUser);
    }

    @Test
    public void testUpdateUserPasswordWrongOldPassword() throws Exception {

        testUser.setId(0);
        testUser.setPassword("1");
        testUser = userService.create(testUser);

        String[] passwords = { "5", "2" };

        mockMvc.perform(put("/user/" + testUser.getId() + "/password").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(passwords))).andExpect(status().is4xxClientError());

        // Cleanup
        userService.delete(testUser);
    }

    @Test
    public void testUpdateUserPasswordForce() throws Exception {

        testUser.setId(0);
        testUser.setPassword("1");
        testUser = userService.create(testUser);

        String password = "2";

        mockMvc.perform(put("/user/" + testUser.getId() + "/password/force")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsString(password)))
                .andExpect(status().isOk());

        testUser = userService.find(testUser.getId());
        assertEquals("The users password should be updated.", true,
                passwordEncoder.matches("2", testUser.getPassword()));

        // Cleanup
        userService.delete(testUser);
    }

    @Test
    public void testGetCurrentAccount() throws Exception {

        mockMvc.perform(get("/account")).andExpect(status().isOk());
    }
}
