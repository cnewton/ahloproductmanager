package hk.com.ahlopm.productmanager.test.integration.controller;

public interface DomainObjectControllerTest {

    public void testGET();

    public void testGETNonExistant();

    public void testGETAll();

    public void testPOST();

    public void testPOSTNotCreate();

    public void testPUT();

    public void testPUTNotUpdate();

    public void testPUTNonExistant();

    public void testDELETE();

    public void testDELETENonExistant();

    public void testCount();

}
