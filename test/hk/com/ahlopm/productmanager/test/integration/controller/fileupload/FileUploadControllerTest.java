package hk.com.ahlopm.productmanager.test.integration.controller.fileupload;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.amazonaws.services.s3.AmazonS3;

import hk.com.ahlopm.productmanager.amazons3.AmazonS3Gateway;
import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.domainobject.productpicture.ProductPicture;
import hk.com.ahlopm.productmanager.service.product.ProductService;
import hk.com.ahlopm.productmanager.service.productpicture.ProductPictureService;
import hk.com.ahlopm.productmanager.test.common.SpringTest;

public class FileUploadControllerTest extends SpringTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMultipartFile uploadFile;

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductPictureService productPictureService;

    @Autowired
    private AmazonS3Gateway gateway;

    @Mock
    private AmazonS3 s3Client;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        when(s3Client.putObject(any())).thenReturn(null);

        ReflectionTestUtils.setField(gateway, "s3Client", s3Client); // Allow
                                                                     // mock
                                                                     // returns
    }

    @After
    public void tearDown() throws Exception {

        ReflectionTestUtils.setField(gateway, "s3Client", null); // repair
                                                                 // context
    }

    @Test
    public void testUpload1Product0ImageXLS() throws Exception {

        File testFile = new ClassPathResource("xls/valid1Product0Image.xlsx").getFile();
        InputStream inputStream = new FileInputStream(testFile);

        uploadFile = new MockMultipartFile("file", inputStream);

        long countBefore = productService.count();
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload/product").file(uploadFile))
                .andExpect(status().isOk());
        long countAfter = productService.count();

        assertEquals("There should be 1 additional product.", 1, countAfter - countBefore);

        // Cleanup
        List<Product> products = productService.findAll();
        Product lastProduct = products.get(products.size() - 1); // Latest added
                                                                 // product
        productService.delete(lastProduct);
    }

    @Test
    public void testFile1Product1Image() throws Exception {

        File testFile = new ClassPathResource("xls/valid1Product1Image.xlsx").getFile();
        InputStream inputStream = new FileInputStream(testFile);

        uploadFile = new MockMultipartFile("file", inputStream);

        long countBefore = productService.count();
        long countBeforePic = productPictureService.count();
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload/product").file(uploadFile))
                .andExpect(status().isOk());
        long countAfter = productService.count();
        long countAfterPic = productPictureService.count();

        assertEquals("There should be 1 additional product.", 1, countAfter - countBefore);
        assertEquals("There should be 1 additional picture.", 1, countAfterPic - countBeforePic);

        List<Product> products = productService.findAll();
        Product lastProduct = products.get(products.size() - 1); // Latest added
                                                                 // product

        // Cleanup
        productService.delete(lastProduct);
    }

    @Test
    public void testFile1ProductMissingImage() throws Exception {

        File testFile = new ClassPathResource("xls/invalid1ProductMissingImage.xlsx").getFile();
        InputStream inputStream = new FileInputStream(testFile);

        uploadFile = new MockMultipartFile("file", inputStream);

        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload/product").file(uploadFile))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testFile1ProductMissingFile() throws Exception {

        byte[] bytes = {};
        InputStream inputStream = new ByteArrayInputStream(bytes);

        uploadFile = new MockMultipartFile("file", inputStream);

        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload/product").file(uploadFile))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testImageUpload() throws Exception {

        File testFile = new ClassPathResource("images/validImage.jpg").getFile();
        InputStream stream = new FileInputStream(testFile);
        uploadFile = new MockMultipartFile("file", "validImage.jpg", "image/jpeg", stream);

        long countBefore = productPictureService.count();
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload/product/1/image").file(uploadFile))
                .andExpect(status().isOk());
        long countAfter = productPictureService.count();

        assertEquals("There should be 1 additional picture.", 1, countAfter - countBefore);

        // Cleanup
        List<ProductPicture> pictures = productPictureService.findAll();
        ProductPicture lastPicture = pictures.get(pictures.size() - 1); // Latest
                                                                        // added
                                                                        // picture
        productPictureService.delete(lastPicture);
    }

    @Test
    public void testImageUploadMultiple() throws Exception {

        File testFile = new ClassPathResource("images/cmyk.jpg").getFile();
        InputStream stream = new FileInputStream(testFile);
        uploadFile = new MockMultipartFile("file", "cmyk.jpg", "image/jpeg", stream);

        long countBefore = productPictureService.count();
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload/product/1/image").file(uploadFile))
                .andExpect(status().isOk());
        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload/product/1/image").file(uploadFile))
                .andExpect(status().isOk());
        long countAfter = productPictureService.count();

        assertEquals("There should be 2 additional pictures.", 2, countAfter - countBefore);

        // Cleanup
        List<ProductPicture> pictures = productPictureService.findAll();
        ProductPicture lastPicture1 = pictures.get(pictures.size() - 1); // Latest
                                                                         // added
                                                                         // picture
        ProductPicture lastPicture2 = pictures.get(pictures.size() - 2);
        productPictureService.delete(lastPicture1);
        productPictureService.delete(lastPicture2);
    }

    @Test
    public void testImageUploadNoProduct() throws Exception {

        File testFile = new ClassPathResource("images/validImage.jpg").getFile();
        InputStream stream = new FileInputStream(testFile);
        uploadFile = new MockMultipartFile("file", "validImage.jpg", "image/jpeg", stream);

        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload/product/-1/image").file(uploadFile))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testImageUploadNotImage() throws Exception {

        File testFile = new ClassPathResource("images/notanimage.jpg").getFile();
        InputStream stream = new FileInputStream(testFile);
        uploadFile = new MockMultipartFile("file", "notanimage.jpg", "text/plain", stream);

        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload/product/1/image").file(uploadFile))
                .andExpect(status().isBadRequest());
    }
}
