package hk.com.ahlopm.productmanager.test.integration.controller.productpicture;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import hk.com.ahlopm.productmanager.amazons3.AmazonS3Gateway;
import hk.com.ahlopm.productmanager.domainobject.productpicture.ProductPicture;
import hk.com.ahlopm.productmanager.domainobject.productpicture.S3ProductPicture;
import hk.com.ahlopm.productmanager.exception.ObjectNotFoundException;
import hk.com.ahlopm.productmanager.service.product.ProductService;
import hk.com.ahlopm.productmanager.service.productpicture.ProductPictureService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;
import hk.com.ahlopm.productmanager.test.integration.controller.DomainObjectControllerTest;
import hk.com.ahlopm.productmanager.test.integration.controller.DomainObjectControllerTester;

import java.io.ByteArrayInputStream;
import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ProductPictureControllerTest extends SpringTest implements DomainObjectControllerTest {

    private MockMvc mockMvc;

    private ProductPicture testProductPicture;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ProductPictureService productPictureService;

    @Autowired
    private ProductService productService;

    @Autowired
    private DomainObjectControllerTester<ProductPicture> tester;

    @Autowired
    private ObjectMapper jsonMapper;

    @Autowired
    private AmazonS3Gateway gateway;

    @Mock
    private AmazonS3 s3Client;

    private byte[] content = { 1, 0, 1, 0 };

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        tester.setMockMvc(mockMvc);
        tester.setService(productPictureService);
        testProductPicture = DomainObjectFactory.createProductPicture();
        tester.setTestObject(testProductPicture);
        tester.setURL("/productpicture");

        MockitoAnnotations.initMocks(this);

        S3Object s3obj = new S3Object();
        s3obj.setObjectContent(new ByteArrayInputStream(content));
        when(s3Client.getObject(any())).thenReturn(s3obj);

        ReflectionTestUtils.setField(gateway, "s3Client", s3Client); // Allow
                                                                     // mock
                                                                     // returns
    }

    @After
    public void tearDown() throws Exception {

        ReflectionTestUtils.setField(gateway, "s3Client", null); // Repair
                                                                 // context
    }

    @Override
    @Test
    public void testGET() {

        try {
            tester.testGET();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testGETNonExistant() {

        try {
            tester.testGETNonExistant();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testGETAll() {

        try {
            tester.testGETAll();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testPOST() {

        try {
            tester.testPOST();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testPOSTNotCreate() {

        try {
            tester.testPOSTNotCreate();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testPUT() {

        testProductPicture.setId(0);
        testProductPicture = productPictureService.create(testProductPicture);

        try {
            tester.testPUT();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testPUTNotUpdate() {

        try {
            tester.testPUTNotUpdate();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testPUTNonExistant() {

        try {
            tester.testPUTNonExistant();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testDELETE() {

        testProductPicture.setId(0);
        productPictureService.create(testProductPicture);

        try {
            tester.testDELETE();

            // This fail should not be reached as the object not found exception
            // should be thrown by delete.
            fail("Failed to throw object not found exception after deletion");
        } catch (ObjectNotFoundException e) {
            // Success

        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testDELETENonExistant() {

        try {
            tester.testDELETENonExistant();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testCount() {

        try {
            tester.testCount();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testSetPrimaryPictureForProduct() throws Exception {

        ProductPicture pic = DomainObjectFactory.createProductPicture();
        pic.setId(0);
        pic.setProduct(productService.find(1));
        productPictureService.create(pic);

        mockMvc.perform(put("/productpicture/primary").param("product", "1").param("picture", "" + pic.getId()))
                .andExpect(status().isOk());

        assertEquals("The primary picture should be the new picture.", pic.getId(),
                productService.getImageIdsForProduct(1)[0]);

        // Cleanup
        productPictureService.delete(pic);
    }

    @Test
    public void testSetPrimaryPictureForNonExistantProduct() throws Exception {

        ProductPicture pic = DomainObjectFactory.createProductPicture();
        pic.setId(0);
        pic.setProduct(productService.find(1));
        productPictureService.create(pic);

        mockMvc.perform(put("/productpicture/primary").param("product", "-1").param("picture", "" + pic.getId()))
                .andExpect(status().isNotFound());
        // Cleanup
        productPictureService.delete(pic);
    }

    @Test
    public void testSetNonExistantPrimaryPictureForProduct() throws Exception {

        mockMvc.perform(put("/productpicture/primary").param("product", "1").param("picture", "-1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSetPrimaryPictureForProductNotContainingPicture() throws Exception {

        ProductPicture pic = DomainObjectFactory.createProductPicture();
        pic.setId(0);
        pic.setProduct(productService.find(2));
        productPictureService.create(pic);

        mockMvc.perform(put("/productpicture/primary").param("product", "1").param("picture", "" + pic.getId()))
                .andExpect(status().isBadRequest());

        // Cleanup
        productPictureService.delete(pic);
    }

    @Test
    public void testGetResolvedS3Picture() throws Exception {

        S3ProductPicture pic = DomainObjectFactory.createS3ProductPicture();
        pic.setId(0);
        pic.setProduct(productService.find(1));
        productPictureService.create(pic);

        MvcResult result = mockMvc.perform(get("/productpicture/" + pic.getId() + "/resolve"))
                .andExpect(status().isOk()).andReturn();

        String responseBody = result.getResponse().getContentAsString();
        byte[] returnedObject = jsonMapper.readValue(responseBody.getBytes(), byte[].class);

        assertEquals("The resolved image should be equal to the original content", true,
                Arrays.equals(content, returnedObject));

        // Cleanup
        productPictureService.delete(pic);
    }

    @Test
    public void testGetResolvedNonS3Picture() throws Exception {

        ProductPicture pic = DomainObjectFactory.createProductPicture();
        pic.setId(0);
        pic.setProduct(productService.find(1));
        productPictureService.create(pic);

        MvcResult result = mockMvc.perform(get("/productpicture/" + pic.getId() + "/resolve"))
                .andExpect(status().isOk()).andReturn();

        String responseBody = result.getResponse().getContentAsString();
        byte[] returnedObject = jsonMapper.readValue(responseBody.getBytes(), byte[].class);

        assertEquals("The resolved image should be empty as it has no resolution mechanism.", 0, returnedObject.length);

        // Cleanup
        productPictureService.delete(pic);
    }

    @Test
    public void testGetResolvedNonExistantPicture() throws Exception {

        mockMvc.perform(get("/productpicture/resolved/-1")).andExpect(status().isNotFound());
    }
}
