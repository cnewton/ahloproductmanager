package hk.com.ahlopm.productmanager.test.integration.controller.quoteproduct;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;
import hk.com.ahlopm.productmanager.exception.ObjectNotFoundException;
import hk.com.ahlopm.productmanager.service.product.ProductService;
import hk.com.ahlopm.productmanager.service.quotation.QuotationService;
import hk.com.ahlopm.productmanager.service.quoteproduct.QuoteProductService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;
import hk.com.ahlopm.productmanager.test.integration.controller.DomainObjectControllerTest;
import hk.com.ahlopm.productmanager.test.integration.controller.DomainObjectControllerTester;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

public class QuoteProductControllerTest extends SpringTest implements DomainObjectControllerTest {

    private MockMvc mockMvc;

    private QuoteProduct testQuoteProduct;

    @Autowired
    private ObjectMapper jsonMapper;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private QuoteProductService quoteProductService;

    @Autowired
    private QuotationService quotationService;

    @Autowired
    private ProductService productService;

    @Autowired
    private DomainObjectControllerTester<QuoteProduct> tester;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        tester.setMockMvc(mockMvc);
        tester.setService(quoteProductService);
        testQuoteProduct = DomainObjectFactory.createQuoteProduct();
        tester.setTestObject(testQuoteProduct);
        tester.setURL("/quoteproduct");
    }

    @After
    public void tearDown() throws Exception {

    }

    @Override
    @Test
    public void testGET() {

        try {
            tester.testGET();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testGETNonExistant() {

        try {
            tester.testGETNonExistant();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testGETAll() {

        try {
            tester.testGETAll();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testPOST() {

        try {
            tester.testPOST();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testPOSTNotCreate() {

        try {
            tester.testPOSTNotCreate();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testPUT() {

        testQuoteProduct.setId(0);
        testQuoteProduct = quoteProductService.create(testQuoteProduct);

        try {
            tester.testPUT();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testPUTNotUpdate() {

        try {
            tester.testPUTNotUpdate();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testPUTNonExistant() {

        try {
            tester.testPUTNonExistant();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testDELETE() {

        testQuoteProduct.setId(0);
        quoteProductService.create(testQuoteProduct);

        try {
            tester.testDELETE();

            // This fail should not be reached as the object not found exception
            // should be thrown by delete.
            fail("Failed to throw object not found exception after deletion");
        } catch (ObjectNotFoundException e) {
            // Success

        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testDELETENonExistant() {

        try {
            tester.testDELETENonExistant();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testCount() {

        try {
            tester.testCount();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testAddProductsToQuotation() throws Exception {

        Product prod1 = DomainObjectFactory.createProduct();
        prod1.setId(0);
        productService.create(prod1);

        Product prod2 = DomainObjectFactory.createProduct();
        prod2.setId(0);
        productService.create(prod2);

        long[] prodIds = { prod1.getId(), prod2.getId() };

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);
        quotationService.create(testQuotation);

        mockMvc.perform(put("/quotation/" + testQuotation.getId() + "/quoteproducts")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(prodIds)))
                .andExpect(status().isOk());

        testQuotation = quotationService.find(testQuotation.getId());

        assertEquals("The Quotation should contain two products.", 2, testQuotation.getProducts().size());

        // Cleanup
        quotationService.delete(testQuotation);
        productService.delete(prod1.getId());
        productService.delete(prod2.getId());
    }

}
