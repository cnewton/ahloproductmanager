package hk.com.ahlopm.productmanager.test.integration.controller.quotation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.quotation.QuotationStatus;
import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;
import hk.com.ahlopm.productmanager.exception.ObjectNotFoundException;
import hk.com.ahlopm.productmanager.service.product.ProductService;
import hk.com.ahlopm.productmanager.service.quotation.QuotationService;
import hk.com.ahlopm.productmanager.service.quoteproduct.QuoteProductService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;
import hk.com.ahlopm.productmanager.test.integration.controller.DomainObjectControllerTest;
import hk.com.ahlopm.productmanager.test.integration.controller.DomainObjectControllerTester;

public class QuotationControllerTest extends SpringTest implements DomainObjectControllerTest {

    private MockMvc mockMvc;

    private Quotation testQuotation;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private QuotationService quotationService;

    @Autowired
    private QuoteProductService quoteProductService;

    @Autowired
    private ProductService productService;

    @Autowired
    private DomainObjectControllerTester<Quotation> tester;

    @Autowired
    private ObjectMapper jsonMapper;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        tester.setMockMvc(mockMvc);
        tester.setService(quotationService);
        testQuotation = DomainObjectFactory.createQuotation();
        tester.setTestObject(testQuotation);
        tester.setURL("/quotation");
    }

    @After
    public void tearDown() throws Exception {

    }

    @Override
    @Test
    public void testGET() {

        try {
            tester.testGET();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testGETNonExistant() {

        try {
            tester.testGETNonExistant();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testGETAll() {

        try {
            tester.testGETAll();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testPOST() {

        try {
            tester.testPOST();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testPOSTNotCreate() {

        try {
            tester.testPOSTNotCreate();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testPUT() {

        testQuotation.setId(0);
        testQuotation.setName("updated");
        testQuotation = quotationService.create(testQuotation);

        try {
            tester.testPUT();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testPUTNotUpdate() {

        try {
            tester.testPUTNotUpdate();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testPUTNonExistant() {

        try {
            tester.testPUTNonExistant();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testDELETE() {

        testQuotation.setId(0);
        quotationService.create(testQuotation);

        try {
            tester.testDELETE();

            // This fail should not be reached as the object not found exception
            // should be thrown by delete.
            fail("Failed to throw object not found exception after deletion");
        } catch (ObjectNotFoundException e) {
            // Success

        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testDELETENonExistant() {

        try {
            tester.testDELETENonExistant();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testCount() {

        try {
            tester.testCount();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testCalculatePriceForQuotation() throws Exception {

        Product prod1 = DomainObjectFactory.createProduct();
        Product prod2 = DomainObjectFactory.createProduct();

        prod1.setPrice(BigDecimal.TEN);
        prod1.setId(0);
        productService.create(prod1);

        prod2.setPrice(BigDecimal.TEN);
        prod2.setId(0);
        productService.create(prod2);

        testQuotation.setId(0);
        quotationService.create(testQuotation);

        long[] prodIds = { prod1.getId(), prod2.getId() };
        quoteProductService.addProductsToQuotation(testQuotation.getId(), prodIds);

        // Set Quantities to one
        testQuotation = quotationService.find(testQuotation.getId());
        for (QuoteProduct qp : testQuotation.getProducts()) {
            qp.setQuantity(1);
        }
        quotationService.update(testQuotation);

        mockMvc.perform(get("/quotation/" + testQuotation.getId() + "/price")).andExpect(status().isOk())
                .andExpect(content().string("20.0000"));

        // Cleanup
        quotationService.delete(testQuotation);
        productService.delete(prod1);
        productService.delete(prod2);
    }

    @Test
    public void testCalculatePriceForNonExistantQuotation() throws Exception {

        mockMvc.perform(get("/quotation/-1/price")).andExpect(status().isNotFound());
    }

    @Test
    public void testGETAllInReview() throws Exception {

        long countBefore = quotationService.findAllInReview().size();

        Quotation quote1 = DomainObjectFactory.createQuotation();
        quote1.setId(0);
        quote1.setStatus(QuotationStatus.REVIEW);

        Quotation quote2 = DomainObjectFactory.createQuotation();
        quote2.setId(0);
        quote2.setStatus(QuotationStatus.ACCEPTED);

        quotationService.create(quote1);
        quotationService.create(quote2);

        MvcResult result = mockMvc.perform(get("/quotation/review")).andExpect(status().isOk()).andReturn();

        String responseBody = result.getResponse().getContentAsString();
        Quotation[] returnedObject = jsonMapper.readValue(responseBody.getBytes(), Quotation[].class);
        List<Quotation> list = Arrays.asList(returnedObject);

        long countAfter = list.size();

        assertEquals("The size of the returned list should only increase by 1.", countBefore + 1, countAfter);

        // Cleanup
        quotationService.delete(quote1);
        quotationService.delete(quote2);
    }

    @Test
    public void testGETAllInManagement() throws Exception {

        long countBefore = quotationService.findAllInManagement().size();

        Quotation quote1 = DomainObjectFactory.createQuotation();
        quote1.setId(0);
        quote1.setStatus(QuotationStatus.REVIEW);

        Quotation quote2 = DomainObjectFactory.createQuotation();
        quote2.setId(0);
        quote2.setStatus(QuotationStatus.ACCEPTED);

        quotationService.create(quote1);
        quotationService.create(quote2);

        MvcResult result = mockMvc.perform(get("/quotation/management")).andExpect(status().isOk()).andReturn();

        String responseBody = result.getResponse().getContentAsString();
        Quotation[] returnedObject = jsonMapper.readValue(responseBody.getBytes(), Quotation[].class);
        List<Quotation> list = Arrays.asList(returnedObject);

        long countAfter = list.size();

        assertEquals("The size of the returned list should only increase by 1.", countBefore + 1, countAfter);

        // Cleanup
        quotationService.delete(quote1);
        quotationService.delete(quote2);
    }
}
