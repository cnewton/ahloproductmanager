package hk.com.ahlopm.productmanager.test.integration.controller.buyer;

import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import hk.com.ahlopm.productmanager.domainobject.buyer.Buyer;
import hk.com.ahlopm.productmanager.exception.ObjectNotFoundException;
import hk.com.ahlopm.productmanager.service.buyer.BuyerService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;
import hk.com.ahlopm.productmanager.test.integration.controller.DomainObjectControllerTest;
import hk.com.ahlopm.productmanager.test.integration.controller.DomainObjectControllerTester;

public class BuyerControllerTest extends SpringTest implements DomainObjectControllerTest {

    private MockMvc mockMvc;

    private Buyer testBuyer;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private BuyerService buyerService;

    @Autowired
    private DomainObjectControllerTester<Buyer> tester;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        tester.setMockMvc(mockMvc);
        tester.setService(buyerService);
        testBuyer = DomainObjectFactory.createBuyer();
        tester.setTestObject(testBuyer);
        tester.setURL("/buyer");
    }

    @After
    public void tearDown() throws Exception {

    }

    @Override
    @Test
    public void testGET() {

        try {
            tester.testGET();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testGETNonExistant() {

        try {
            tester.testGETNonExistant();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testGETAll() {

        try {
            tester.testGETAll();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testPOST() {

        try {
            tester.testPOST();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testPOSTNotCreate() {

        try {
            tester.testPOSTNotCreate();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testPUT() {

        testBuyer.setId(0);
        testBuyer.setName("updated");
        testBuyer = buyerService.create(testBuyer);

        try {
            tester.testPUT();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testPUTNotUpdate() {

        try {
            tester.testPUTNotUpdate();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testPUTNonExistant() {

        try {
            tester.testPUTNonExistant();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testDELETE() {

        testBuyer.setId(0);
        buyerService.create(testBuyer);

        try {
            tester.testDELETE();

            // This fail should not be reached as the object not found exception
            // should be thrown by delete.
            fail("Failed to throw object not found exception after deletion");
        } catch (ObjectNotFoundException e) {
            // Success

        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testDELETENonExistant() {

        try {
            tester.testDELETENonExistant();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testCount() {

        try {
            tester.testCount();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }
}
