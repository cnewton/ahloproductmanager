package hk.com.ahlopm.productmanager.test.integration.controller.serializedquotation;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import hk.com.ahlopm.productmanager.domainobject.serializedquotation.SerializedQuotation;
import hk.com.ahlopm.productmanager.exception.ObjectNotFoundException;
import hk.com.ahlopm.productmanager.service.serializedquotation.SerializedQuotationService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;
import hk.com.ahlopm.productmanager.test.integration.controller.DomainObjectControllerTest;
import hk.com.ahlopm.productmanager.test.integration.controller.DomainObjectControllerTester;

public class SerializedQuotationControllerTest extends SpringTest implements DomainObjectControllerTest {

    private MockMvc mockMvc;

    private SerializedQuotation testSerializedQuotation;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private SerializedQuotationService serializedQuotationService;

    @Autowired
    private DomainObjectControllerTester<SerializedQuotation> tester;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        tester.setMockMvc(mockMvc);
        tester.setService(serializedQuotationService);
        testSerializedQuotation = DomainObjectFactory.createSerializedQuotation();
        tester.setTestObject(testSerializedQuotation);
        tester.setURL("/records/quotation");
    }

    @After
    public void tearDown() throws Exception {

    }

    @Override
    @Test
    public void testGET() {

        try {
            tester.testGET();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testGETNonExistant() {

        try {
            tester.testGETNonExistant();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testGETAll() {

        try {
            tester.testGETAll();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testPOST() {

        try {
            tester.testPOST();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testPOSTNotCreate() {

        try {
            tester.testPOSTNotCreate();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testPUT() {

        testSerializedQuotation.setId(0);
        testSerializedQuotation = serializedQuotationService.create(testSerializedQuotation);

        try {
            tester.testPUT();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testPUTNotUpdate() {

        try {
            tester.testPUTNotUpdate();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testPUTNonExistant() {

        try {
            tester.testPUTNonExistant();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testDELETE() {

        testSerializedQuotation.setId(0);
        serializedQuotationService.create(testSerializedQuotation);

        try {
            tester.testDELETE();

            // This fail should not be reached as the object not found exception
            // should be thrown by delete.
            fail("Failed to throw object not found exception after deletion");
        } catch (ObjectNotFoundException e) {
            // Success

        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testDELETENonExistant() {

        try {
            tester.testDELETENonExistant();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testCount() {

        try {
            tester.testCount();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testGETAllDeserialized() throws Exception {

        mockMvc.perform(get("/records/quotation/deserialized")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(jsonPath("$", hasSize(3)));
    }
}
