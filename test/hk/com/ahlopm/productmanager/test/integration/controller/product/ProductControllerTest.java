package hk.com.ahlopm.productmanager.test.integration.controller.product;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import hk.com.ahlopm.productmanager.common.CustomObjectMapper;
import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.domainobject.productpicture.S3ProductPicture;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.exception.ObjectNotFoundException;
import hk.com.ahlopm.productmanager.service.product.ProductService;
import hk.com.ahlopm.productmanager.service.quotation.QuotationService;
import hk.com.ahlopm.productmanager.service.quoteproduct.QuoteProductService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;
import hk.com.ahlopm.productmanager.test.integration.controller.DomainObjectControllerTest;
import hk.com.ahlopm.productmanager.test.integration.controller.DomainObjectControllerTester;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

public class ProductControllerTest extends SpringTest implements DomainObjectControllerTest {

    private MockMvc mockMvc;

    private Product testProduct;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ProductService productService;

    @Autowired
    private QuotationService quotationService;

    @Autowired
    private QuoteProductService quoteProductService;

    @Autowired
    private CustomObjectMapper jsonMapper;

    @Autowired
    private DomainObjectControllerTester<Product> tester;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        tester.setMockMvc(mockMvc);
        tester.setService(productService);
        testProduct = DomainObjectFactory.createProduct();
        tester.setTestObject(testProduct);
        tester.setURL("/product");
    }

    @After
    public void tearDown() throws Exception {

    }

    @Override
    @Test
    public void testGET() {

        try {
            tester.testGET();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testGETNonExistant() {

        try {
            tester.testGETNonExistant();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testGETAll() {

        try {
            tester.testGETAll();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testPOST() {

        try {
            tester.testPOST();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testPOSTNotCreate() {

        try {
            tester.testPOSTNotCreate();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testPUT() {

        testProduct.setId(0);
        testProduct = productService.create(testProduct);

        try {
            tester.testPUT();
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testPUTNotUpdate() {

        try {
            tester.testPUTNotUpdate();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testPUTNonExistant() {

        try {
            tester.testPUTNonExistant();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testDELETE() {

        testProduct.setId(0);
        productService.create(testProduct);

        try {
            tester.testDELETE();

            // This fail should not be reached as the object not found exception
            // should be thrown by delete.
            fail("Failed to throw object not found exception after deletion");
        } catch (ObjectNotFoundException e) {
            // Success

        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Override
    @Test
    public void testDELETENonExistant() {

        try {
            tester.testDELETENonExistant();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Override
    @Test
    public void testCount() {

        try {
            tester.testCount();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testGetAvailableProductsForQuotation() throws Exception {

        long originalCount = productService.count();

        Product prod1 = DomainObjectFactory.createProduct();
        prod1.setId(0);
        productService.create(prod1);

        Product prod2 = DomainObjectFactory.createProduct();
        prod2.setId(0);
        productService.create(prod2);

        Quotation quote = DomainObjectFactory.createQuotation();
        quote.setId(0);
        quotationService.create(quote);

        long[] prodIds = { prod1.getId(), prod2.getId() };
        quoteProductService.addProductsToQuotation(quote.getId(), prodIds);

        assertEquals("There should now be " + originalCount + 2 + " products.", originalCount + 2,
                productService.count());
        assertEquals("There should now be 2 products in the quotation.", 2,
                quotationService.find(quote.getId()).getProducts().size());

        mockMvc.perform(get("/product/visible/" + quote.getId())).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize((int) originalCount)));

        // Then cleanup
        quotationService.delete(quote);
        productService.delete(prod1);
        productService.delete(prod2);
    }

    @Test
    public void testGetAvaialableProductsForNonExistantQuotation() throws Exception {

        mockMvc.perform(get("/product/visible/-1")).andExpect(status().isNotFound());
    }

    @Test
    public void testGetProductImageIds() throws Exception {

        Product prod1 = DomainObjectFactory.createProduct();
        S3ProductPicture s3pp1 = DomainObjectFactory.createS3ProductPicture();
        S3ProductPicture s3pp2 = DomainObjectFactory.createS3ProductPicture();
        s3pp1.setProduct(prod1);
        s3pp1.setId(0);
        s3pp2.setProduct(prod1);
        s3pp2.setId(0);
        prod1.getPictures().add(s3pp1);
        prod1.getPictures().add(s3pp2);
        prod1.setPrimaryPicture(s3pp2);
        prod1.setId(0);
        productService.create(prod1);

        MvcResult result = mockMvc.perform(get("/product/" + prod1.getId() + "/images")).andExpect(status().isOk())
                .andReturn();

        String responseBody = result.getResponse().getContentAsString();
        long[] returnedObject = jsonMapper.readValue(responseBody.getBytes(), long[].class);

        assertEquals("The returned list should contain two elements.", 2, returnedObject.length);
        assertEquals("The first id should be the primary picture.", prod1.getPrimaryPicture().getId(),
                returnedObject[0]);

        // Cleanup
        productService.delete(prod1);
    }

    @Test
    public void testGetEmptyProductImages() throws Exception {

        Product prod1 = DomainObjectFactory.createProduct();
        prod1.setId(0);
        productService.create(prod1);

        MvcResult result = mockMvc.perform(get("/product/" + prod1.getId() + "/images")).andExpect(status().isOk())
                .andReturn();

        String responseBody = result.getResponse().getContentAsString();
        long[] returnedObject = jsonMapper.readValue(responseBody.getBytes(), long[].class);

        assertEquals("The returned list should contain zero elements.", 0, returnedObject.length);

        productService.delete(prod1);
    }

    @Test
    public void testGetNonExistantProductImages() throws Exception {

        mockMvc.perform(get("/product/-1/images")).andExpect(status().isNotFound());
    }

}
