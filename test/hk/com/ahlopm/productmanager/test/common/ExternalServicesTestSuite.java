package hk.com.ahlopm.productmanager.test.common;

import hk.com.ahlopm.productmanager.test.amazon.gateway.AmazonS3GatewayTest;
import hk.com.ahlopm.productmanager.test.service.invoice.InvoiceServiceExternalTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AmazonS3GatewayTest.class, InvoiceServiceExternalTest.class })
public class ExternalServicesTestSuite {
    // Leave Empty.
}
