package hk.com.ahlopm.productmanager.test.common;

import hk.com.ahlopm.productmanager.domainobject.buyer.Buyer;
import hk.com.ahlopm.productmanager.domainobject.invoice.Invoice;
import hk.com.ahlopm.productmanager.domainobject.invoice.S3Invoice;
import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.domainobject.productpicture.ProductPicture;
import hk.com.ahlopm.productmanager.domainobject.productpicture.S3ProductPicture;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.quotation.QuotationStatus;
import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;
import hk.com.ahlopm.productmanager.domainobject.serializedquotation.SerializedQuotation;
import hk.com.ahlopm.productmanager.domainobject.user.User;
import hk.com.ahlopm.productmanager.domainobject.user.UserRole;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;

/**
 * This class provides predefined values for each of the Domain Objects.
 * 
 * @author Cameron Newton
 * @author Gene Cross
 *
 */
public class DomainObjectFactory {

    public static Buyer createBuyer() {

        Buyer buyer = new Buyer();

        buyer.setAddress("123 Fake St, Simpsonville");
        buyer.setCompany("NewCross Inc");
        buyer.setEmail("test@me.com");
        buyer.setId(1);
        buyer.setName("Test Smith");
        buyer.setPhone("+85211112222");
        return buyer;
    }

    public static Product createProduct() {

        Product product = new Product();

        product.setDescription("Silver Rings");
        product.setFinish("Silver");
        product.setId(1);
        product.setPrice(new BigDecimal(42.8));
        product.setProductCode("SR106-1");
        product.setQuantity("7");
        product.setStockPosition("A7");
        product.setStyle("Ring");
        product.setPictures(new HashSet<ProductPicture>());

        return product;
    }

    public static Quotation createQuotation() {

        Quotation quotation = new Quotation();

        quotation.setBuyer(createBuyer());
        quotation.setDiscount(new BigDecimal(1.4));
        quotation.setId(1);
        quotation.setName("Jewellery Quote");
        quotation.setPrice(new BigDecimal(18.4321));
        quotation.setProducts(new HashSet<QuoteProduct>());
        quotation.setStatus(QuotationStatus.ACCEPTED);
        quotation.setRevisionNo(0);
        quotation.setLastModified(LocalDateTime.now());
        quotation.setRemarks("No Remarks");
        quotation.setInvoice(createS3Invoice());

        return quotation;
    }

    public static QuoteProduct createQuoteProduct() {

        QuoteProduct quoteProduct = new QuoteProduct();

        quoteProduct.setProduct(createProduct());
        quoteProduct.setQuotation(createQuotation());
        quoteProduct.setId(1);
        quoteProduct.setDiscount(new BigDecimal(4.1));
        quoteProduct.setQuantity(1);

        return quoteProduct;
    }

    public static S3ProductPicture createS3ProductPicture() {

        S3ProductPicture pp = new S3ProductPicture();
        pp.setId(1);
        pp.setEncodeType("jpg");
        pp.setS3Bucket("Bucket");
        pp.setS3FileKey("FileKey");

        return pp;
    }

    public static ProductPicture createProductPicture() {

        ProductPicture pp = new ProductPicture();
        pp.setId(1);
        pp.setEncodeType("jpg");
        pp.setProduct(createProduct());

        return pp;
    }

    public static SerializedQuotation createSerializedQuotation() {

        SerializedQuotation sQuote = new SerializedQuotation();
        sQuote.setId(1);
        sQuote.setQuotationId(1);
        sQuote.setStatus(QuotationStatus.ACCEPTED);
        sQuote.setQuotation(
                "{\"id\":1,\"name\":\"famous\",\"buyer\":{\"id\":1,\"name\":\"bossman of test\",\"phone\":\"+85211112222\",\"email\":\"test@me.com\",\"address\":\"123 Fake St, Simpsonville\",\"company\":\"NewCross Inc\",\"products\":[],\"price\":18.43209999999999837427822058089077472686767578125,\"status\":\"ACCEPTED\",\"discount\":1.399999999999999911182158029987476766109466552734375,\"revisionNo\":0,\"remarks\":\"No Remarks\",\"lastModified\":[2016,1,23,19,53,11,398000000]}");
        return sQuote;
    }

    public static Invoice createInvoice() {

        Invoice inv = new Invoice();
        inv.setId(1);
        return inv;
    }

    public static S3Invoice createS3Invoice() {

        S3Invoice inv = new S3Invoice();
        inv.setId(1);
        inv.setS3Bucket("bucket");
        inv.setS3FileKey("filekey");
        return inv;
    }

    public static User createUser() {

        User user = new User();
        user.setEmail("test@test.com");
        user.setName("John Tester");
        user.setUsername("TestName");
        user.setId(1);
        user.setPassword("password1");
        user.setRoles(new HashSet<UserRole>());
        user.setEnabled(true);
        return user;
    }

    public static UserRole createUserRole() {

        UserRole role = new UserRole();
        role.setId(1);
        role.setUser(createUser());
        role.setRole("ROLE_USER");
        return role;
    }

}
