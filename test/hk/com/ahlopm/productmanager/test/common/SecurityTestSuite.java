package hk.com.ahlopm.productmanager.test.common;

import hk.com.ahlopm.productmanager.test.security.controller.buyer.BuyerControllerSecurityTest;
import hk.com.ahlopm.productmanager.test.security.controller.fileupload.FileUploadControllerSecurityTest;
import hk.com.ahlopm.productmanager.test.security.controller.product.ProductControllerSecurityTest;
import hk.com.ahlopm.productmanager.test.security.controller.productpicture.ProductPictureControllerSecurityTest;
import hk.com.ahlopm.productmanager.test.security.controller.quotation.QuotationControllerSecurityTest;
import hk.com.ahlopm.productmanager.test.security.controller.quotationmanagement.QuotationManagementControllerSecurityTest;
import hk.com.ahlopm.productmanager.test.security.controller.quoteproduct.QuoteProductControllerSecurityTest;
import hk.com.ahlopm.productmanager.test.security.controller.serializedquotation.SerializedQuotationControllerSecurityTest;
import hk.com.ahlopm.productmanager.test.security.controller.user.UserControllerSecurityTest;
import hk.com.ahlopm.productmanager.test.security.core.CoreSecurityTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BuyerControllerSecurityTest.class, QuotationControllerSecurityTest.class,
        ProductControllerSecurityTest.class, ProductPictureControllerSecurityTest.class,
        QuoteProductControllerSecurityTest.class, SerializedQuotationControllerSecurityTest.class,
        QuotationManagementControllerSecurityTest.class, FileUploadControllerSecurityTest.class, CoreSecurityTest.class,
        UserControllerSecurityTest.class })
public class SecurityTestSuite {
    // Remains Empty
}
