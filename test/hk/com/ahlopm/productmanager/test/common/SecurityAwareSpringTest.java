package hk.com.ahlopm.productmanager.test.common;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import hk.com.ahlopm.productmanager.domainobject.user.User;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "classpath:security-servlet.xml" })
public class SecurityAwareSpringTest extends SpringTest {

    protected static final String USER_USERNAME = "Test User";

    protected static final String ADMIN_USERNAME = "Test Admin";

    protected static final String RAW_PASSWORD = "password";

    protected String getUserAuthToken(MockMvc mvc) throws Exception {

        User user = DomainObjectFactory.createUser();
        user.setUsername(USER_USERNAME);
        user.setPassword(RAW_PASSWORD);

        return login(mvc, user).andReturn().getResponse().getHeader("X-AUTH-TOKEN");
    }

    protected String getAdminAuthToken(MockMvc mvc) throws Exception {

        User user = DomainObjectFactory.createUser();
        user.setUsername(ADMIN_USERNAME);
        user.setPassword(RAW_PASSWORD);

        return login(mvc, user).andReturn().getResponse().getHeader("X-AUTH-TOKEN");
    }

    protected ResultActions login(MockMvc mvc, User user) throws Exception {

        RequestBuilder request = formLogin("/j_spring_security_check").user(user.getUsername())
                .password(user.getPassword());

        return mvc.perform(request);
    }
}