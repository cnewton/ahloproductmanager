package hk.com.ahlopm.productmanager.test.common;

import hk.com.ahlopm.productmanager.test.integration.controller.buyer.BuyerControllerTest;
import hk.com.ahlopm.productmanager.test.integration.controller.fileupload.FileUploadControllerTest;
import hk.com.ahlopm.productmanager.test.integration.controller.product.ProductControllerTest;
import hk.com.ahlopm.productmanager.test.integration.controller.productpicture.ProductPictureControllerTest;
import hk.com.ahlopm.productmanager.test.integration.controller.quotation.QuotationControllerTest;
import hk.com.ahlopm.productmanager.test.integration.controller.quotationmanagement.QuotationManagementControllerTest;
import hk.com.ahlopm.productmanager.test.integration.controller.quoteproduct.QuoteProductControllerTest;
import hk.com.ahlopm.productmanager.test.integration.controller.serializedquotation.SerializedQuotationControllerTest;
import hk.com.ahlopm.productmanager.test.integration.controller.user.UserControllerTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BuyerControllerTest.class, UserControllerTest.class, ProductControllerTest.class,
        SerializedQuotationControllerTest.class, QuoteProductControllerTest.class,
        QuotationManagementControllerTest.class, ProductPictureControllerTest.class, FileUploadControllerTest.class,
        QuotationControllerTest.class
        // Classes go here
})
public class IntegrationTestSuite {
    // Remains Empty.
}
