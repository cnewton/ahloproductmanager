package hk.com.ahlopm.productmanager.test.common;

import hk.com.ahlopm.productmanager.domainobject.productpicture.S3ProductPicture;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;

import java.math.BigDecimal;
import java.util.Random;

public class ComplexObjectFactory {

    private static String TEST_BUCKET_NAME = "ahlopmtest";

    private static String TEST_FILEKEY_PREFIX = "objtest/";

    public static S3ProductPicture createExternS3ProductPicture(String testName) {

        S3ProductPicture pic = new S3ProductPicture();
        pic.setS3Bucket(TEST_BUCKET_NAME);
        pic.setS3FileKey(TEST_FILEKEY_PREFIX + testName);
        pic.setObjectData(new byte[] { 't', 'e', 's', 't', ' ', 'd', 'a', 't', 'a' });
        pic.setId(1);

        return pic;
    }

    public static Quotation createExtern8SquareImagesQuotation(String filePrefix) {

        Quotation quote = DomainObjectFactory.createQuotation();

        for (int ii = 0; ii < 8; ii++) {
            S3ProductPicture pic = createExternS3ProductPicture(filePrefix + "/" + ii + ".jpg");
            pic.setEncodeType("jpg");
            QuoteProduct prod = createRandomQuoteProduct();
            prod.getProduct().setPrimaryPicture(pic);
            quote.getProducts().add(prod);
        }

        return quote;
    }

    public static Quotation createExtern24SquareImagesQuotation(String filePrefix) {

        Quotation quote = DomainObjectFactory.createQuotation();

        for (int ii = 0; ii < 24; ii++) {
            S3ProductPicture pic = createExternS3ProductPicture(filePrefix + "/" + ii + ".jpg");
            pic.setEncodeType("jpg");
            QuoteProduct prod = createRandomQuoteProduct();
            prod.getProduct().setPrimaryPicture(pic);
            quote.getProducts().add(prod);
        }

        return quote;
    }

    public static Quotation createExtern8WideImagesQuotation(String filePrefix) {

        Quotation quote = DomainObjectFactory.createQuotation();

        for (int ii = 0; ii < 8; ii++) {
            S3ProductPicture pic = createExternS3ProductPicture(filePrefix + "/" + ii + ".jpg");
            pic.setEncodeType("jpg");
            QuoteProduct prod = createRandomQuoteProduct();
            prod.getProduct().setPrimaryPicture(pic);
            quote.getProducts().add(prod);
        }

        return quote;
    }

    public static Quotation createExtern2OversizedImagesQuotation(String filePrefix) {

        Quotation quote = DomainObjectFactory.createQuotation();

        for (int ii = 0; ii < 2; ii++) {
            S3ProductPicture pic = createExternS3ProductPicture(filePrefix + "/" + ii + ".jpg");
            pic.setEncodeType("jpg");
            QuoteProduct prod = createRandomQuoteProduct();
            prod.getProduct().setPrimaryPicture(pic);
            quote.getProducts().add(prod);
        }

        return quote;
    }

    public static QuoteProduct createRandomQuoteProduct() {

        QuoteProduct quoteProduct = DomainObjectFactory.createQuoteProduct();
        quoteProduct.getProduct().setDescription("Test Data");
        Random rand = new Random();

        int style = rand.nextInt(2);

        switch (style) {

        case 0:
            quoteProduct.getProduct().setStyle("Ring");
            break;
        case 1:
            quoteProduct.getProduct().setStyle("Necklace");
            break;
        default:
            quoteProduct.getProduct().setStyle("Problem");
        }

        int finish = rand.nextInt(2);

        switch (finish) {

        case 0:
            quoteProduct.getProduct().setFinish("Silver");
            break;
        case 1:
            quoteProduct.getProduct().setFinish("Gold");
            break;
        default:
            quoteProduct.getProduct().setFinish("Problem");
        }

        quoteProduct.getProduct().setProductCode("" + rand.longs(10000, 100000000).findFirst().getAsLong());
        quoteProduct.setQuantity(rand.nextInt(500));
        quoteProduct.getProduct().setPrice(BigDecimal.valueOf(rand.nextInt(500)));

        return quoteProduct;
    }

}
