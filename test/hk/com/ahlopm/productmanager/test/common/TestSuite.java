package hk.com.ahlopm.productmanager.test.common;

import hk.com.ahlopm.productmanager.test.builder.invoice.InvoiceBuilderTest;
import hk.com.ahlopm.productmanager.test.dao.buyer.BuyerDAOTest;
import hk.com.ahlopm.productmanager.test.dao.product.ProductDAOTest;
import hk.com.ahlopm.productmanager.test.dao.productpicture.ProductPictureDAOTest;
import hk.com.ahlopm.productmanager.test.dao.quotation.QuotationDAOTest;
import hk.com.ahlopm.productmanager.test.dao.quoteproduct.QuoteProductDAOTest;
import hk.com.ahlopm.productmanager.test.dao.serializedquotation.SerializedQuotationDAOTest;
import hk.com.ahlopm.productmanager.test.dao.user.UserDAOTest;
import hk.com.ahlopm.productmanager.test.service.buyer.BuyerServiceTest;
import hk.com.ahlopm.productmanager.test.service.configuration.ConfigurationServiceTest;
import hk.com.ahlopm.productmanager.test.service.fileupload.FileUploadServiceTest;
import hk.com.ahlopm.productmanager.test.service.invoice.InvoiceServiceTest;
import hk.com.ahlopm.productmanager.test.service.invoice.InvoiceServiceUtilsTest;
import hk.com.ahlopm.productmanager.test.service.product.ProductServiceTest;
import hk.com.ahlopm.productmanager.test.service.productpicture.ProductPictureServiceTest;
import hk.com.ahlopm.productmanager.test.service.quotation.QuotationServiceTest;
import hk.com.ahlopm.productmanager.test.service.quotationmanagement.QuotationManagementServiceTest;
import hk.com.ahlopm.productmanager.test.service.quoteproduct.QuoteProductServiceTest;
import hk.com.ahlopm.productmanager.test.service.serializedquotation.SerializedQuotationServiceTest;
import hk.com.ahlopm.productmanager.test.service.user.UserServiceTest;
import hk.com.ahlopm.productmanager.test.parser.xls.XLSProductParserTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BuyerServiceTest.class, ProductServiceTest.class, QuotationServiceTest.class,
        ConfigurationServiceTest.class, SerializedQuotationServiceTest.class, QuoteProductServiceTest.class,
        QuotationManagementServiceTest.class, ProductPictureServiceTest.class, FileUploadServiceTest.class,
        InvoiceServiceTest.class, UserServiceTest.class,

        XLSProductParserTest.class, InvoiceBuilderTest.class, InvoiceServiceUtilsTest.class,

        ProductPictureDAOTest.class, UserDAOTest.class, QuoteProductDAOTest.class, SerializedQuotationDAOTest.class,
        BuyerDAOTest.class, ProductDAOTest.class, QuotationDAOTest.class })
public class TestSuite {
    // Leave Empty.
}
