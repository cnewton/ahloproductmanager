package hk.com.ahlopm.productmanager.test.builder.invoice;

import static org.junit.Assert.assertNotNull;
import hk.com.ahlopm.productmanager.builder.invoice.InvoiceBuilder;
import hk.com.ahlopm.productmanager.domainobject.invoice.Invoice;
import hk.com.ahlopm.productmanager.domainobject.productpicture.ProductPicture;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.transaction.annotation.Transactional;

import com.amazonaws.util.IOUtils;

public class InvoiceBuilderTest extends SpringTest {

    private InvoiceBuilder testBuilder;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

        // DB gets cleaned up automatically, no work to do here.
    }

    @Test
    @Transactional
    public void testInvoiceBuilderPageHeader() {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        testBuilder = new InvoiceBuilder(testQuote);
        testBuilder.addPageHeader();
    }

    @Test
    @Transactional
    public void testInvoiceBuilderPageHeaderBuild() throws IOException {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        testBuilder = new InvoiceBuilder(testQuote);
        Invoice inv = testBuilder.addPageHeader().build();
        assertNotNull(inv);
    }

    @Test
    @Transactional
    public void testInvoiceBuilder1ProductBlockNullPicture() throws IOException {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        QuoteProduct product = DomainObjectFactory.createQuoteProduct();
        ProductPicture pp = DomainObjectFactory.createProductPicture();

        pp.setPicture(null);
        product.getProduct().setPrimaryPicture(pp);
        testQuote.getProducts().add(product);

        testBuilder = new InvoiceBuilder(testQuote);
        Invoice inv = testBuilder.addProductBlock(testQuote.getProducts()).build();
        assertNotNull(inv);
    }

    @Test
    @Transactional
    public void testInvoiceBuilder1ProductBlockTestPicture() throws IOException {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        QuoteProduct product = DomainObjectFactory.createQuoteProduct();
        ProductPicture pp = DomainObjectFactory.createProductPicture();

        File testFile = new ClassPathResource("images/validImage.jpg").getFile();
        InputStream stream = new FileInputStream(testFile);

        pp.setPicture(IOUtils.toByteArray(stream));
        stream.close();
        product.getProduct().setPrimaryPicture(pp);
        testQuote.getProducts().add(product);

        testBuilder = new InvoiceBuilder(testQuote);
        Invoice inv = testBuilder.addProductBlock(testQuote.getProducts()).build();
        assertNotNull(inv);
    }

    @Test
    @Transactional
    public void testInvoiceBuilder1ProductBlockTestPictureNullPicture() throws IOException {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        QuoteProduct product = DomainObjectFactory.createQuoteProduct();
        QuoteProduct product2 = DomainObjectFactory.createQuoteProduct();
        ProductPicture pp = DomainObjectFactory.createProductPicture();
        ProductPicture pp2 = DomainObjectFactory.createProductPicture();

        File testFile = new ClassPathResource("images/validImage.jpg").getFile();
        InputStream stream = new FileInputStream(testFile);

        pp.setPicture(IOUtils.toByteArray(stream));
        stream.close();
        pp2.setPicture(null);
        product.getProduct().setPrimaryPicture(pp);
        product2.getProduct().setPrimaryPicture(pp2);
        testQuote.getProducts().add(product);
        testQuote.getProducts().add(product2);

        testBuilder = new InvoiceBuilder(testQuote);
        Invoice inv = testBuilder.addProductBlock(testQuote.getProducts()).build();
        assertNotNull(inv);
    }

    @Test
    @Transactional
    public void testInvoiceBuilder2PictureRows() throws IOException {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        QuoteProduct product = DomainObjectFactory.createQuoteProduct();
        QuoteProduct product2 = DomainObjectFactory.createQuoteProduct();
        QuoteProduct product3 = DomainObjectFactory.createQuoteProduct();
        QuoteProduct product4 = DomainObjectFactory.createQuoteProduct();
        QuoteProduct product5 = DomainObjectFactory.createQuoteProduct();
        QuoteProduct product6 = DomainObjectFactory.createQuoteProduct();
        QuoteProduct product7 = DomainObjectFactory.createQuoteProduct();
        ProductPicture pp = DomainObjectFactory.createProductPicture();

        File testFile = new ClassPathResource("images/validImage.jpg").getFile();
        InputStream stream = new FileInputStream(testFile);

        pp.setPicture(IOUtils.toByteArray(stream));
        stream.close();
        product.getProduct().setPrimaryPicture(pp);
        product2.getProduct().setPrimaryPicture(pp);
        product3.getProduct().setPrimaryPicture(pp);
        product4.getProduct().setPrimaryPicture(pp);
        product5.getProduct().setPrimaryPicture(pp);
        product6.getProduct().setPrimaryPicture(pp);
        product7.getProduct().setPrimaryPicture(pp);
        testQuote.getProducts().add(product);
        testQuote.getProducts().add(product2);
        testQuote.getProducts().add(product3);
        testQuote.getProducts().add(product4);
        testQuote.getProducts().add(product5);
        testQuote.getProducts().add(product6);
        testQuote.getProducts().add(product7);

        testBuilder = new InvoiceBuilder(testQuote);
        Invoice inv = testBuilder.addProductBlock(testQuote.getProducts()).build();
        assertNotNull(inv);
    }
}
