package hk.com.ahlopm.productmanager.test.security.core;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.servlet.Filter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import hk.com.ahlopm.productmanager.domainobject.user.User;
import hk.com.ahlopm.productmanager.domainobject.user.UserRole;
import hk.com.ahlopm.productmanager.security.token.TokenHandler;
import hk.com.ahlopm.productmanager.security.token.UserToken;
import hk.com.ahlopm.productmanager.service.user.UserService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SecurityAwareSpringTest;

public class CoreSecurityTest extends SecurityAwareSpringTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private Filter springSecurityFilterChain;

    @Autowired
    private UserService userService;

    @Autowired
    private TokenHandler tokenHandler;

    private User testUser;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilter(springSecurityFilterChain)
                .build();

        testUser = DomainObjectFactory.createUser();
        testUser.setId(0);
        UserRole role = DomainObjectFactory.createUserRole();
        role.setId(0);
        role.setUser(testUser);
        testUser.getRoles().add(role);
        userService.create(testUser);
        testUser = DomainObjectFactory.createUser();
    }

    @After
    public void tearDown() {

    }

    @Test
    @Transactional
    public void testValidUserLogin() throws Exception {

        login(mockMvc, testUser).andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void testInValidLoginNoUsername() throws Exception {

        testUser.setUsername(null);

        login(mockMvc, testUser).andExpect(status().is4xxClientError());
    }

    @Test
    @Transactional
    public void testInValidLoginWrongUsername() throws Exception {

        testUser.setUsername("Invalid");

        login(mockMvc, testUser).andExpect(status().is4xxClientError());
    }

    @Test
    @Transactional
    public void testInValidLoginNoPassword() throws Exception {

        testUser.setPassword(null);

        login(mockMvc, testUser).andExpect(status().is4xxClientError());
    }

    @Test
    @Transactional
    public void testInValidLoginWrongPassword() throws Exception {

        testUser.setPassword("Invalid");

        login(mockMvc, testUser).andExpect(status().is4xxClientError());
    }

    @Test
    @Transactional
    public void testRequestWithTamperedToken() throws Exception {

        String token = getUserAuthToken(mockMvc);
        token = token.substring(5);

        mockMvc.perform(get("/buyer/1").header("X-AUTH-TOKEN", token)).andExpect(status().is3xxRedirection());
    }

    @Test
    @Transactional
    public void testRequestWithMalformedToken() throws Exception {

        String token = "Malformed";

        mockMvc.perform(get("/buyer/1").header("X-AUTH-TOKEN", token)).andExpect(status().is3xxRedirection());
    }

    @Test
    @Transactional
    public void testRequestWithExpiredToken() throws Exception {

        String token = getUserAuthToken(mockMvc);

        UserToken uToken = tokenHandler.parseTokenString(token);

        uToken.setExpiryTime(1);

        String expiredToken = tokenHandler.createTokenForUser(uToken);

        mockMvc.perform(get("/buyer/1").header("X-AUTH-TOKEN", expiredToken)).andExpect(status().is3xxRedirection());
    }
}
