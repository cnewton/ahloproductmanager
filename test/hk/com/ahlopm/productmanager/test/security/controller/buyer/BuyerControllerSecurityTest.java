package hk.com.ahlopm.productmanager.test.security.controller.buyer;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import hk.com.ahlopm.productmanager.domainobject.buyer.Buyer;
import hk.com.ahlopm.productmanager.service.buyer.BuyerService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SecurityAwareSpringTest;

import javax.servlet.Filter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

public class BuyerControllerSecurityTest extends SecurityAwareSpringTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ObjectMapper jsonMapper;

    @Autowired
    private BuyerService buyerService;

    @Autowired
    private Filter springSecurityFilterChain;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilter(springSecurityFilterChain)
                .build();

    }

    @After
    public void tearDown() {

    }

    @Test
    public void testGETWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/buyer/1").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc))).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(jsonPath("$.id", is(1)));
    }

    @Test
    public void testGETAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/buyer/1").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(jsonPath("$.id", is(1)));

    }

    @Test
    public void testGETNotAuthenticated() throws Exception {

        mockMvc.perform(get("/buyer/1")).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testGETAllWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/buyer").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc))).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void testGETAllAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/buyer").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

    }

    @Test
    public void testGETAllNotAuthenticated() throws Exception {

        mockMvc.perform(get("/buyer")).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testPUTWithAuthenticatedUser() throws Exception {

        Buyer testBuyer = DomainObjectFactory.createBuyer();
        testBuyer.setId(0);
        testBuyer = buyerService.create(testBuyer);
        testBuyer.setEmail("updated@email.com");

        mockMvc.perform(put("/buyer").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc))
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(testBuyer)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.email", is("updated@email.com")));

        // Cleanup
        buyerService.delete(testBuyer);
    }

    @Test
    public void testPUTAuthenticatedAdmin() throws Exception {

        Buyer testBuyer = DomainObjectFactory.createBuyer();
        testBuyer.setId(0);
        testBuyer = buyerService.create(testBuyer);
        testBuyer.setEmail("updated@email.com");

        mockMvc.perform(put("/buyer").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(testBuyer)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.email", is("updated@email.com")));

        // Cleanup
        buyerService.delete(testBuyer);

    }

    @Test
    public void testPUTNotAuthenticated() throws Exception {

        Buyer testBuyer = DomainObjectFactory.createBuyer();
        testBuyer.setId(0);
        testBuyer = buyerService.create(testBuyer);

        mockMvc.perform(put("/buyer").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(testBuyer))).andExpect(status().is3xxRedirection());

        // Cleanup
        buyerService.delete(testBuyer);

    }

    @Test
    public void testPOSTWithAuthenticatedUser() throws Exception {

        Buyer testBuyer = DomainObjectFactory.createBuyer();
        testBuyer.setId(0);
        testBuyer.setEmail("updated@email.com");

        MvcResult result = mockMvc
                .perform(post("/buyer").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc))
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(testBuyer)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.email", is("updated@email.com"))).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        Buyer returnedObject = jsonMapper.readValue(responseBody.getBytes(), Buyer.class);

        // Cleanup
        buyerService.delete(returnedObject.getId());
    }

    @Test
    public void testPOSTAuthenticatedAdmin() throws Exception {

        Buyer testBuyer = DomainObjectFactory.createBuyer();
        testBuyer.setId(0);
        testBuyer.setEmail("updated@email.com");

        MvcResult result = mockMvc
                .perform(post("/buyer").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(testBuyer)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.email", is("updated@email.com"))).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        Buyer returnedObject = jsonMapper.readValue(responseBody.getBytes(), Buyer.class);

        // Cleanup
        buyerService.delete(returnedObject.getId());

    }

    @Test
    public void testPOSTNotAuthenticated() throws Exception {

        Buyer testBuyer = DomainObjectFactory.createBuyer();
        testBuyer.setId(0);

        mockMvc.perform(post("/buyer").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(testBuyer))).andExpect(status().is3xxRedirection());
    }

    @Test
    public void testDELETEWithAuthenticatedUser() throws Exception {

        Buyer testBuyer = DomainObjectFactory.createBuyer();
        testBuyer.setId(0);
        testBuyer = buyerService.create(testBuyer);

        mockMvc.perform(delete("/buyer/" + testBuyer.getId()).header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testDELETEAuthenticatedAdmin() throws Exception {

        Buyer testBuyer = DomainObjectFactory.createBuyer();
        testBuyer.setId(0);
        testBuyer = buyerService.create(testBuyer);

        mockMvc.perform(delete("/buyer/" + testBuyer.getId()).header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testDELETENotAuthenticated() throws Exception {

        Buyer testBuyer = DomainObjectFactory.createBuyer();
        testBuyer.setId(0);
        testBuyer = buyerService.create(testBuyer);

        mockMvc.perform(delete("/buyer/" + testBuyer.getId())).andExpect(status().is3xxRedirection());

        // Cleanup
        buyerService.delete(testBuyer);
    }

    @Test
    public void testCountWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/buyer/count").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testCountAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/buyer/count").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testCountNotAuthenticated() throws Exception {

        mockMvc.perform(get("/buyer/count")).andExpect(status().is3xxRedirection());
    }

}
