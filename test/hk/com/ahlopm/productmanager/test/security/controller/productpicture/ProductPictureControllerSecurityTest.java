package hk.com.ahlopm.productmanager.test.security.controller.productpicture;

import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import hk.com.ahlopm.productmanager.amazons3.AmazonS3Gateway;
import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.domainobject.productpicture.ProductPicture;
import hk.com.ahlopm.productmanager.service.product.ProductService;
import hk.com.ahlopm.productmanager.service.productpicture.ProductPictureService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SecurityAwareSpringTest;

import java.io.ByteArrayInputStream;

import javax.servlet.Filter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ProductPictureControllerSecurityTest extends SecurityAwareSpringTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ObjectMapper jsonMapper;

    @Autowired
    private ProductPictureService productpictureService;

    @Autowired
    private ProductService productService;

    @Autowired
    private Filter springSecurityFilterChain;

    @Autowired
    private AmazonS3Gateway gateway;

    @Mock
    private AmazonS3 s3Client;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilter(springSecurityFilterChain)
                .build();

        MockitoAnnotations.initMocks(this);

        S3Object s3obj = new S3Object();
        s3obj.setObjectContent(new ByteArrayInputStream(new byte[] { 0, 1, 0, 1 }));
        when(s3Client.getObject(any())).thenReturn(s3obj);

        ReflectionTestUtils.setField(gateway, "s3Client", s3Client); // Allow
                                                                     // mock
                                                                     // returns
    }

    @After
    public void tearDown() throws Exception {

        ReflectionTestUtils.setField(gateway, "s3Client", null); // Repair
                                                                 // context

    }

    @Test
    public void testGETWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/productpicture/1").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(1)));
    }

    @Test
    public void testGETAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/productpicture/1").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(1)));

    }

    @Test
    public void testGETNotAuthenticated() throws Exception {

        mockMvc.perform(get("/productpicture/1")).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testGETAllWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/productpicture").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void testGETAllAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/productpicture").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

    }

    @Test
    public void testGETAllNotAuthenticated() throws Exception {

        mockMvc.perform(get("/productpicture")).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testPUTWithAuthenticatedUser() throws Exception {

        ProductPicture testProductPicture = DomainObjectFactory.createProductPicture();
        testProductPicture.setId(0);
        testProductPicture = productpictureService.create(testProductPicture);
        testProductPicture.setEncodeType("updated");

        mockMvc.perform(put("/productpicture").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc))
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(testProductPicture)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.encodeType", is("updated")));

        // Cleanup
        productpictureService.delete(testProductPicture);
    }

    @Test
    public void testPUTAuthenticatedAdmin() throws Exception {

        ProductPicture testProductPicture = DomainObjectFactory.createProductPicture();
        testProductPicture.setId(0);
        testProductPicture = productpictureService.create(testProductPicture);
        testProductPicture.setEncodeType("updated");

        mockMvc.perform(put("/productpicture").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(testProductPicture)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.encodeType", is("updated")));

        // Cleanup
        productpictureService.delete(testProductPicture);

    }

    @Test
    public void testPUTNotAuthenticated() throws Exception {

        ProductPicture testProductPicture = DomainObjectFactory.createProductPicture();
        testProductPicture.setId(0);
        testProductPicture = productpictureService.create(testProductPicture);

        mockMvc.perform(put("/productpicture").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(testProductPicture))).andExpect(status().is3xxRedirection());

        // Cleanup
        productpictureService.delete(testProductPicture);

    }

    @Test
    public void testPOSTWithAuthenticatedUser() throws Exception {

        ProductPicture testProductPicture = DomainObjectFactory.createProductPicture();
        testProductPicture.setId(0);
        testProductPicture.setEncodeType("updated");

        MvcResult result = mockMvc
                .perform(post("/productpicture").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(testProductPicture)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.encodeType", is("updated"))).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        ProductPicture returnedObject = jsonMapper.readValue(responseBody.getBytes(), ProductPicture.class);

        // Cleanup
        productpictureService.delete(returnedObject.getId());
    }

    @Test
    public void testPOSTAuthenticatedAdmin() throws Exception {

        ProductPicture testProductPicture = DomainObjectFactory.createProductPicture();
        testProductPicture.setId(0);
        testProductPicture.setEncodeType("updated");

        MvcResult result = mockMvc
                .perform(post("/productpicture").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(testProductPicture)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.encodeType", is("updated"))).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        ProductPicture returnedObject = jsonMapper.readValue(responseBody.getBytes(), ProductPicture.class);

        // Cleanup
        productpictureService.delete(returnedObject.getId());

    }

    @Test
    public void testPOSTNotAuthenticated() throws Exception {

        ProductPicture testProductPicture = DomainObjectFactory.createProductPicture();
        testProductPicture.setId(0);

        mockMvc.perform(post("/productpicture").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(testProductPicture))).andExpect(status().is3xxRedirection());
    }

    @Test
    public void testDELETEWithAuthenticatedUser() throws Exception {

        ProductPicture testProductPicture = DomainObjectFactory.createProductPicture();
        testProductPicture.setId(0);
        testProductPicture = productpictureService.create(testProductPicture);

        mockMvc.perform(delete("/productpicture/" + testProductPicture.getId()).header("X-AUTH-TOKEN",
                getUserAuthToken(mockMvc))).andExpect(status().isOk());
    }

    @Test
    public void testDELETEAuthenticatedAdmin() throws Exception {

        ProductPicture testProductPicture = DomainObjectFactory.createProductPicture();
        testProductPicture.setId(0);
        testProductPicture = productpictureService.create(testProductPicture);

        mockMvc.perform(delete("/productpicture/" + testProductPicture.getId()).header("X-AUTH-TOKEN",
                getAdminAuthToken(mockMvc))).andExpect(status().isOk());
    }

    @Test
    public void testDELETENotAuthenticated() throws Exception {

        ProductPicture testProductPicture = DomainObjectFactory.createProductPicture();
        testProductPicture.setId(0);
        testProductPicture = productpictureService.create(testProductPicture);

        mockMvc.perform(delete("/productpicture/" + testProductPicture.getId())).andExpect(status().is3xxRedirection());

        // Cleanup
        productpictureService.delete(testProductPicture);
    }

    @Test
    public void testCountWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/productpicture/count").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testCountAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/productpicture/count").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testCountNotAuthenticated() throws Exception {

        mockMvc.perform(get("/productpicture/count")).andExpect(status().is3xxRedirection());
    }

    @Test
    public void testGETResolvedImageWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/productpicture/1/resolve").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testGETResolvedImageAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/productpicture/1/resolve").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk());

    }

    @Test
    public void testGETResolvedImageNotAuthenticated() throws Exception {

        mockMvc.perform(get("/productpicture/1/resolve")).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testPUTPrimaryWithAuthenticatedUser() throws Exception {

        ProductPicture testProductPicture = DomainObjectFactory.createProductPicture();
        Product product = productService.find(1);
        testProductPicture.setId(0);
        testProductPicture.setProduct(product);
        testProductPicture = productpictureService.create(testProductPicture);
        testProductPicture.setEncodeType("updated");

        mockMvc.perform(put("/productpicture/primary").param("product", "" + product.getId())
                .param("picture", "" + testProductPicture.getId()).header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk());

        // Cleanup
        productpictureService.delete(testProductPicture);
    }

    @Test
    public void testPUTPrimaryAuthenticatedAdmin() throws Exception {

        ProductPicture testProductPicture = DomainObjectFactory.createProductPicture();
        Product product = productService.find(1);
        testProductPicture.setId(0);
        testProductPicture.setProduct(product);
        testProductPicture = productpictureService.create(testProductPicture);
        testProductPicture.setEncodeType("updated");

        mockMvc.perform(put("/productpicture/primary").param("product", "" + product.getId())
                .param("picture", "" + testProductPicture.getId()).header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk());

        // Cleanup
        productpictureService.delete(testProductPicture);

    }

    @Test
    public void testPUTPrimaryNotAuthenticated() throws Exception {

        ProductPicture testProductPicture = DomainObjectFactory.createProductPicture();
        Product product = productService.find(1);
        testProductPicture.setId(0);
        testProductPicture.setProduct(product);
        testProductPicture = productpictureService.create(testProductPicture);
        testProductPicture.setEncodeType("updated");

        mockMvc.perform(put("/productpicture/primary").param("product", "" + product.getId()).param("picture",
                "" + testProductPicture.getId())).andExpect(status().is3xxRedirection());

        // Cleanup
        productpictureService.delete(testProductPicture);

    }

}
