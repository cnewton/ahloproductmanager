package hk.com.ahlopm.productmanager.test.security.controller.quotationmanagement;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.servlet.Filter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.quotation.QuotationStatus;
import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;
import hk.com.ahlopm.productmanager.service.quotation.QuotationService;
import hk.com.ahlopm.productmanager.service.quotationmanagement.QuotationManagementService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SecurityAwareSpringTest;

public class QuotationManagementControllerSecurityTest extends SecurityAwareSpringTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private QuotationService quotationService;

    @Autowired
    private QuotationManagementService quotationManagementService;

    @Autowired
    private Filter springSecurityFilterChain;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilter(springSecurityFilterChain)
                .build();

    }

    @After
    public void tearDown() {

    }

    @Test
    public void testFinalizeWithAuthenticatedUser() throws Exception {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        testQuote.setId(0);
        testQuote.setStatus(QuotationStatus.REVIEW);

        QuoteProduct product = DomainObjectFactory.createQuoteProduct();
        product.setId(0);
        product.setQuotation(testQuote);
        testQuote.getProducts().add(product);

        testQuote = quotationService.create(testQuote);

        mockMvc.perform(
                put("/quotation/" + testQuote.getId() + "/finalize").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk());

        // Cleanup
        quotationService.delete(testQuote);
    }

    @Test
    public void testFinalizeAuthenticatedAdmin() throws Exception {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        testQuote.setId(0);
        testQuote.setStatus(QuotationStatus.REVIEW);

        QuoteProduct product = DomainObjectFactory.createQuoteProduct();
        product.setId(0);
        product.setQuotation(testQuote);
        testQuote.getProducts().add(product);

        testQuote = quotationService.create(testQuote);

        mockMvc.perform(
                put("/quotation/" + testQuote.getId() + "/finalize").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk());

        // Cleanup
        quotationService.delete(testQuote);

    }

    @Test
    public void testFinalizeNotAuthenticated() throws Exception {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        testQuote.setId(0);
        testQuote.setStatus(QuotationStatus.REVIEW);

        QuoteProduct product = DomainObjectFactory.createQuoteProduct();
        product.setId(0);
        product.setQuotation(testQuote);
        testQuote.getProducts().add(product);

        testQuote = quotationService.create(testQuote);

        mockMvc.perform(put("/quotation/" + testQuote.getId() + "/finalize")).andExpect(status().is3xxRedirection());

        // Cleanup
        quotationService.delete(testQuote);

    }

    @Test
    public void testReviewWithAuthenticatedUser() throws Exception {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        testQuote.setId(0);
        testQuote.setStatus(QuotationStatus.FINALIZING);
        testQuote = quotationService.create(testQuote);

        mockMvc.perform(
                put("/quotation/" + testQuote.getId() + "/review").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk());

        // Cleanup
        quotationService.delete(testQuote);
    }

    @Test
    public void testReviewAuthenticatedAdmin() throws Exception {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        testQuote.setId(0);
        testQuote.setStatus(QuotationStatus.FINALIZING);
        testQuote = quotationService.create(testQuote);

        mockMvc.perform(
                put("/quotation/" + testQuote.getId() + "/review").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk());

        // Cleanup
        quotationService.delete(testQuote);

    }

    @Test
    public void testReviewNotAuthenticated() throws Exception {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        testQuote.setId(0);
        testQuote.setStatus(QuotationStatus.FINALIZING);
        testQuote = quotationService.create(testQuote);

        mockMvc.perform(put("/quotation/" + testQuote.getId() + "/review")).andExpect(status().is3xxRedirection());

        // Cleanup
        quotationService.delete(testQuote);

    }

    @Test
    public void testPresentWithAuthenticatedUser() throws Exception {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        testQuote.setId(0);
        testQuote.setStatus(QuotationStatus.FINALIZING);
        testQuote = quotationService.create(testQuote);

        mockMvc.perform(
                put("/quotation/" + testQuote.getId() + "/present").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk());

        // Cleanup
        quotationService.delete(testQuote);
    }

    @Test
    public void testPresentAuthenticatedAdmin() throws Exception {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        testQuote.setId(0);
        testQuote.setStatus(QuotationStatus.FINALIZING);
        testQuote = quotationService.create(testQuote);

        mockMvc.perform(
                put("/quotation/" + testQuote.getId() + "/present").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk());

        // Cleanup
        quotationService.delete(testQuote);

    }

    @Test
    public void testPresentNotAuthenticated() throws Exception {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        testQuote.setId(0);
        testQuote.setStatus(QuotationStatus.FINALIZING);
        testQuote = quotationService.create(testQuote);

        mockMvc.perform(put("/quotation/" + testQuote.getId() + "/present")).andExpect(status().is3xxRedirection());

        // Cleanup
        quotationService.delete(testQuote);

    }

    @Test
    public void testAcceptWithAuthenticatedUser() throws Exception {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        testQuote.setId(0);
        testQuote.setStatus(QuotationStatus.FINALIZING);
        testQuote = quotationService.create(testQuote);
        quotationManagementService.beginPresentingQuotation(testQuote.getId());

        mockMvc.perform(
                put("/quotation/" + testQuote.getId() + "/accept").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk());

        // Cleanup
        quotationService.delete(testQuote);
    }

    @Test
    public void testAcceptAuthenticatedAdmin() throws Exception {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        testQuote.setId(0);
        testQuote.setStatus(QuotationStatus.FINALIZING);
        testQuote = quotationService.create(testQuote);
        quotationManagementService.beginPresentingQuotation(testQuote.getId());

        mockMvc.perform(
                put("/quotation/" + testQuote.getId() + "/accept").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk());

        // Cleanup
        quotationService.delete(testQuote);

    }

    @Test
    public void testAcceptNotAuthenticated() throws Exception {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        testQuote.setId(0);
        testQuote.setStatus(QuotationStatus.FINALIZING);
        testQuote = quotationService.create(testQuote);
        quotationManagementService.beginPresentingQuotation(testQuote.getId());

        mockMvc.perform(put("/quotation/" + testQuote.getId() + "/accept")).andExpect(status().is3xxRedirection());

        // Cleanup
        quotationService.delete(testQuote);

    }

    @Test
    public void testDeclineWithAuthenticatedUser() throws Exception {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        testQuote.setId(0);
        testQuote.setStatus(QuotationStatus.FINALIZING);
        testQuote = quotationService.create(testQuote);
        quotationManagementService.beginPresentingQuotation(testQuote.getId());

        mockMvc.perform(
                put("/quotation/" + testQuote.getId() + "/decline").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk());

        // Cleanup
        quotationService.delete(testQuote);
    }

    @Test
    public void testDeclineAuthenticatedAdmin() throws Exception {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        testQuote.setId(0);
        testQuote.setStatus(QuotationStatus.FINALIZING);
        testQuote = quotationService.create(testQuote);
        quotationManagementService.beginPresentingQuotation(testQuote.getId());

        mockMvc.perform(
                put("/quotation/" + testQuote.getId() + "/decline").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk());

        // Cleanup
        quotationService.delete(testQuote);

    }

    @Test
    public void testDeclineNotAuthenticated() throws Exception {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        testQuote.setId(0);
        testQuote.setStatus(QuotationStatus.FINALIZING);
        testQuote = quotationService.create(testQuote);
        quotationManagementService.beginPresentingQuotation(testQuote.getId());

        mockMvc.perform(put("/quotation/" + testQuote.getId() + "/decline")).andExpect(status().is3xxRedirection());

        // Cleanup
        quotationService.delete(testQuote);

    }

    @Test
    public void testCancelWithAuthenticatedUser() throws Exception {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        testQuote.setId(0);
        testQuote.setStatus(QuotationStatus.FINALIZING);
        testQuote = quotationService.create(testQuote);
        quotationManagementService.beginPresentingQuotation(testQuote.getId());

        mockMvc.perform(
                put("/quotation/" + testQuote.getId() + "/cancel").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk());

        // Cleanup
        quotationService.delete(testQuote);
    }

    @Test
    public void testCancelAuthenticatedAdmin() throws Exception {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        testQuote.setId(0);
        testQuote.setStatus(QuotationStatus.FINALIZING);
        testQuote = quotationService.create(testQuote);
        quotationManagementService.beginPresentingQuotation(testQuote.getId());

        mockMvc.perform(
                put("/quotation/" + testQuote.getId() + "/cancel").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk());

        // Cleanup
        quotationService.delete(testQuote);

    }

    @Test
    public void testCancelNotAuthenticated() throws Exception {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        testQuote.setId(0);
        testQuote.setStatus(QuotationStatus.FINALIZING);
        testQuote = quotationService.create(testQuote);
        quotationManagementService.beginPresentingQuotation(testQuote.getId());

        mockMvc.perform(put("/quotation/" + testQuote.getId() + "/cancel")).andExpect(status().is3xxRedirection());

        // Cleanup
        quotationService.delete(testQuote);

    }

}
