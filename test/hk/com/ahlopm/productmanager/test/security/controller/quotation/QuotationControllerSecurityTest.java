package hk.com.ahlopm.productmanager.test.security.controller.quotation;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.service.quotation.QuotationService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SecurityAwareSpringTest;

import javax.servlet.Filter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

public class QuotationControllerSecurityTest extends SecurityAwareSpringTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ObjectMapper jsonMapper;

    @Autowired
    private QuotationService quotationService;

    @Autowired
    private Filter springSecurityFilterChain;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilter(springSecurityFilterChain)
                .build();

    }

    @After
    public void tearDown() {

    }

    @Test
    public void testGETWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/quotation/1").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(1)));
    }

    @Test
    public void testGETAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/quotation/1").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(1)));

    }

    @Test
    public void testGETNotAuthenticated() throws Exception {

        mockMvc.perform(get("/quotation/1")).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testGETAllWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/quotation").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc))).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void testGETAllAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/quotation").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

    }

    @Test
    public void testGETAllNotAuthenticated() throws Exception {

        mockMvc.perform(get("/quotation")).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testPUTWithAuthenticatedUser() throws Exception {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);
        testQuotation = quotationService.create(testQuotation);
        testQuotation.setName("updated");

        mockMvc.perform(put("/quotation").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc))
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(testQuotation)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.name", is("updated")));

        // Cleanup
        quotationService.delete(testQuotation);
    }

    @Test
    public void testPUTAuthenticatedAdmin() throws Exception {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);
        testQuotation = quotationService.create(testQuotation);
        testQuotation.setName("updated");

        mockMvc.perform(put("/quotation").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(testQuotation)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.name", is("updated")));

        // Cleanup
        quotationService.delete(testQuotation);

    }

    @Test
    public void testPUTNotAuthenticated() throws Exception {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);
        testQuotation = quotationService.create(testQuotation);

        mockMvc.perform(put("/quotation").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(testQuotation))).andExpect(status().is3xxRedirection());

        // Cleanup
        quotationService.delete(testQuotation);

    }

    @Test
    public void testPOSTWithAuthenticatedUser() throws Exception {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);
        testQuotation.setName("updated");

        MvcResult result = mockMvc
                .perform(post("/quotation").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(testQuotation)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.name", is("updated"))).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        Quotation returnedObject = jsonMapper.readValue(responseBody.getBytes(), Quotation.class);

        // Cleanup
        quotationService.delete(returnedObject.getId());
    }

    @Test
    public void testPOSTAuthenticatedAdmin() throws Exception {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);
        testQuotation.setName("updated");

        MvcResult result = mockMvc
                .perform(post("/quotation").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(testQuotation)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.name", is("updated"))).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        Quotation returnedObject = jsonMapper.readValue(responseBody.getBytes(), Quotation.class);

        // Cleanup
        quotationService.delete(returnedObject.getId());

    }

    @Test
    public void testPOSTNotAuthenticated() throws Exception {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);

        mockMvc.perform(post("/quotation").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(testQuotation))).andExpect(status().is3xxRedirection());
    }

    @Test
    public void testDELETEWithAuthenticatedUser() throws Exception {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);
        testQuotation = quotationService.create(testQuotation);

        mockMvc.perform(delete("/quotation/" + testQuotation.getId()).header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testDELETEAuthenticatedAdmin() throws Exception {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);
        testQuotation = quotationService.create(testQuotation);

        mockMvc.perform(
                delete("/quotation/" + testQuotation.getId()).header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testDELETENotAuthenticated() throws Exception {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);
        testQuotation = quotationService.create(testQuotation);

        mockMvc.perform(delete("/quotation/" + testQuotation.getId())).andExpect(status().is3xxRedirection());

        // Cleanup
        quotationService.delete(testQuotation);
    }

    @Test
    public void testCountWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/quotation/count").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testCountAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/quotation/count").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testCountNotAuthenticated() throws Exception {

        mockMvc.perform(get("/quotation/count")).andExpect(status().is3xxRedirection());
    }

    @Test
    public void testGETReviewWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/quotation/review").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testGETReviewAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/quotation/review").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk());

    }

    @Test
    public void testGETReviewNotAuthenticated() throws Exception {

        mockMvc.perform(get("/quotation/review")).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testGETManagementWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/quotation/management").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testGETManagementAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/quotation/management").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk());

    }

    @Test
    public void testGETManagementNotAuthenticated() throws Exception {

        mockMvc.perform(get("/quotation/management")).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testGETPriceWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/quotation/1/price").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testGETPriceAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/quotation/1/price").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk());

    }

    @Test
    public void testGETPriceNotAuthenticated() throws Exception {

        mockMvc.perform(get("/quotation/1/price")).andExpect(status().is3xxRedirection());

    }

}
