package hk.com.ahlopm.productmanager.test.security.controller.product;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.service.product.ProductService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SecurityAwareSpringTest;

import javax.servlet.Filter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ProductControllerSecurityTest extends SecurityAwareSpringTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ObjectMapper jsonMapper;

    @Autowired
    private ProductService productService;

    @Autowired
    private Filter springSecurityFilterChain;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilter(springSecurityFilterChain)
                .build();

    }

    @After
    public void tearDown() {

    }

    @Test
    public void testGETWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/product/1").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc))).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(jsonPath("$.id", is(1)));
    }

    @Test
    public void testGETAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/product/1").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(jsonPath("$.id", is(1)));

    }

    @Test
    public void testGETNotAuthenticated() throws Exception {

        mockMvc.perform(get("/product/1")).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testGETAllWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/product").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc))).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void testGETAllAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/product").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

    }

    @Test
    public void testGETAllNotAuthenticated() throws Exception {

        mockMvc.perform(get("/product")).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testPUTWithAuthenticatedUser() throws Exception {

        Product testProduct = DomainObjectFactory.createProduct();
        testProduct.setId(0);
        testProduct = productService.create(testProduct);
        testProduct.setProductCode("updated");

        mockMvc.perform(put("/product").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc))
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(testProduct)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.productCode", is("updated")));

        // Cleanup
        productService.delete(testProduct);
    }

    @Test
    public void testPUTAuthenticatedAdmin() throws Exception {

        Product testProduct = DomainObjectFactory.createProduct();
        testProduct.setId(0);
        testProduct = productService.create(testProduct);
        testProduct.setProductCode("updated");

        mockMvc.perform(put("/product").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(testProduct)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.productCode", is("updated")));

        // Cleanup
        productService.delete(testProduct);

    }

    @Test
    public void testPUTNotAuthenticated() throws Exception {

        Product testProduct = DomainObjectFactory.createProduct();
        testProduct.setId(0);
        testProduct = productService.create(testProduct);

        mockMvc.perform(put("/product").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(testProduct))).andExpect(status().is3xxRedirection());

        // Cleanup
        productService.delete(testProduct);

    }

    @Test
    public void testPOSTWithAuthenticatedUser() throws Exception {

        Product testProduct = DomainObjectFactory.createProduct();
        testProduct.setId(0);
        testProduct.setProductCode("updated");

        MvcResult result = mockMvc
                .perform(post("/product").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(testProduct)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.productCode", is("updated"))).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        Product returnedObject = jsonMapper.readValue(responseBody.getBytes(), Product.class);

        // Cleanup
        productService.delete(returnedObject.getId());
    }

    @Test
    public void testPOSTAuthenticatedAdmin() throws Exception {

        Product testProduct = DomainObjectFactory.createProduct();
        testProduct.setId(0);
        testProduct.setProductCode("updated");

        MvcResult result = mockMvc
                .perform(post("/product").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(testProduct)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.productCode", is("updated"))).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        Product returnedObject = jsonMapper.readValue(responseBody.getBytes(), Product.class);

        // Cleanup
        productService.delete(returnedObject.getId());

    }

    @Test
    public void testPOSTNotAuthenticated() throws Exception {

        Product testProduct = DomainObjectFactory.createProduct();
        testProduct.setId(0);

        mockMvc.perform(post("/product").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(testProduct))).andExpect(status().is3xxRedirection());
    }

    @Test
    public void testDELETEWithAuthenticatedUser() throws Exception {

        Product testProduct = DomainObjectFactory.createProduct();
        testProduct.setId(0);
        testProduct = productService.create(testProduct);

        mockMvc.perform(delete("/product/" + testProduct.getId()).header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testDELETEAuthenticatedAdmin() throws Exception {

        Product testProduct = DomainObjectFactory.createProduct();
        testProduct.setId(0);
        testProduct = productService.create(testProduct);

        mockMvc.perform(delete("/product/" + testProduct.getId()).header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testDELETENotAuthenticated() throws Exception {

        Product testProduct = DomainObjectFactory.createProduct();
        testProduct.setId(0);
        testProduct = productService.create(testProduct);

        mockMvc.perform(delete("/product/" + testProduct.getId())).andExpect(status().is3xxRedirection());

        // Cleanup
        productService.delete(testProduct);
    }

    @Test
    public void testCountWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/product/count").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testCountAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/product/count").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testCountNotAuthenticated() throws Exception {

        mockMvc.perform(get("/product/count")).andExpect(status().is3xxRedirection());
    }

    @Test
    public void testGETVisibleWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/product/visible/1").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void testGETVisibleAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/product/visible/1").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

    }

    @Test
    public void testGETVisibleNotAuthenticated() throws Exception {

        mockMvc.perform(get("/product/visible/1")).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testGETUnresolvedImagesWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/product/1/images").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testGETUnresolvedImagesAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/product/1/images").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk());

    }

    @Test
    public void testGETUnresolvedImagesNotAuthenticated() throws Exception {

        mockMvc.perform(get("/product/1/images")).andExpect(status().is3xxRedirection());

    }

}
