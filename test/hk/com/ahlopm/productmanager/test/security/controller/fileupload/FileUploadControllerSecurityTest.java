package hk.com.ahlopm.productmanager.test.security.controller.fileupload;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.Filter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.amazonaws.services.s3.AmazonS3;

import hk.com.ahlopm.productmanager.amazons3.AmazonS3Gateway;
import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.domainobject.productpicture.ProductPicture;
import hk.com.ahlopm.productmanager.service.product.ProductService;
import hk.com.ahlopm.productmanager.service.productpicture.ProductPictureService;
import hk.com.ahlopm.productmanager.test.common.SecurityAwareSpringTest;

public class FileUploadControllerSecurityTest extends SecurityAwareSpringTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private Filter springSecurityFilterChain;

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductPictureService productPictureService;

    @Autowired
    private AmazonS3Gateway gateway;

    @Mock
    private AmazonS3 s3Client;

    @Before
    public void setUp() throws IOException {

        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilter(springSecurityFilterChain)
                .build();

        when(s3Client.putObject(any())).thenReturn(null);

        ReflectionTestUtils.setField(gateway, "s3Client", s3Client); // Allow
                                                                     // mock
                                                                     // returns
    }

    @After
    public void tearDown() {

        ReflectionTestUtils.setField(gateway, "s3Client", null); // repair
                                                                 // context
    }

    @Test
    public void testUploadXLSWithAuthenticatedUser() throws Exception {

        File testFile = new ClassPathResource("xls/valid1Product0Image.xlsx").getFile();
        InputStream inputStream = new FileInputStream(testFile);

        MockMultipartFile uploadFile = new MockMultipartFile("file", inputStream);

        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload/product").file(uploadFile).header("X-AUTH-TOKEN",
                getUserAuthToken(mockMvc))).andExpect(status().isOk());

        // Cleanup
        List<Product> products = productService.findAll();
        Product lastProduct = products.get(products.size() - 1); // Latest added
                                                                 // product
        productService.delete(lastProduct);
    }

    @Test
    public void testUploadXLSAuthenticatedAdmin() throws Exception {

        File testFile = new ClassPathResource("xls/valid1Product0Image.xlsx").getFile();
        InputStream inputStream = new FileInputStream(testFile);

        MockMultipartFile uploadFile = new MockMultipartFile("file", inputStream);

        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload/product").file(uploadFile).header("X-AUTH-TOKEN",
                getAdminAuthToken(mockMvc))).andExpect(status().isOk());

        // Cleanup
        List<Product> products = productService.findAll();
        Product lastProduct = products.get(products.size() - 1); // Latest added
                                                                 // product
        productService.delete(lastProduct);

    }

    @Test
    public void testUploadXLSNotAuthenticated() throws Exception {

        File testFile = new ClassPathResource("xls/valid1Product0Image.xlsx").getFile();
        InputStream inputStream = new FileInputStream(testFile);

        MockMultipartFile uploadFile = new MockMultipartFile("file", inputStream);

        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload/product").file(uploadFile))
                .andExpect(status().is3xxRedirection());

    }

    @Test
    public void testUploadPictureWithAuthenticatedUser() throws Exception {

        File testFile = new ClassPathResource("images/validImage.jpg").getFile();
        InputStream stream = new FileInputStream(testFile);
        MockMultipartFile uploadFile = new MockMultipartFile("file", "validImage.jpg", "image/jpeg", stream);

        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload/product/1/image").file(uploadFile)
                .header("X-AUTH-TOKEN", getUserAuthToken(mockMvc))).andExpect(status().isOk());

        // Cleanup
        List<ProductPicture> pictures = productPictureService.findAll();
        ProductPicture lastPicture = pictures.get(pictures.size() - 1); // Latest
                                                                        // added
                                                                        // picture
        productPictureService.delete(lastPicture);
    }

    @Test
    public void testUploadPictureAuthenticatedAdmin() throws Exception {

        File testFile = new ClassPathResource("images/validImage.jpg").getFile();
        InputStream stream = new FileInputStream(testFile);
        MockMultipartFile uploadFile = new MockMultipartFile("file", "validImage.jpg", "image/jpeg", stream);

        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload/product/1/image").file(uploadFile)
                .header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))).andExpect(status().isOk());

        // Cleanup
        List<ProductPicture> pictures = productPictureService.findAll();
        ProductPicture lastPicture = pictures.get(pictures.size() - 1); // Latest
                                                                        // added
                                                                        // picture
        productPictureService.delete(lastPicture);

    }

    @Test
    public void testUploadPictureNotAuthenticated() throws Exception {

        File testFile = new ClassPathResource("images/validImage.jpg").getFile();
        InputStream stream = new FileInputStream(testFile);
        MockMultipartFile uploadFile = new MockMultipartFile("file", "validImage.jpg", "image/jpeg", stream);

        mockMvc.perform(MockMvcRequestBuilders.fileUpload("/upload/product/1/image").file(uploadFile))
                .andExpect(status().is3xxRedirection());

    }
}
