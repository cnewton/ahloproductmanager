package hk.com.ahlopm.productmanager.test.security.controller.serializedquotation;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import hk.com.ahlopm.productmanager.domainobject.serializedquotation.SerializedQuotation;
import hk.com.ahlopm.productmanager.service.serializedquotation.SerializedQuotationService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SecurityAwareSpringTest;

import javax.servlet.Filter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

public class SerializedQuotationControllerSecurityTest extends SecurityAwareSpringTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ObjectMapper jsonMapper;

    @Autowired
    private SerializedQuotationService serializedQuotationService;

    @Autowired
    private Filter springSecurityFilterChain;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilter(springSecurityFilterChain)
                .build();

    }

    @After
    public void tearDown() {

    }

    @Test
    public void testGETWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/records/quotation/1").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(1)));
    }

    @Test
    public void testGETAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/records/quotation/1").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(1)));

    }

    @Test
    public void testGETNotAuthenticated() throws Exception {

        mockMvc.perform(get("/records/quotation/1")).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testGETAllWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/records/quotation").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void testGETAllAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/records/quotation").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

    }

    @Test
    public void testGETAllNotAuthenticated() throws Exception {

        mockMvc.perform(get("/records/quotation")).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testPUTWithAuthenticatedUser() throws Exception {

        SerializedQuotation testSerializedQuotation = DomainObjectFactory.createSerializedQuotation();
        testSerializedQuotation.setId(0);
        testSerializedQuotation = serializedQuotationService.create(testSerializedQuotation);
        testSerializedQuotation.setQuotation("updated");

        mockMvc.perform(put("/records/quotation").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(testSerializedQuotation))).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.quotation", is("updated")));

        // Cleanup
        serializedQuotationService.delete(testSerializedQuotation);
    }

    @Test
    public void testPUTAuthenticatedAdmin() throws Exception {

        SerializedQuotation testSerializedQuotation = DomainObjectFactory.createSerializedQuotation();
        testSerializedQuotation.setId(0);
        testSerializedQuotation = serializedQuotationService.create(testSerializedQuotation);
        testSerializedQuotation.setQuotation("updated");

        mockMvc.perform(put("/records/quotation").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(testSerializedQuotation))).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.quotation", is("updated")));

        // Cleanup
        serializedQuotationService.delete(testSerializedQuotation);

    }

    @Test
    public void testPUTNotAuthenticated() throws Exception {

        SerializedQuotation testSerializedQuotation = DomainObjectFactory.createSerializedQuotation();
        testSerializedQuotation.setId(0);
        testSerializedQuotation = serializedQuotationService.create(testSerializedQuotation);

        mockMvc.perform(put("/records/quotation").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(testSerializedQuotation))).andExpect(status().is3xxRedirection());

        // Cleanup
        serializedQuotationService.delete(testSerializedQuotation);

    }

    @Test
    public void testPOSTWithAuthenticatedUser() throws Exception {

        SerializedQuotation testSerializedQuotation = DomainObjectFactory.createSerializedQuotation();
        testSerializedQuotation.setId(0);
        testSerializedQuotation.setQuotation("updated");

        MvcResult result = mockMvc
                .perform(post("/records/quotation").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(testSerializedQuotation)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.quotation", is("updated"))).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        SerializedQuotation returnedObject = jsonMapper.readValue(responseBody.getBytes(), SerializedQuotation.class);

        // Cleanup
        serializedQuotationService.delete(returnedObject.getId());
    }

    @Test
    public void testPOSTAuthenticatedAdmin() throws Exception {

        SerializedQuotation testSerializedQuotation = DomainObjectFactory.createSerializedQuotation();
        testSerializedQuotation.setId(0);
        testSerializedQuotation.setQuotation("updated");

        MvcResult result = mockMvc
                .perform(post("/records/quotation").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(testSerializedQuotation)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.quotation", is("updated"))).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        SerializedQuotation returnedObject = jsonMapper.readValue(responseBody.getBytes(), SerializedQuotation.class);

        // Cleanup
        serializedQuotationService.delete(returnedObject.getId());

    }

    @Test
    public void testPOSTNotAuthenticated() throws Exception {

        SerializedQuotation testSerializedQuotation = DomainObjectFactory.createSerializedQuotation();
        testSerializedQuotation.setId(0);

        mockMvc.perform(post("/records/quotation").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(testSerializedQuotation))).andExpect(status().is3xxRedirection());
    }

    @Test
    public void testDELETEWithAuthenticatedUser() throws Exception {

        SerializedQuotation testSerializedQuotation = DomainObjectFactory.createSerializedQuotation();
        testSerializedQuotation.setId(0);
        testSerializedQuotation = serializedQuotationService.create(testSerializedQuotation);

        mockMvc.perform(delete("/records/quotation/" + testSerializedQuotation.getId()).header("X-AUTH-TOKEN",
                getUserAuthToken(mockMvc))).andExpect(status().isOk());
    }

    @Test
    public void testDELETEAuthenticatedAdmin() throws Exception {

        SerializedQuotation testSerializedQuotation = DomainObjectFactory.createSerializedQuotation();
        testSerializedQuotation.setId(0);
        testSerializedQuotation = serializedQuotationService.create(testSerializedQuotation);

        mockMvc.perform(delete("/records/quotation/" + testSerializedQuotation.getId()).header("X-AUTH-TOKEN",
                getAdminAuthToken(mockMvc))).andExpect(status().isOk());
    }

    @Test
    public void testDELETENotAuthenticated() throws Exception {

        SerializedQuotation testSerializedQuotation = DomainObjectFactory.createSerializedQuotation();
        testSerializedQuotation.setId(0);
        testSerializedQuotation = serializedQuotationService.create(testSerializedQuotation);

        mockMvc.perform(delete("/records/quotation/" + testSerializedQuotation.getId()))
                .andExpect(status().is3xxRedirection());

        // Cleanup
        serializedQuotationService.delete(testSerializedQuotation);
    }

    @Test
    public void testCountWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/records/quotation/count").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testCountAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/records/quotation/count").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testCountNotAuthenticated() throws Exception {

        mockMvc.perform(get("/records/quotation/count")).andExpect(status().is3xxRedirection());
    }

    @Test
    public void testGETAllDeserializedWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/records/quotation/deserialized").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void testGETAllDeserializedAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/records/quotation/deserialized").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

    }

    @Test
    public void testGETAllDeserializedNotAuthenticated() throws Exception {

        mockMvc.perform(get("/records/quotation/deserialized")).andExpect(status().is3xxRedirection());

    }

}
