package hk.com.ahlopm.productmanager.test.security.controller.user;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import hk.com.ahlopm.productmanager.domainobject.user.User;
import hk.com.ahlopm.productmanager.domainobject.user.UserRole;
import hk.com.ahlopm.productmanager.service.user.UserService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SecurityAwareSpringTest;

import javax.servlet.Filter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

public class UserControllerSecurityTest extends SecurityAwareSpringTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ObjectMapper jsonMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private Filter springSecurityFilterChain;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilter(springSecurityFilterChain)
                .build();
    }

    @After
    public void tearDown() {

    }

    @Test
    public void testGETWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/user/1").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testGETAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/user/1").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(jsonPath("$.id", is(1)));

    }

    @Test
    public void testGETNotAuthenticated() throws Exception {

        mockMvc.perform(get("/user/1")).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testGETAllWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/user").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testGETAllAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/user").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

    }

    @Test
    public void testGETAllNotAuthenticated() throws Exception {

        mockMvc.perform(get("/user")).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testPUTWithAuthenticatedUserSameUser() throws Exception {

        User testUser = DomainObjectFactory.createUser();
        testUser.setId(0);

        UserRole role = DomainObjectFactory.createUserRole();
        role.setId(0);
        role.setRole("ROLE_USER");
        role.setUser(testUser);
        testUser.getRoles().add(role);

        String rawPassword = testUser.getPassword();
        testUser = userService.create(testUser);

        testUser.setPassword(rawPassword);

        String tokenForUser = login(mockMvc, testUser).andReturn().getResponse().getHeader("X-AUTH-TOKEN");

        mockMvc.perform(put("/user").header("X-AUTH-TOKEN", tokenForUser).contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(testUser))).andExpect(status().isOk());

        // Cleanup
        userService.delete(testUser);
    }

    @Test
    public void testPUTWithAuthenticatedUserDifferentUser() throws Exception {

        User testUser = DomainObjectFactory.createUser();
        testUser.setId(0);
        testUser = userService.create(testUser);
        testUser.setEmail("updated@email.com");

        mockMvc.perform(put("/user").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc))
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(testUser)))
                .andExpect(status().is4xxClientError());

        // Cleanup
        userService.delete(testUser);
    }

    @Test
    public void testPUTAuthenticatedAdmin() throws Exception {

        User testUser = DomainObjectFactory.createUser();
        testUser.setId(0);
        testUser = userService.create(testUser);
        testUser.setEmail("updated@email.com");

        mockMvc.perform(put("/user").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(testUser)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.email", is("updated@email.com")));

        // Cleanup
        userService.delete(testUser);

    }

    @Test
    public void testPUTNotAuthenticated() throws Exception {

        User testUser = DomainObjectFactory.createUser();
        testUser.setId(0);
        testUser = userService.create(testUser);

        mockMvc.perform(put("/user").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(testUser))).andExpect(status().is3xxRedirection());

        // Cleanup
        userService.delete(testUser);

    }

    @Test
    public void testPOSTWithAuthenticatedUser() throws Exception {

        User testUser = DomainObjectFactory.createUser();
        testUser.setId(0);
        testUser.setEmail("updated@email.com");

        mockMvc.perform(post("/user").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc))
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(testUser)))
                .andExpect(status().is4xxClientError());

    }

    @Test
    public void testPOSTAuthenticatedAdmin() throws Exception {

        User testUser = DomainObjectFactory.createUser();
        testUser.setId(0);
        testUser.setEmail("updated@email.com");

        MvcResult result = mockMvc
                .perform(post("/user").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(testUser)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.email", is("updated@email.com"))).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        User returnedObject = jsonMapper.readValue(responseBody.getBytes(), User.class);

        // Cleanup
        userService.delete(returnedObject.getId());

    }

    @Test
    public void testPOSTNotAuthenticated() throws Exception {

        User testUser = DomainObjectFactory.createUser();
        testUser.setId(0);

        mockMvc.perform(post("/user").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(testUser))).andExpect(status().is3xxRedirection());
    }

    @Test
    public void testDELETEWithAuthenticatedUser() throws Exception {

        User testUser = DomainObjectFactory.createUser();
        testUser.setId(0);
        testUser = userService.create(testUser);

        mockMvc.perform(delete("/user/" + testUser.getId()).header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().is4xxClientError());

        userService.delete(testUser);
    }

    @Test
    public void testDELETEAuthenticatedAdmin() throws Exception {

        User testUser = DomainObjectFactory.createUser();
        testUser.setId(0);
        testUser = userService.create(testUser);

        mockMvc.perform(delete("/user/" + testUser.getId()).header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testDELETENotAuthenticated() throws Exception {

        User testUser = DomainObjectFactory.createUser();
        testUser.setId(0);
        testUser = userService.create(testUser);

        mockMvc.perform(delete("/user/" + testUser.getId())).andExpect(status().is3xxRedirection());

        // Cleanup
        userService.delete(testUser);
    }

    @Test
    public void testCountWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/user/count").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testCountAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/user/count").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testCountNotAuthenticated() throws Exception {

        mockMvc.perform(get("/user/count")).andExpect(status().is3xxRedirection());
    }

    @Test
    public void testUpdateUserPasswordAsUser() throws Exception {

        User testUser = DomainObjectFactory.createUser();
        testUser.setId(0);
        testUser.setPassword("1");
        testUser = userService.create(testUser);

        String[] passwords = { "1", "2" };

        mockMvc.perform(put("/user/" + testUser.getId() + "/password").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc))
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(passwords)))
                .andExpect(status().isOk());

        // Cleanup
        userService.delete(testUser);
    }

    @Test
    public void testUpdateUserPasswordAsAdmin() throws Exception {

        User testUser = DomainObjectFactory.createUser();
        testUser.setId(0);
        testUser.setPassword("1");
        testUser = userService.create(testUser);

        String[] passwords = { "1", "2" };

        mockMvc.perform(
                put("/user/" + testUser.getId() + "/password").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(passwords)))
                .andExpect(status().isOk());

        // Cleanup
        userService.delete(testUser);
    }

    @Test
    public void testUpdateUserPasswordAsNoAuth() throws Exception {

        User testUser = DomainObjectFactory.createUser();
        testUser.setId(0);
        testUser.setPassword("1");
        testUser = userService.create(testUser);

        String[] passwords = { "1", "2" };

        mockMvc.perform(put("/user/" + testUser.getId() + "/password").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(passwords))).andExpect(status().is3xxRedirection());

        // Cleanup
        userService.delete(testUser);
    }

    @Test
    public void testUpdateUserPasswordForceAsUser() throws Exception {

        User testUser = DomainObjectFactory.createUser();
        testUser.setId(0);
        testUser.setPassword("1");
        testUser = userService.create(testUser);

        String password = "2";

        mockMvc.perform(
                put("/user/" + testUser.getId() + "/password/force").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc))
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsString(password)))
                .andExpect(status().is4xxClientError());

        // Cleanup
        userService.delete(testUser);
    }

    @Test
    public void testUpdateUserPasswordForceAsAdmin() throws Exception {

        User testUser = DomainObjectFactory.createUser();
        testUser.setId(0);
        testUser.setPassword("1");
        testUser = userService.create(testUser);

        String password = "2";

        mockMvc.perform(
                put("/user/" + testUser.getId() + "/password/force").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsString(password)))
                .andExpect(status().isOk());

        // Cleanup
        userService.delete(testUser);
    }

    @Test
    public void testUpdateUserPasswordForceAsNoAuth() throws Exception {

        User testUser = DomainObjectFactory.createUser();
        testUser.setId(0);
        testUser.setPassword("1");
        testUser = userService.create(testUser);

        String password = "2";

        mockMvc.perform(put("/user/" + testUser.getId() + "/password/force")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsString(password)))
                .andExpect(status().is3xxRedirection());

        // Cleanup
        userService.delete(testUser);
    }

    @Test
    public void getCurrentUserAsUser() throws Exception {

        MvcResult result = mockMvc.perform(get("/account").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk()).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        User returnedObject = jsonMapper.readValue(responseBody.getBytes(), User.class);

        assertNotNull(returnedObject);
    }

    @Test
    public void getCurrentUserAsAdmin() throws Exception {

        MvcResult result = mockMvc.perform(get("/account").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk()).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        User returnedObject = jsonMapper.readValue(responseBody.getBytes(), User.class);

        assertNotNull(returnedObject);
    }

    @Test
    public void getCurrentUserAsNoAuth() throws Exception {

        mockMvc.perform(get("/account")).andExpect(status().is3xxRedirection());
    }

}
