package hk.com.ahlopm.productmanager.test.security.controller.quoteproduct;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;
import hk.com.ahlopm.productmanager.service.product.ProductService;
import hk.com.ahlopm.productmanager.service.quotation.QuotationService;
import hk.com.ahlopm.productmanager.service.quoteproduct.QuoteProductService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SecurityAwareSpringTest;

import javax.servlet.Filter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

public class QuoteProductControllerSecurityTest extends SecurityAwareSpringTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ObjectMapper jsonMapper;

    @Autowired
    private QuoteProductService quoteproductService;

    @Autowired
    private Filter springSecurityFilterChain;

    @Autowired
    private ProductService productService;

    @Autowired
    private QuotationService quotationService;

    @Before
    public void setUp() {

        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).addFilter(springSecurityFilterChain)
                .build();

    }

    @After
    public void tearDown() {

    }

    @Test
    public void testGETWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/quoteproduct/1").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(1)));
    }

    @Test
    public void testGETAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/quoteproduct/1").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(1)));

    }

    @Test
    public void testGETNotAuthenticated() throws Exception {

        mockMvc.perform(get("/quoteproduct/1")).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testGETAllWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/quoteproduct").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void testGETAllAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/quoteproduct").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

    }

    @Test
    public void testGETAllNotAuthenticated() throws Exception {

        mockMvc.perform(get("/quoteproduct")).andExpect(status().is3xxRedirection());

    }

    @Test
    public void testPUTWithAuthenticatedUser() throws Exception {

        QuoteProduct testQuoteProduct = DomainObjectFactory.createQuoteProduct();
        testQuoteProduct.setId(0);
        testQuoteProduct = quoteproductService.create(testQuoteProduct);
        testQuoteProduct.setQuantity(1);

        mockMvc.perform(put("/quoteproduct").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc))
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(testQuoteProduct)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.quantity", is(1)));

        // Cleanup
        quoteproductService.delete(testQuoteProduct);
    }

    @Test
    public void testPUTAuthenticatedAdmin() throws Exception {

        QuoteProduct testQuoteProduct = DomainObjectFactory.createQuoteProduct();
        testQuoteProduct.setId(0);
        testQuoteProduct = quoteproductService.create(testQuoteProduct);
        testQuoteProduct.setQuantity(1);

        mockMvc.perform(put("/quoteproduct").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(testQuoteProduct)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.quantity", is(1)));

        // Cleanup
        quoteproductService.delete(testQuoteProduct);

    }

    @Test
    public void testPUTNotAuthenticated() throws Exception {

        QuoteProduct testQuoteProduct = DomainObjectFactory.createQuoteProduct();
        testQuoteProduct.setId(0);
        testQuoteProduct = quoteproductService.create(testQuoteProduct);

        mockMvc.perform(put("/quoteproduct").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(testQuoteProduct))).andExpect(status().is3xxRedirection());

        // Cleanup
        quoteproductService.delete(testQuoteProduct);

    }

    @Test
    public void testPOSTWithAuthenticatedUser() throws Exception {

        QuoteProduct testQuoteProduct = DomainObjectFactory.createQuoteProduct();
        testQuoteProduct.setId(0);
        testQuoteProduct.setQuantity(1);

        MvcResult result = mockMvc
                .perform(post("/quoteproduct").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(testQuoteProduct)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.quantity", is(1))).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        QuoteProduct returnedObject = jsonMapper.readValue(responseBody.getBytes(), QuoteProduct.class);

        // Cleanup
        quoteproductService.delete(returnedObject.getId());
    }

    @Test
    public void testPOSTAuthenticatedAdmin() throws Exception {

        QuoteProduct testQuoteProduct = DomainObjectFactory.createQuoteProduct();
        testQuoteProduct.setId(0);
        testQuoteProduct.setQuantity(1);

        MvcResult result = mockMvc
                .perform(post("/quoteproduct").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonMapper.writeValueAsBytes(testQuoteProduct)))
                .andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.quantity", is(1))).andReturn();

        String responseBody = result.getResponse().getContentAsString();

        QuoteProduct returnedObject = jsonMapper.readValue(responseBody.getBytes(), QuoteProduct.class);

        // Cleanup
        quoteproductService.delete(returnedObject.getId());

    }

    @Test
    public void testPOSTNotAuthenticated() throws Exception {

        QuoteProduct testQuoteProduct = DomainObjectFactory.createQuoteProduct();
        testQuoteProduct.setId(0);

        mockMvc.perform(post("/quoteproduct").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(jsonMapper.writeValueAsBytes(testQuoteProduct))).andExpect(status().is3xxRedirection());
    }

    @Test
    public void testDELETEWithAuthenticatedUser() throws Exception {

        QuoteProduct testQuoteProduct = DomainObjectFactory.createQuoteProduct();
        testQuoteProduct.setId(0);
        testQuoteProduct = quoteproductService.create(testQuoteProduct);

        mockMvc.perform(
                delete("/quoteproduct/" + testQuoteProduct.getId()).header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testDELETEAuthenticatedAdmin() throws Exception {

        QuoteProduct testQuoteProduct = DomainObjectFactory.createQuoteProduct();
        testQuoteProduct.setId(0);
        testQuoteProduct = quoteproductService.create(testQuoteProduct);

        mockMvc.perform(
                delete("/quoteproduct/" + testQuoteProduct.getId()).header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testDELETENotAuthenticated() throws Exception {

        QuoteProduct testQuoteProduct = DomainObjectFactory.createQuoteProduct();
        testQuoteProduct.setId(0);
        testQuoteProduct = quoteproductService.create(testQuoteProduct);

        mockMvc.perform(delete("/quoteproduct/" + testQuoteProduct.getId())).andExpect(status().is3xxRedirection());

        // Cleanup
        quoteproductService.delete(testQuoteProduct);
    }

    @Test
    public void testCountWithAuthenticatedUser() throws Exception {

        mockMvc.perform(get("/quoteproduct/count").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testCountAuthenticatedAdmin() throws Exception {

        mockMvc.perform(get("/quoteproduct/count").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc)))
                .andExpect(status().isOk());
    }

    @Test
    public void testCountNotAuthenticated() throws Exception {

        mockMvc.perform(get("/quoteproduct/count")).andExpect(status().is3xxRedirection());
    }

    @Test
    public void testPUTProductsWithAuthenticatedUser() throws Exception {

        Quotation quote = DomainObjectFactory.createQuotation();
        quote.setId(0);
        quotationService.create(quote);
        Product prod1 = DomainObjectFactory.createProduct();
        prod1.setId(0);
        productService.create(prod1);
        Product prod2 = DomainObjectFactory.createProduct();
        prod2.setId(0);
        productService.create(prod2);
        long[] products = { prod1.getId(), prod2.getId() };
        mockMvc.perform(
                put("/quotation/" + quote.getId() + "/quoteproducts").header("X-AUTH-TOKEN", getUserAuthToken(mockMvc))
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(products)))
                .andExpect(status().isOk());

        quotationService.delete(quote);
    }

    @Test
    public void testPUTProductsAuthenticatedAdmin() throws Exception {

        Quotation quote = DomainObjectFactory.createQuotation();
        quote.setId(0);
        quotationService.create(quote);
        Product prod1 = DomainObjectFactory.createProduct();
        prod1.setId(0);
        productService.create(prod1);
        Product prod2 = DomainObjectFactory.createProduct();
        prod2.setId(0);
        productService.create(prod2);
        long[] products = { prod1.getId(), prod2.getId() };
        mockMvc.perform(
                put("/quotation/" + quote.getId() + "/quoteproducts").header("X-AUTH-TOKEN", getAdminAuthToken(mockMvc))
                        .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(products)))
                .andExpect(status().isOk());

        quotationService.delete(quote);
    }

    @Test
    public void testPUTProductsNotAuthenticated() throws Exception {

        Quotation quote = DomainObjectFactory.createQuotation();
        quote.setId(0);
        quotationService.create(quote);
        Product prod1 = DomainObjectFactory.createProduct();
        prod1.setId(0);
        productService.create(prod1);
        Product prod2 = DomainObjectFactory.createProduct();
        prod2.setId(0);
        productService.create(prod2);
        long[] products = { prod1.getId(), prod2.getId() };
        mockMvc.perform(put("/quotation/" + quote.getId() + "/quoteproducts")
                .contentType(MediaType.APPLICATION_JSON_UTF8).content(jsonMapper.writeValueAsBytes(products)))
                .andExpect(status().is3xxRedirection());

        quotationService.delete(quote);
    }

}
