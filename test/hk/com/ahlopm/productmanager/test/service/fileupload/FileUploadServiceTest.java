package hk.com.ahlopm.productmanager.test.service.fileupload;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import hk.com.ahlopm.productmanager.amazons3.AmazonS3Gateway;
import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.domainobject.productpicture.ProductPicture;
import hk.com.ahlopm.productmanager.exception.FileUploadException;
import hk.com.ahlopm.productmanager.exception.ObjectNotFoundException;
import hk.com.ahlopm.productmanager.service.fileupload.FileUploadService;
import hk.com.ahlopm.productmanager.service.product.ProductService;
import hk.com.ahlopm.productmanager.service.productpicture.ProductPictureService;
import hk.com.ahlopm.productmanager.test.common.SpringTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import com.amazonaws.services.s3.AmazonS3;

public class FileUploadServiceTest extends SpringTest {

    @Autowired
    private ProductService productService;

    @Autowired
    private FileUploadService fileUploadService;

    @Autowired
    private ProductPictureService productPictureService;

    private MockMultipartFile uploadedFile;

    @Autowired
    private AmazonS3Gateway gateway;

    @Mock
    private AmazonS3 s3Client;

    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);

        when(s3Client.putObject(any())).thenReturn(null);

        ReflectionTestUtils.setField(gateway, "s3Client", s3Client); // Allow
                                                                     // mock
                                                                     // returns

    }

    @After
    public void tearDown() throws Exception {

        ReflectionTestUtils.setField(gateway, "s3Client", null); // Repair
                                                                 // context
    }

    @Test
    @Transactional
    public void testFile1Product0Image() throws IOException {

        File testFile = new ClassPathResource("xls/valid1Product0Image.xlsx").getFile();
        InputStream stream = new FileInputStream(testFile);
        uploadedFile = new MockMultipartFile("file", stream);

        long countBefore = productService.count();
        fileUploadService.parseProductsXLSFile(uploadedFile);
        long countAfter = productService.count();

        assertEquals("There should be 1 additional product.", 1, countAfter - countBefore);
    }

    @Test
    @Transactional
    public void testFile1Product1Image() throws IOException {

        File testFile = new ClassPathResource("xls/valid1Product1Image.xlsx").getFile();
        InputStream stream = new FileInputStream(testFile);
        uploadedFile = new MockMultipartFile("file", stream);

        long countBefore = productService.count();
        long countBeforePic = productPictureService.count();
        fileUploadService.parseProductsXLSFile(uploadedFile);
        long countAfter = productService.count();
        long countAfterPic = productPictureService.count();

        assertEquals("There should be 1 additional product.", 1, countAfter - countBefore);
        assertEquals("There should be 1 additional picture.", 1, countAfterPic - countBeforePic);

        List<Product> products = productService.findAll();
        Product lastProduct = products.get(products.size() - 1); // Latest added
                                                                 // product
        List<ProductPicture> pictures = productPictureService.findAll();
        ProductPicture lastPicture = pictures.get(pictures.size() - 1); // Latest
                                                                        // added
                                                                        // picture

        assertEquals("The last picture should reference the last product.", lastPicture.getProduct().getId(),
                lastProduct.getId());

        assertEquals("The last product should reference the last picture.", lastProduct.getPrimaryPicture().getId(),
                lastPicture.getId());
    }

    @Test(expected = FileUploadException.class)
    @Transactional
    public void testFileCorruptColumns() throws IOException {

        File testFile = new ClassPathResource("xls/invalid1ProductMissingImage.xlsx").getFile();
        InputStream stream = new FileInputStream(testFile);
        uploadedFile = new MockMultipartFile("file", stream);

        fileUploadService.parseProductsXLSFile(uploadedFile);
    }

    @Test
    @Transactional
    public void testImageUpload() throws IOException {

        File testFile = new ClassPathResource("images/validImage.jpg").getFile();
        InputStream stream = new FileInputStream(testFile);
        uploadedFile = new MockMultipartFile("file", "validImage.jpg", "image/jpeg", stream);

        long countBefore = productPictureService.count();
        fileUploadService.uploadProductImage(1, uploadedFile);
        long countAfter = productPictureService.count();

        assertEquals("There should be 1 additional picture.", 1, countAfter - countBefore);
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testImageUploadNoProduct() throws IOException {

        File testFile = new ClassPathResource("images/validImage.jpg").getFile();
        InputStream stream = new FileInputStream(testFile);
        uploadedFile = new MockMultipartFile("file", "validImage.jpg", "image/jpeg", stream);

        fileUploadService.uploadProductImage(-1, uploadedFile);
    }

    @Test(expected = FileUploadException.class)
    @Transactional
    public void testImageUploadNotImage() throws IOException {

        File testFile = new ClassPathResource("images/notanimage.jpg").getFile();
        InputStream stream = new FileInputStream(testFile);
        uploadedFile = new MockMultipartFile("file", "notanimage.jpg", "text/plain", stream);

        fileUploadService.uploadProductImage(1, uploadedFile);
    }
}
