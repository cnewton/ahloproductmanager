package hk.com.ahlopm.productmanager.test.service.productpicture;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import hk.com.ahlopm.productmanager.amazons3.AmazonS3Gateway;
import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.domainobject.productpicture.ProductPicture;
import hk.com.ahlopm.productmanager.domainobject.productpicture.S3ProductPicture;
import hk.com.ahlopm.productmanager.exception.NotCreateOperationException;
import hk.com.ahlopm.productmanager.exception.NotUpdateOperationException;
import hk.com.ahlopm.productmanager.exception.ObjectNotFoundException;
import hk.com.ahlopm.productmanager.exception.PictureMemberException;
import hk.com.ahlopm.productmanager.service.product.ProductService;
import hk.com.ahlopm.productmanager.service.productpicture.ProductPictureService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;
import hk.com.ahlopm.productmanager.test.service.DomainObjectServiceTest;
import hk.com.ahlopm.productmanager.test.service.DomainObjectServiceTester;

import java.io.ByteArrayInputStream;
import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.transaction.annotation.Transactional;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;

public class ProductPictureServiceTest extends SpringTest implements DomainObjectServiceTest {

    @Autowired
    private ProductPictureService productPictureService;

    @Autowired
    private ProductService productService;

    @Autowired
    private DomainObjectServiceTester<ProductPicture> tester;

    private ProductPicture testProductPicture;

    @Autowired
    private AmazonS3Gateway gateway;

    @Mock
    private AmazonS3 s3Client;

    private byte[] content = { 1, 0, 1, 0 };

    @Before
    public void setUp() throws Exception {

        testProductPicture = DomainObjectFactory.createProductPicture();
        tester.setService(productPictureService);
        tester.setTestObject(testProductPicture);

        MockitoAnnotations.initMocks(this);

        S3Object s3obj = new S3Object();
        s3obj.setObjectContent(new ByteArrayInputStream(content));
        when(s3Client.getObject(any())).thenReturn(s3obj);
        when(s3Client.putObject(any())).thenReturn(null);

        ReflectionTestUtils.setField(gateway, "s3Client", s3Client); // Allow
                                                                     // mock
                                                                     // returns
    }

    @After
    public void tearDown() throws Exception {

        ReflectionTestUtils.setField(gateway, "s3Client", null); // Repair
                                                                 // context
    }

    @Test
    @Transactional
    public void testFind() {

        tester.testFind();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testFindNonExistant() {

        tester.testFindNonExistant();
    }

    @Test
    @Transactional
    public void testFindAll() {

        tester.testFindAll();
    }

    @Test
    @Transactional
    public void testCreate() {

        testProductPicture.setId(0);
        tester.testCreate();
    }

    @Test(expected = NotCreateOperationException.class)
    @Transactional
    public void testCreateNotCreateOperation() {

        tester.testCreateNotCreateOperation();
    }

    @Test
    @Transactional
    public void testUpdate() {

        testProductPicture = productPictureService.find(1);
        testProductPicture.setEncodeType("TestUpdate");
        tester.setTestObject(testProductPicture);

        tester.testUpdate();

        assertEquals("The names do not match", "TestUpdate", testProductPicture.getEncodeType());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testUpdateNonExistant() {

        tester.testUpdateNonExistant();
    }

    @Test(expected = NotUpdateOperationException.class)
    @Transactional
    public void testUpdateNotUpdateOperation() {

        tester.testUpdateNotUpdateOperation();
    }

    @Test
    @Transactional
    public void testDeleteByObject() {

        testProductPicture.setId(0); // Create object to test deletion.
        productPictureService.create(testProductPicture);

        tester.testDeleteByObject();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeleteNonExistantByObject() {

        tester.testDeleteNonExistantByObject();
    }

    @Test
    @Transactional
    public void testDeleteById() {

        tester.testDeleteById();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeleteNonExistantById() {

        tester.testDeleteNonExistantById();
    }

    @Test
    @Transactional
    public void testCount() {

        tester.testCount();
    }

    @Test
    @Transactional
    public void testSetPrimaryPictureForProduct() throws PictureMemberException {

        ProductPicture pic = DomainObjectFactory.createProductPicture();
        pic.setId(0);
        pic.setProduct(productService.find(1));
        productPictureService.create(pic);

        productPictureService.setPrimaryPictureForProduct(pic.getId(), pic.getProduct().getId());

        assertEquals("The primary picture should be the new picture.", pic, productService.find(1).getPrimaryPicture());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testSetPrimaryPictureForNonExistantProduct() throws PictureMemberException {

        ProductPicture pic = DomainObjectFactory.createProductPicture();
        pic.setId(0);
        pic.setProduct(productService.find(1));
        productPictureService.create(pic);

        productPictureService.setPrimaryPictureForProduct(pic.getId(), -1);
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testSetNonExistantPrimaryPictureForProduct() throws PictureMemberException {

        productPictureService.setPrimaryPictureForProduct(-1, 1);
    }

    @Test(expected = PictureMemberException.class)
    @Transactional
    public void testSetPrimaryPictureForProductNotContainingPicture() throws PictureMemberException {

        ProductPicture pic = DomainObjectFactory.createProductPicture();
        pic.setId(0);
        pic.setProduct(productService.find(2));
        productPictureService.create(pic);

        productPictureService.setPrimaryPictureForProduct(pic.getId(), 1);
    }

    @Test
    @Transactional
    public void testGetResolvedS3Picture() {

        S3ProductPicture pic = DomainObjectFactory.createS3ProductPicture();
        pic.setId(0);
        pic.setProduct(productService.find(1));
        productPictureService.create(pic);

        byte[] result = productPictureService.getResolvedPicture(pic.getId());

        assertEquals("The resolved image should be equal to the original content", true,
                Arrays.equals(content, result));
    }

    @Test
    @Transactional
    public void testGetResolvedNonS3Picture() {

        ProductPicture pic = DomainObjectFactory.createProductPicture();
        pic.setId(0);
        pic.setProduct(productService.find(1));
        productPictureService.create(pic);

        byte[] result = productPictureService.getResolvedPicture(pic.getId());

        assertEquals("The resolved image should be null as it has no resolution mechanism.", true,
                Arrays.equals(new byte[0], result));
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testGetResolvedNonExistantPicture() {

        productPictureService.getResolvedPicture(-1);
    }

    @Test(expected = PictureMemberException.class)
    @Transactional
    public void testCreateS3ProductPictureNoProduct() throws PictureMemberException {

        S3ProductPicture pic = DomainObjectFactory.createS3ProductPicture();
        pic.setProduct(null);
        productPictureService.constructS3ProductPicture(pic);
    }

    @Test(expected = PictureMemberException.class)
    @Transactional
    public void testCreateS3ProductPictureProductIdZero() throws PictureMemberException {

        S3ProductPicture pic = DomainObjectFactory.createS3ProductPicture();
        pic.setProduct(DomainObjectFactory.createProduct());
        pic.getProduct().setId(0);
        productPictureService.constructS3ProductPicture(pic);
    }

    @Test(expected = PictureMemberException.class)
    @Transactional
    public void testCreateS3ProductPictureNoData() throws PictureMemberException {

        S3ProductPicture pic = DomainObjectFactory.createS3ProductPicture();
        pic.setProduct(DomainObjectFactory.createProduct());
        pic.getProduct().setId(1);
        pic.setPicture(null);
        productPictureService.constructS3ProductPicture(pic);
    }

    @Test(expected = PictureMemberException.class)
    @Transactional
    public void testCreateS3ProductPictureNoBucket() throws PictureMemberException {

        S3ProductPicture pic = DomainObjectFactory.createS3ProductPicture();
        pic.setProduct(DomainObjectFactory.createProduct());
        pic.getProduct().setId(1);
        pic.setPicture(new byte[] { 1, 2, 3 });
        pic.setS3Bucket(null);
        productPictureService.constructS3ProductPicture(pic);
    }

    @Test(expected = NotCreateOperationException.class)
    @Transactional
    public void testCreateS3ProductPictureIdNonZero() throws PictureMemberException {

        S3ProductPicture pic = DomainObjectFactory.createS3ProductPicture();
        pic.setProduct(DomainObjectFactory.createProduct());
        pic.getProduct().setId(1);
        pic.setPicture(new byte[] { 1, 2, 3 });
        pic.setId(1);
        productPictureService.constructS3ProductPicture(pic);
    }

    @Test
    @Transactional
    public void testCreateS3ProductPicture() throws PictureMemberException {

        S3ProductPicture pic = DomainObjectFactory.createS3ProductPicture();
        pic.setProduct(DomainObjectFactory.createProduct());
        pic.getProduct().setId(1);
        pic.setPicture(new byte[] { 1, 2, 3 });
        pic.setId(0);
        pic.setS3Bucket("test");
        productPictureService.constructS3ProductPicture(pic);
        assertEquals("The picture should be created.", pic, productPictureService.find(pic.getId()));
    }

    @Test
    @Transactional
    public void testCreateS3ProductPictureNoPrimary() throws PictureMemberException {

        S3ProductPicture pic = DomainObjectFactory.createS3ProductPicture();
        pic.setProduct(DomainObjectFactory.createProduct());
        pic.getProduct().setId(1);
        pic.getProduct().setPrimaryPicture(null);
        pic.setPicture(new byte[] { 1, 2, 3 });
        pic.setId(0);
        pic.setS3Bucket("test");
        productPictureService.constructS3ProductPicture(pic);
        assertEquals("The picture should be created.", pic, productPictureService.find(pic.getId()));
        assertEquals("The products primary picture should now be set.", pic, pic.getProduct().getPrimaryPicture());
    }

    @Test
    @Transactional
    public void testCreateS3ProductPictureWithPrimary() throws PictureMemberException {

        S3ProductPicture pic = DomainObjectFactory.createS3ProductPicture();
        S3ProductPicture primPic = DomainObjectFactory.createS3ProductPicture();
        pic.setProduct(DomainObjectFactory.createProduct());
        pic.getProduct().setId(1);
        pic.getProduct().setPrimaryPicture(primPic);
        pic.setPicture(new byte[] { 1, 2, 3 });
        pic.setId(0);
        pic.setS3Bucket("test");
        productPictureService.constructS3ProductPicture(pic);
        assertEquals("The picture should be created.", pic, productPictureService.find(pic.getId()));
        assertEquals("The products primary picture should not be changed.", primPic,
                pic.getProduct().getPrimaryPicture());
    }

    @Test
    @Transactional
    public void testPrimaryPictureIntegrity() {

        Product product = DomainObjectFactory.createProduct();
        product.setId(0);
        productService.create(product);

        ProductPicture pic1 = DomainObjectFactory.createProductPicture();
        pic1.setId(0);
        pic1.setProduct(product);
        product.getPictures().add(pic1);
        productPictureService.create(pic1);

        ProductPicture pic2 = DomainObjectFactory.createProductPicture();
        pic2.setId(0);
        pic2.setProduct(product);
        product.getPictures().add(pic2);
        productPictureService.create(pic2);

        product.setPrimaryPicture(pic1);
        productService.update(product);

        assertEquals("The primary picture should be pic1.", pic1, product.getPrimaryPicture());

        productPictureService.delete(pic1);

        assertEquals("The primary picture should have changed to pic2.", pic2, product.getPrimaryPicture());

    }

    @Test
    @Transactional
    public void testPrimaryPictureIntegrityNoOtherPictures() {

        Product product = DomainObjectFactory.createProduct();
        product.setId(0);
        productService.create(product);

        ProductPicture pic1 = DomainObjectFactory.createProductPicture();
        pic1.setId(0);
        pic1.setProduct(product);
        product.getPictures().add(pic1);
        productPictureService.create(pic1);

        product.setPrimaryPicture(pic1);
        productService.update(product);

        assertEquals("The primary picture should be pic1.", pic1, product.getPrimaryPicture());

        productPictureService.delete(pic1);

        assertEquals("The primary picture should have changed to null.", null, product.getPrimaryPicture());

    }

    @Test
    @Transactional
    public void testPrimaryPictureIntegrityNotPrimary() {

        Product product = DomainObjectFactory.createProduct();
        product.setId(0);
        productService.create(product);

        ProductPicture pic1 = DomainObjectFactory.createProductPicture();
        pic1.setId(0);
        pic1.setProduct(product);
        product.getPictures().add(pic1);
        productPictureService.create(pic1);

        ProductPicture pic2 = DomainObjectFactory.createProductPicture();
        pic2.setId(0);
        pic2.setProduct(product);
        product.getPictures().add(pic2);
        productPictureService.create(pic2);

        product.setPrimaryPicture(pic2);
        productService.update(product);

        assertEquals("The primary picture should be pic2.", pic2, product.getPrimaryPicture());

        productPictureService.delete(pic1);

        assertEquals("The primary picture should still be pic2.", pic2, product.getPrimaryPicture());

    }
}
