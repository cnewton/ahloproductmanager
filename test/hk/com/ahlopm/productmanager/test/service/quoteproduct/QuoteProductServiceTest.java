package hk.com.ahlopm.productmanager.test.service.quoteproduct;

import static org.junit.Assert.assertEquals;
import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;
import hk.com.ahlopm.productmanager.exception.NotCreateOperationException;
import hk.com.ahlopm.productmanager.exception.NotUpdateOperationException;
import hk.com.ahlopm.productmanager.exception.ObjectNotFoundException;
import hk.com.ahlopm.productmanager.service.product.ProductService;
import hk.com.ahlopm.productmanager.service.quotation.QuotationService;
import hk.com.ahlopm.productmanager.service.quoteproduct.QuoteProductService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;
import hk.com.ahlopm.productmanager.test.service.DomainObjectServiceTest;
import hk.com.ahlopm.productmanager.test.service.DomainObjectServiceTester;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class QuoteProductServiceTest extends SpringTest implements DomainObjectServiceTest {

    @Autowired
    private QuoteProductService quoteProductService;

    @Autowired
    private QuotationService quotationService;

    @Autowired
    private ProductService productService;

    @Autowired
    private DomainObjectServiceTester<QuoteProduct> tester;

    private QuoteProduct testQuoteProduct;

    @Before
    public void setUp() throws Exception {

        testQuoteProduct = DomainObjectFactory.createQuoteProduct();
        tester.setService(quoteProductService);
        tester.setTestObject(testQuoteProduct);
    }

    @After
    public void tearDown() throws Exception {

        // DB gets cleaned up automatically, no work to do here.
    }

    @Test
    @Transactional
    public void testFind() {

        tester.testFind();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testFindNonExistant() {

        tester.testFindNonExistant();
    }

    @Test
    @Transactional
    public void testFindAll() {

        tester.testFindAll();
    }

    @Test
    @Transactional
    public void testCreate() {

        testQuoteProduct.setId(0);
        tester.testCreate();
    }

    @Test(expected = NotCreateOperationException.class)
    @Transactional
    public void testCreateNotCreateOperation() {

        tester.testCreateNotCreateOperation();
    }

    @Test
    @Transactional
    public void testUpdate() {

        testQuoteProduct = quoteProductService.find(1);
        testQuoteProduct.setQuantity(111);
        tester.setTestObject(testQuoteProduct);

        tester.testUpdate();

        assertEquals("The quantity did not match", 111, testQuoteProduct.getQuantity());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testUpdateNonExistant() {

        tester.testUpdateNonExistant();
    }

    @Test(expected = NotUpdateOperationException.class)
    @Transactional
    public void testUpdateNotUpdateOperation() {

        tester.testUpdateNotUpdateOperation();
    }

    @Test
    @Transactional
    public void testDeleteByObject() {

        testQuoteProduct.setId(0); // Create object to test deletion.
        quoteProductService.create(testQuoteProduct);

        tester.testDeleteByObject();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeleteNonExistantByObject() {

        tester.testDeleteNonExistantByObject();
    }

    @Test
    @Transactional
    public void testDeleteById() {

        tester.testDeleteById();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeleteNonExistantById() {

        tester.testDeleteNonExistantById();
    }

    @Test
    @Transactional
    public void testCount() {

        tester.testCount();
    }

    @Test
    @Transactional
    public void testAddProductToQuotation() {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);
        quotationService.create(testQuotation);

        Product testProduct = DomainObjectFactory.createProduct();
        testProduct.setId(0);
        productService.create(testProduct);

        long[] prodIds = { testProduct.getId() };
        quoteProductService.addProductsToQuotation(testQuotation.getId(), prodIds);

        testQuotation = quotationService.find(testQuotation.getId());

        assertEquals("The quotation did not contain a product.", 1, testQuotation.getProducts().size());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testAddProductToNonExistantQuotation() {

        Product testProduct = DomainObjectFactory.createProduct();
        testProduct.setId(0);
        productService.create(testProduct);

        long[] prodIds = { testProduct.getId() };
        quoteProductService.addProductsToQuotation(-1, prodIds);
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testAddNonExistantProductToQuotation() {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);
        quotationService.create(testQuotation);

        long[] prodIds = { -1 };
        quoteProductService.addProductsToQuotation(testQuotation.getId(), prodIds);
    }
}
