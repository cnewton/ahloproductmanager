package hk.com.ahlopm.productmanager.test.service.user;

import static org.junit.Assert.assertEquals;
import hk.com.ahlopm.productmanager.domainobject.user.User;
import hk.com.ahlopm.productmanager.exception.CustomSecurityException;
import hk.com.ahlopm.productmanager.exception.NotCreateOperationException;
import hk.com.ahlopm.productmanager.exception.NotUpdateOperationException;
import hk.com.ahlopm.productmanager.exception.ObjectNotFoundException;
import hk.com.ahlopm.productmanager.service.user.UserService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;
import hk.com.ahlopm.productmanager.test.service.DomainObjectServiceTest;
import hk.com.ahlopm.productmanager.test.service.DomainObjectServiceTester;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

public class UserServiceTest extends SpringTest implements DomainObjectServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private DomainObjectServiceTester<User> tester;

    private User testUser;

    @Before
    public void setUp() throws Exception {

        testUser = DomainObjectFactory.createUser();
        tester.setService(userService);
        tester.setTestObject(testUser);
    }

    @After
    public void tearDown() throws Exception {

        // DB gets cleaned up automatically, no work to do here.
    }

    @Test
    @Transactional
    public void testFind() {

        tester.testFind();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testFindNonExistant() {

        tester.testFindNonExistant();
    }

    @Test
    @Transactional
    public void testFindAll() {

        tester.testFindAll();
    }

    @Test
    @Transactional
    public void testCreate() {

        testUser.setId(0);
        tester.testCreate();
    }

    @Test(expected = NotCreateOperationException.class)
    @Transactional
    public void testCreateNotCreateOperation() {

        tester.testCreateNotCreateOperation();
    }

    @Test
    @Transactional
    public void testUpdate() {

        testUser = userService.find(1);
        testUser.setName("TestUpdate");
        tester.setTestObject(testUser);

        tester.testUpdate();

        assertEquals("The names do not match", "TestUpdate", testUser.getName());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testUpdateNonExistant() {

        tester.testUpdateNonExistant();
    }

    @Test(expected = NotUpdateOperationException.class)
    @Transactional
    public void testUpdateNotUpdateOperation() {

        tester.testUpdateNotUpdateOperation();
    }

    @Test
    @Transactional
    public void testDeleteByObject() {

        testUser.setId(0); // Create object to test deletion.
        userService.create(testUser);

        tester.testDeleteByObject();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeleteNonExistantByObject() {

        tester.testDeleteNonExistantByObject();
    }

    @Test
    @Transactional
    public void testDeleteById() {

        tester.testDeleteById();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeleteNonExistantById() {

        tester.testDeleteNonExistantById();
    }

    @Test
    @Transactional
    public void testCount() {

        tester.testCount();
    }

    @Test
    @Transactional
    public void testPasswordHashing() {

        testUser.setId(0);
        testUser.setPassword("password1");
        testUser = userService.create(testUser);

        assertEquals("The password should have been hashed.", true,
                passwordEncoder.matches("password1", testUser.getPassword()));
    }

    // Non transactional test to allow specific testing of update.
    // Must avoid making the changes via setter calls in this context.
    @Test
    public void testUpdateUserDetails() {

        testUser.setId(0);
        testUser.setPassword("1");
        testUser = userService.create(testUser);
        testUser.setUsername("Not Original");
        testUser.setPassword("Not Original");
        testUser = userService.updateUserDetails(testUser);

        assertEquals("The username should not have been changed by update.", false,
                testUser.getUsername().equals("Not Original"));

        assertEquals("The password should not have been changed by update.", false,
                passwordEncoder.matches("Not Original", testUser.getPassword()));

        // Cleanup non transactional
        userService.delete(testUser);

    }

    @Test
    @Transactional
    public void testUpdateUserPassword() {

        testUser.setId(0);
        testUser.setPassword("1");
        testUser = userService.create(testUser);

        userService.updateUserPassword(testUser.getId(), "1", "2");

        assertEquals("The users password should be updated.", true,
                passwordEncoder.matches("2", testUser.getPassword()));
    }

    @Test(expected = CustomSecurityException.class)
    @Transactional
    public void testUpdateUserPasswordWrongOldPassword() {

        testUser.setId(0);
        testUser.setPassword("1");
        testUser = userService.create(testUser);

        userService.updateUserPassword(testUser.getId(), "5", "2");
    }

    @Test
    @Transactional
    public void testUpdateUserPasswordForce() {

        testUser.setId(0);
        testUser.setPassword("1");
        testUser = userService.create(testUser);

        userService.forceUpdateUserPassword(testUser.getId(), "2");

        assertEquals("The users password should be updated.", true,
                passwordEncoder.matches("2", testUser.getPassword()));
    }
}
