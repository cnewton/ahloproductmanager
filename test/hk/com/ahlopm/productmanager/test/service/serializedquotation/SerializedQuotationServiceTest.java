package hk.com.ahlopm.productmanager.test.service.serializedquotation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.quotation.QuotationStatus;
import hk.com.ahlopm.productmanager.domainobject.serializedquotation.SerializedQuotation;
import hk.com.ahlopm.productmanager.exception.NotCreateOperationException;
import hk.com.ahlopm.productmanager.exception.NotUpdateOperationException;
import hk.com.ahlopm.productmanager.exception.ObjectNotFoundException;
import hk.com.ahlopm.productmanager.exception.SerializedQuotationPersistenceException;
import hk.com.ahlopm.productmanager.service.quotation.QuotationService;
import hk.com.ahlopm.productmanager.service.serializedquotation.SerializedQuotationService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;
import hk.com.ahlopm.productmanager.test.service.DomainObjectServiceTest;
import hk.com.ahlopm.productmanager.test.service.DomainObjectServiceTester;

import java.io.IOException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SerializedQuotationServiceTest extends SpringTest implements DomainObjectServiceTest {

    @Autowired
    private SerializedQuotationService serializedQuotationService;

    @Autowired
    private QuotationService quotationService;

    @Autowired
    private ObjectMapper jsonMapper;

    @Autowired
    private DomainObjectServiceTester<SerializedQuotation> tester;

    private SerializedQuotation testSerializedQuotation;

    @Before
    public void setUp() throws Exception {

        testSerializedQuotation = DomainObjectFactory.createSerializedQuotation();
        tester.setService(serializedQuotationService);
        tester.setTestObject(testSerializedQuotation);
    }

    @After
    public void tearDown() throws Exception {

        // DB gets cleaned up automatically, no work to do here.
    }

    @Test
    @Transactional
    public void testFind() {

        tester.testFind();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testFindNonExistant() {

        tester.testFindNonExistant();
    }

    @Test
    @Transactional
    public void testFindAll() {

        tester.testFindAll();
    }

    @Test
    @Transactional
    public void testCreate() {

        tester.testCreate();
    }

    @Test(expected = NotCreateOperationException.class)
    @Transactional
    public void testCreateNotCreateOperation() {

        tester.testCreateNotCreateOperation();
    }

    @Test
    @Transactional
    public void testUpdate() {

        testSerializedQuotation = serializedQuotationService.find(1);
        testSerializedQuotation.setQuotationId(111);
        tester.setTestObject(testSerializedQuotation);

        tester.testUpdate();

        assertEquals("The id does not match", 111, testSerializedQuotation.getQuotationId());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testUpdateNonExistant() {

        tester.testUpdateNonExistant();
    }

    @Test(expected = NotUpdateOperationException.class)
    @Transactional
    public void testUpdateNotUpdateOperation() {

        tester.testUpdateNotUpdateOperation();
    }

    @Test
    @Transactional
    public void testDeleteByObject() {

        testSerializedQuotation.setId(0); // Create object to test deletion.
        serializedQuotationService.create(testSerializedQuotation);

        tester.testDeleteByObject();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeleteNonExistantByObject() {

        tester.testDeleteNonExistantByObject();
    }

    @Test
    @Transactional
    public void testDeleteById() {

        tester.testDeleteById();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeleteNonExistantById() {

        tester.testDeleteNonExistantById();
    }

    @Test
    @Transactional
    public void testCount() {

        tester.testCount();
    }

    @Test
    @Transactional
    public void testSerializeQuotation()
            throws SerializedQuotationPersistenceException, JsonParseException, JsonMappingException, IOException {

        Quotation quote = DomainObjectFactory.createQuotation();
        quote.setId(0);
        quotationService.create(quote);

        SerializedQuotation sQuote = serializedQuotationService.serializeQuotation(quote.getId());

        assertEquals("The serialized quote should have the same status as the quote.", quote.getStatus(),
                sQuote.getStatus());
        assertEquals("The serialized quote should reference the id of the quote.", quote.getId(),
                sQuote.getQuotationId());

        // The serialized quote should exist in the database
        serializedQuotationService.find(sQuote.getId());

        Quotation jsonQuote = jsonMapper.readValue(sQuote.getQuotation(), Quotation.class);

        assertEquals("The reconstructed quote should match the original.", quote, jsonQuote);
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testSerializeNonExistantQuotation() throws SerializedQuotationPersistenceException {

        serializedQuotationService.serializeQuotation(-1);
    }

    @Test
    @Transactional
    public void testDeserializeQuotation() throws SerializedQuotationPersistenceException {

        Quotation quote = DomainObjectFactory.createQuotation();
        quote.setId(0);
        quotationService.create(quote);

        SerializedQuotation sQuote = serializedQuotationService.serializeQuotation(quote.getId());

        Quotation dsQuote = serializedQuotationService.deserializeQuotation(sQuote.getId());

        assertEquals("The deserialized quote should match the original.", quote, dsQuote);
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeserializeNonExistantQuotation() throws SerializedQuotationPersistenceException {

        serializedQuotationService.deserializeQuotation(-1);
    }

    @Test
    @Transactional
    public void testDeserializeAll() {

        List<Quotation> list = serializedQuotationService.getAllDeserializedQuotations();
        long size = list.size();
        assertEquals("The list should contain a quotation for each serialized entry.",
                serializedQuotationService.count(), size);

        testSerializedQuotation.setId(0);
        serializedQuotationService.create(testSerializedQuotation);

        assertEquals("The count should have increased by 1 with new SerializedQuotation.", size + 1,
                serializedQuotationService.count());
    }

    @Test
    @Transactional
    public void testFindAllByQuotation() throws SerializedQuotationPersistenceException {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);
        quotationService.create(testQuotation);

        serializedQuotationService.serializeQuotation(testQuotation.getId());

        assertEquals("The method should have returned a list of size 1.", 1,
                serializedQuotationService.findAllByQuotation(testQuotation.getId()).size());

        assertEquals("The method should have returned a serial entry with the id of the quotation",
                testQuotation.getId(), serializedQuotationService.findAllByQuotation(testQuotation.getId()).iterator()
                        .next().getQuotationId());
    }

    @Test
    @Transactional
    public void testFindByNonExistantQuotation() {

        assertEquals("The list should be empty", 0, serializedQuotationService.findAllByQuotation(-1).size());
    }

    @Test
    @Transactional
    public void testFindPresentingByQuotation() throws SerializedQuotationPersistenceException {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.PRESENTING);
        quotationService.create(testQuotation);

        Quotation testQuotation2 = DomainObjectFactory.createQuotation();
        testQuotation2.setId(0);
        testQuotation2.setStatus(QuotationStatus.DECLINED);
        quotationService.create(testQuotation2);

        serializedQuotationService.serializeQuotation(testQuotation.getId());
        serializedQuotationService.serializeQuotation(testQuotation2.getId());

        assertNotNull(serializedQuotationService.findPresentingByQuotation(testQuotation.getId()));

        try {
            serializedQuotationService.findPresentingByQuotation(testQuotation2.getId());
            fail("Failed to throw expected exception.");
        } catch (ObjectNotFoundException e) {
            // success
        }
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testFindPresentingByNonExistantQuotation() {

        serializedQuotationService.findPresentingByQuotation(-1);
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testFindPresentingByQuotationNoPresenting() throws SerializedQuotationPersistenceException {

        Quotation testQuotation = DomainObjectFactory.createQuotation();
        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.ACCEPTED);
        quotationService.create(testQuotation);

        serializedQuotationService.serializeQuotation(testQuotation.getId());

        serializedQuotationService.findPresentingByQuotation(testQuotation.getId());
    }
}
