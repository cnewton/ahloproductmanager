package hk.com.ahlopm.productmanager.test.service.product;

import static org.junit.Assert.assertEquals;
import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.domainobject.productpicture.ProductPicture;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.exception.NotCreateOperationException;
import hk.com.ahlopm.productmanager.exception.NotUpdateOperationException;
import hk.com.ahlopm.productmanager.exception.ObjectNotFoundException;
import hk.com.ahlopm.productmanager.exception.S3ResolutionFailureException;
import hk.com.ahlopm.productmanager.service.product.ProductService;
import hk.com.ahlopm.productmanager.service.quotation.QuotationService;
import hk.com.ahlopm.productmanager.service.quoteproduct.QuoteProductService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;
import hk.com.ahlopm.productmanager.test.service.DomainObjectServiceTest;
import hk.com.ahlopm.productmanager.test.service.DomainObjectServiceTester;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class ProductServiceTest extends SpringTest implements DomainObjectServiceTest {

    @Autowired
    private ProductService productService;

    @Autowired
    private QuotationService quotationService;

    @Autowired
    private QuoteProductService quoteProductService;

    @Autowired
    private DomainObjectServiceTester<Product> tester;

    private Product testProduct;

    @Before
    public void setUp() throws Exception {

        testProduct = DomainObjectFactory.createProduct();
        tester.setService(productService);
        tester.setTestObject(testProduct);

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    @Transactional
    public void testFind() {

        tester.testFind();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testFindNonExistant() {

        tester.testFindNonExistant();
    }

    @Test
    @Transactional
    public void testFindAll() {

        tester.testFindAll();
    }

    @Test
    @Transactional
    public void testCreate() {

        testProduct.setId(0);
        tester.testCreate();
    }

    @Test(expected = NotCreateOperationException.class)
    @Transactional
    public void testCreateNotCreateOperation() {

        tester.testCreateNotCreateOperation();
    }

    @Test
    @Transactional
    public void testUpdate() {

        testProduct = productService.find(1);
        testProduct.setQuantity("TestUpdate");
        tester.setTestObject(testProduct);

        tester.testUpdate();

        assertEquals("The names do not match", "TestUpdate", testProduct.getQuantity());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testUpdateNonExistant() {

        tester.testUpdateNonExistant();
    }

    @Test(expected = NotUpdateOperationException.class)
    @Transactional
    public void testUpdateNotUpdateOperation() {

        tester.testUpdateNotUpdateOperation();
    }

    @Test
    @Transactional
    public void testDeleteByObject() {

        testProduct.setId(0); // Create object to test deletion.
        productService.create(testProduct);

        tester.testDeleteByObject();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeleteNonExistantByObject() {

        tester.testDeleteNonExistantByObject();
    }

    @Test
    @Transactional
    public void testDeleteById() {

        tester.testDeleteById();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeleteNonExistantById() {

        tester.testDeleteNonExistantById();
    }

    @Test
    @Transactional
    public void testCount() {

        tester.testCount();
    }

    @Test
    @Transactional
    public void testFindAvailableProductsForQuotation() {

        long originalCount = productService.count();

        Product prod1 = DomainObjectFactory.createProduct();
        prod1.setId(0);
        productService.create(prod1);

        Product prod2 = DomainObjectFactory.createProduct();
        prod2.setId(0);
        productService.create(prod2);

        Quotation quote = DomainObjectFactory.createQuotation();
        quote.setId(0);
        quotationService.create(quote);

        long[] prodIds = { prod1.getId(), prod2.getId() };
        quoteProductService.addProductsToQuotation(quote.getId(), prodIds);

        assertEquals("There should now be " + originalCount + 2 + " products.", originalCount + 2,
                productService.count());
        assertEquals("There should now be 2 products in the quotation.", 2,
                quotationService.find(quote.getId()).getProducts().size());

        assertEquals(
                "There should only be a list of " + originalCount + " products available to add to this quotation.",
                originalCount, productService.getAvailableProductsForQuotation(quote.getId()).size());

    }

    @Test
    @Transactional
    public void testFindAvailableProductsForEmptyQuotation() {

        Quotation quote = DomainObjectFactory.createQuotation();
        quote.setId(0);
        quotationService.create(quote);

        assertEquals("There should only be a list of equal size from each method.", productService.findAll().size(),
                productService.getAvailableProductsForQuotation(quote.getId()).size());

    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testFindAvailableProductsForNonExistantQuotation() {

        productService.getAvailableProductsForQuotation(-1);
    }

    @Test
    @Transactional
    public void testGetProductImageIds() throws S3ResolutionFailureException {

        testProduct.setId(0);

        ProductPicture pic1 = DomainObjectFactory.createProductPicture();
        pic1.setId(0);
        pic1.setProduct(testProduct);
        ProductPicture pic2 = DomainObjectFactory.createProductPicture();
        pic2.setId(0);
        pic2.setProduct(testProduct);

        testProduct.getPictures().add(pic1);
        testProduct.getPictures().add(pic2);
        testProduct.setPrimaryPicture(pic2);
        productService.create(testProduct);

        long[] ids = productService.getImageIdsForProduct(testProduct.getId());

        assertEquals("The list should contain 2 pictures.", 2, ids.length);
        assertEquals("The id in the first position should be the primary picture's id.",
                testProduct.getPrimaryPicture().getId(), ids[0]);
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testGetProductImagesNonExistantProduct() {

        productService.getImageIdsForProduct(-1);
    }

    @Test
    @Transactional
    public void testGetNonExistantProductImages() {

        testProduct.setId(0);
        productService.create(testProduct);

        assertEquals(0, productService.getImageIdsForProduct(testProduct.getId()).length);
    }
}
