package hk.com.ahlopm.productmanager.test.service.quotation;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.quotation.QuotationStatus;
import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;
import hk.com.ahlopm.productmanager.exception.NotCreateOperationException;
import hk.com.ahlopm.productmanager.exception.NotUpdateOperationException;
import hk.com.ahlopm.productmanager.exception.ObjectNotFoundException;
import hk.com.ahlopm.productmanager.service.product.ProductService;
import hk.com.ahlopm.productmanager.service.quotation.QuotationService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;
import hk.com.ahlopm.productmanager.test.service.DomainObjectServiceTest;
import hk.com.ahlopm.productmanager.test.service.DomainObjectServiceTester;

public class QuotationServiceTest extends SpringTest implements DomainObjectServiceTest {

    @Autowired
    private QuotationService quotationService;

    @Autowired
    private ProductService productService;

    @Autowired
    private DomainObjectServiceTester<Quotation> tester;

    private Quotation testQuotation;

    @Before
    public void setUp() throws Exception {

        testQuotation = DomainObjectFactory.createQuotation();
        tester.setService(quotationService);
        tester.setTestObject(testQuotation);
    }

    @After
    public void tearDown() throws Exception {

        // DB gets cleaned up automatically, no work to do here.
    }

    @Test
    @Transactional
    public void testFind() {

        tester.testFind();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testFindNonExistant() {

        tester.testFindNonExistant();
    }

    @Test
    @Transactional
    public void testFindAll() {

        tester.testFindAll();
    }

    @Test
    @Transactional
    public void testCreate() {

        testQuotation.setId(0);
        tester.testCreate();
    }

    @Test(expected = NotCreateOperationException.class)
    @Transactional
    public void testCreateNotCreateOperation() {

        tester.testCreateNotCreateOperation();
    }

    @Test
    @Transactional
    public void testUpdate() {

        testQuotation = quotationService.find(1);
        testQuotation.setName("TestUpdate");
        tester.setTestObject(testQuotation);

        tester.testUpdate();

        assertEquals("The names do not match", "TestUpdate", testQuotation.getName());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testUpdateNonExistant() {

        tester.testUpdateNonExistant();
    }

    @Test(expected = NotUpdateOperationException.class)
    @Transactional
    public void testUpdateNotUpdateOperation() {

        tester.testUpdateNotUpdateOperation();
    }

    @Test
    @Transactional
    public void testDeleteByObject() {

        testQuotation.setId(0); // Create object to test deletion.
        quotationService.create(testQuotation);

        tester.testDeleteByObject();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeleteNonExistantByObject() {

        tester.testDeleteNonExistantByObject();
    }

    @Test
    @Transactional
    public void testDeleteById() {

        tester.testDeleteById();
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeleteNonExistantById() {

        tester.testDeleteNonExistantById();
    }

    @Test
    @Transactional
    public void testCount() {

        tester.testCount();
    }

    @Test
    @Transactional
    public void testCalculatePrice() {

        QuoteProduct qp1 = DomainObjectFactory.createQuoteProduct();
        QuoteProduct qp2 = DomainObjectFactory.createQuoteProduct();
        Product prod1 = DomainObjectFactory.createProduct();
        Product prod2 = DomainObjectFactory.createProduct();

        prod1.setPrice(BigDecimal.TEN);
        prod1.setId(0);
        productService.create(prod1);

        prod2.setPrice(BigDecimal.TEN);
        prod2.setId(0);
        productService.create(prod2);

        qp1.setId(0);
        qp1.setDiscount(BigDecimal.ONE);
        qp1.setProduct(prod1);
        testQuotation.getProducts().add(qp1);

        qp2.setId(0);
        qp2.setDiscount(BigDecimal.ONE);
        qp2.setProduct(prod2);
        testQuotation.getProducts().add(qp2);

        testQuotation.setId(0);
        quotationService.create(testQuotation);

        BigDecimal retPrice = quotationService.calculatePrice(testQuotation.getId());

        assertEquals("The returned price should be equal to bigdecimal of 18.", BigDecimal.valueOf(18), retPrice);
    }

    @Test
    @Transactional
    public void testCalculatePriceNoProducts() {

        testQuotation.setId(0);
        quotationService.create(testQuotation);

        BigDecimal retPrice = quotationService.calculatePrice(testQuotation.getId());

        assertEquals("The returned price should be equal to bigdecimal of 0.", BigDecimal.valueOf(0), retPrice);
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testCalculatePriceNonExistantQuotation() {

        quotationService.calculatePrice(-1);
    }

    @Test
    @Transactional
    public void testFindAllInReview() {

        long countBefore = quotationService.findAllInReview().size();

        Quotation quote1 = DomainObjectFactory.createQuotation();
        quote1.setId(0);
        quote1.setStatus(QuotationStatus.REVIEW);

        Quotation quote2 = DomainObjectFactory.createQuotation();
        quote2.setId(0);
        quote2.setStatus(QuotationStatus.ACCEPTED);

        quotationService.create(quote1);
        quotationService.create(quote2);

        long countAfter = quotationService.findAllInReview().size();

        assertEquals("The size of the returned list should only increase by 1.", countBefore + 1, countAfter);
        assertEquals("The returned list should contain the quote of the correct state.", true,
                quotationService.findAllInReview().contains(quote1));
        assertEquals("The returned list should not contain the quote of the incorrect state.", false,
                quotationService.findAllInReview().contains(quote2));
    }

    @Test
    @Transactional
    public void testFindAllInManagement() {

        long countBefore = quotationService.findAllInManagement().size();

        Quotation quote1 = DomainObjectFactory.createQuotation();
        quote1.setId(0);
        quote1.setStatus(QuotationStatus.REVIEW);

        Quotation quote2 = DomainObjectFactory.createQuotation();
        quote2.setId(0);
        quote2.setStatus(QuotationStatus.ACCEPTED);

        quotationService.create(quote1);
        quotationService.create(quote2);

        long countAfter = quotationService.findAllInManagement().size();

        assertEquals("The size of the returned list should only increase by 1.", countBefore + 1, countAfter);
        assertEquals("The returned list should contain the quote of the correct state.", true,
                quotationService.findAllInManagement().contains(quote2));
        assertEquals("The returned list should not contain the quote of the incorrect state.", false,
                quotationService.findAllInManagement().contains(quote1));
    }
}
