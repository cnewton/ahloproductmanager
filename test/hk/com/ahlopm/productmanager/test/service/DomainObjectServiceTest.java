package hk.com.ahlopm.productmanager.test.service;

public interface DomainObjectServiceTest {

    public void testFind();

    public void testFindNonExistant();

    public void testFindAll();

    public void testCreate();

    public void testCreateNotCreateOperation();

    public void testUpdate();

    public void testUpdateNonExistant();

    public void testUpdateNotUpdateOperation();

    public void testDeleteByObject();

    public void testDeleteNonExistantByObject();

    public void testDeleteById();

    public void testDeleteNonExistantById();

    public void testCount();

}
