package hk.com.ahlopm.productmanager.test.service.configuration;

import static org.junit.Assert.*;
import hk.com.ahlopm.productmanager.service.configuration.ConfigurationService;
import hk.com.ahlopm.productmanager.test.common.SpringTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class ConfigurationServiceTest extends SpringTest {

    @Autowired
    private ConfigurationService configurationService;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

        // DB gets cleaned up automatically, no work to do here.
    }

    @Test
    @Transactional
    public void test_get_INVALID_HEADER_NAME() {

        assertNotNull(configurationService.get_INVALID_HEADER_NAME());
    }

    @Test
    @Transactional
    public void test_get_AWS_S3_ACCESS_KEY() {

        assertNotNull(configurationService.get_AWS_S3_ACCESS_KEY());
    }

    @Test
    @Transactional
    public void test_get_AWS_S3_SECRET_KEY() {

        assertNotNull(configurationService.get_AWS_S3_SECRET_KEY());
    }

    @Test
    @Transactional
    public void test_get_DEFAULT_S3_BUCKET_NAME() {

        assertNotNull(configurationService.get_DEFAULT_S3_BUCKET_NAME());
    }

    @Test
    @Transactional
    public void test_set_INVALID_HEADER_NAME() {

        String newVal = "test";

        configurationService.set_INVALID_HEADER_NAME(newVal);
        assertEquals(newVal, configurationService.get_INVALID_HEADER_NAME());

        // cleanup
        configurationService.set_INVALID_HEADER_NAME(null);
    }

    @Test
    @Transactional
    public void test_set_AWS_S3_ACCESS_KEY() {

        String newVal = "test";

        configurationService.set_AWS_S3_ACCESS_KEY(newVal);
        assertEquals(newVal, configurationService.get_AWS_S3_ACCESS_KEY());

        // cleanup
        configurationService.set_AWS_S3_ACCESS_KEY(null);
    }

    @Test
    @Transactional
    public void test_set_AWS_S3_SECRET_KEY() {

        String newVal = "test";

        configurationService.set_AWS_S3_SECRET_KEY(newVal);
        assertEquals(newVal, configurationService.get_AWS_S3_SECRET_KEY());

        // cleanup
        configurationService.set_AWS_S3_SECRET_KEY(null);
    }

    @Test
    @Transactional
    public void test_set_DEFAULT_S3_BUCKET_NAME() {

        String newVal = "test";

        configurationService.set_DEFAULT_S3_BUCKET_NAME(newVal);
        assertEquals(newVal, configurationService.get_DEFAULT_S3_BUCKET_NAME());

        // cleanup
        configurationService.set_DEFAULT_S3_BUCKET_NAME(null);
    }

    @Test
    @Transactional
    public void test_set_AUTH_TOKEN_LIFE_SECONDS() {

        long originalVal = configurationService.get_AUTH_TOKEN_LIFE_SECONDS();
        long newVal = 55;

        configurationService.set_AUTH_TOKEN_LIFE_SECONDS(newVal);
        assertEquals(newVal, configurationService.get_AUTH_TOKEN_LIFE_SECONDS());

        // cleanup
        configurationService.set_AUTH_TOKEN_LIFE_SECONDS(originalVal);
    }

    @Test
    @Transactional
    public void test_set_HMAC_SECRET_KEY() {

        String newVal = "test";

        configurationService.set_HMAC_SECRET_KEY(newVal);
        assertEquals(newVal, configurationService.get_HMAC_SECRET_KEY());

        // cleanup
        configurationService.set_HMAC_SECRET_KEY(null);
    }

    @Test
    @Transactional
    public void test_set_AUTH_HEADER_NAME() {

        String newVal = "test";

        configurationService.set_AUTH_HEADER_NAME(newVal);
        assertEquals(newVal, configurationService.get_AUTH_HEADER_NAME());

        // cleanup
        configurationService.set_AUTH_HEADER_NAME(null);
    }

    @Test
    public void test_update_ALL_CONFIGURATIONS() {

        // TODO: with method
    }

}
