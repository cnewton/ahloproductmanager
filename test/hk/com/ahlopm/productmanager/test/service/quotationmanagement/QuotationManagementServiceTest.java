package hk.com.ahlopm.productmanager.test.service.quotationmanagement;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import hk.com.ahlopm.productmanager.domainobject.buyer.Buyer;
import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.quotation.QuotationStatus;
import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;
import hk.com.ahlopm.productmanager.exception.IllegalStateProgressionException;
import hk.com.ahlopm.productmanager.exception.ObjectNotFoundException;
import hk.com.ahlopm.productmanager.exception.SerializedQuotationPersistenceException;
import hk.com.ahlopm.productmanager.service.buyer.BuyerService;
import hk.com.ahlopm.productmanager.service.product.ProductService;
import hk.com.ahlopm.productmanager.service.quotation.QuotationService;
import hk.com.ahlopm.productmanager.service.quotationmanagement.QuotationManagementService;
import hk.com.ahlopm.productmanager.service.serializedquotation.SerializedQuotationService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class QuotationManagementServiceTest extends SpringTest {

    @Autowired
    private QuotationService quotationService;

    @Autowired
    private QuotationManagementService quotationManagementService;

    @Autowired
    private SerializedQuotationService serializedQuotationService;

    @Autowired
    private ProductService productService;

    @Autowired
    private BuyerService buyerService;

    private Quotation testQuotation;

    @Before
    public void setUp() throws Exception {

        testQuotation = DomainObjectFactory.createQuotation();
    }

    @After
    public void tearDown() throws Exception {

        // DB gets cleaned up automatically, no work to do here.
    }

    @Test
    @Transactional
    public void testFinalizeValidQuotation() {

        Product prod = DomainObjectFactory.createProduct();
        prod.setId(0);
        Buyer buyer = DomainObjectFactory.createBuyer();
        buyer.setId(0);
        productService.create(prod);
        buyerService.create(buyer);

        testQuotation.setId(0);
        testQuotation.setBuyer(buyer);

        QuoteProduct qp = DomainObjectFactory.createQuoteProduct();
        qp.setProduct(prod);
        qp.setId(0);
        qp.setQuotation(testQuotation);

        testQuotation.getProducts().add(qp);
        testQuotation.setStatus(QuotationStatus.REVIEW);

        quotationService.create(testQuotation);

        quotationManagementService.beginFinalizingQuotation(testQuotation.getId());

        assertEquals("The quotation status should now be 'Finalizing'.", QuotationStatus.FINALIZING,
                testQuotation.getStatus());

    }

    @Test(expected = IllegalStateProgressionException.class)
    @Transactional
    public void testFinalizeQuotationNoBuyer() {

        Product prod = DomainObjectFactory.createProduct();
        prod.setId(0);
        productService.create(prod);

        testQuotation.setId(0);
        testQuotation.setBuyer(null);

        QuoteProduct qp = DomainObjectFactory.createQuoteProduct();
        qp.setProduct(prod);
        qp.setId(0);
        qp.setQuotation(testQuotation);

        testQuotation.getProducts().add(qp);
        testQuotation.setStatus(QuotationStatus.REVIEW);

        quotationService.create(testQuotation);

        quotationManagementService.beginFinalizingQuotation(testQuotation.getId());

    }

    @Test(expected = IllegalStateProgressionException.class)
    @Transactional
    public void testFinalizeQuotationNoProduct() {

        Buyer buyer = DomainObjectFactory.createBuyer();
        buyer.setId(0);
        buyerService.create(buyer);

        testQuotation.setId(0);
        testQuotation.setBuyer(buyer);

        testQuotation.setStatus(QuotationStatus.REVIEW);

        quotationService.create(testQuotation);

        quotationManagementService.beginFinalizingQuotation(testQuotation.getId());

    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testFinalizeNonExistantQuotation() {

        quotationManagementService.beginFinalizingQuotation(-1);
    }

    @Test(expected = IllegalStateProgressionException.class)
    @Transactional
    public void testFinalizeQuotationWrongState() {

        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.ACCEPTED);
        quotationService.create(testQuotation);
        quotationManagementService.beginFinalizingQuotation(testQuotation.getId());
    }

    @Test
    @Transactional
    public void testReviewQuotation() {

        Product prod = DomainObjectFactory.createProduct();
        prod.setId(0);
        Buyer buyer = DomainObjectFactory.createBuyer();
        buyer.setId(0);
        productService.create(prod);
        buyerService.create(buyer);

        testQuotation.setId(0);
        testQuotation.setBuyer(buyer);

        QuoteProduct qp = DomainObjectFactory.createQuoteProduct();
        qp.setProduct(prod);
        qp.setId(0);
        qp.setQuotation(testQuotation);

        testQuotation.getProducts().add(qp);
        testQuotation.setStatus(QuotationStatus.REVIEW);

        quotationService.create(testQuotation);

        int revision = testQuotation.getRevisionNo();

        quotationManagementService.beginFinalizingQuotation(testQuotation.getId());

        quotationManagementService.reviewQuotation(testQuotation.getId());

        assertEquals("The quotation should be returned to the review state.", QuotationStatus.REVIEW,
                testQuotation.getStatus());
        assertEquals("The quotation should have an incremented revision number.", revision + 1,
                testQuotation.getRevisionNo());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testReviewNonExistantQuotation() {

        quotationManagementService.reviewQuotation(-1);
    }

    @Test(expected = IllegalStateProgressionException.class)
    @Transactional
    public void testReviewQuotationWrongState() {

        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.REVIEW);
        quotationService.create(testQuotation);
        quotationManagementService.reviewQuotation(testQuotation.getId());
    }

    @Test
    @Transactional
    public void testBeginPresentingQuotation() throws SerializedQuotationPersistenceException {

        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.FINALIZING);
        quotationService.create(testQuotation);

        quotationManagementService.beginPresentingQuotation(testQuotation.getId());

        assertEquals("The quotation should now be in the presenting state.", QuotationStatus.PRESENTING,
                testQuotation.getStatus());
        assertNotNull(serializedQuotationService.findPresentingByQuotation(testQuotation.getId()));
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testBeginPresentingNonExistantQuotation() throws SerializedQuotationPersistenceException {

        quotationManagementService.beginPresentingQuotation(-1);
    }

    @Test(expected = IllegalStateProgressionException.class)
    @Transactional
    public void testBeginPresentingQuotationWrongState() throws SerializedQuotationPersistenceException {

        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.REVIEW);
        quotationService.create(testQuotation);

        quotationManagementService.beginPresentingQuotation(testQuotation.getId());
    }

    @Test
    @Transactional
    public void testAcceptQuotation() throws SerializedQuotationPersistenceException {

        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.FINALIZING);
        quotationService.create(testQuotation);

        quotationManagementService.beginPresentingQuotation(testQuotation.getId());
        quotationManagementService.acceptQuotation(testQuotation.getId());

        assertEquals("The quotation should now be in the accepted state.", QuotationStatus.ACCEPTED,
                testQuotation.getStatus());

        assertEquals("The serialized quotation should now be in the accepted state.", QuotationStatus.ACCEPTED,
                serializedQuotationService.findAllByQuotation(testQuotation.getId()).iterator().next().getStatus());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testAcceptNonExistantQuotation() throws SerializedQuotationPersistenceException {

        quotationManagementService.acceptQuotation(-1);
    }

    @Test(expected = IllegalStateProgressionException.class)
    @Transactional
    public void testAcceptQuotationWrongState() throws SerializedQuotationPersistenceException {

        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.REVIEW);
        quotationService.create(testQuotation);

        quotationManagementService.acceptQuotation(testQuotation.getId());
    }

    @Test
    @Transactional
    public void testDeclineQuotation() throws SerializedQuotationPersistenceException {

        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.FINALIZING);
        quotationService.create(testQuotation);

        quotationManagementService.beginPresentingQuotation(testQuotation.getId());
        quotationManagementService.declineQuotation(testQuotation.getId());

        assertEquals("The quotation should now be in the accepted state.", QuotationStatus.DECLINED,
                testQuotation.getStatus());

        assertEquals("The serialized quotation should now be in the accepted state.", QuotationStatus.DECLINED,
                serializedQuotationService.findAllByQuotation(testQuotation.getId()).iterator().next().getStatus());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testDeclineNonExistantQuotation() throws SerializedQuotationPersistenceException {

        quotationManagementService.declineQuotation(-1);
    }

    @Test(expected = IllegalStateProgressionException.class)
    @Transactional
    public void testDeclineQuotationWrongState() throws SerializedQuotationPersistenceException {

        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.REVIEW);
        quotationService.create(testQuotation);

        quotationManagementService.declineQuotation(testQuotation.getId());
    }

    @Test
    @Transactional
    public void testCancelQuotation() throws SerializedQuotationPersistenceException {

        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.FINALIZING);
        quotationService.create(testQuotation);

        quotationManagementService.beginPresentingQuotation(testQuotation.getId());
        quotationManagementService.cancelQuotation(testQuotation.getId());

        assertEquals("The quotation should now be in the accepted state.", QuotationStatus.CANCELLED,
                testQuotation.getStatus());

        assertEquals("There should be no serialized entries for the quotation.", 0,
                serializedQuotationService.findAllByQuotation(testQuotation.getId()).size());
    }

    @Test(expected = ObjectNotFoundException.class)
    @Transactional
    public void testCancelNonExistantQuotation() throws SerializedQuotationPersistenceException {

        quotationManagementService.cancelQuotation(-1);
    }

    @Test(expected = IllegalStateProgressionException.class)
    @Transactional
    public void testCancelQuotationWrongState() throws SerializedQuotationPersistenceException {

        testQuotation.setId(0);
        testQuotation.setStatus(QuotationStatus.REVIEW);
        quotationService.create(testQuotation);

        quotationManagementService.cancelQuotation(testQuotation.getId());
    }
}
