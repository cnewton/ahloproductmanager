package hk.com.ahlopm.productmanager.test.service.invoice;

import static org.junit.Assert.assertEquals;
import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;
import hk.com.ahlopm.productmanager.service.invoice.InvoiceServiceUtils;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

public class InvoiceServiceUtilsTest extends SpringTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    @Transactional
    public void testSeparateProductsByFinish2Finishes1Element() {

        String fin1 = "Fin1";
        String fin2 = "Fin2";

        List<QuoteProduct> products = new ArrayList<QuoteProduct>();

        QuoteProduct qp1 = DomainObjectFactory.createQuoteProduct();
        qp1.getProduct().setFinish(fin1);
        QuoteProduct qp2 = DomainObjectFactory.createQuoteProduct();
        qp2.getProduct().setFinish(fin2);

        products.add(qp1);
        products.add(qp2);

        List<List<QuoteProduct>> result = InvoiceServiceUtils.separateProductsByFinish(products);

        assertEquals("The result should contain 2 sublists.", 2, result.size());
    }

    @Test
    @Transactional
    public void testSeparateProductsByFinish2Finishes2Elements() {

        String fin1 = "Fin1";
        String fin2 = "Fin2";

        List<QuoteProduct> products = new ArrayList<QuoteProduct>();

        QuoteProduct qp1 = DomainObjectFactory.createQuoteProduct();
        qp1.getProduct().setFinish(fin1);
        QuoteProduct qp2 = DomainObjectFactory.createQuoteProduct();
        qp2.getProduct().setFinish(fin2);
        QuoteProduct qp3 = DomainObjectFactory.createQuoteProduct();
        qp3.getProduct().setFinish(fin1);
        QuoteProduct qp4 = DomainObjectFactory.createQuoteProduct();
        qp4.getProduct().setFinish(fin2);

        products.add(qp1);
        products.add(qp2);
        products.add(qp3);
        products.add(qp4);

        List<List<QuoteProduct>> result = InvoiceServiceUtils.separateProductsByFinish(products);

        List<QuoteProduct> sublist = result.get(0);
        assertEquals("The result should contain 2 sublists.", 2, result.size());
        assertEquals("The sublist should be of size 2.", 2, sublist.size());
        assertEquals("Both elements in the sublist should have the same finish.",
                sublist.get(0).getProduct().getFinish(), sublist.get(1).getProduct().getFinish());
    }

    @Test
    @Transactional
    public void testSeparateProductsByStyle2Styles1Element() {

        String style1 = "Style1";
        String style2 = "Style2";

        List<QuoteProduct> products = new ArrayList<QuoteProduct>();

        QuoteProduct qp1 = DomainObjectFactory.createQuoteProduct();
        qp1.getProduct().setStyle(style1);
        QuoteProduct qp2 = DomainObjectFactory.createQuoteProduct();
        qp2.getProduct().setStyle(style2);

        products.add(qp1);
        products.add(qp2);

        List<List<QuoteProduct>> result = InvoiceServiceUtils.separateProductsByStyle(products);

        assertEquals("The result should contain 2 sublists.", 2, result.size());
    }

    @Test
    @Transactional
    public void testSeparateProductsByStyle2Styles2Elements() {

        String style1 = "Style1";
        String style2 = "Style2";

        List<QuoteProduct> products = new ArrayList<QuoteProduct>();

        QuoteProduct qp1 = DomainObjectFactory.createQuoteProduct();
        qp1.getProduct().setStyle(style1);
        QuoteProduct qp2 = DomainObjectFactory.createQuoteProduct();
        qp2.getProduct().setStyle(style2);
        QuoteProduct qp3 = DomainObjectFactory.createQuoteProduct();
        qp3.getProduct().setStyle(style1);
        QuoteProduct qp4 = DomainObjectFactory.createQuoteProduct();
        qp4.getProduct().setStyle(style2);

        products.add(qp1);
        products.add(qp2);
        products.add(qp3);
        products.add(qp4);

        List<List<QuoteProduct>> result = InvoiceServiceUtils.separateProductsByStyle(products);

        List<QuoteProduct> sublist = result.get(0);
        assertEquals("The result should contain 2 sublists.", 2, result.size());
        assertEquals("The sublist should be of size 2.", 2, sublist.size());
        assertEquals("Both elements in the sublist should have the same finish.",
                sublist.get(0).getProduct().getStyle(), sublist.get(1).getProduct().getStyle());
    }

    @Test
    @Transactional
    public void testSeparateProducts2Styles2Finishes() {

        String style1 = "Style1";
        String style2 = "Style2";
        String fin1 = "Fin1";
        String fin2 = "Fin2";

        List<QuoteProduct> products = new ArrayList<QuoteProduct>();

        QuoteProduct qp1 = DomainObjectFactory.createQuoteProduct();
        qp1.getProduct().setStyle(style1);
        qp1.getProduct().setFinish(fin1);
        QuoteProduct qp2 = DomainObjectFactory.createQuoteProduct();
        qp2.getProduct().setStyle(style1);
        qp2.getProduct().setFinish(fin2);

        QuoteProduct qp3 = DomainObjectFactory.createQuoteProduct();
        qp3.getProduct().setStyle(style2);
        qp3.getProduct().setFinish(fin1);
        QuoteProduct qp4 = DomainObjectFactory.createQuoteProduct();
        qp4.getProduct().setStyle(style2);
        qp4.getProduct().setFinish(fin2);

        products.add(qp1);
        products.add(qp2);
        products.add(qp3);
        products.add(qp4);

        List<List<QuoteProduct>> result = InvoiceServiceUtils.separateProducts(products);

        List<QuoteProduct> sublist = result.get(0);
        assertEquals("The result should contain 4 sublists.", 4, result.size());
        assertEquals("The sublist should be of size 1.", 1, sublist.size());
    }

    @Test
    @Transactional
    public void testSortProductsByCode2Products() {

        List<QuoteProduct> products = new ArrayList<QuoteProduct>();

        QuoteProduct qp1 = DomainObjectFactory.createQuoteProduct();
        QuoteProduct qp2 = DomainObjectFactory.createQuoteProduct();
        qp1.getProduct().setProductCode("A");
        qp2.getProduct().setProductCode("B");

        products.add(qp1);
        products.add(qp2);

        List<QuoteProduct> result = InvoiceServiceUtils.sortProductsByCode(products);

        assertEquals("The first element should be alphabetically ordered.", "A",
                result.get(0).getProduct().getProductCode());
    }

    @Test
    @Transactional
    public void testSortProductsByCode3Products() {

        List<QuoteProduct> products = new ArrayList<QuoteProduct>();

        QuoteProduct qp1 = DomainObjectFactory.createQuoteProduct();
        QuoteProduct qp2 = DomainObjectFactory.createQuoteProduct();
        QuoteProduct qp3 = DomainObjectFactory.createQuoteProduct();
        qp1.getProduct().setProductCode("A");
        qp2.getProduct().setProductCode("B");
        qp3.getProduct().setProductCode("C");

        products.add(qp3);
        products.add(qp2);
        products.add(qp1);

        List<QuoteProduct> result = InvoiceServiceUtils.sortProductsByCode(products);

        assertEquals("The first element should be alphabetically ordered.", "A",
                result.get(0).getProduct().getProductCode());
    }

    @Test
    @Transactional
    public void testSortCollectionsBySize2Collections() {

        List<QuoteProduct> products = new ArrayList<QuoteProduct>();

        QuoteProduct qp1 = DomainObjectFactory.createQuoteProduct();
        QuoteProduct qp2 = DomainObjectFactory.createQuoteProduct();
        QuoteProduct qp3 = DomainObjectFactory.createQuoteProduct();

        products.add(qp3);
        products.add(qp2);
        products.add(qp1);

        List<QuoteProduct> products2 = new ArrayList<QuoteProduct>();

        QuoteProduct qp4 = DomainObjectFactory.createQuoteProduct();
        QuoteProduct qp5 = DomainObjectFactory.createQuoteProduct();

        products2.add(qp4);
        products2.add(qp5);

        List<Collection<QuoteProduct>> lists = new ArrayList<Collection<QuoteProduct>>();

        lists.add(products2);
        lists.add(products);

        List<List<QuoteProduct>> result = InvoiceServiceUtils.sortCollectionsBySize(lists);

        assertEquals("The first element should be size ordered.", products.size(), result.get(0).size());
    }

    @Test
    @Transactional
    public void testSortCollectionsBySize3Collections() {

        List<QuoteProduct> products = new ArrayList<QuoteProduct>();

        QuoteProduct qp1 = DomainObjectFactory.createQuoteProduct();
        QuoteProduct qp2 = DomainObjectFactory.createQuoteProduct();
        QuoteProduct qp3 = DomainObjectFactory.createQuoteProduct();

        products.add(qp3);
        products.add(qp2);
        products.add(qp1);

        List<QuoteProduct> products2 = new ArrayList<QuoteProduct>();

        QuoteProduct qp4 = DomainObjectFactory.createQuoteProduct();

        products2.add(qp4);

        List<QuoteProduct> products3 = new ArrayList<QuoteProduct>();

        QuoteProduct qp5 = DomainObjectFactory.createQuoteProduct();
        QuoteProduct qp6 = DomainObjectFactory.createQuoteProduct();

        products3.add(qp5);
        products3.add(qp6);

        List<Collection<QuoteProduct>> lists = new ArrayList<Collection<QuoteProduct>>();

        lists.add(products2);
        lists.add(products);
        lists.add(products3);

        List<List<QuoteProduct>> result = InvoiceServiceUtils.sortCollectionsBySize(lists);

        assertEquals("The first element should be size ordered.", products.size(), result.get(0).size());
    }

    @Test
    @Transactional
    public void testSortProducts() {

        List<QuoteProduct> products = new ArrayList<QuoteProduct>();

        QuoteProduct qp1 = DomainObjectFactory.createQuoteProduct();
        QuoteProduct qp2 = DomainObjectFactory.createQuoteProduct();
        QuoteProduct qp3 = DomainObjectFactory.createQuoteProduct();
        qp1.getProduct().setProductCode("A");
        qp2.getProduct().setProductCode("C");
        qp3.getProduct().setProductCode("B");

        products.add(qp3);
        products.add(qp2);
        products.add(qp1);

        List<QuoteProduct> products2 = new ArrayList<QuoteProduct>();

        QuoteProduct qp4 = DomainObjectFactory.createQuoteProduct();
        qp4.getProduct().setProductCode("A");

        products2.add(qp4);

        List<QuoteProduct> products3 = new ArrayList<QuoteProduct>();

        QuoteProduct qp5 = DomainObjectFactory.createQuoteProduct();
        QuoteProduct qp6 = DomainObjectFactory.createQuoteProduct();
        qp5.getProduct().setProductCode("B");
        qp6.getProduct().setProductCode("A");

        products3.add(qp5);
        products3.add(qp6);

        List<Collection<QuoteProduct>> lists = new ArrayList<Collection<QuoteProduct>>();

        lists.add(products2);
        lists.add(products);
        lists.add(products3);

        List<List<QuoteProduct>> result = InvoiceServiceUtils.sortProducts(lists);

        assertEquals("The first element should be size ordered.", products.size(), result.get(0).size());

        for (List<QuoteProduct> subList : result) {
            assertEquals("The first element should be alphabetically ordered.", "A",
                    subList.get(0).getProduct().getProductCode());
        }

    }

    @Test
    @Transactional
    public void testSeparateAndSort() throws IOException {

        List<QuoteProduct> products = new ArrayList<QuoteProduct>();

        for (int i = 1000; i > 0; i--) {
            QuoteProduct qp = DomainObjectFactory.createQuoteProduct();
            Integer val = i % 128;
            qp.getProduct().setProductCode(val.toString());
            if (val % 2 == 0) {
                qp.getProduct().setStyle("even");
            } else {
                qp.getProduct().setStyle("odd");
            }
            products.add(qp);
        }

        List<List<QuoteProduct>> result = InvoiceServiceUtils.separateAndSortProducts(products);

        assertEquals("There should be 2 sublists.", 2, result.size());

        assertEquals("The first element of the first list should have a code of 0.", "0",
                result.get(0).get(0).getProduct().getProductCode());
    }
}
