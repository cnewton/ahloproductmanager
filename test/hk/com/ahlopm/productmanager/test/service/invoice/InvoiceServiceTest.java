package hk.com.ahlopm.productmanager.test.service.invoice;

import static org.mockito.Mockito.when;
import hk.com.ahlopm.productmanager.domainobject.productpicture.S3ProductPicture;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;
import hk.com.ahlopm.productmanager.domainobject.serializedquotation.SerializedQuotation;
import hk.com.ahlopm.productmanager.exception.SerializedQuotationPersistenceException;
import hk.com.ahlopm.productmanager.service.invoice.InvoiceService;
import hk.com.ahlopm.productmanager.service.quotation.QuotationService;
import hk.com.ahlopm.productmanager.service.serializedquotation.SerializedQuotationService;
import hk.com.ahlopm.productmanager.test.common.DomainObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.transaction.annotation.Transactional;

import com.amazonaws.util.IOUtils;

public class InvoiceServiceTest extends SpringTest {

    @Autowired
    private InvoiceService invoiceService;

    @Mock
    private S3ProductPicture mockPicture, mockPicture2, mockPicture3;

    @Autowired
    private QuotationService quotationService;

    @Autowired
    private SerializedQuotationService serializedQuotationService;

    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    @Transactional
    public void testGenerateEmptyQuotation() throws IOException {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        invoiceService.generateInvoice(testQuote);
    }

    @Test
    @Transactional
    public void testInvoiceBuilder1ProductBlockNullPicture() throws IOException {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        QuoteProduct product = DomainObjectFactory.createQuoteProduct();

        pictureMockNull(mockPicture);
        product.getProduct().setPrimaryPicture(mockPicture);
        testQuote.getProducts().add(product);

        invoiceService.generateInvoice(testQuote);
    }

    @Test
    @Transactional
    public void testInvoiceBuilder1ProductBlockTestPicture() throws IOException {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        QuoteProduct product = DomainObjectFactory.createQuoteProduct();

        pictureMockReal(mockPicture);
        product.getProduct().setPrimaryPicture(mockPicture);
        testQuote.getProducts().add(product);

        invoiceService.generateInvoice(testQuote);
    }

    @Test
    @Transactional
    public void testInvoiceBuilder1ProductBlockTestPictureNullPicture() throws IOException {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        QuoteProduct product = DomainObjectFactory.createQuoteProduct();
        QuoteProduct product2 = DomainObjectFactory.createQuoteProduct();

        pictureMockNull(mockPicture);
        pictureMockReal(mockPicture2);

        product.getProduct().setPrimaryPicture(mockPicture);
        product2.getProduct().setPrimaryPicture(mockPicture2);
        testQuote.getProducts().add(product);
        testQuote.getProducts().add(product2);

        invoiceService.generateInvoice(testQuote);
    }

    @Test
    @Transactional
    public void testInvoiceBuilder2PictureRows() throws IOException {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        QuoteProduct product = DomainObjectFactory.createQuoteProduct();
        QuoteProduct product2 = DomainObjectFactory.createQuoteProduct();
        QuoteProduct product3 = DomainObjectFactory.createQuoteProduct();
        QuoteProduct product4 = DomainObjectFactory.createQuoteProduct();
        QuoteProduct product5 = DomainObjectFactory.createQuoteProduct();
        QuoteProduct product6 = DomainObjectFactory.createQuoteProduct();
        QuoteProduct product7 = DomainObjectFactory.createQuoteProduct();

        pictureMockReal(mockPicture);

        product.getProduct().setPrimaryPicture(mockPicture);
        product2.getProduct().setPrimaryPicture(mockPicture);
        product3.getProduct().setPrimaryPicture(mockPicture);
        product4.getProduct().setPrimaryPicture(mockPicture);
        product5.getProduct().setPrimaryPicture(mockPicture);
        product6.getProduct().setPrimaryPicture(mockPicture);
        product7.getProduct().setPrimaryPicture(mockPicture);
        testQuote.getProducts().add(product);
        testQuote.getProducts().add(product2);
        testQuote.getProducts().add(product3);
        testQuote.getProducts().add(product4);
        testQuote.getProducts().add(product5);
        testQuote.getProducts().add(product6);
        testQuote.getProducts().add(product7);

        invoiceService.generateInvoice(testQuote);
    }

    @Test
    @Transactional
    public void testBuilderFromRelational() throws IOException {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        testQuote.setId(0);
        QuoteProduct product = DomainObjectFactory.createQuoteProduct();
        product.setId(0);
        product.setQuotation(testQuote);
        QuoteProduct product2 = DomainObjectFactory.createQuoteProduct();
        product2.setId(0);
        product2.setQuotation(testQuote);

        testQuote.getProducts().add(product);
        testQuote.getProducts().add(product2);

        quotationService.create(testQuote);

        invoiceService.buildFromRelationalQuotation(testQuote.getId());

    }

    @Test
    @Transactional
    public void testBuilderFromSerialized() throws IOException, SerializedQuotationPersistenceException {

        Quotation testQuote = DomainObjectFactory.createQuotation();
        testQuote.setId(0);
        QuoteProduct product = DomainObjectFactory.createQuoteProduct();
        product.setId(0);
        product.setQuotation(testQuote);
        QuoteProduct product2 = DomainObjectFactory.createQuoteProduct();
        product2.setId(0);
        product2.setQuotation(testQuote);

        testQuote.getProducts().add(product);
        testQuote.getProducts().add(product2);

        quotationService.create(testQuote);

        SerializedQuotation testSerial = serializedQuotationService.serializeQuotation(testQuote.getId());

        invoiceService.buildFromSerializedQuotation(testSerial.getId());

    }

    private void pictureMockNull(S3ProductPicture pic) {

        when(pic.getPicture()).thenReturn(null);
        when(pic.getEncodeType()).thenReturn(null);
    }

    private void pictureMockReal(S3ProductPicture pic) throws IOException {

        File testFile = new ClassPathResource("images/validImage.jpg").getFile();
        InputStream stream = new FileInputStream(testFile);

        byte[] picData = IOUtils.toByteArray(stream);
        stream.close();
        when(pic.getPicture()).thenReturn(picData);
        when(pic.getEncodeType()).thenReturn("jpeg");
    }
}
