package hk.com.ahlopm.productmanager.test.service.invoice;

import hk.com.ahlopm.productmanager.domainobject.invoice.Invoice;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.service.invoice.InvoiceService;
import hk.com.ahlopm.productmanager.test.common.ComplexObjectFactory;
import hk.com.ahlopm.productmanager.test.common.SpringTest;

import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.springframework.beans.factory.annotation.Autowired;

public class InvoiceServiceExternalTest extends SpringTest {

    @Rule
    public TestName testName = new TestName();

    private String filePrefix;

    private String savePrefix = "build/reports/";

    @Autowired
    private InvoiceService invoiceService;

    @Before
    public void setUp() throws Exception {

        filePrefix = this.getClass().getSimpleName() + "/" + testName.getMethodName();
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void test8SquareResolvedImages() throws IOException {

        Quotation quote = ComplexObjectFactory.createExtern8SquareImagesQuotation(filePrefix);

        Invoice result = invoiceService.generateInvoice(quote);

        FileOutputStream fos = new FileOutputStream(savePrefix + testName.getMethodName() + ".xls");
        fos.write(result.getInvoice());
        fos.close();
    }

    @Test
    public void test8WideResolvedImages() throws IOException {

        Quotation quote = ComplexObjectFactory.createExtern8WideImagesQuotation(filePrefix);

        Invoice result = invoiceService.generateInvoice(quote);

        FileOutputStream fos = new FileOutputStream(savePrefix + testName.getMethodName() + ".xls");
        fos.write(result.getInvoice());
        fos.close();
    }

    @Test
    public void test24SquareResolvedImages() throws IOException {

        Quotation quote = ComplexObjectFactory.createExtern24SquareImagesQuotation(filePrefix);

        Invoice result = invoiceService.generateInvoice(quote);

        FileOutputStream fos = new FileOutputStream(savePrefix + testName.getMethodName() + ".xls");
        fos.write(result.getInvoice());
        fos.close();
    }

    @Test
    public void test2OversizedResolvedImages() throws IOException {

        Quotation quote = ComplexObjectFactory.createExtern2OversizedImagesQuotation(filePrefix);

        Invoice result = invoiceService.generateInvoice(quote);

        FileOutputStream fos = new FileOutputStream(savePrefix + testName.getMethodName() + ".xls");
        fos.write(result.getInvoice());
        fos.close();
    }
}
