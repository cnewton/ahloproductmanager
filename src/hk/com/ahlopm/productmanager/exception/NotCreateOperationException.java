package hk.com.ahlopm.productmanager.exception;

/**
 * A runtime exception that indicates an attempt was made to create a new domain object with an invalid identifier.
 * 
 * @author Cameron Newton
 *
 */
public class NotCreateOperationException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 4557421682683765597L;

    public NotCreateOperationException() {

        super();
    }

    public NotCreateOperationException(String msg) {

        super(msg);
    }

    public NotCreateOperationException(Throwable cause) {

        super(cause);
    }

    public NotCreateOperationException(String msg, Throwable cause) {

        super(msg, cause);
    }

}
