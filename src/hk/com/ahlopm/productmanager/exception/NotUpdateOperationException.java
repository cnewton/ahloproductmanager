package hk.com.ahlopm.productmanager.exception;

/**
 * A runtime exception that indicates an attempt was made to update a domain object with an invalid identifier.
 * 
 * @author Cameron Newton
 *
 */
public class NotUpdateOperationException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 4557421682683765597L;

    public NotUpdateOperationException() {

        super();
    }

    public NotUpdateOperationException(String msg) {

        super(msg);
    }

    public NotUpdateOperationException(Throwable cause) {

        super(cause);
    }

    public NotUpdateOperationException(String msg, Throwable cause) {

        super(msg, cause);
    }

}
