package hk.com.ahlopm.productmanager.exception;

/**
 * An exception that indicates a failure while trying to resolve an external S3 object.
 * 
 * @author Cameron Newton
 *
 */
public class S3ResolutionFailureException extends ResolutionFailureException {

    /**
     * 
     */
    private static final long serialVersionUID = 7268438971393128853L;

    public S3ResolutionFailureException() {

        super();
    }

    public S3ResolutionFailureException(String msg) {

        super(msg);
    }

    public S3ResolutionFailureException(Throwable cause) {

        super(cause);
    }

    public S3ResolutionFailureException(String msg, Throwable cause) {

        super(msg, cause);
    }

}
