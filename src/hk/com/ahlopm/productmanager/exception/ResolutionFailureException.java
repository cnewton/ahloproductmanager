package hk.com.ahlopm.productmanager.exception;

/**
 * An exception that indicates a failure while trying to resolve an external object.
 * 
 * @author Cameron Newton
 *
 */
public class ResolutionFailureException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 7268438971393128853L;

    public ResolutionFailureException() {

        super();
    }

    public ResolutionFailureException(String msg) {

        super(msg);
    }

    public ResolutionFailureException(Throwable cause) {

        super(cause);
    }

    public ResolutionFailureException(String msg, Throwable cause) {

        super(msg, cause);
    }

}
