package hk.com.ahlopm.productmanager.exception;

/**
 * A runtime exception that indicates an attempt was made to access a non-existant record within the database.
 * 
 * @author Cameron Newton
 *
 */
public class IllegalStateProgressionException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = -1671783979184080072L;

    public IllegalStateProgressionException() {

        super();
    }

    public IllegalStateProgressionException(String msg) {

        super(msg);
    }

    public IllegalStateProgressionException(Throwable cause) {

        super(cause);
    }

    public IllegalStateProgressionException(String msg, Throwable cause) {

        super(msg, cause);
    }

}
