package hk.com.ahlopm.productmanager.exception;

/**
 * A runtime exception that indicates a failure to successfully handle an uploaded file.
 * 
 * @author Cameron Newton
 *
 */
public class FileUploadException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = 1834066034759726034L;

    public FileUploadException() {

        super();
    }

    public FileUploadException(String msg) {

        super(msg);
    }

    public FileUploadException(Throwable cause) {

        super(cause);
    }

    public FileUploadException(String msg, Throwable cause) {

        super(msg, cause);
    }

}
