package hk.com.ahlopm.productmanager.exception;

/**
 * An exception that indicates a failure while trying to modify a products member picture structure.
 * 
 * @author Cameron Newton
 *
 */
public class PictureMemberException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 2116891931856385893L;

    public PictureMemberException() {

        super();
    }

    public PictureMemberException(String msg) {

        super(msg);
    }

    public PictureMemberException(Throwable cause) {

        super(cause);
    }

    public PictureMemberException(String msg, Throwable cause) {

        super(msg, cause);
    }

}
