package hk.com.ahlopm.productmanager.exception;

/**
 * A runtime exception that indicates an attempt was made to access or update a non-existant configuration record within
 * the database.
 * 
 * @author Cameron Newton
 *
 */
public class ConfigurationNotFoundException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = -3948958586573776887L;

    public ConfigurationNotFoundException() {
        super();
    }

    public ConfigurationNotFoundException(String msg) {
        super(msg);
    }

    public ConfigurationNotFoundException(Throwable cause) {
        super(cause);
    }

    public ConfigurationNotFoundException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
