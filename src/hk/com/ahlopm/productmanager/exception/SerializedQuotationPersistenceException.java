package hk.com.ahlopm.productmanager.exception;

/**
 * An exception that indicates a failure while trying to serialize or deserialize a quotation object during the blob
 * persistence process.
 * 
 * @author Cameron Newton
 *
 */
public class SerializedQuotationPersistenceException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -6501773245609206567L;

    public SerializedQuotationPersistenceException() {

        super();
    }

    public SerializedQuotationPersistenceException(String msg) {

        super(msg);
    }

    public SerializedQuotationPersistenceException(Throwable cause) {

        super(cause);
    }

    public SerializedQuotationPersistenceException(String msg, Throwable cause) {

        super(msg, cause);
    }

}
