package hk.com.ahlopm.productmanager.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

/**
 * Custom Access Denied handler. Handles failed REST requests by returning a HTTP 401 and logging the request and reason
 * for failure.
 * 
 * @author Cameron Newton
 */
public class CustomAccessDeniedHandler implements AccessDeniedHandler {

    protected static final Logger logger = LogManager.getLogger(CustomAccessDeniedHandler.class);

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response,
            AccessDeniedException accessDeniedException) throws ServletException, IOException {

        logger.info("Access denied for: " + request.getRequestURI() + " reason: " + accessDeniedException.getMessage());

        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
    }

}
