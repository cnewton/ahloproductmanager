package hk.com.ahlopm.productmanager.security;

import hk.com.ahlopm.productmanager.domainobject.user.UserRole;
import hk.com.ahlopm.productmanager.security.token.UserToken;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public class UserAuthentication implements Authentication {

    /**
     * 
     */
    private static final long serialVersionUID = -3229087372822591301L;

    private UserToken token;

    private List<GrantedAuthority> authorities;

    private boolean authenticated = false;

    public UserAuthentication(UserToken authUser) {

        this.token = authUser;

        authorities = new ArrayList<GrantedAuthority>();
        for (UserRole role : token.getRoles()) {
            authorities.add(new SimpleGrantedAuthority(role.getRole()));
        }
    }

    @Override
    public String getName() {

        return token.getUsername();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        return authorities;
    }

    @Override
    public Object getCredentials() {

        return null;
    }

    @Override
    public Object getDetails() {

        return token;
    }

    @Override
    public Object getPrincipal() {

        return token.getUsername();
    }

    @Override
    public boolean isAuthenticated() {

        return authenticated;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {

        this.authenticated = isAuthenticated;
    }

}
