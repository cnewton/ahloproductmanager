package hk.com.ahlopm.productmanager.security;

import hk.com.ahlopm.productmanager.domainobject.user.User;
import hk.com.ahlopm.productmanager.domainobject.user.UserRole;
import hk.com.ahlopm.productmanager.exception.ObjectNotFoundException;
import hk.com.ahlopm.productmanager.service.user.UserService;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Custom Authentication provider to authenticate REST calls. Will get the user from the database based on username, and
 * compare the hashed password values to determine if authentication should be granted.
 * 
 * @author Cameron Newton
 */
public class CustomAuthenticationProvider implements AuthenticationProvider {

    protected static final Logger logger = LogManager.getLogger(CustomAuthenticationProvider.class);

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private UserService userService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken) authentication;
        String username = String.valueOf(auth.getPrincipal().toString());
        String password = String.valueOf(auth.getCredentials());

        if (username.isEmpty() || password.isEmpty()) {
            throw new UsernameNotFoundException("No username or password entered");
        } else {
            logger.info("Authenticating user \"" + username + "\"...");
            return authenticateUser(username, password);
        }
    }

    /**
     * Verify the credentials are valid. This method hashes the supplied password and checks it against the database
     * values that correspond to the given username.
     * 
     * @param suppliedUsername
     * @param suppliedPassword
     * @return Authentication
     */

    private Authentication authenticateUser(String suppliedUsername, String suppliedPassword) {

        User user = null;

        try {
            user = userService.findByUsername(suppliedUsername);
        } catch (ObjectNotFoundException e) {
            logger.info("Incorrect username (user not found)");
            throw new UsernameNotFoundException("Incorrect username");
        }

        if (user.isEnabled() && passwordEncoder.matches(suppliedPassword, user.getPassword())) {
            logger.info("Authenticated user: " + user.getUsername());
            List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
            // Loop through all Roles and create GrantedAuthority's for them
            for (UserRole role : user.getRoles()) {
                authorities.add(new SimpleGrantedAuthority(role.getRole()));
            }
            UsernamePasswordAuthenticationToken userAuth = new UsernamePasswordAuthenticationToken(suppliedUsername,
                    null, authorities);
            userAuth.setDetails(user);
            return userAuth;
        } else {
            logger.info("Unsuccessful login attempt. User: " + suppliedUsername);
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {

        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
    }
}
