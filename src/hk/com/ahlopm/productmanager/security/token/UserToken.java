package hk.com.ahlopm.productmanager.security.token;

import hk.com.ahlopm.productmanager.domainobject.user.UserRole;

import java.util.Set;

public class UserToken {

    private long userId;

    private String username;

    private long expiryTime;

    private Set<UserRole> roles;

    public long getUserId() {

        return userId;
    }

    public long getExpiryTime() {

        return expiryTime;
    }

    public void setUserId(long userId) {

        this.userId = userId;
    }

    public void setExpiryTime(long expiryTime) {

        this.expiryTime = expiryTime;
    }

    public Set<UserRole> getRoles() {

        return roles;
    }

    public void setRoles(Set<UserRole> roles) {

        this.roles = roles;
    }

    public String getUsername() {

        return username;
    }

    public void setUsername(String username) {

        this.username = username;
    }

}
