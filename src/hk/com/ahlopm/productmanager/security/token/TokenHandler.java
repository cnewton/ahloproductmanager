package hk.com.ahlopm.productmanager.security.token;

import hk.com.ahlopm.productmanager.domainobject.user.User;

public interface TokenHandler {

    public String createTokenForUser(User user);

    public UserToken parseTokenString(String token);

    public String createTokenForUser(UserToken token);

}
