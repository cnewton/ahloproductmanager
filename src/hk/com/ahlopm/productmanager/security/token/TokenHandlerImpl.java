package hk.com.ahlopm.productmanager.security.token;

import hk.com.ahlopm.productmanager.domainobject.user.User;
import hk.com.ahlopm.productmanager.exception.CustomSecurityException;
import hk.com.ahlopm.productmanager.service.configuration.ConfigurationService;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Arrays;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class TokenHandlerImpl implements TokenHandler {

    protected static final Logger logger = LogManager.getLogger(TokenHandlerImpl.class);

    private static final String HMAC_ALGORITHM = "HmacSHA256";

    private static final long NOW = 0;

    private static final String DEFAULT_TIMEZONE = "+08:00";

    private static final String SEPARATOR = ".";

    private static final String SPLIT_SEPARATOR = "\\.";

    private Mac hmac = null;

    @Autowired
    private ObjectMapper jsonMapper;

    @Autowired
    private ConfigurationService configService;

    @Override
    public String createTokenForUser(User user) {

        UserToken token = new UserToken();
        token.setUsername(user.getUsername());
        token.setExpiryTime(getLongTime(configService.get_AUTH_TOKEN_LIFE_SECONDS()));
        token.setUserId(user.getId());
        token.setRoles(user.getRoles());

        return createTokenForUser(token);
    }

    @Override
    public String createTokenForUser(UserToken token) {

        byte[] tokenBytes = toJSON(token);
        byte[] tokenHash = createHmac(tokenBytes);
        StringBuilder strBuild = new StringBuilder(200);
        strBuild.append(StringUtils.newStringUtf8(Base64.encodeBase64(tokenBytes)));
        strBuild.append(SEPARATOR);
        strBuild.append(StringUtils.newStringUtf8(Base64.encodeBase64(tokenHash)));
        return strBuild.toString();
    }

    @Override
    public UserToken parseTokenString(String tokenString) {

        final String[] parts = tokenString.split(SPLIT_SEPARATOR);
        if (parts.length == 2 && parts[0].length() > 0 && parts[1].length() > 0) {

            final byte[] tokenBytes = Base64.decodeBase64(parts[0]);
            final byte[] tokenHash = Base64.decodeBase64(parts[1]);

            if (Arrays.equals(createHmac(tokenBytes), tokenHash)) {

                UserToken userToken = fromJSON(tokenBytes);

                if (getLongTime(NOW) < userToken.getExpiryTime()) {
                    return userToken;
                }

            } else {
                logger.warn("User token was tampered with, auth failure.");
            }
        } else {
            logger.warn("Malformed Token string, auth failure.");
        }

        return null;
    }

    private synchronized byte[] createHmac(byte[] content) {

        // Currently disallows horizontal scaling.
        if (hmac == null) {
            try {
                hmac = Mac.getInstance(HMAC_ALGORITHM);
                SecretKeySpec secretKey = new SecretKeySpec(configService.get_HMAC_SECRET_KEY().getBytes("UTF-8"),
                        HMAC_ALGORITHM);
                hmac.init(secretKey);
            } catch (NoSuchAlgorithmException | InvalidKeyException | UnsupportedEncodingException e) {
                throw new CustomSecurityException(e);
            }
        }

        return hmac.doFinal(content);

    }

    private long getLongTime(long secondsFromNow) {

        return LocalDateTime.now(ZoneId.of(DEFAULT_TIMEZONE)).plusSeconds(secondsFromNow)
                .toEpochSecond(ZoneOffset.of(DEFAULT_TIMEZONE));
    }

    private byte[] toJSON(UserToken obj) {

        try {
            return jsonMapper.writeValueAsBytes(obj);
        } catch (JsonProcessingException e) {
            throw new CustomSecurityException("Failure to parse JSON.");
        }
    }

    private UserToken fromJSON(byte[] obj) {

        try {
            return jsonMapper.readValue(obj, UserToken.class);
        } catch (IOException e) {
            throw new CustomSecurityException("Failure to parse JSON.");
        }
    }

}
