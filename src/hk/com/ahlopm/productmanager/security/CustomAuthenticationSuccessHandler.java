package hk.com.ahlopm.productmanager.security;

import hk.com.ahlopm.productmanager.domainobject.user.User;
import hk.com.ahlopm.productmanager.security.token.TokenHandler;
import hk.com.ahlopm.productmanager.service.configuration.ConfigurationService;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

/**
 * Custom Authentication Success handler. Handles successful REST authentication requests by adding a header into the
 * response that contains a token that can be decoded by the server in future requests to authenticate the user.
 * 
 * @author Cameron Newton
 */
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    protected static final Logger logger = LogManager.getLogger(CustomAuthenticationSuccessHandler.class);

    @Autowired
    private TokenHandler tokenHandler;

    @Autowired
    private ConfigurationService configService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
            Authentication authentication) throws IOException, ServletException {

        User user = (User) authentication.getDetails();

        logger.info("Authentication Success for:" + user.getUsername());

        String authToken = tokenHandler.createTokenForUser(user);

        response.addHeader(configService.get_AUTH_HEADER_NAME(), authToken);
    }
}