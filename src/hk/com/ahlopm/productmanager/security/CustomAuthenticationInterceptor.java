package hk.com.ahlopm.productmanager.security;

import hk.com.ahlopm.productmanager.security.token.TokenHandler;
import hk.com.ahlopm.productmanager.security.token.UserToken;
import hk.com.ahlopm.productmanager.service.configuration.ConfigurationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * Intercepts any REST controller call, and extracts the authentication header from the HTTP request, authenticates the
 * user against the token handler and allows the request to proceed if the authentication succeeds.
 * 
 * @author Cameron Newton
 */
public class CustomAuthenticationInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private TokenHandler tokenHandler;

    @Autowired
    private ConfigurationService configService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {

        String authToken = request.getHeader(configService.get_AUTH_HEADER_NAME());

        if (authToken == null) {
            return true;
        }

        UserToken authUser = tokenHandler.parseTokenString(authToken);

        if (authUser == null) {
            return true;
        }

        Authentication auth = new UserAuthentication(authUser);
        auth.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(auth);

        return true;
    }
}
