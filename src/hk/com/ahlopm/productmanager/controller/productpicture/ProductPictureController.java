package hk.com.ahlopm.productmanager.controller.productpicture;

import hk.com.ahlopm.productmanager.controller.DomainObjectController;
import hk.com.ahlopm.productmanager.domainobject.productpicture.ProductPicture;
import hk.com.ahlopm.productmanager.exception.PictureMemberException;
import hk.com.ahlopm.productmanager.service.productpicture.ProductPictureService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * The ProductPictureController is responsible for exposing endpoints that enable the manipulation of the ProductPicture
 * domain object.
 * 
 * @author Cameron Newton
 *
 */
@RestController
@Secured({ "ROLE_USER" })
public class ProductPictureController implements DomainObjectController<ProductPicture> {

    @Autowired
    private ProductPictureService productPictureService;

    @Override
    @RequestMapping(value = "/productpicture/{id}", method = RequestMethod.GET)
    public ProductPicture get(@PathVariable("id") long id) {

        return productPictureService.find(id);
    }

    @Override
    @RequestMapping(value = "/productpicture", method = RequestMethod.GET)
    public List<ProductPicture> getAll() {

        return productPictureService.findAll();
    }

    @Override
    @RequestMapping(value = "/productpicture", method = RequestMethod.PUT)
    public ProductPicture put(@RequestBody ProductPicture productPicture) {

        return productPictureService.update(productPicture);
    }

    @Override
    @RequestMapping(value = "/productpicture/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") long id) {

        productPictureService.delete(id);
    }

    @Override
    @RequestMapping(value = "/productpicture", method = RequestMethod.POST)
    public ProductPicture post(@RequestBody ProductPicture productPicture) {

        return productPictureService.create(productPicture);
    }

    @Override
    @RequestMapping(value = "/productpicture/count", method = RequestMethod.GET)
    public long count() {

        return productPictureService.count();
    }

    @RequestMapping(value = "/productpicture/{id}/resolve", method = RequestMethod.GET)
    public byte[] getResolved(@PathVariable("id") long id) {

        return productPictureService.getResolvedPicture(id);
    }

    @RequestMapping(value = "/productpicture/primary", method = RequestMethod.PUT)
    public void setPrimaryPictureForProduct(@RequestParam("product") long productId,
            @RequestParam("picture") long pictureId) throws PictureMemberException {

        productPictureService.setPrimaryPictureForProduct(pictureId, productId);
    }
}
