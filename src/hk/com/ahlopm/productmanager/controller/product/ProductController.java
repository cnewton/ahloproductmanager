package hk.com.ahlopm.productmanager.controller.product;

import hk.com.ahlopm.productmanager.controller.DomainObjectController;
import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.service.product.ProductService;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * The ProductController is responsible for exposing endpoints that enable the manipulation of the Product domain
 * object.
 * 
 * @author Cameron Newton
 *
 */
@RestController
@Secured({ "ROLE_USER" })
public class ProductController implements DomainObjectController<Product> {

    protected static final Logger logger = LogManager.getLogger(ProductController.class);

    @Autowired
    private ProductService productService;

    @Override
    @RequestMapping(value = "/product", method = RequestMethod.GET)
    public List<Product> getAll() {

        return productService.findAll();
    }

    @Override
    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
    public Product get(@PathVariable("id") long id) {

        return productService.find(id);
    }

    @Override
    @RequestMapping(value = "/product", method = RequestMethod.PUT)
    public Product put(@RequestBody Product product) {

        return productService.update(product);
    }

    @Override
    @RequestMapping(value = "/product/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") long id) {

        productService.delete(id);
    }

    @Override
    @RequestMapping(value = "/product", method = RequestMethod.POST)
    public Product post(@RequestBody Product product) {

        return productService.create(product);
    }

    @Override
    @RequestMapping(value = "/product/count", method = RequestMethod.GET)
    public long count() {

        return productService.count();
    }

    @RequestMapping(value = "/product/visible/{quotationId}", method = RequestMethod.GET)
    public List<Product> getAvailableProductsForQuotation(@PathVariable("quotationId") long quotationId) {

        return productService.getAvailableProductsForQuotation(quotationId);
    }

    @RequestMapping(value = "/product/{id}/images", method = RequestMethod.GET)
    public long[] getUnresolvedImagesForProduct(@PathVariable("id") long productId) {

        return productService.getImageIdsForProduct(productId);
    }
}
