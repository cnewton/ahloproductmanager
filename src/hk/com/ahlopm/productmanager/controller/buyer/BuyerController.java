package hk.com.ahlopm.productmanager.controller.buyer;

import hk.com.ahlopm.productmanager.controller.DomainObjectController;
import hk.com.ahlopm.productmanager.domainobject.buyer.Buyer;
import hk.com.ahlopm.productmanager.service.buyer.BuyerService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * The BuyerController is responsible for exposing endpoints that enable the manipulation of the Buyer domain object.
 * 
 * @author Cameron Newton
 *
 */
@RestController
@Secured({ "ROLE_USER" })
public class BuyerController implements DomainObjectController<Buyer> {

    @Autowired
    private BuyerService buyerService;

    @Override
    @RequestMapping(value = "/buyer", method = RequestMethod.GET)
    public List<Buyer> getAll() {

        return buyerService.findAll();
    }

    @Override
    @RequestMapping(value = "/buyer/{id}", method = RequestMethod.GET)
    public Buyer get(@PathVariable("id") long id) {

        return buyerService.find(id);
    }

    @Override
    @RequestMapping(value = "/buyer", method = RequestMethod.PUT)
    public Buyer put(@RequestBody Buyer buyer) {

        return buyerService.update(buyer);
    }

    @Override
    @RequestMapping(value = "/buyer/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") long id) {

        buyerService.delete(id);
    }

    @Override
    @RequestMapping(value = "/buyer", method = RequestMethod.POST)
    public Buyer post(@RequestBody Buyer buyer) {

        return buyerService.create(buyer);
    }

    @Override
    @RequestMapping(value = "/buyer/count", method = RequestMethod.GET)
    public long count() {

        return buyerService.count();
    }

}
