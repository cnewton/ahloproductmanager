package hk.com.ahlopm.productmanager.controller.serializedquotation;

import hk.com.ahlopm.productmanager.controller.DomainObjectController;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.serializedquotation.SerializedQuotation;
import hk.com.ahlopm.productmanager.service.serializedquotation.SerializedQuotationService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * The SerializedQuotationController is responsible for exposing endpoints that enable the manipulation of the
 * SerializedQuotation domain object.
 * 
 * @author Cameron Newton
 *
 */
@RestController
@Secured({ "ROLE_USER" })
public class SerializedQuotationController implements DomainObjectController<SerializedQuotation> {

    @Autowired
    private SerializedQuotationService serializedQuotationService;

    @Override
    @RequestMapping(value = "/records/quotation", method = RequestMethod.GET)
    public List<SerializedQuotation> getAll() {

        return serializedQuotationService.findAll();
    }

    @Override
    @RequestMapping(value = "/records/quotation/{id}", method = RequestMethod.GET)
    public SerializedQuotation get(@PathVariable("id") long id) {

        return serializedQuotationService.find(id);
    }

    @Override
    @RequestMapping(value = "/records/quotation", method = RequestMethod.PUT)
    public SerializedQuotation put(@RequestBody SerializedQuotation quotation) {

        return serializedQuotationService.update(quotation);
    }

    @Override
    @RequestMapping(value = "/records/quotation/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") long id) {

        serializedQuotationService.delete(id);
    }

    @Override
    @RequestMapping(value = "/records/quotation", method = RequestMethod.POST)
    public SerializedQuotation post(@RequestBody SerializedQuotation quotation) {

        return serializedQuotationService.create(quotation);
    }

    @Override
    @RequestMapping(value = "/records/quotation/count", method = RequestMethod.GET)
    public long count() {

        return serializedQuotationService.count();
    }

    @RequestMapping(value = "/records/quotation/deserialized", method = RequestMethod.GET)
    public List<Quotation> getAllDeserialized() {

        return serializedQuotationService.getAllDeserializedQuotations();
    }
}
