package hk.com.ahlopm.productmanager.controller.quotationmanagement;

import hk.com.ahlopm.productmanager.exception.SerializedQuotationPersistenceException;
import hk.com.ahlopm.productmanager.service.quotationmanagement.QuotationManagementService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * The QuotationManagementController is responsible for exposing endpoints that enable the manipulation of the Quotation
 * domain object state.
 * 
 * @author Cameron Newton
 *
 */
@RestController
@Secured({ "ROLE_USER" })
public class QuotationManagementController {

    @Autowired
    private QuotationManagementService quotationManagementService;

    @RequestMapping(value = "/quotation/{id}/finalize", method = RequestMethod.PUT)
    public void beginFinalizingQuotation(@PathVariable("id") long quoteId) {

        quotationManagementService.beginFinalizingQuotation(quoteId);
    }

    @RequestMapping(value = "/quotation/{id}/review", method = RequestMethod.PUT)
    public String reviewQuotation(@PathVariable("id") long quoteId) {

        return quotationManagementService.reviewQuotation(quoteId);
    }

    @RequestMapping(value = "/quotation/{id}/present", method = RequestMethod.PUT)
    public void beginPresentingQuotation(@PathVariable("id") long quoteId)
            throws SerializedQuotationPersistenceException {

        quotationManagementService.beginPresentingQuotation(quoteId);
    }

    @RequestMapping(value = "/quotation/{id}/accept", method = RequestMethod.PUT)
    public void acceptQuotation(@PathVariable("id") long quoteId) throws SerializedQuotationPersistenceException {

        quotationManagementService.acceptQuotation(quoteId);
    }

    @RequestMapping(value = "/quotation/{id}/decline", method = RequestMethod.PUT)
    public void declineQuotation(@PathVariable("id") long quoteId) throws SerializedQuotationPersistenceException {

        quotationManagementService.declineQuotation(quoteId);
    }

    @RequestMapping(value = "/quotation/{id}/cancel", method = RequestMethod.PUT)
    public void cancelQuotation(@PathVariable("id") long quoteId) throws SerializedQuotationPersistenceException {

        quotationManagementService.cancelQuotation(quoteId);
    }

}
