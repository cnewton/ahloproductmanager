package hk.com.ahlopm.productmanager.controller.user;

import hk.com.ahlopm.productmanager.controller.DomainObjectController;
import hk.com.ahlopm.productmanager.domainobject.user.User;
import hk.com.ahlopm.productmanager.exception.CustomSecurityException;
import hk.com.ahlopm.productmanager.service.user.UserService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * The UserController is responsible for exposing endpoints that enable the manipulation of the User domain object.
 * 
 * @author Cameron Newton
 *
 */
@RestController
public class UserController implements DomainObjectController<User> {

    @Autowired
    private UserService userService;

    @Override
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    @Secured({ "ROLE_ADMIN" })
    public List<User> getAll() {

        return userService.findAll();
    }

    @Override
    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    @Secured({ "ROLE_ADMIN" })
    public User get(@PathVariable("id") long id) {

        return userService.find(id);
    }

    @RequestMapping(value = "/account", method = RequestMethod.GET)
    @Secured({ "ROLE_USER" })
    public User getAccount() {

        Authentication auth = getCurrentAuth();

        // Only possible from a non-security context. (Tests)
        if (auth == null)
            return null;

        return userService.findByUsername(auth.getName());
    }

    @Override
    @RequestMapping(value = "/user", method = RequestMethod.PUT)
    @Secured({ "ROLE_USER" })
    public User put(@RequestBody User user) {

        Authentication auth = getCurrentAuth();
        // If the auth is null - skip this (non security context)
        if (auth != null) {
            // If the calling user is not the user being updated
            if (!auth.getName().equals(user.getUsername())) {
                // And if the calling user is not an admin
                if (!auth.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN"))) {
                    throw new CustomSecurityException("Unable to update another users account!");
                }
            }
        }
        return userService.updateUserDetails(user);
    }

    @RequestMapping(value = "/user/{id}/password", method = RequestMethod.PUT)
    @Secured({ "ROLE_USER" })
    public void changePassword(@RequestBody String[] passwords, @PathVariable("id") long id) {

        userService.updateUserPassword(id, passwords[0], passwords[1]);
    }

    @RequestMapping(value = "/user/{id}/password/force", method = RequestMethod.PUT)
    @Secured({ "ROLE_ADMIN" })
    public void changePassword(@RequestBody String newPassword, @PathVariable("id") long id) {

        userService.forceUpdateUserPassword(id, newPassword);
    }

    @Override
    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    @Secured({ "ROLE_ADMIN" })
    public void delete(@PathVariable("id") long id) {

        userService.delete(id);
    }

    @Override
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    @Secured({ "ROLE_ADMIN" })
    public User post(@RequestBody User user) {

        return userService.create(user);
    }

    @Override
    @RequestMapping(value = "/user/count", method = RequestMethod.GET)
    @Secured({ "ROLE_ADMIN" })
    public long count() {

        return userService.count();
    }

    private final Authentication getCurrentAuth() {

        return SecurityContextHolder.getContext().getAuthentication();
    }
}
