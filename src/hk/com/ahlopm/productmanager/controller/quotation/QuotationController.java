package hk.com.ahlopm.productmanager.controller.quotation;

import hk.com.ahlopm.productmanager.controller.DomainObjectController;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.service.quotation.QuotationService;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * The QuotationController is responsible for exposing endpoints that enable the manipulation of the Quotation domain
 * object.
 * 
 * @author Cameron Newton
 *
 */
@RestController
@Secured({ "ROLE_USER" })
public class QuotationController implements DomainObjectController<Quotation> {

    @Autowired
    private QuotationService quotationService;

    @Override
    @RequestMapping(value = "/quotation", method = RequestMethod.GET)
    public List<Quotation> getAll() {

        return quotationService.findAll();
    }

    @RequestMapping(value = "/quotation/review", method = RequestMethod.GET)
    public List<Quotation> getAllInReview() {

        return quotationService.findAllInReview();
    }

    @RequestMapping(value = "/quotation/management", method = RequestMethod.GET)
    public List<Quotation> getAllInManagement() {

        return quotationService.findAllInManagement();
    }

    @Override
    @RequestMapping(value = "/quotation/{id}", method = RequestMethod.GET)
    public Quotation get(@PathVariable("id") long id) {

        return quotationService.find(id);
    }

    @Override
    @RequestMapping(value = "/quotation", method = RequestMethod.PUT)
    public Quotation put(@RequestBody Quotation quotation) {

        return quotationService.update(quotation);
    }

    @Override
    @RequestMapping(value = "/quotation/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") long id) {

        quotationService.delete(id);
    }

    @Override
    @RequestMapping(value = "/quotation", method = RequestMethod.POST)
    public Quotation post(@RequestBody Quotation quotation) {

        return quotationService.create(quotation);
    }

    @Override
    @RequestMapping(value = "/quotation/count", method = RequestMethod.GET)
    public long count() {

        return quotationService.count();
    }

    @RequestMapping(value = "/quotation/{id}/price", method = RequestMethod.GET)
    public BigDecimal getPriceForQuotation(@PathVariable("id") long quoteId) {

        return quotationService.calculatePrice(quoteId);
    }

}
