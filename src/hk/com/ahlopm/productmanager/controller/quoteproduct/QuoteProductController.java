package hk.com.ahlopm.productmanager.controller.quoteproduct;

import hk.com.ahlopm.productmanager.controller.DomainObjectController;
import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;
import hk.com.ahlopm.productmanager.service.quoteproduct.QuoteProductService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * The QuoteProductController is responsible for exposing endpoints that enable the manipulation of the QuoteProduct
 * domain object.
 * 
 * @author Cameron Newton
 *
 */
@RestController
@Secured({ "ROLE_USER" })
public class QuoteProductController implements DomainObjectController<QuoteProduct> {

    @Autowired
    private QuoteProductService quoteProductService;

    @Override
    @RequestMapping(value = "/quoteproduct", method = RequestMethod.GET)
    public List<QuoteProduct> getAll() {

        return quoteProductService.findAll();
    }

    @Override
    @RequestMapping(value = "/quoteproduct/{id}", method = RequestMethod.GET)
    public QuoteProduct get(@PathVariable("id") long id) {

        return quoteProductService.find(id);
    }

    @Override
    @RequestMapping(value = "/quoteproduct", method = RequestMethod.PUT)
    public QuoteProduct put(@RequestBody QuoteProduct product) {

        return quoteProductService.update(product);
    }

    @Override
    @RequestMapping(value = "/quoteproduct/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") long id) {

        quoteProductService.delete(id);
    }

    @Override
    @RequestMapping(value = "/quoteproduct", method = RequestMethod.POST)
    public QuoteProduct post(@RequestBody QuoteProduct product) {

        return quoteProductService.create(product);
    }

    @Override
    @RequestMapping(value = "/quoteproduct/count", method = RequestMethod.GET)
    public long count() {

        return quoteProductService.count();
    }

    @RequestMapping(value = "/quotation/{id}/quoteproducts", method = RequestMethod.PUT)
    public void addProductsToQuotation(@PathVariable("id") long quoteId, @RequestBody long[] productIds) {

        quoteProductService.addProductsToQuotation(quoteId, productIds);
    }

}
