package hk.com.ahlopm.productmanager.controller.fileupload;

import hk.com.ahlopm.productmanager.service.fileupload.FileUploadService;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@Secured({ "ROLE_USER" })
public class FileUploadController {

    protected static final Logger logger = LogManager.getLogger(FileUploadController.class);

    @Autowired
    private FileUploadService fileUploadService;

    @RequestMapping(value = "/upload/product", method = RequestMethod.POST)
    public void uploadProductXLS(@RequestBody MultipartFile file) {

        fileUploadService.parseProductsXLSFile(file);
    }

    @RequestMapping(value = "/upload/product/{id}/image", method = RequestMethod.POST)
    public void uploadProductImage(@PathVariable("id") long productId, @RequestBody MultipartFile file) {

        fileUploadService.uploadProductImage(productId, file);
    }
}
