package hk.com.ahlopm.productmanager.controller.invoice;

import hk.com.ahlopm.productmanager.domainobject.invoice.Invoice;
import hk.com.ahlopm.productmanager.exception.SerializedQuotationPersistenceException;
import hk.com.ahlopm.productmanager.service.invoice.InvoiceService;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * The QuotationManagementController is responsible for exposing endpoints that enable the manipulation of the Quotation
 * domain object state.
 * 
 * @author Cameron Newton
 *
 */
@RestController
public class InvoiceController {

    @Autowired
    private InvoiceService invoiceService;

    @RequestMapping(value = "/quotation/{quoteId}/invoice/generate", method = RequestMethod.GET)
    public void generateInvoiceFromRelational(HttpServletResponse response, @PathVariable("quoteId") long quoteId)
            throws IOException {

        Invoice invoice = invoiceService.buildFromRelationalQuotation(quoteId);
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=\"quotation_" + quoteId + ".xls\"");
        response.setContentLength(invoice.getInvoice().length);
        InputStream in = new ByteArrayInputStream(invoice.getInvoice());

        StreamUtils.copy(in, response.getOutputStream());
    }

    @RequestMapping(value = "/records/{quoteId}/invoice/generate", method = RequestMethod.GET)
    public void generateInvoiceFromSerialized(HttpServletResponse response, @PathVariable("quoteId") long quoteId)
            throws IOException, SerializedQuotationPersistenceException {

        Invoice invoice = invoiceService.buildFromSerializedQuotation(quoteId);
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=\"quotation_" + quoteId + ".xls\"");
        response.setContentLength(invoice.getInvoice().length);
        InputStream in = new ByteArrayInputStream(invoice.getInvoice());

        StreamUtils.copy(in, response.getOutputStream());
    }
}
