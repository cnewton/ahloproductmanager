package hk.com.ahlopm.productmanager.common;

import hk.com.ahlopm.productmanager.exception.CustomSecurityException;
import hk.com.ahlopm.productmanager.exception.FileUploadException;
import hk.com.ahlopm.productmanager.exception.IllegalStateProgressionException;
import hk.com.ahlopm.productmanager.exception.NotCreateOperationException;
import hk.com.ahlopm.productmanager.exception.NotUpdateOperationException;
import hk.com.ahlopm.productmanager.exception.ObjectNotFoundException;
import hk.com.ahlopm.productmanager.exception.PictureMemberException;
import hk.com.ahlopm.productmanager.service.configuration.ConfigurationService;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * The RestErrorHandler is responsible for catching exceptions thrown out of top level rest controllers. This class will
 * generally translate the exceptions into HTTP response codes to return to the calling client.
 * 
 * @author Cameron Newton
 *
 */
@ControllerAdvice
public class RestErrorHandler {

    protected static final Logger logger = LogManager.getLogger(RestErrorHandler.class);

    @Autowired
    private ConfigurationService configService;

    @ExceptionHandler(ObjectNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleObjectNotFoundException(ObjectNotFoundException e) {

        logger.warn("Unknown Target Object: " + e.getMessage(), e);
    }

    @ExceptionHandler(NotCreateOperationException.class)
    public ResponseEntity<String> handleNotCreateException(NotCreateOperationException e) {

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(configService.get_INVALID_HEADER_NAME(), e.getMessage());

        return new ResponseEntity<String>("Bad post request " + e.getMessage(), responseHeaders,
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotUpdateOperationException.class)
    public ResponseEntity<String> handleNotUpdateException(NotUpdateOperationException e) {

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(configService.get_INVALID_HEADER_NAME(), e.getMessage());

        return new ResponseEntity<String>("Bad put request " + e.getMessage(), responseHeaders, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IllegalStateProgressionException.class)
    public ResponseEntity<String> handleIllegalStateProgressionException(IllegalStateProgressionException e) {

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(configService.get_INVALID_HEADER_NAME(), e.getMessage());

        logger.warn("Illegal state progression exception", e);
        return new ResponseEntity<String>(e.getMessage(), responseHeaders, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(PictureMemberException.class)
    public ResponseEntity<String> handlePictureMemberException(PictureMemberException e) {

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(configService.get_INVALID_HEADER_NAME(), e.getMessage());

        logger.warn("Picture Member Exception", e);
        return new ResponseEntity<String>(e.getMessage(), responseHeaders, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(FileUploadException.class)
    public ResponseEntity<String> handleFileUploadException(FileUploadException e) {

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(configService.get_INVALID_HEADER_NAME(), e.getMessage());

        logger.warn("File upload exception", e);
        return new ResponseEntity<String>(e.getMessage(), responseHeaders, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CustomSecurityException.class)
    public ResponseEntity<String> handleCustomSecurityException(CustomSecurityException e) {

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(configService.get_INVALID_HEADER_NAME(), e.getMessage());

        logger.warn("Custom Security Exception", e);
        return new ResponseEntity<String>(e.getMessage(), responseHeaders, HttpStatus.UNAUTHORIZED);
    }

}
