package hk.com.ahlopm.productmanager.common;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@Component
public class CustomObjectMapper extends ObjectMapper {

    /**
     * 
     */
    private static final long serialVersionUID = -1750188005422729107L;

    public CustomObjectMapper() {

        super();

        // Only include non null fields when serialising.
        this.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        this.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        // Change to serialise as yyyy-mm-ddThh:mm
        this.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

        this.registerModule(new Hibernate4Module());

        // Needed to (de)serialise the new Java8 LocalDateTime types properly.
        this.registerModule(new JavaTimeModule());

    }
}
