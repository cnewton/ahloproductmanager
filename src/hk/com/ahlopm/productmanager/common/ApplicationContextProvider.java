package hk.com.ahlopm.productmanager.common;

import hk.com.ahlopm.productmanager.amazons3.AmazonS3Gateway;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class ApplicationContextProvider implements ApplicationContextAware {

    private static ApplicationContext context;

    public static AmazonS3Gateway getS3PictureResolver() {

        AmazonS3Gateway bean;
        String beanName = "amazonS3Gateway";

        if (context == null) {
            throw new IllegalStateException("Application context was not constructed successfully.");
        }

        if (context.containsBean(beanName)) {
            bean = (AmazonS3Gateway) context.getBean(beanName);
        } else {
            throw new IllegalStateException("Requested bean: " + beanName + " did not exist.");
        }

        return bean;
    }

    public static ApplicationContext getApplicationContext() {

        return context;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

        context = applicationContext;
    }

}
