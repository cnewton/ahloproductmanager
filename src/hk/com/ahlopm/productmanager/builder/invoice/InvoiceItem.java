package hk.com.ahlopm.productmanager.builder.invoice;

import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;
import hk.com.ahlopm.productmanager.parser.image.ProductImageParser;

import java.awt.image.BufferedImage;

/**
 * This class holds the data needed to insert a QuoteProduct into a generated Invoice.
 * 
 * @author Cameron Newton
 *
 */
public class InvoiceItem {

    /**
     * The pixel width of the largest character (using the default font size and style)
     */
    private static final int CHARACTER_WIDTH = 7;

    private QuoteProduct product;

    private BufferedImage picture;

    private String pictureEncoding;

    public QuoteProduct getProduct() {

        return product;
    }

    public BufferedImage getPicture() {

        return picture;
    }

    public void setProduct(QuoteProduct product) {

        this.product = product;
    }

    public void setPicture(BufferedImage picture) {

        this.picture = picture;
    }

    public int getPictureHeight() {

        return picture.getHeight();
    }

    /**
     * Returns the pixel width of the items picture.
     */
    public int getPictureWidth() {

        return picture.getWidth();
    }

    /**
     * Returns the pixel width of the items label.
     */
    public int getLabelWidth() {

        return (product.getProduct().getProductCode().length() + 2) * CHARACTER_WIDTH;
    }

    /**
     * Returns the integer width of the largest out of either picture or label.
     */
    public int getItemWidth() {

        // Larger of the two.
        return getPictureWidth() > getLabelWidth() ? getPictureWidth() : getLabelWidth();
    }

    /**
     * Scale both dimensions of the image by a ratio.
     * 
     * @param scale
     *            - coefficient of scale
     */
    public void scalePicture(double scale) {

        this.picture = ProductImageParser.scaleImage(picture, scale);
    }

    public String getPictureEncoding() {

        return pictureEncoding;
    }

    public void setPictureEncoding(String pictureEncoding) {

        this.pictureEncoding = pictureEncoding;
    }
}
