package hk.com.ahlopm.productmanager.builder.invoice;

import hk.com.ahlopm.productmanager.domainobject.invoice.Invoice;
import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;
import hk.com.ahlopm.productmanager.exception.PictureMemberException;
import hk.com.ahlopm.productmanager.parser.image.ProductImageParser;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.lf5.util.StreamUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPicture;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFTextbox;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;

/**
 * The Invoice Builder allows builder pattern construction of an Invoice out of a Quotation and its products.
 * 
 * @author Cameron Newton
 *
 */
public class InvoiceBuilder {

    protected static final Logger logger = LogManager.getLogger(InvoiceBuilder.class);

    /**
     * Page width in pixels
     */
    private int pageWidth;

    /**
     * Column width in pixels
     */
    private int columnWidth;

    /**
     * Row height in pixels
     */
    private int rowHeight;

    /**
     * Horizontal padding in pixels
     */
    private int horizontalPadding;

    /**
     * Horizontal padding in pixels
     */
    private int verticalPadding;

    /**
     * Number of columns on page
     */
    private int columnCount = 8;

    private HSSFWorkbook book;

    private HSSFSheet sheet;

    private int rowNum;

    private HSSFRow row;

    private HSSFCell cell;

    private Quotation quote;

    public InvoiceBuilder(Quotation quote) {

        this.book = new HSSFWorkbook();
        this.sheet = book.createSheet(quote.getName());
        this.rowNum = 0;
        this.quote = quote;
        this.setRowHeight(16);
        this.setColumnWidth(16);
        this.setHorizontalPadding(15);
        this.setVerticalPadding(5);
        sheet.createDrawingPatriarch();
    }

    public InvoiceBuilder setColumnWidth(int columnWidth) {

        sheet.setDefaultColumnWidth(columnWidth);
        this.columnWidth = Float.valueOf(sheet.getColumnWidthInPixels(0)).intValue();
        this.pageWidth = columnCount * this.columnWidth;
        return this;
    }

    public InvoiceBuilder setRowHeight(int rowHeight) {

        sheet.setDefaultRowHeightInPoints(rowHeight);
        this.rowHeight = Double.valueOf(rowHeight / 0.75).intValue();
        return this;
    }

    public InvoiceBuilder setHorizontalPadding(int horizontalPadding) {

        this.horizontalPadding = horizontalPadding;
        return this;
    }

    public InvoiceBuilder setVerticalPadding(int verticalPadding) {

        this.verticalPadding = verticalPadding;
        return this;
    }

    public InvoiceBuilder setPrintArea(int sC, int eC, int sR, int eR) {

        book.setPrintArea(book.getSheetIndex(sheet), sC, eC, sR, eR);

        return this;
    }

    /**
     * Adds the (hardcoded) header block. Some fields are built off of the buyer included with the quotation.
     * 
     * @return
     */
    public InvoiceBuilder addPageHeader() {

        HSSFCellStyle style16TimesCenter = book.createCellStyle();
        HSSFFont font16Times = book.createFont();
        font16Times.setFontName("Times New Roman");
        font16Times.setFontHeightInPoints((short) 16);
        style16TimesCenter.setFont(font16Times);
        style16TimesCenter.setAlignment(HSSFCellStyle.ALIGN_CENTER);

        HSSFCellStyle style10TimesCenter = book.createCellStyle();
        HSSFFont font10Times = book.createFont();
        font10Times.setFontName("Times New Roman");
        font10Times.setFontHeightInPoints((short) 10);
        style10TimesCenter.setFont(font10Times);
        style10TimesCenter.setAlignment(HSSFCellStyle.ALIGN_CENTER);

        HSSFCellStyle style12TimesBoldLeft = book.createCellStyle();
        HSSFFont font12TimesBold = book.createFont();
        font12TimesBold.setFontName("Times New Roman");
        font12TimesBold.setFontHeightInPoints((short) 12);
        font12TimesBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        style12TimesBoldLeft.setFont(font12TimesBold);
        style12TimesBoldLeft.setAlignment(HSSFCellStyle.ALIGN_LEFT);

        HSSFCellStyle style12TimesBoldCenter = book.createCellStyle();
        style12TimesBoldCenter.setFont(font12TimesBold);
        style12TimesBoldCenter.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
        style12TimesBoldCenter.setAlignment(HSSFCellStyle.ALIGN_CENTER);

        HSSFCellStyle style12TimesLeft = book.createCellStyle();
        HSSFFont font12Times = book.createFont();
        font12Times.setFontName("Times New Roman");
        font12Times.setFontHeightInPoints((short) 12);
        style12TimesLeft.setFont(font12Times);
        style12TimesLeft.setAlignment(HSSFCellStyle.ALIGN_LEFT);

        HSSFCellStyle style18TimesBoldUnderlineCenter = book.createCellStyle();
        HSSFFont font18TimesBoldUnderline = book.createFont();
        font18TimesBoldUnderline.setFontName("Times New Roman");
        font18TimesBoldUnderline.setFontHeightInPoints((short) 18);
        font18TimesBoldUnderline.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font18TimesBoldUnderline.setUnderline(HSSFFont.U_SINGLE);
        style18TimesBoldUnderlineCenter.setFont(font18TimesBoldUnderline);
        style18TimesBoldUnderlineCenter.setAlignment(HSSFCellStyle.ALIGN_CENTER);

        HSSFCellStyle style12TimesBoldUnderlineLeft = book.createCellStyle();
        HSSFFont font12TimesBoldUnderline = book.createFont();
        font12TimesBoldUnderline.setFontName("Times New Roman");
        font12TimesBoldUnderline.setFontHeightInPoints((short) 12);
        font12TimesBoldUnderline.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font12TimesBoldUnderline.setUnderline(HSSFFont.U_SINGLE);
        style12TimesBoldUnderlineLeft.setFont(font12TimesBoldUnderline);
        style12TimesBoldUnderlineLeft.setAlignment(HSSFCellStyle.ALIGN_LEFT);

        HSSFCellStyle style12TimesBoldUnderlineCenter = book.createCellStyle();
        style12TimesBoldUnderlineCenter.setFont(font12TimesBoldUnderline);
        style12TimesBoldUnderlineCenter.setAlignment(HSSFCellStyle.ALIGN_CENTER);

        HSSFCellStyle styleBorderBottom = book.createCellStyle();
        styleBorderBottom.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);

        // Load logo resource
        InvoiceItem item = new InvoiceItem();

        try (InputStream is = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("hk/com/ahlopm/productmanager/resource/logo.jpg")) {
            item.setPicture(ProductImageParser.parseImageBytes(StreamUtils.getBytes(is)));
            item.setPictureEncoding("jpg");
        } catch (IOException | PictureMemberException e) {
            logger.fatal("Failed to read standard disk resource.", e);
        }

        // Scale logo down to fit 3 rows.
        double verticalScale = (double) rowHeight * 3 / item.getPictureHeight();
        item.scalePicture(verticalScale);

        // Insert logo.
        HSSFClientAnchor logoAnchor = buildAnchor(0, item.getPictureWidth(), rowNum, 0, item.getPictureHeight());
        insertPicture(item, logoAnchor, null);

        row = sheet.createRow(rowNum);
        row.createCell(0);
        cell = row.createCell(1);
        row.createCell(7);
        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 1, 7));
        cell.setCellValue("ELIFONE (HONG KONG) LTD.");
        cell.setCellStyle(style16TimesCenter);
        rowNum++;

        row = sheet.createRow(rowNum);
        row.createCell(0);
        cell = row.createCell(1);
        row.createCell(7);
        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 1, 7));
        cell.setCellValue("Flat G, 2/F, Yen Ying Building, No. 221, Jaffe Road, Wan Chai, Hong Kong");
        cell.setCellStyle(style10TimesCenter);
        rowNum++;

        row = sheet.createRow(rowNum);
        row.createCell(0);
        cell = row.createCell(1);
        row.createCell(7);
        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 1, 7));
        cell.setCellValue("Tel: (852)3621 0886 Fax: (852)3621 0883 Email: info@elifone.com Site: www.elifone.com");
        cell.setCellStyle(style10TimesCenter);
        rowNum++;

        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        row.createCell(4);
        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 4));
        cell.setCellValue(quote.getBuyer().getCompany());
        cell.setCellStyle(style12TimesBoldLeft);
        cell = row.createCell(5);
        cell.setCellValue("Date:");
        cell.setCellStyle(style12TimesBoldLeft);
        cell = row.createCell(6);
        cell.setCellValue(new Date());
        cell.setCellStyle(style12TimesLeft);
        rowNum++;

        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        row.createCell(4);
        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 4));
        cell.setCellValue(quote.getBuyer().getAddress());
        cell.setCellStyle(style12TimesLeft);
        cell = row.createCell(5);
        cell.setCellValue("Quotation No:");
        cell.setCellStyle(style12TimesBoldLeft);
        cell = row.createCell(6);
        cell.setCellValue(quote.getId());
        cell.setCellStyle(style12TimesLeft);
        rowNum++;

        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        row.createCell(4);
        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 4));
        cell.setCellValue("Attn: " + quote.getBuyer().getName());
        cell.setCellStyle(style12TimesLeft);
        cell = row.createCell(5);
        cell.setCellValue("From:");
        cell.setCellStyle(style12TimesBoldLeft);
        cell = row.createCell(6);
        cell.setCellValue(quote.getStatus().toString());
        cell.setCellStyle(style12TimesLeft);
        rowNum++;

        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        row.createCell(4);
        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 4));
        cell.setCellValue("Tel: " + quote.getBuyer().getPhone());
        cell.setCellStyle(style12TimesLeft);
        cell = row.createCell(5);
        cell.setCellValue("Info. Ex:");
        cell.setCellStyle(style12TimesBoldLeft);
        cell = row.createCell(6);
        cell.setCellValue(quote.getStatus().toString());
        cell.setCellStyle(style12TimesLeft);
        rowNum++;

        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        row.createCell(4);
        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 4));
        cell.setCellValue("Email: " + quote.getBuyer().getEmail());
        cell.setCellStyle(style12TimesLeft);
        cell = row.createCell(5);
        cell.setCellValue("Page:");
        cell.setCellStyle(style12TimesBoldLeft);
        cell = row.createCell(6);
        cell.setCellValue(sheet.getRowBreaks().length);
        cell.setCellStyle(style12TimesLeft);
        rowNum++;

        row = sheet.createRow(rowNum);
        row.setHeightInPoints((float) (row.getHeightInPoints() * 1.75));
        cell = row.createCell(0);
        row.createCell(7);
        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 7));
        cell.setCellValue("QUOTATION");
        cell.setCellStyle(style18TimesBoldUnderlineCenter);
        rowNum++;

        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        cell.setCellValue("Item No.");
        cell.setCellStyle(style12TimesBoldUnderlineLeft);
        cell = row.createCell(1);
        row.createCell(3);
        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 1, 3));
        cell.setCellValue("Description");
        cell.setCellStyle(style12TimesBoldUnderlineLeft);
        cell = row.createCell(4);
        row.createCell(5);
        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 4, 5));
        cell.setCellValue("Quantity");
        cell.setCellStyle(style12TimesBoldUnderlineCenter);
        cell = row.createCell(6);
        cell.setCellValue("Unit Price");
        cell.setCellStyle(style12TimesBoldUnderlineCenter);
        cell = row.createCell(7);
        cell.setCellValue("Total (US$)");
        cell.setCellStyle(style12TimesBoldUnderlineCenter);
        rowNum++;

        row = sheet.createRow(rowNum);
        row.setRowStyle(styleBorderBottom);
        cell = row.createCell(4);
        cell.setCellValue("M.O.Q");
        cell.setCellStyle(style12TimesBoldCenter);
        cell = row.createCell(5);
        cell.setCellValue("Unit");
        cell.setCellStyle(style12TimesBoldCenter);
        cell = row.createCell(6);
        cell.setCellValue("(US$)");
        cell.setCellStyle(style12TimesBoldCenter);
        cell = row.createCell(7);
        cell.setCellValue("FOB HK");
        cell.setCellStyle(style12TimesBoldCenter);
        rowNum++;

        return this;
    }

    /**
     * Performs all operations necessary to include a group of products. The first line added under this method will be
     * the finish and style of the first product in the list. The Images for each product will be normalized toward
     * median sizes. The products will be separated into rows (However many images can fit on the specified pagewidth).
     * Each group will have the information of all the products followed by each of the images and finally labels.
     * 
     */
    public InvoiceBuilder addProductBlock(Collection<QuoteProduct> products) {

        List<InvoiceItem> items = createInvoiceItems(products);

        Product firstProduct = items.get(0).getProduct().getProduct();

        HSSFCellStyle style12TimesBoldUnderlineLeft = book.createCellStyle();
        HSSFFont font12TimesBoldUnderline = book.createFont();
        font12TimesBoldUnderline.setFontName("Times New Roman");
        font12TimesBoldUnderline.setFontHeightInPoints((short) 12);
        font12TimesBoldUnderline.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font12TimesBoldUnderline.setUnderline(HSSFFont.U_SINGLE);
        style12TimesBoldUnderlineLeft.setFont(font12TimesBoldUnderline);
        style12TimesBoldUnderlineLeft.setAlignment(HSSFCellStyle.ALIGN_LEFT);

        HSSFCellStyle style12TimesLeft = book.createCellStyle();
        HSSFFont font12Times = book.createFont();
        font12Times.setFontName("Times New Roman");
        font12Times.setFontHeightInPoints((short) 12);
        style12TimesLeft.setFont(font12Times);
        style12TimesLeft.setAlignment(HSSFCellStyle.ALIGN_LEFT);

        HSSFCellStyle style12TimesCenter = book.createCellStyle();
        style12TimesCenter.setFont(font12Times);
        style12TimesCenter.setAlignment(HSSFCellStyle.ALIGN_CENTER);

        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        row.createCell(7);
        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 7));
        cell.setCellValue(firstProduct.getFinish() + " " + firstProduct.getStyle() + "s");
        cell.setCellStyle(style12TimesBoldUnderlineLeft);
        rowNum++;

        for (List<InvoiceItem> itemBlock : splitItemsByImageRows(items)) {
            for (InvoiceItem item : itemBlock) {

                QuoteProduct product = item.getProduct();
                double productPrice = product.getProduct().getPrice().subtract(product.getDiscount()).doubleValue();

                row = sheet.createRow(rowNum);
                cell = row.createCell(0);
                cell.setCellValue(product.getProduct().getProductCode()); // Product
                                                                          // Code
                cell.setCellStyle(style12TimesLeft);
                cell = row.createCell(1);
                row.createCell(3);
                sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 1, 3));
                cell.setCellValue(product.getProduct().getDescription()); // Description
                cell.setCellStyle(style12TimesLeft);
                cell = row.createCell(4);
                cell.setCellValue(product.getQuantity()); // Quantity
                cell.setCellStyle(style12TimesCenter);
                cell = row.createCell(5);
                cell.setCellValue("pcs"); // Unit
                cell.setCellStyle(style12TimesCenter);
                cell = row.createCell(6);
                cell.setCellValue(productPrice);
                cell.setCellStyle(style12TimesCenter);
                cell = row.createCell(7);
                cell.setCellValue(productPrice * product.getQuantity());
                cell.setCellStyle(style12TimesCenter);
                rowNum++;
            }
            // Padding row
            rowNum++;
            addPictureRow(itemBlock);
        }

        return this;
    }

    /**
     * Adds the (hardcoded) footer block.
     * 
     * @return
     */
    public InvoiceBuilder addPageFooter() {

        HSSFCellStyle style12TimesLeft = book.createCellStyle();
        HSSFFont font12Times = book.createFont();
        font12Times.setFontName("Times New Roman");
        font12Times.setFontHeightInPoints((short) 12);
        style12TimesLeft.setFont(font12Times);
        style12TimesLeft.setAlignment(HSSFCellStyle.ALIGN_LEFT);

        HSSFCellStyle style14TimesLeftBold = book.createCellStyle();
        HSSFFont font14TimesBold = book.createFont();
        font14TimesBold.setFontName("Times New Roman");
        font14TimesBold.setFontHeightInPoints((short) 14);
        font14TimesBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        style14TimesLeftBold.setFont(font14TimesBold);
        style14TimesLeftBold.setAlignment(HSSFCellStyle.ALIGN_LEFT);

        HSSFCellStyle style20TimesLeft = book.createCellStyle();
        HSSFFont font20Times = book.createFont();
        font20Times.setFontName("Times New Roman");
        font20Times.setFontHeightInPoints((short) 20);
        style20TimesLeft.setFont(font20Times);
        style20TimesLeft.setAlignment(HSSFCellStyle.ALIGN_LEFT);

        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        row.createCell(7);
        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 7));
        cell.setCellValue("REMARKS: ");
        cell.setCellStyle(style12TimesLeft);
        rowNum++;

        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        row.createCell(7);
        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 7));
        cell.setCellValue("Payment Terms : 30% deposit by T/T , balance by T/T as 10 days before shipment.");
        cell.setCellStyle(style12TimesLeft);
        rowNum++;

        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        row.createCell(7);
        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 7));
        cell.setCellValue("Delivery : Around 40-45 days upon receipt of deposit.");
        cell.setCellStyle(style12TimesLeft);
        rowNum++;

        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        row.createCell(7);
        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 7));
        cell.setCellValue("Shipping / Courier Information : To be advised by customer.");
        cell.setCellStyle(style12TimesLeft);
        rowNum++;

        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        row.createCell(7);
        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 7));
        cell.setCellValue("Packing : By bulk pack with poly bag of each item.");
        cell.setCellStyle(style12TimesLeft);
        rowNum++;

        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        row.createCell(7);
        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 7));
        cell.setCellValue("Laser logo: US$0.12/pc");
        cell.setCellStyle(style14TimesLeftBold);
        rowNum++;

        rowNum++;

        row = sheet.createRow(rowNum);
        cell = row.createCell(0);
        row.createCell(7);
        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 7));
        cell.setCellValue("Elifone (Hong Kong) Ltd.");
        cell.setCellStyle(style14TimesLeftBold);
        rowNum++;

        rowNum++;

        row = sheet.createRow(rowNum);
        sheet.createRow(rowNum + 1);
        cell = row.createCell(0);
        row.createCell(7);
        sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum + 1, 0, 7));
        cell.setCellValue("Flora / Janis");
        cell.setCellStyle(style20TimesLeft);
        rowNum += 2;

        return this;

    }

    /**
     * Returns an Invoice object containing a byte array representing the completed XLS file.
     * 
     * @return
     * @throws IOException
     */
    public Invoice build() throws IOException {

        this.setPrintArea(0, columnCount, 0, rowNum); // TODO: Decide whether to
                                                      // move responsibility.

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        book.write(out);

        Invoice invoice = new Invoice();
        invoice.setId(0);
        invoice.setInvoice(out.toByteArray());

        out.close();
        book.close();
        return invoice;
    }

    /**
     * Positions each of the pictures and labels for the block.
     * 
     * @param itemBlock
     */
    private void addPictureRow(List<InvoiceItem> itemBlock) {

        int pixelsRemaining = pageWidth - getStartingOffset(itemBlock);

        int tallestPicture = itemBlock.get(0).getPictureHeight();

        for (InvoiceItem item : itemBlock) {

            if (pixelsRemaining - item.getPictureWidth() < 0) {
                throw new IllegalStateException("Items were not correctly separated by image rows.");
            }

            int imgHorizontalStartPixel = pageWidth - pixelsRemaining;
            int imgHorizontalEndPixel = imgHorizontalStartPixel + item.getPictureWidth();
            int labelHorizontalStartPixel = pageWidth - pixelsRemaining;
            int labelHorizontalEndPixel = labelHorizontalStartPixel + item.getLabelWidth();
            int endHorizontalPixelDifference = imgHorizontalEndPixel - labelHorizontalEndPixel;

            if (endHorizontalPixelDifference > 0) {
                int move = (endHorizontalPixelDifference / 2);
                labelHorizontalStartPixel += move;
                labelHorizontalEndPixel += move;
            } else {
                int move = (endHorizontalPixelDifference * -1 / 2);
                imgHorizontalStartPixel += move;
                imgHorizontalEndPixel += move;
            }

            HSSFClientAnchor imgAnchor = buildAnchor(imgHorizontalStartPixel, imgHorizontalEndPixel, rowNum, 0,
                    item.getPictureHeight());

            int labelStartRow = rowNum + ((item.getPictureHeight() + verticalPadding) / rowHeight);
            int labelVerticalStartPixel = (item.getPictureHeight() + verticalPadding) % rowHeight;

            HSSFClientAnchor labelAnchor = buildAnchor(labelHorizontalStartPixel, labelHorizontalEndPixel,
                    labelStartRow, labelVerticalStartPixel, rowHeight);

            insertPicture(item, imgAnchor, labelAnchor);

            if (item.getPictureHeight() > tallestPicture) {
                tallestPicture = item.getPictureHeight();
            }

            pixelsRemaining -= (item.getItemWidth() + horizontalPadding);
        }

        rowNum += (tallestPicture / rowHeight + 3); // move row pointer
    }

    /**
     * Constructs an anchor for a picture (or label) based on the given parameters.
     * 
     * @param horizontalStartPixel
     * @param horizontalEndPixel
     * @param startRow
     * @param startRowOffsetPixels
     * @param itemHeight
     * @return
     */
    private HSSFClientAnchor buildAnchor(int horizontalStartPixel, int horizontalEndPixel, int startRow,
            int startRowOffsetPixels, int itemHeight) {

        int startColumnOffset = (horizontalStartPixel % columnWidth) * 1023 / columnWidth;
        int startRowOffset = startRowOffsetPixels * 254 / rowHeight;
        int endColumnOffset = (horizontalEndPixel % columnWidth) * 1023 / columnWidth;
        int endRowOffset = ((startRowOffsetPixels + itemHeight) % rowHeight) * 254 / rowHeight;
        int startColumn = horizontalStartPixel / columnWidth;
        int endColumn = horizontalEndPixel / columnWidth;
        int endRow = startRow + (startRowOffsetPixels + itemHeight) / rowHeight;

        return new HSSFClientAnchor(startColumnOffset, startRowOffset, endColumnOffset, endRowOffset,
                (short) startColumn, startRow, (short) endColumn, endRow);
    }

    /**
     * Calculates the total width of all items in a block, returns a pixel offset (# of pixels to indent row start)
     * 
     * @param itemList
     * @return offset
     */
    private int getStartingOffset(List<InvoiceItem> itemList) {

        int totalWidth = 0;
        for (InvoiceItem item : itemList) {
            totalWidth += item.getItemWidth() + horizontalPadding;
        }
        totalWidth -= horizontalPadding; // Last image need not be padded.

        return (pageWidth - totalWidth) / 2; // Remaining pixel space divided by
                                             // 2 to get space on left.
    }

    private List<InvoiceItem> createInvoiceItems(Collection<QuoteProduct> products) {

        List<InvoiceItem> items = new ArrayList<InvoiceItem>();
        for (QuoteProduct qp : products) {
            try {
                items.add(createInvoiceItem(qp));
            } catch (PictureMemberException e) {
                logger.fatal("Failed to read standard disk resource.", e);
            }
        }
        return items;
    }

    /**
     * Constructs the invoice items from quote products. Building the bufferedImage objects from the included byte
     * arrays.
     * 
     * @param product
     * @return
     * @throws PictureMemberException
     */
    private InvoiceItem createInvoiceItem(QuoteProduct product) throws PictureMemberException {

        InvoiceItem invItem = new InvoiceItem();

        byte[] pictureData = null;
        String encodeType = "";

        if (!(product.getProduct().getPrimaryPicture() == null
                || product.getProduct().getPrimaryPicture().getPicture() == null)) {
            encodeType = product.getProduct().getPrimaryPicture().getEncodeType();
            pictureData = product.getProduct().getPrimaryPicture().getPicture();
        } else {
            try (InputStream is = Thread.currentThread().getContextClassLoader()
                    .getResourceAsStream("hk/com/ahlopm/productmanager/resource/image_error.jpg")) {
                pictureData = StreamUtils.getBytes(is);
                encodeType = "jpeg";
            } catch (IOException e) {
                throw new PictureMemberException("Failed to load image error resource.");
            }
        }

        invItem.setPicture(ProductImageParser.parseImageBytes(pictureData));
        invItem.setProduct(product);
        invItem.setPictureEncoding(encodeType);

        return invItem;
    }

    /**
     * Separates the item block into sub lists based on how many items can fit the page width.
     * 
     * @param invoiceItems
     * @return
     */
    private List<List<InvoiceItem>> splitItemsByImageRows(List<InvoiceItem> invoiceItems) {

        // Normalize image sizes for all images in a category.
        normalizeImageSizes(invoiceItems);

        List<InvoiceItem> subList = new ArrayList<InvoiceItem>();
        List<List<InvoiceItem>> list = new ArrayList<List<InvoiceItem>>();
        list.add(subList);

        int availableWidth = pageWidth;

        for (InvoiceItem item : invoiceItems) {

            while (item.getPictureWidth() > pageWidth) {
                item.scalePicture(0.98);
            }

            if ((availableWidth - item.getItemWidth()) < 0) {
                subList = new ArrayList<InvoiceItem>();
                list.add(subList);
                availableWidth = pageWidth;
            }

            subList.add(item);
            availableWidth -= (item.getItemWidth() + horizontalPadding);
        }

        return list;
    }

    /**
     * Normalizes picture dimensions towards the median values for all other items in the block.
     * 
     * @param invoiceItems
     */
    private void normalizeImageSizes(List<InvoiceItem> invoiceItems) {

        int[] heights = new int[invoiceItems.size()];
        int[] widths = new int[invoiceItems.size()];

        int ii = 0;
        for (InvoiceItem item : invoiceItems) {
            heights[ii] = item.getPictureHeight();
            widths[ii] = item.getPictureWidth();
            ii++;
        }
        Arrays.sort(heights);
        Arrays.sort(widths);

        int medianHeight;
        int medianWidth;

        if (invoiceItems.size() % 2 == 0) {
            medianHeight = (heights[heights.length / 2] + heights[heights.length / 2 - 1]) / 2;
            medianWidth = (widths[widths.length / 2] + widths[widths.length / 2 - 1]) / 2;
        } else {
            medianHeight = heights[heights.length / 2];
            medianWidth = widths[widths.length / 2];
        }

        for (InvoiceItem item : invoiceItems) {
            // W / H aspect ratio.
            int heightDiff = item.getPictureHeight() - medianHeight;
            int widthDiff = item.getPictureWidth() - medianWidth;

            if (Math.abs(heightDiff) > Math.abs(widthDiff)) {
                double scale = (double) medianHeight / item.getPictureHeight();
                item.scalePicture(scale);
            } else if (Math.abs(heightDiff) < Math.abs(widthDiff)) {
                double scale = (double) medianWidth / item.getPictureWidth();
                item.scalePicture(scale);
            }
        }
    }

    /**
     * Inserts the item's image into the workbook with the given anchor. (Optionally creates a label for the item if an
     * anchor is provided).
     * 
     * @param item
     * @param imgAnchor
     * @param labelAnchor
     *            (optional)
     */
    private void insertPicture(InvoiceItem item, HSSFClientAnchor imgAnchor, HSSFClientAnchor labelAnchor) {

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(item.getPicture(), item.getPictureEncoding(), baos);

            byte[] pictureData = baos.toByteArray();

            int picIdx;
            switch (item.getPictureEncoding().toLowerCase()) {
            case ("jpg"):
            case ("jpeg"):
                picIdx = book.addPicture(pictureData, HSSFPicture.PICTURE_TYPE_JPEG);
                break;
            case ("png"):
                picIdx = book.addPicture(pictureData, HSSFPicture.PICTURE_TYPE_PNG);
                break;
            case ("dib"):
                picIdx = book.addPicture(pictureData, HSSFPicture.PICTURE_TYPE_DIB);
                break;
            case ("emf"):
                picIdx = book.addPicture(pictureData, HSSFPicture.PICTURE_TYPE_EMF);
                break;
            case ("pict"):
                picIdx = book.addPicture(pictureData, HSSFPicture.PICTURE_TYPE_PICT);
                break;
            case ("wmf"):
                picIdx = book.addPicture(pictureData, HSSFPicture.PICTURE_TYPE_WMF);
                break;
            default:
                throw new IllegalStateException(
                        "Fatal error while processing images in workbook. No matching encoding, found: "
                                + item.getPictureEncoding());
            }

            sheet.getDrawingPatriarch().createPicture(imgAnchor, picIdx);

            if (labelAnchor != null) {
                HSSFTextbox label = sheet.getDrawingPatriarch().createTextbox(labelAnchor);
                label.setHorizontalAlignment(HSSFTextbox.HORIZONTAL_ALIGNMENT_CENTERED);
                label.setString(new HSSFRichTextString(item.getProduct().getProduct().getProductCode()));
            }

        } catch (IOException e) {
            logger.error("IO Exception during picture insertion");
        }
    }
}
