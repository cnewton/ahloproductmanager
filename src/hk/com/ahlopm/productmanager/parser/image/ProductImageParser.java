package hk.com.ahlopm.productmanager.parser.image;

import hk.com.ahlopm.productmanager.domainobject.productpicture.ProductPicture;
import hk.com.ahlopm.productmanager.exception.PictureMemberException;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.imgscalr.Scalr;

/**
 * The product image parser provides static methods for standard operations on BufferedImages
 * 
 * @author Cameron Newton
 *
 */
public class ProductImageParser {

    /**
     * Read the provided byte array directly into a new BufferedImage object.
     * 
     * @throws PictureMemberException
     */
    public static BufferedImage parseImageBytes(byte[] image) throws PictureMemberException {

        try {
            return ImageIO.read(new ByteArrayInputStream(image));
        } catch (IOException e) {
            throw new PictureMemberException("Unable to parse provided byte stream into image", e);
        }
    }

    /**
     * Read the provided byte array, discern its underlying format and store the verified byte array and format in a new
     * ProductPicture object to return.
     * 
     * @throws PictureMemberException
     */
    public static ProductPicture wrapImage(byte[] image) throws PictureMemberException {

        ProductPicture wrapper = new ProductPicture();
        try {
            ImageInputStream iis = ImageIO.createImageInputStream(new ByteArrayInputStream(image));
            Iterator<ImageReader> readers = ImageIO.getImageReaders(iis);
            if (readers.hasNext()) {
                ImageReader reader = readers.next();
                reader.setInput(iis);
                wrapper.setEncodeType(reader.getFormatName());

                BufferedImage img = reader.read(0);
                wrapper.setPicture(parseBufferedImage(img, wrapper.getEncodeType()));
                return wrapper;
            } else {
                throw new PictureMemberException("Picture is not in a valid readable format!");
            }
        } catch (IOException e) {
            throw new PictureMemberException("Unable to parse provided byte stream into image", e);
        }
    }

    /**
     * Write the provided BufferedImage into a byte array with the supplied format.
     * 
     * @throws PictureMemberException
     */
    public static byte[] parseBufferedImage(BufferedImage image, String format) throws PictureMemberException {

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(image, format, baos);
            return baos.toByteArray();
        } catch (IOException e) {
            throw new PictureMemberException("Unable to parse image into byte stream", e);
        }
    }

    /**
     * Return a new scaled instance of the supplied image where both dimensions are scaled by the provided factor.
     */
    public static BufferedImage scaleImage(BufferedImage image, double scale) {

        int targetHeight = Double.valueOf(image.getHeight() * scale).intValue();
        int targetWidth = Double.valueOf(image.getWidth() * scale).intValue();
        return Scalr.resize(image, targetWidth, targetHeight);
    }

    /**
     * Return a new scaled instance of the supplied image where dimensions are scaled by their respective provided
     * factor.
     */
    public static BufferedImage scaleImage(BufferedImage image, double scaleX, double scaleY) {

        int targetHeight = Double.valueOf(image.getHeight() * scaleY).intValue();
        int targetWidth = Double.valueOf(image.getWidth() * scaleX).intValue();
        return Scalr.resize(image, targetWidth, targetHeight);
    }

}
