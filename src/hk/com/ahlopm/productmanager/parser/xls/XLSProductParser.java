package hk.com.ahlopm.productmanager.parser.xls;

import hk.com.ahlopm.productmanager.domainobject.product.Product;

import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.PictureData;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * The XLS Product Parser is designed to read product information and pictures out of a Excel (XLS or XLSX) document.
 * This parser is not forgiving on the structure of the document. Documents must follow the format expected by the
 * parser as this parser is also designed to handle images it is overly difficult to design it to handle format
 * mistakes. Pictures must be ADDED to the xls document in the same order as products. Positioning of the pictures has
 * no bearing on which product they will be associated with. Each product row must also specify how many pictures belong
 * to that product.
 * 
 * @author Cameron Newton
 *
 */
public class XLSProductParser {

    protected static final Logger logger = LogManager.getLogger(XLSProductParser.class);

    private Workbook book;

    private Iterator<Row> rowIter;

    private int numPics;

    private int currentPicture;

    private DataFormatter df;

    public XLSProductParser(byte[] xlsFile) {

        try {
            book = new XSSFWorkbook(new ByteArrayInputStream(xlsFile));
        } catch (Exception e) {
            logger.warn("Uploaded workbook is not XSSF compatible", e);
            try {
                book = new HSSFWorkbook(new ByteArrayInputStream(xlsFile));
            } catch (Exception e2) {
                logger.error("Uploaded workbook is not HSSF compatible", e2);
                throw new IllegalStateException("Unable to Parse workbook");
            }
        }
        numPics = 0;
        currentPicture = 0;
        df = new DataFormatter();

        validateStructure();
        rowIter = book.getSheetAt(0).iterator();
        rowIter.next(); // Drop header row.
    }

    public Product parseProductRow() {

        currentPicture += numPics;
        Product product = null;
        if (rowIter.hasNext()) {
            product = new Product();

            Iterator<Cell> iter = rowIter.next().cellIterator();

            product.setProductCode(df.formatCellValue(iter.next()));
            product.setDescription(df.formatCellValue(iter.next()));
            product.setFinish(df.formatCellValue(iter.next()));
            product.setStyle(df.formatCellValue(iter.next()));
            product.setPrice(BigDecimal.valueOf(Double.valueOf(df.formatCellValue(iter.next()))));
            product.setQuantity(df.formatCellValue(iter.next()));
            product.setStockPosition(df.formatCellValue(iter.next()));
            this.numPics = Integer.valueOf(df.formatCellValue(iter.next()));
        }
        return product;
    }

    public List<PictureData> parseProductPictures() {

        List<PictureData> pictures = new ArrayList<PictureData>(numPics);

        for (int i = currentPicture; i < (currentPicture + numPics); i++) {
            pictures.add(book.getAllPictures().get(i));
        }

        return pictures;
    }

    public boolean eof() {

        return !rowIter.hasNext();
    }

    private void validateStructure() {

        Iterator<Row> rowIter = book.getSheetAt(0).iterator();

        Row row = rowIter.next(); // Ignore header row.
        int expectedCellCount = row.getPhysicalNumberOfCells();
        int expectedPictureCount = 0;

        while (rowIter.hasNext()) {
            row = rowIter.next();

            if (row.getPhysicalNumberOfCells() != expectedCellCount) {
                throw new IllegalStateException("There is a formatting problem in the XLS document, row: "
                        + (row.getRowNum() + 1) + "\nExpected " + expectedCellCount + " cells but found "
                        + row.getPhysicalNumberOfCells() + " cells.");
            }

            expectedPictureCount += Integer.valueOf(df.formatCellValue(row.getCell(row.getLastCellNum() - 1)));
        }

        if (expectedPictureCount != book.getAllPictures().size()) {
            throw new IllegalStateException("There is a mismatch in the expected number of pictures, expected "
                    + expectedPictureCount + " but found " + book.getAllPictures().size());
        }
    }
}
