package hk.com.ahlopm.productmanager.service.quotation;

import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.service.DomainObjectService;

import java.math.BigDecimal;
import java.util.List;

/**
 * The QuotationService is responsible for all manipulations and all logic regarding the Quotation domain object.
 * 
 * @author Cameron Newton
 *
 */
public interface QuotationService extends DomainObjectService<Quotation> {

    /**
     * Calculates and returns the raw price of a given quotation. The price is calculated by the cumulative prices and
     * discounts of each included product. The price is *NOT* persisted with this method, only returned.
     * 
     * @param quoteId
     * @return price
     */
    public BigDecimal calculatePrice(long quoteId);

    /**
     * Exposes the DAO method that returns all Quotations in the REVIEW State
     * 
     * @return list of all quotations in review
     */
    public List<Quotation> findAllInReview();

    /**
     * Exposes the DAO method that returns all Quotations in management states. States include: Presenting, Accepted,
     * Declined or Finalizing.
     * 
     * @return list of all quotations in management states
     */
    public List<Quotation> findAllInManagement();

}
