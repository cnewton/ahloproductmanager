package hk.com.ahlopm.productmanager.service.quotation;

import hk.com.ahlopm.productmanager.dao.quotation.QuotationDAO;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;
import hk.com.ahlopm.productmanager.exception.NotCreateOperationException;
import hk.com.ahlopm.productmanager.exception.NotUpdateOperationException;
import hk.com.ahlopm.productmanager.exception.ObjectNotFoundException;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class QuotationServiceImpl implements QuotationService {

    protected static final Logger logger = LogManager.getLogger(QuotationServiceImpl.class);

    @Autowired
    private QuotationDAO quotationDAO;

    @Override
    @Transactional
    public Quotation find(long id) {

        Quotation quotation = quotationDAO.find(id);

        if (quotation == null) {
            throw new ObjectNotFoundException("No Quotation with id: " + id + " exists.");
        }

        // Pull lazy loaded products.
        quotation.getProducts().size();

        return quotation;
    }

    @Override
    @Transactional
    public List<Quotation> findAll() {

        return quotationDAO.findAll();
    }

    @Override
    @Transactional
    public List<Quotation> findAllInReview() {

        return quotationDAO.findAllInReview();
    }

    @Override
    @Transactional
    public List<Quotation> findAllInManagement() {

        return quotationDAO.findAllInManagement();
    }

    @Override
    @Transactional
    public Quotation create(Quotation obj) {

        if (obj.getId() != 0) {
            throw new NotCreateOperationException("The identifier was not empty, not a create operation.");
        }

        obj.setLastModified(LocalDateTime.now());
        quotationDAO.createOrUpdate(obj);
        return obj;
    }

    @Override
    @Transactional
    public Quotation update(Quotation obj) {

        if (obj.getId() == 0) {
            throw new NotUpdateOperationException("The identifier was not set, not an update operation.");
        }

        if (!quotationDAO.idExists(obj.getId())) {
            throw new ObjectNotFoundException("No Quotation with id: " + obj.getId() + " exists.");
        }
        obj.setLastModified(LocalDateTime.now());
        quotationDAO.createOrUpdate(obj);

        return obj;
    }

    @Override
    @Transactional
    public void delete(Quotation obj) {

        quotationDAO.delete(find(obj.getId()));
    }

    @Override
    @Transactional
    public void delete(long id) {

        find(id);
        quotationDAO.delete(id);
    }

    @Override
    @Transactional
    public long count() {

        return quotationDAO.count();
    }

    @Override
    @Transactional
    public BigDecimal calculatePrice(long quoteId) {

        Quotation quote = find(quoteId);

        BigDecimal price = BigDecimal.ZERO;

        for (QuoteProduct qp : quote.getProducts()) {
            BigDecimal productPrice = BigDecimal.ZERO;

            productPrice = productPrice.add(qp.getProduct().getPrice());
            productPrice = productPrice.subtract(qp.getDiscount());
            productPrice = productPrice.multiply(BigDecimal.valueOf(qp.getQuantity()));

            price = price.add(productPrice);
        }

        return price;
    }
}
