package hk.com.ahlopm.productmanager.service.invoice;

import hk.com.ahlopm.productmanager.domainobject.invoice.Invoice;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.exception.SerializedQuotationPersistenceException;

import java.io.IOException;

public interface InvoiceService {

    public Invoice generateInvoice(Quotation quote) throws IOException;

    public Invoice buildFromRelationalQuotation(long quotationId) throws IOException;

    public Invoice buildFromSerializedQuotation(long serializedQuotationId)
            throws IOException, SerializedQuotationPersistenceException;
}
