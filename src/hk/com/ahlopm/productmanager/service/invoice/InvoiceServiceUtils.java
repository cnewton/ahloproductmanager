package hk.com.ahlopm.productmanager.service.invoice;

import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class InvoiceServiceUtils {

    public static List<List<QuoteProduct>> separateAndSortProducts(Collection<QuoteProduct> products) {

        List<List<QuoteProduct>> separated = separateProducts(products);
        List<Collection<QuoteProduct>> typeFix = new ArrayList<Collection<QuoteProduct>>(separated);
        return sortProducts(typeFix);
    }

    public static List<List<QuoteProduct>> sortProducts(Collection<Collection<QuoteProduct>> products) {

        List<Collection<QuoteProduct>> codeSorted = new ArrayList<Collection<QuoteProduct>>();

        for (Collection<QuoteProduct> subList : products) {
            codeSorted.add(sortProductsByCode(subList));
        }

        return sortCollectionsBySize(codeSorted);
    }

    public static List<QuoteProduct> sortProductsByCode(Collection<QuoteProduct> products) {

        List<QuoteProduct> list = new ArrayList<QuoteProduct>(products);

        Collections.sort(list,
                (c1, c2) -> c1.getProduct().getProductCode().compareTo(c2.getProduct().getProductCode()));

        return list;
    }

    public static List<List<QuoteProduct>> sortCollectionsBySize(Collection<Collection<QuoteProduct>> lists) {

        List<List<QuoteProduct>> sortedLists = new ArrayList<List<QuoteProduct>>();
        for (Collection<QuoteProduct> sublist : lists) {
            sortedLists.add(new ArrayList<QuoteProduct>(sublist));
        }

        Collections.sort(sortedLists, (l1, l2) -> l2.size() - l1.size());

        return sortedLists;
    }

    public static List<List<QuoteProduct>> separateProducts(Collection<QuoteProduct> products) {

        List<List<QuoteProduct>> separatedProducts = new ArrayList<List<QuoteProduct>>();
        for (List<QuoteProduct> list : separateProductsByStyle(products)) {
            separatedProducts.addAll(separateProductsByFinish(list));
        }
        return separatedProducts;
    }

    public static List<List<QuoteProduct>> separateProductsByStyle(Collection<QuoteProduct> products) {

        List<List<QuoteProduct>> productLists = new ArrayList<List<QuoteProduct>>();

        while (products.size() > 0) {

            List<QuoteProduct> currentList = new ArrayList<QuoteProduct>();
            Iterator<QuoteProduct> iter = products.iterator();
            String currentStyle = null;

            while (iter.hasNext()) {
                QuoteProduct qp = iter.next();

                if (qp.getProduct().getStyle().equals(currentStyle)) {
                    currentList.add(qp);
                    iter.remove();

                } else if (currentStyle == null) {
                    currentStyle = qp.getProduct().getStyle();
                    currentList.add(qp);
                    iter.remove();
                }
            }
            productLists.add(currentList);
        }
        return productLists;
    }

    public static List<List<QuoteProduct>> separateProductsByFinish(Collection<QuoteProduct> products) {

        List<List<QuoteProduct>> productLists = new ArrayList<List<QuoteProduct>>();

        while (products.size() > 0) {

            List<QuoteProduct> currentList = new ArrayList<QuoteProduct>();
            Iterator<QuoteProduct> iter = products.iterator();
            String currentFinish = null;

            while (iter.hasNext()) {
                QuoteProduct qp = iter.next();

                if (qp.getProduct().getFinish().equals(currentFinish)) {
                    currentList.add(qp);
                    iter.remove();

                } else if (currentFinish == null) {
                    currentFinish = qp.getProduct().getFinish();
                    currentList.add(qp);
                    iter.remove();
                }
            }
            productLists.add(currentList);
        }
        return productLists;
    }

}
