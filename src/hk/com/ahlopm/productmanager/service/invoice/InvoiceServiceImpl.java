package hk.com.ahlopm.productmanager.service.invoice;

import hk.com.ahlopm.productmanager.builder.invoice.InvoiceBuilder;
import hk.com.ahlopm.productmanager.domainobject.invoice.Invoice;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;
import hk.com.ahlopm.productmanager.exception.ResolutionFailureException;
import hk.com.ahlopm.productmanager.exception.SerializedQuotationPersistenceException;
import hk.com.ahlopm.productmanager.service.quotation.QuotationService;
import hk.com.ahlopm.productmanager.service.serializedquotation.SerializedQuotationService;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InvoiceServiceImpl implements InvoiceService {

    protected static final Logger logger = LogManager.getLogger(InvoiceServiceImpl.class);

    @Autowired
    private QuotationService quotationService;

    @Autowired
    private SerializedQuotationService serializedQuotationService;

    @Override
    public Invoice generateInvoice(Quotation quote) throws IOException {

        InvoiceBuilder builder = new InvoiceBuilder(quote);

        builder.addPageHeader();

        for (List<QuoteProduct> productBlock : InvoiceServiceUtils.separateAndSortProducts(quote.getProducts())) {
            // Resolve each of the images in the product block
            for (QuoteProduct qp : productBlock) {
                if (qp.getProduct().getPrimaryPicture() != null) {
                    try {
                        qp.getProduct().getPrimaryPicture().resolveImage();
                    } catch (ResolutionFailureException e) {
                        logger.warn(e);
                    }
                }
            }

            builder.addProductBlock(productBlock);

            // Free memory.
            for (QuoteProduct qp : productBlock) {
                qp.setProduct(null);
            }
        }

        builder.addPageFooter();

        return builder.build();
    }

    @Override
    public Invoice buildFromRelationalQuotation(long quotationId) throws IOException {

        Quotation quote = quotationService.find(quotationId);
        return generateInvoice(quote);
    }

    @Override
    public Invoice buildFromSerializedQuotation(long serializedQuotationId)
            throws IOException, SerializedQuotationPersistenceException {

        Quotation quote = serializedQuotationService.deserializeQuotation(serializedQuotationId);
        return generateInvoice(quote);
    }

}
