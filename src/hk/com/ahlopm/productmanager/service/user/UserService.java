package hk.com.ahlopm.productmanager.service.user;

import hk.com.ahlopm.productmanager.domainobject.user.User;
import hk.com.ahlopm.productmanager.service.DomainObjectService;

/**
 * The UserService is responsible for all manipulations and all logic regarding the User domain object.
 * 
 * @author Cameron Newton
 *
 */
public interface UserService extends DomainObjectService<User> {

    public User findByUsername(String username);

    public User updateUserDetails(User user);

    public void updateUserPassword(long id, String oldPassword, String newPassword);

    public void forceUpdateUserPassword(long id, String newPassword);

}
