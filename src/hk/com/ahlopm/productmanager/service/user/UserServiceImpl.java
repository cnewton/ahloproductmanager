package hk.com.ahlopm.productmanager.service.user;

import hk.com.ahlopm.productmanager.dao.user.UserDAO;
import hk.com.ahlopm.productmanager.domainobject.user.User;
import hk.com.ahlopm.productmanager.exception.CustomSecurityException;
import hk.com.ahlopm.productmanager.exception.NotCreateOperationException;
import hk.com.ahlopm.productmanager.exception.NotUpdateOperationException;
import hk.com.ahlopm.productmanager.exception.ObjectNotFoundException;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {

    protected static final Logger logger = LogManager.getLogger(UserServiceImpl.class);

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public User find(long id) {

        User user = userDAO.find(id);

        if (user == null) {
            throw new ObjectNotFoundException("No User with id: " + id + " exists.");
        }
        return user;
    }

    @Override
    @Transactional
    public List<User> findAll() {

        return userDAO.findAll();
    }

    @Override
    @Transactional
    public User create(User obj) {

        if (obj.getId() != 0) {
            throw new NotCreateOperationException("The identifier was not empty, not a create operation.");
        }
        if (obj.getUsername() == null || obj.getUsername().equals("")) {
            throw new NotCreateOperationException("The username was empty.");
        }
        obj.setPassword(passwordEncoder.encode(obj.getPassword()));
        userDAO.createOrUpdate(obj);
        return obj;
    }

    @Override
    @Transactional
    public User update(User obj) {

        if (obj.getId() == 0) {
            throw new NotUpdateOperationException("The identifier was not set, not an update operation.");
        }

        if (!userDAO.idExists(obj.getId())) {
            throw new ObjectNotFoundException("No User with id: " + obj.getId() + " exists.");
        }

        userDAO.createOrUpdate(obj);

        return obj;
    }

    @Override
    @Transactional
    public void delete(User obj) {

        userDAO.delete(find(obj.getId()));
    }

    @Override
    @Transactional
    public void delete(long id) {

        userDAO.delete(find(id));
    }

    @Override
    @Transactional
    public long count() {

        return userDAO.count();
    }

    @Override
    @Transactional
    public User findByUsername(String username) {

        User user = userDAO.findByUsername(username);

        if (user == null) {
            throw new ObjectNotFoundException("No User with username: " + username + " exists.");
        }
        return user;
    }

    @Override
    @Transactional
    public User updateUserDetails(User user) {

        if (user.getId() == 0) {
            throw new NotUpdateOperationException("The identifier was not set, not an update operation.");
        }

        User dbUser = find(user.getId());
        dbUser.setEmail(user.getEmail());
        dbUser.setEnabled(user.isEnabled());
        dbUser.setName(user.getName());
        update(dbUser);

        return dbUser;
    }

    @Override
    @Transactional
    public void updateUserPassword(long id, String oldPassword, String newPassword) {

        User user = find(id);

        if (!passwordEncoder.matches(oldPassword, user.getPassword())) {
            throw new CustomSecurityException("Incorrect old password.");
        }

        user.setPassword(passwordEncoder.encode(newPassword));
        update(user);

    }

    @Override
    @Transactional
    public void forceUpdateUserPassword(long id, String newPassword) {

        User user = find(id);
        user.setPassword(passwordEncoder.encode(newPassword));
        update(user);
    }
}
