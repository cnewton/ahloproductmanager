package hk.com.ahlopm.productmanager.service.configuration;

public interface ConfigurationService {

    public String get_INVALID_HEADER_NAME();

    public String get_AWS_S3_ACCESS_KEY();

    public String get_AWS_S3_SECRET_KEY();

    public String get_DEFAULT_S3_BUCKET_NAME();

    public String get_HMAC_SECRET_KEY();

    public String get_AUTH_HEADER_NAME();

    public long get_AUTH_TOKEN_LIFE_SECONDS();

    public void set_INVALID_HEADER_NAME(String config);

    public void set_AWS_S3_ACCESS_KEY(String config);

    public void set_AWS_S3_SECRET_KEY(String config);

    public void set_DEFAULT_S3_BUCKET_NAME(String config);

    public void set_HMAC_SECRET_KEY(String config);

    public void set_AUTH_TOKEN_LIFE_SECONDS(long config);

    public void set_AUTH_HEADER_NAME(String config);

    public void update_ALL_CONFIGURATIONS();

}
