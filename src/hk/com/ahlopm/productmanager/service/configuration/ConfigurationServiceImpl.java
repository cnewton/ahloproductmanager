package hk.com.ahlopm.productmanager.service.configuration;

import hk.com.ahlopm.productmanager.dao.configuration.ConfigurationDAO;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ConfigurationServiceImpl implements ConfigurationService {

    protected static final Logger logger = LogManager.getLogger(ConfigurationServiceImpl.class);

    @Autowired
    private ConfigurationDAO configurationDAO;

    private volatile String cfg_INVALID_HEADER_NAME;

    private volatile String cfg_AWS_S3_ACCESS_KEY;

    private volatile String cfg_AWS_S3_SECRET_KEY;

    private volatile String cfg_DEFAULT_S3_BUCKET_NAME;

    private volatile String cfg_HMAC_SECRET_KEY;

    private volatile String cfg_AUTH_TOKEN_LIFE_SECONDS;

    private volatile String cfg_AUTH_HEADER_NAME;

    @Override
    @Transactional
    public String get_INVALID_HEADER_NAME() {

        if (cfg_INVALID_HEADER_NAME == null) {
            cfg_INVALID_HEADER_NAME = configurationDAO.getConfigSetting("INVALID_HEADER_NAME");
        }
        return cfg_INVALID_HEADER_NAME;
    }

    @Override
    @Transactional
    public String get_AWS_S3_ACCESS_KEY() {

        if (cfg_AWS_S3_ACCESS_KEY == null) {
            cfg_AWS_S3_ACCESS_KEY = configurationDAO.getConfigSetting("AWS_S3_ACCESS_KEY");
        }
        return cfg_AWS_S3_ACCESS_KEY;
    }

    @Override
    @Transactional
    public String get_AWS_S3_SECRET_KEY() {

        if (cfg_AWS_S3_SECRET_KEY == null) {
            cfg_AWS_S3_SECRET_KEY = configurationDAO.getConfigSetting("AWS_S3_SECRET_KEY");
        }
        return cfg_AWS_S3_SECRET_KEY;
    }

    @Override
    @Transactional
    public String get_DEFAULT_S3_BUCKET_NAME() {

        if (cfg_DEFAULT_S3_BUCKET_NAME == null) {
            cfg_DEFAULT_S3_BUCKET_NAME = configurationDAO.getConfigSetting("DEFAULT_S3_BUCKET_NAME");
        }
        return cfg_DEFAULT_S3_BUCKET_NAME;
    }

    @Override
    @Transactional
    public String get_HMAC_SECRET_KEY() {

        if (cfg_HMAC_SECRET_KEY == null) {
            cfg_HMAC_SECRET_KEY = configurationDAO.getConfigSetting("HMAC_SECRET_KEY");
        }
        return cfg_HMAC_SECRET_KEY;
    }

    @Override
    @Transactional
    public long get_AUTH_TOKEN_LIFE_SECONDS() {

        if (cfg_AUTH_TOKEN_LIFE_SECONDS == null) {
            cfg_AUTH_TOKEN_LIFE_SECONDS = configurationDAO.getConfigSetting("AUTH_TOKEN_LIFE_SECONDS");
        }
        return Long.valueOf(cfg_AUTH_TOKEN_LIFE_SECONDS);
    }

    @Override
    @Transactional
    public String get_AUTH_HEADER_NAME() {

        if (cfg_AUTH_HEADER_NAME == null) {
            cfg_AUTH_HEADER_NAME = configurationDAO.getConfigSetting("AUTH_HEADER_NAME");
        }
        return cfg_AUTH_HEADER_NAME;
    }

    @Override
    @Transactional
    public void set_INVALID_HEADER_NAME(String config) {

        configurationDAO.setConfigSetting("INVALID_HEADER_NAME", config);

        this.cfg_INVALID_HEADER_NAME = config;
    }

    @Override
    @Transactional
    public void set_AWS_S3_ACCESS_KEY(String config) {

        configurationDAO.setConfigSetting("AWS_S3_ACCESS_KEY", config);

        this.cfg_AWS_S3_ACCESS_KEY = config;
    }

    @Override
    @Transactional
    public void set_AWS_S3_SECRET_KEY(String config) {

        configurationDAO.setConfigSetting("AWS_S3_SECRET_KEY", config);

        this.cfg_AWS_S3_SECRET_KEY = config;
    }

    @Override
    @Transactional
    public void set_DEFAULT_S3_BUCKET_NAME(String config) {

        configurationDAO.setConfigSetting("DEFAULT_S3_BUCKET_NAME", config);

        this.cfg_DEFAULT_S3_BUCKET_NAME = config;
    }

    @Override
    @Transactional
    public void set_HMAC_SECRET_KEY(String config) {

        configurationDAO.setConfigSetting("HMAC_SECRET_KEY", config);

        this.cfg_HMAC_SECRET_KEY = config;
    }

    @Override
    @Transactional
    public void set_AUTH_TOKEN_LIFE_SECONDS(long config) {

        configurationDAO.setConfigSetting("AUTH_TOKEN_LIFE_SECONDS", String.valueOf(config));

        this.cfg_AUTH_TOKEN_LIFE_SECONDS = String.valueOf(config);
    }

    @Override
    @Transactional
    public void set_AUTH_HEADER_NAME(String config) {

        configurationDAO.setConfigSetting("AUTH_HEADER_NAME", config);

        this.cfg_AUTH_HEADER_NAME = config;
    }

    @Override
    public void update_ALL_CONFIGURATIONS() {

        // TODO Auto-generated method stub

    }

}
