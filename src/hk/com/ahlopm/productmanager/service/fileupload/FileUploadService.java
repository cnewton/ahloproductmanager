package hk.com.ahlopm.productmanager.service.fileupload;

import org.springframework.web.multipart.MultipartFile;

public interface FileUploadService {

    public void parseProductsXLSFile(MultipartFile file);

    public void uploadProductImage(long productId, MultipartFile file);

}
