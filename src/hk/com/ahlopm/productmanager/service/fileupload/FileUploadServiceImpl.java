package hk.com.ahlopm.productmanager.service.fileupload;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.PictureData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.domainobject.productpicture.DatabaseProductPicture;
import hk.com.ahlopm.productmanager.exception.FileUploadException;
import hk.com.ahlopm.productmanager.exception.PictureMemberException;
import hk.com.ahlopm.productmanager.parser.image.ProductImageParser;
import hk.com.ahlopm.productmanager.parser.xls.XLSProductParser;
import hk.com.ahlopm.productmanager.service.product.ProductService;
import hk.com.ahlopm.productmanager.service.productpicture.ProductPictureService;

@Service
public class FileUploadServiceImpl implements FileUploadService {

    protected static final Logger logger = LogManager.getLogger(FileUploadServiceImpl.class);

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductPictureService productPictureService;

    @Override
    public void parseProductsXLSFile(MultipartFile file) throws FileUploadException {

        try {
            byte[] book = file.getBytes();

            XLSProductParser parser = new XLSProductParser(book);

            List<Product> products = new ArrayList<>();
            List<DatabaseProductPicture> pictures = new ArrayList<>();
            while (!parser.eof()) {
                Product product = parser.parseProductRow();
                products.add(product);
                for (PictureData picData : parser.parseProductPictures()) {
                    try {
                        DatabaseProductPicture pic = new DatabaseProductPicture(
                                ProductImageParser.wrapImage(picData.getData()));
                        pic.setProduct(product);
                        pictures.add(pic);
                    } catch (PictureMemberException e) {
                        logger.warn("Error with picture included with product " + product.getProductCode(), e);
                    }
                }
            }
            productService.create(products);
            productPictureService.constructDatabaseProductPicture(pictures);
        } catch (Exception e) {
            logger.error("Corruption in XLS File, parsing failed.", e);
            throw new FileUploadException("Corrupt XLS products file\n" + e.getMessage(), e);
        }
    }

    @Override
    public void uploadProductImage(long productId, MultipartFile file) {

        try {
            if (!file.getContentType().contains("image/")) {
                throw new FileUploadException("Uploaded file must be of type IMAGE");
            }
            byte[] imageData = file.getBytes();

            Product product = productService.find(productId);

            DatabaseProductPicture pic = new DatabaseProductPicture(ProductImageParser.wrapImage(imageData));
            pic.setProduct(product);

            productPictureService.constructDatabaseProductPicture(pic);
        } catch (PictureMemberException | IOException e) {
            throw new FileUploadException("Picture creation error.", e);
        }
    }

}
