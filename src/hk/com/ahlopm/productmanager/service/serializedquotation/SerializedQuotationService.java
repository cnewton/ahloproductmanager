package hk.com.ahlopm.productmanager.service.serializedquotation;

import java.util.List;

import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.serializedquotation.SerializedQuotation;
import hk.com.ahlopm.productmanager.exception.SerializedQuotationPersistenceException;
import hk.com.ahlopm.productmanager.service.DomainObjectService;

/**
 * The SerializedQuotationService is responsible for all manipulations and all logic regarding the SerializedQuotation
 * domain object.
 * 
 * @author Cameron Newton
 *
 */
public interface SerializedQuotationService extends DomainObjectService<SerializedQuotation> {

    /**
     * Method used to create a serialized quotation blob object from a relational quotation object.
     * 
     * @param quotationId
     * @return
     * @throws SerializedQuotationPersistenceException
     */
    public SerializedQuotation serializeQuotation(long quotationId) throws SerializedQuotationPersistenceException;

    /**
     * Method used to construct a quotation object from a given serialized blob object
     * 
     * @param serializedQuotationId
     * @return
     * @throws SerializedQuotationPersistenceException
     */
    public Quotation deserializeQuotation(long serializedQuotationId) throws SerializedQuotationPersistenceException;

    /**
     * Returns a list of Quotation objects stored in the SerializedQuotation table, each entry is reconstructed into the
     * mapped Quotation object.
     * 
     * @return list of Quotations
     */
    public List<Quotation> getAllDeserializedQuotations();

    public List<SerializedQuotation> findAllByQuotation(long quotationId);

    public SerializedQuotation findPresentingByQuotation(long quotationId);

}
