package hk.com.ahlopm.productmanager.service.serializedquotation;

import hk.com.ahlopm.productmanager.dao.serializedquotation.SerializedQuotationDAO;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.serializedquotation.SerializedQuotation;
import hk.com.ahlopm.productmanager.exception.NotCreateOperationException;
import hk.com.ahlopm.productmanager.exception.NotUpdateOperationException;
import hk.com.ahlopm.productmanager.exception.ObjectNotFoundException;
import hk.com.ahlopm.productmanager.exception.SerializedQuotationPersistenceException;
import hk.com.ahlopm.productmanager.service.quotation.QuotationService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class SerializedQuotationServiceImpl implements SerializedQuotationService {

    protected static final Logger logger = LogManager.getLogger(SerializedQuotationServiceImpl.class);

    @Autowired
    private SerializedQuotationDAO serializedQuotationDAO;

    @Autowired
    private ObjectMapper jsonMapper;

    @Autowired
    private QuotationService quotationService;

    @Override
    @Transactional
    public SerializedQuotation find(long id) {

        SerializedQuotation serializedQuotation = serializedQuotationDAO.find(id);

        if (serializedQuotation == null) {
            throw new ObjectNotFoundException("No SerializedQuotation with id: " + id + " exists.");
        }

        return serializedQuotation;
    }

    @Override
    @Transactional
    public SerializedQuotation findPresentingByQuotation(long quotationId) {

        SerializedQuotation serializedQuotation = serializedQuotationDAO.findPresentingByQuotationId(quotationId);

        if (serializedQuotation == null) {
            throw new ObjectNotFoundException("No SerializedQuotation belonging to quotation: " + quotationId
                    + " exists in the presenting state");
        }

        return serializedQuotation;
    }

    @Override
    @Transactional
    public List<SerializedQuotation> findAll() {

        return serializedQuotationDAO.findAll();
    }

    @Override
    @Transactional
    public List<SerializedQuotation> findAllByQuotation(long quotationId) {

        return serializedQuotationDAO.findByQuotationId(quotationId);
    }

    @Override
    @Transactional
    public SerializedQuotation create(SerializedQuotation obj) {

        if (obj.getId() != 0) {
            throw new NotCreateOperationException("The identifier was not empty, not a create operation.");
        }
        serializedQuotationDAO.createOrUpdate(obj);
        return obj;
    }

    @Override
    @Transactional
    public SerializedQuotation update(SerializedQuotation obj) {

        if (obj.getId() == 0) {
            throw new NotUpdateOperationException("The identifier was not set, not an update operation.");
        }

        if (!serializedQuotationDAO.idExists(obj.getId())) {
            throw new ObjectNotFoundException("No SerializedQuotation with id: " + obj.getId() + " exists.");
        }

        serializedQuotationDAO.createOrUpdate(obj);

        return obj;
    }

    @Override
    @Transactional
    public void delete(SerializedQuotation obj) {

        serializedQuotationDAO.delete(find(obj.getId()));
    }

    @Override
    @Transactional
    public void delete(long id) {

        find(id);
        serializedQuotationDAO.delete(id);
    }

    @Override
    @Transactional
    public long count() {

        return serializedQuotationDAO.count();
    }

    @Override
    @Transactional
    public SerializedQuotation serializeQuotation(long quotationId) throws SerializedQuotationPersistenceException {

        Quotation quote = quotationService.find(quotationId);

        SerializedQuotation sQuote = new SerializedQuotation();
        sQuote.setQuotationId(quote.getId());
        sQuote.setStatus(quote.getStatus());

        try {
            sQuote.setQuotation(jsonMapper.writeValueAsString(quote));
        } catch (IOException e) {
            throw new SerializedQuotationPersistenceException(
                    "Unable to serialize the given quotation object: " + quote.getId());
        }

        serializedQuotationDAO.createOrUpdate(sQuote);

        return sQuote;
    }

    @Override
    @Transactional
    public Quotation deserializeQuotation(long serializedQuotationId) throws SerializedQuotationPersistenceException {

        SerializedQuotation sQuote = find(serializedQuotationId);
        Quotation quote;
        try {
            quote = jsonMapper.readValue(sQuote.getQuotation(), Quotation.class);
        } catch (IOException e) {
            throw new SerializedQuotationPersistenceException(
                    "Unable to deserialize the given blob object: " + sQuote.getId());
        }
        return quote;
    }

    @Override
    @Transactional
    public List<Quotation> getAllDeserializedQuotations() {

        List<Quotation> quotes = new ArrayList<Quotation>();

        for (SerializedQuotation sq : findAll()) {
            try {
                quotes.add(jsonMapper.readValue(sq.getQuotation(), Quotation.class));
            } catch (IOException e) {
                logger.error("Unable to deserialize SerializedQuotation: " + sq.getId());
            }
        }

        return quotes;
    }
}
