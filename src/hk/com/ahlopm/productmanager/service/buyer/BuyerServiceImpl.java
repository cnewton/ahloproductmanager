package hk.com.ahlopm.productmanager.service.buyer;

import hk.com.ahlopm.productmanager.dao.buyer.BuyerDAO;
import hk.com.ahlopm.productmanager.domainobject.buyer.Buyer;
import hk.com.ahlopm.productmanager.exception.NotCreateOperationException;
import hk.com.ahlopm.productmanager.exception.NotUpdateOperationException;
import hk.com.ahlopm.productmanager.exception.ObjectNotFoundException;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BuyerServiceImpl implements BuyerService {

    protected static final Logger logger = LogManager.getLogger(BuyerServiceImpl.class);

    @Autowired
    private BuyerDAO buyerDAO;

    @Override
    @Transactional
    public Buyer find(long id) {

        Buyer buyer = buyerDAO.find(id);

        if (buyer == null) {
            throw new ObjectNotFoundException("No Buyer with id: " + id + " exists.");
        }
        return buyer;
    }

    @Override
    @Transactional
    public List<Buyer> findAll() {

        return buyerDAO.findAll();
    }

    @Override
    @Transactional
    public Buyer create(Buyer obj) {

        if (obj.getId() != 0) {
            throw new NotCreateOperationException("The identifier was not empty, not a create operation.");
        }
        buyerDAO.createOrUpdate(obj);
        return obj;
    }

    @Override
    @Transactional
    public Buyer update(Buyer obj) {

        if (obj.getId() == 0) {
            throw new NotUpdateOperationException("The identifier was not set, not an update operation.");
        }

        if (!buyerDAO.idExists(obj.getId())) {
            throw new ObjectNotFoundException("No Buyer with id: " + obj.getId() + " exists.");
        }

        buyerDAO.createOrUpdate(obj);

        return obj;
    }

    @Override
    @Transactional
    public void delete(Buyer obj) {

        buyerDAO.delete(find(obj.getId()));
    }

    @Override
    @Transactional
    public void delete(long id) {

        buyerDAO.delete(find(id));
    }

    @Override
    @Transactional
    public long count() {

        return buyerDAO.count();
    }
}
