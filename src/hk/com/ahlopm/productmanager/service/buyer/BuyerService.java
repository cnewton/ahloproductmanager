package hk.com.ahlopm.productmanager.service.buyer;

import hk.com.ahlopm.productmanager.domainobject.buyer.Buyer;
import hk.com.ahlopm.productmanager.service.DomainObjectService;

/**
 * The BuyerService is responsible for all manipulations and all logic regarding the Buyer domain object.
 * 
 * @author Cameron Newton
 *
 */
public interface BuyerService extends DomainObjectService<Buyer> {

}
