package hk.com.ahlopm.productmanager.service.product;

import hk.com.ahlopm.productmanager.dao.product.ProductDAO;
import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.domainobject.productpicture.ProductPicture;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;
import hk.com.ahlopm.productmanager.exception.NotCreateOperationException;
import hk.com.ahlopm.productmanager.exception.NotUpdateOperationException;
import hk.com.ahlopm.productmanager.exception.ObjectNotFoundException;
import hk.com.ahlopm.productmanager.service.quotation.QuotationService;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductServiceImpl implements ProductService {

    protected static final Logger logger = LogManager.getLogger(ProductServiceImpl.class);

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private QuotationService quotationService;

    @Override
    @Transactional
    public Product find(long id) {

        Product product = productDAO.find(id);

        if (product == null) {
            throw new ObjectNotFoundException("No Product with id: " + id + " exists.");
        }
        return product;
    }

    @Override
    @Transactional
    public List<Product> findAll() {

        return productDAO.findAll();
    }

    @Override
    @Transactional
    public Product create(Product obj) {

        if (obj.getId() != 0) {
            throw new NotCreateOperationException("The identifier was not empty, not a create operation.");
        }
        productDAO.createOrUpdate(obj);
        return obj;
    }

    @Override
    @Transactional
    public void create(List<Product> products) {

        for (Product product : products) {
            create(product);
        }
    }

    @Override
    @Transactional
    public Product update(Product obj) {

        if (obj.getId() == 0) {
            throw new NotUpdateOperationException("The identifier was not set, not an update operation.");
        }

        if (!productDAO.idExists(obj.getId())) {
            throw new ObjectNotFoundException("No Product with id: " + obj.getId() + " exists.");
        }

        productDAO.createOrUpdate(obj);

        return obj;
    }

    @Override
    @Transactional
    public void delete(Product obj) {

        productDAO.delete(find(obj.getId()));
    }

    @Override
    @Transactional
    public void delete(long id) {

        productDAO.delete(find(id));
    }

    @Override
    @Transactional
    public long count() {

        return productDAO.count();
    }

    @Override
    @Transactional
    public List<Product> getAvailableProductsForQuotation(long quotationId) {

        Quotation quote = quotationService.find(quotationId);
        List<Product> products = findAll();

        Iterator<Product> iter = products.iterator();

        while (iter.hasNext()) {
            Product p = iter.next();
            for (QuoteProduct qp : quote.getProducts()) {
                if (p.getId() == qp.getProduct().getId()) {
                    iter.remove();
                    break;
                }
            }
        }
        return products;
    }

    @Override
    @Transactional
    public long[] getImageIdsForProduct(long productId) {

        Product prod = find(productId);

        long[] pictureIds = new long[prod.getPictures().size()];

        if (prod.getPictures().size() > 0) {

            pictureIds[0] = prod.getPrimaryPicture().getId();

            int ii = 1;
            for (ProductPicture pic : prod.getPictures()) {

                if (pic.getId() != pictureIds[0]) {
                    pictureIds[ii] = pic.getId();
                    ii++;
                }
            }

        }
        return pictureIds;
    }
}
