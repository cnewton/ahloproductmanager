package hk.com.ahlopm.productmanager.service.product;

import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.service.DomainObjectService;

import java.util.List;

/**
 * The ProductService is responsible for all manipulations and all logic regarding the Product domain object.
 * 
 * @author Cameron Newton
 *
 */
public interface ProductService extends DomainObjectService<Product> {

    /**
     * Returns a list of products that are not marked as archived and are not already being used by the given quotation.
     * 
     * @param quotationId
     * @return list of products that may be added to the given quotation.
     */
    public List<Product> getAvailableProductsForQuotation(long quotationId);

    /**
     * Returns a list of Id's to be used for retrieving the images for a given product.
     * 
     * @param productId
     * @return array of long id's
     */
    public long[] getImageIdsForProduct(long productId);

    /**
     * Extends the create method to accept a list of products instead of a single product.
     * 
     * @param products
     */
    public void create(List<Product> products);

}
