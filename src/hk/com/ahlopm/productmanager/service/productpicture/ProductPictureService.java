package hk.com.ahlopm.productmanager.service.productpicture;

import java.util.List;

import hk.com.ahlopm.productmanager.domainobject.productpicture.DatabaseProductPicture;
import hk.com.ahlopm.productmanager.domainobject.productpicture.ProductPicture;
import hk.com.ahlopm.productmanager.domainobject.productpicture.S3ProductPicture;
import hk.com.ahlopm.productmanager.exception.PictureMemberException;
import hk.com.ahlopm.productmanager.service.DomainObjectService;

/**
 * The ProductPictureService is responsible for all manipulations and all logic regarding the Product Picture domain
 * object.
 * 
 * @author Cameron Newton
 *
 */
public interface ProductPictureService extends DomainObjectService<ProductPicture> {

    public void setPrimaryPictureForProduct(long pictureId, long productId) throws PictureMemberException;

    public byte[] getResolvedPicture(long pictureId);

    public void constructS3ProductPicture(S3ProductPicture pic) throws PictureMemberException;

    public void constructS3ProductPicture(List<S3ProductPicture> pictures) throws PictureMemberException;

    public void constructDatabaseProductPicture(DatabaseProductPicture pic) throws PictureMemberException;

    public void constructDatabaseProductPicture(List<DatabaseProductPicture> pictures) throws PictureMemberException;
}
