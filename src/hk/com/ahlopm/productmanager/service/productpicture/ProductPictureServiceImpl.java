package hk.com.ahlopm.productmanager.service.productpicture;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import hk.com.ahlopm.productmanager.amazons3.AmazonS3Gateway;
import hk.com.ahlopm.productmanager.dao.productpicture.ProductPictureDAO;
import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.domainobject.productpicture.DatabaseProductPicture;
import hk.com.ahlopm.productmanager.domainobject.productpicture.ProductPicture;
import hk.com.ahlopm.productmanager.domainobject.productpicture.S3ProductPicture;
import hk.com.ahlopm.productmanager.exception.NotCreateOperationException;
import hk.com.ahlopm.productmanager.exception.NotUpdateOperationException;
import hk.com.ahlopm.productmanager.exception.ObjectNotFoundException;
import hk.com.ahlopm.productmanager.exception.PictureMemberException;
import hk.com.ahlopm.productmanager.exception.ResolutionFailureException;
import hk.com.ahlopm.productmanager.exception.S3ResolutionFailureException;
import hk.com.ahlopm.productmanager.service.product.ProductService;

@Service
public class ProductPictureServiceImpl implements ProductPictureService {

    protected static final Logger logger = LogManager.getLogger(ProductPictureServiceImpl.class);

    @Autowired
    private ProductPictureDAO productPictureDAO;

    @Autowired
    private ProductService productService;

    @Autowired
    private AmazonS3Gateway amazonS3;

    @Override
    @Transactional
    public ProductPicture find(long id) {

        ProductPicture obj = productPictureDAO.find(id);

        if (obj == null)
            throw new ObjectNotFoundException("No Object with id: " + id + " exists.");

        return obj;
    }

    @Override
    @Transactional
    public List<ProductPicture> findAll() {

        return productPictureDAO.findAll();
    }

    @Override
    @Transactional
    public ProductPicture create(ProductPicture obj) {

        if (obj.getId() != 0) {
            throw new NotCreateOperationException("The identifier was not empty, not a create operation.");
        }

        productPictureDAO.createOrUpdate(obj);

        return obj;
    }

    @Override
    @Transactional
    public ProductPicture update(ProductPicture obj) {

        if (obj.getId() == 0) {
            throw new NotUpdateOperationException("The identifier was not set, not an update operation.");
        }

        if (!productPictureDAO.idExists(obj.getId())) {
            throw new ObjectNotFoundException("No object with id: " + obj.getId() + " exists.");
        }

        productPictureDAO.createOrUpdate(obj);

        return obj;
    }

    @Override
    @Transactional
    public void delete(ProductPicture obj) {

        delete(obj.getId());
    }

    @Override
    @Transactional
    public void delete(long id) {

        ProductPicture persistent = find(id);
        Product product = persistent.getProduct();

        if (product != null) {
            product.getPictures().remove(persistent);

            if (product.getPrimaryPicture() == persistent && product.getPictures().size() > 0) {
                product.setPrimaryPicture(product.getPictures().iterator().next());
            } else if (product.getPrimaryPicture() == persistent) {
                product.setPrimaryPicture(null);
            }
        }
        productPictureDAO.delete(persistent);
    }

    @Override
    @Transactional
    public long count() {

        return productPictureDAO.count();
    }

    @Override
    @Transactional
    public void setPrimaryPictureForProduct(long pictureId, long productId) throws PictureMemberException {

        ProductPicture pic = find(pictureId);
        Product prod = productService.find(productId);

        if (prod.getPictures().contains(pic)) {
            prod.setPrimaryPicture(pic);
        } else {
            throw new PictureMemberException("The picture id: " + pic.getId() + " must be a member of product id: "
                    + prod.getId() + " before it can be set as the primary picture.");
        }
    }

    @Override
    @Transactional
    public byte[] getResolvedPicture(long pictureId) {

        ProductPicture pic = find(pictureId);
        try {
            pic.resolveImage();
        } catch (ResolutionFailureException e) {
            pic.setPicture(new byte[0]);
            logger.warn("Attempted to resolve an image: " + pic.getId() + " but failed.", e);
        }
        return pic.getPicture();

    }

    @Override
    @Transactional
    public void constructS3ProductPicture(S3ProductPicture pic) throws PictureMemberException {

        if (pic.getProduct() == null || pic.getProduct().getId() <= 0) {
            throw new PictureMemberException("Picture is missing product, unable to create.");
        }
        if (pic.getPicture() == null) {
            throw new PictureMemberException("Picture has no picture data.");
        }
        if (pic.getS3Bucket() == null) {
            throw new PictureMemberException("Picture has no s3Bucket.");
        }
        create(pic);
        pic.setS3FileKey(pic.getProduct().getProductCode() + "_" + pic.getId() + "." + pic.getEncodeType());
        update(pic);

        if (pic.getProduct().getPrimaryPicture() == null) {
            pic.getProduct().setPrimaryPicture(pic);
            productService.update(pic.getProduct());
        }
        try {
            amazonS3.createS3Object(pic);
        } catch (S3ResolutionFailureException e) {
            throw new PictureMemberException("Unable to resolve s3 upload.", e);
        }
    }

    @Override
    @Transactional
    public void constructS3ProductPicture(List<S3ProductPicture> pictures) throws PictureMemberException {

        for (S3ProductPicture pic : pictures) {
            constructS3ProductPicture(pic);
        }
    }

    @Override
    @Transactional
    public void constructDatabaseProductPicture(DatabaseProductPicture pic) throws PictureMemberException {

        if (pic.getProduct() == null || pic.getProduct().getId() <= 0) {
            throw new PictureMemberException("Picture is missing product, unable to create.");
        }
        if (pic.getPicture() == null) {
            throw new PictureMemberException("Picture has no picture data.");
        }
        if (pic.getPicture().length > DatabaseProductPicture.MAXIMUM_SIZE) {
            throw new PictureMemberException("Pictures must be smaller than 16MB.");
        }
        create(pic);

        if (pic.getProduct().getPrimaryPicture() == null) {
            pic.getProduct().setPrimaryPicture(pic);
            productService.update(pic.getProduct());
        }
    }

    @Override
    @Transactional
    public void constructDatabaseProductPicture(List<DatabaseProductPicture> pictures) throws PictureMemberException {

        for (DatabaseProductPicture pic : pictures) {
            constructDatabaseProductPicture(pic);
        }
    }
}
