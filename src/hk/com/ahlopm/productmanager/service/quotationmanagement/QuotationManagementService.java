package hk.com.ahlopm.productmanager.service.quotationmanagement;

import hk.com.ahlopm.productmanager.exception.SerializedQuotationPersistenceException;

/**
 * The QuotationService is responsible for all manipulations and all logic regarding the Quotation domain object.
 * 
 * @author Cameron Newton
 *
 */
public interface QuotationManagementService {

    /**
     * Begins the process of moving a quotation from the review state to the finalizing state.
     * 
     * @param quotationId
     */
    public void beginFinalizingQuotation(long quotationId);

    /**
     * Returns a quotation from a later state to the review state, returning all of its duplicated reference objects to
     * the originals
     * 
     * @param quotationId
     * @return message description of review state.
     */
    public String reviewQuotation(long quotationId);

    /**
     * Takes a quotation in the finalizing state and moves it to the presenting state. During this process a serialized
     * quotation copy is created and stored in the database.
     * 
     * 
     * @param quotationId
     * @throws SerializedQuotationPersistenceException
     */
    public void beginPresentingQuotation(long quotationId) throws SerializedQuotationPersistenceException;

    /**
     * Takes a quotation in the presenting state and moves it to the accepted state. During this process the previous
     * presenting state serialized quotation record is also updated to the new accepted state.
     * 
     * @param quotationId
     * @throws SerializedQuotationPersistenceException
     */
    public void acceptQuotation(long quotationId) throws SerializedQuotationPersistenceException;

    /**
     * Takes a quotation in the presenting state and moves it to the declined state. During this process the previous
     * presenting state serialized quotation record is also updated to the new declined state.
     * 
     * @param quotationId
     * @throws SerializedQuotationPersistenceException
     */
    public void declineQuotation(long quotationId) throws SerializedQuotationPersistenceException;

    /**
     * Takes a quotation in the presenting state and moves it to the cancelled state. During this process the previous
     * presenting state serialized quotation record is deleted.
     * 
     * @param quotationId
     * @throws SerializedQuotationPersistenceException
     */
    public void cancelQuotation(long quotationId) throws SerializedQuotationPersistenceException;

}
