package hk.com.ahlopm.productmanager.service.quotationmanagement;

import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.quotation.QuotationStatus;
import hk.com.ahlopm.productmanager.domainobject.serializedquotation.SerializedQuotation;
import hk.com.ahlopm.productmanager.exception.IllegalStateProgressionException;
import hk.com.ahlopm.productmanager.exception.SerializedQuotationPersistenceException;
import hk.com.ahlopm.productmanager.service.quotation.QuotationService;
import hk.com.ahlopm.productmanager.service.serializedquotation.SerializedQuotationService;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class QuotationManagementServiceImpl implements QuotationManagementService {

    protected static final Logger logger = LogManager.getLogger(QuotationManagementServiceImpl.class);

    @Autowired
    private QuotationService quotationService;

    @Autowired
    private SerializedQuotationService serializedQuotationService;

    @Override
    @Transactional
    public void beginFinalizingQuotation(long quotationId) {

        Quotation quote = quotationService.find(quotationId);

        // This operation can only be performed on a quotation in the review
        // state.
        if (quote.getStatus() != QuotationStatus.REVIEW)
            throw new IllegalStateProgressionException("May not finalize a quotation that is not in the review state");

        if (quote.getBuyer() == null)
            throw new IllegalStateProgressionException("May not finalize a quotation without selecting a buyer.");

        if (quote.getProducts().size() == 0)
            throw new IllegalStateProgressionException("May not finalize a quotation without any products.");

        // Calculate the final price.
        quote.setPrice(quotationService.calculatePrice(quotationId));

        quote.setStatus(QuotationStatus.FINALIZING);

        quotationService.update(quote);
    }

    @Override
    @Transactional
    public String reviewQuotation(long quotationId) {

        String reviewMessage = "";

        Quotation quote = quotationService.find(quotationId);

        // Ensure the current state allows for this operation.
        if (quote.getStatus() == QuotationStatus.REVIEW || quote.getStatus() == QuotationStatus.PRESENTING) {

            throw new IllegalStateProgressionException(
                    "May not return a quotation to review from state: " + quote.getStatus());
        }

        quote.setStatus(QuotationStatus.REVIEW);
        quote.setRevisionNo(quote.getRevisionNo() + 1);

        reviewMessage = quote.getName() + " revision " + quote.getRevisionNo() + " has been created.";

        quotationService.update(quote);

        return reviewMessage;
    }

    @Override
    @Transactional
    public void beginPresentingQuotation(long quotationId) throws SerializedQuotationPersistenceException {

        Quotation quote = quotationService.find(quotationId);

        if (quote.getStatus() != QuotationStatus.FINALIZING) {
            throw new IllegalStateProgressionException(
                    "Only Quotations in the Finalizing state may be moved to Presenting.");
        }

        quote.setStatus(QuotationStatus.PRESENTING);
        quotationService.update(quote);

        serializedQuotationService.serializeQuotation(quote.getId());
    }

    @Override
    @Transactional
    public void acceptQuotation(long quotationId) throws SerializedQuotationPersistenceException {

        Quotation quote = quotationService.find(quotationId);

        if (quote.getStatus() != QuotationStatus.PRESENTING) {
            throw new IllegalStateProgressionException(
                    "Only Quotations in the Presenting state may be moved to accepted.");
        }
        SerializedQuotation serializedRecord = serializedQuotationService.findPresentingByQuotation(quote.getId());

        quote.setStatus(QuotationStatus.ACCEPTED);
        serializedRecord.setStatus(QuotationStatus.ACCEPTED);

        serializedQuotationService.update(serializedRecord);
        quotationService.update(quote);
    }

    @Override
    @Transactional
    public void declineQuotation(long quotationId) throws SerializedQuotationPersistenceException {

        Quotation quote = quotationService.find(quotationId);

        if (quote.getStatus() != QuotationStatus.PRESENTING) {
            throw new IllegalStateProgressionException(
                    "Only Quotations in the Presenting state may be moved to declined.");
        }
        SerializedQuotation serializedRecord = serializedQuotationService.findPresentingByQuotation(quote.getId());

        quote.setStatus(QuotationStatus.DECLINED);
        serializedRecord.setStatus(QuotationStatus.DECLINED);

        serializedQuotationService.update(serializedRecord);
        quotationService.update(quote);
    }

    @Override
    @Transactional
    public void cancelQuotation(long quotationId) throws SerializedQuotationPersistenceException {

        Quotation quote = quotationService.find(quotationId);

        if (quote.getStatus() != QuotationStatus.PRESENTING) {
            throw new IllegalStateProgressionException(
                    "Only Quotations in the Presenting state may be moved to cancelled.");
        }
        SerializedQuotation serializedRecord = serializedQuotationService.findPresentingByQuotation(quote.getId());

        quote.setStatus(QuotationStatus.CANCELLED);

        serializedQuotationService.delete(serializedRecord);
        quotationService.update(quote);
    }
}
