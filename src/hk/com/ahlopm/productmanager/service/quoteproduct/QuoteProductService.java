package hk.com.ahlopm.productmanager.service.quoteproduct;

import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;
import hk.com.ahlopm.productmanager.service.DomainObjectService;

/**
 * The QuoteProductService is responsible for all manipulations and all logic regarding the QuoteProduct domain object.
 * 
 * @author Cameron Newton
 *
 */
public interface QuoteProductService extends DomainObjectService<QuoteProduct> {

    public void addProductsToQuotation(long quotationId, long[] productIds);

}
