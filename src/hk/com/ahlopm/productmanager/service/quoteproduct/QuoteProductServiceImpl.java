package hk.com.ahlopm.productmanager.service.quoteproduct;

import hk.com.ahlopm.productmanager.dao.quoteproduct.QuoteProductDAO;
import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;
import hk.com.ahlopm.productmanager.exception.NotCreateOperationException;
import hk.com.ahlopm.productmanager.exception.NotUpdateOperationException;
import hk.com.ahlopm.productmanager.exception.ObjectNotFoundException;
import hk.com.ahlopm.productmanager.service.product.ProductService;
import hk.com.ahlopm.productmanager.service.quotation.QuotationService;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class QuoteProductServiceImpl implements QuoteProductService {

    protected static final Logger logger = LogManager.getLogger(QuoteProductServiceImpl.class);

    @Autowired
    private QuoteProductDAO quoteProductDAO;

    @Autowired
    private QuotationService quotationService;

    @Autowired
    private ProductService productService;

    @Override
    @Transactional
    public QuoteProduct find(long id) {

        QuoteProduct quoteProduct = quoteProductDAO.find(id);

        if (quoteProduct == null) {
            throw new ObjectNotFoundException("No QuoteProduct with id: " + id + " exists.");
        }
        return quoteProduct;
    }

    @Override
    @Transactional
    public List<QuoteProduct> findAll() {

        return quoteProductDAO.findAll();
    }

    @Override
    @Transactional
    public QuoteProduct create(QuoteProduct obj) {

        if (obj.getId() != 0) {
            throw new NotCreateOperationException("The identifier was not empty, not a create operation.");
        }
        quoteProductDAO.createOrUpdate(obj);
        return obj;
    }

    @Override
    @Transactional
    public QuoteProduct update(QuoteProduct obj) {

        if (obj.getId() == 0) {
            throw new NotUpdateOperationException("The identifier was not set, not an update operation.");
        }

        QuoteProduct databaseQP = find(obj.getId());
        databaseQP.setDiscount(obj.getDiscount());
        databaseQP.setQuantity(obj.getQuantity());

        quoteProductDAO.createOrUpdate(databaseQP);
        return obj;
    }

    @Override
    @Transactional
    public void delete(QuoteProduct obj) {

        quoteProductDAO.delete(find(obj.getId()));
    }

    @Override
    @Transactional
    public void delete(long id) {

        quoteProductDAO.delete(find(id));
    }

    @Override
    @Transactional
    public long count() {

        return quoteProductDAO.count();
    }

    @Override
    @Transactional
    public void addProductsToQuotation(long quotationId, long[] productIds) {

        Quotation quote = quotationService.find(quotationId);

        for (long productId : productIds) {
            QuoteProduct qp = new QuoteProduct();
            Product p = productService.find(productId);
            qp.setProduct(p);
            qp.setQuotation(quote);
            quote.getProducts().add(qp);
        }
        quotationService.update(quote);
    }
}
