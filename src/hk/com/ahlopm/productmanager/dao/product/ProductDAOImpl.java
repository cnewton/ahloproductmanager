package hk.com.ahlopm.productmanager.dao.product;

import hk.com.ahlopm.productmanager.dao.DomainObjectDAOImpl;
import hk.com.ahlopm.productmanager.domainobject.product.Product;

import org.springframework.stereotype.Repository;

@Repository
public class ProductDAOImpl extends DomainObjectDAOImpl<Product> implements ProductDAO {

}
