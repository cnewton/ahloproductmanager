package hk.com.ahlopm.productmanager.dao.product;

import hk.com.ahlopm.productmanager.dao.DomainObjectDAO;
import hk.com.ahlopm.productmanager.domainobject.product.Product;

public interface ProductDAO extends DomainObjectDAO<Product> {

}
