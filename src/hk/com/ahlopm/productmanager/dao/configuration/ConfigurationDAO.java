package hk.com.ahlopm.productmanager.dao.configuration;

import hk.com.ahlopm.productmanager.exception.ConfigurationNotFoundException;

/**
 * This DAO makes use of the ConfigurationEntity domain object to allow hibernate to perform mappings. When retrieving a
 * setting the variable name must be passed into the request. The ConfigurationEntity object is NOT returned, only the
 * configuration field.
 * 
 * @author Cameron Newton
 *
 */
public interface ConfigurationDAO {

    /**
     * @throws ConfigurationNotFoundException
     * @param variable
     * @return configuredSetting
     */
    public String getConfigSetting(String variable);

    /**
     * @throws ConfigurationNotFoundException
     * @param variable
     * @param config
     */
    public void setConfigSetting(String variable, String config);

}
