package hk.com.ahlopm.productmanager.dao.configuration;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This entity is only used to allow hibernate to map configuration database results to an object, the object is not
 * exposed to any other layer or outer class as the DAO only returns the configuration field.
 * 
 * @author Cameron Newton
 *
 */
@Entity
@Table(name = "Configuration_Settings")
public class ConfigurationEntity {

    @Id
    private String variable;

    private String configuration;

    public String getVariable() {

        return variable;
    }

    public String getConfiguration() {

        return configuration;
    }

    public void setVariable(String variable) {

        this.variable = variable;
    }

    public void setConfiguration(String configuration) {

        this.configuration = configuration;
    }

}
