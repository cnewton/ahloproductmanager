package hk.com.ahlopm.productmanager.dao.configuration;

import hk.com.ahlopm.productmanager.exception.ConfigurationNotFoundException;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ConfigurationDAOImpl implements ConfigurationDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public String getConfigSetting(String variable) {

        ConfigurationEntity ce = findConfigSetting(variable);

        return ce.getConfiguration();
    }

    @Override
    public void setConfigSetting(String variable, String config) {

        ConfigurationEntity ce = findConfigSetting(variable);
        ce.setConfiguration(config);
        getCurrentSession().update(ce);
    }

    private ConfigurationEntity findConfigSetting(String variable) {

        Criteria query = getCurrentSession().createCriteria(ConfigurationEntity.class);
        query.add(Restrictions.idEq(variable));

        ConfigurationEntity ce = (ConfigurationEntity) query.uniqueResult();

        if (ce == null) {
            throw new ConfigurationNotFoundException("The requested configuration setting " + "does not exist.");
        }

        return ce;
    }

    protected Session getCurrentSession() {

        return sessionFactory.getCurrentSession();
    }

}
