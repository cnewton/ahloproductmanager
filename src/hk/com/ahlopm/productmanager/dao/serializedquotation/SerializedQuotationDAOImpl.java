package hk.com.ahlopm.productmanager.dao.serializedquotation;

import hk.com.ahlopm.productmanager.dao.DomainObjectDAOImpl;
import hk.com.ahlopm.productmanager.domainobject.quotation.QuotationStatus;
import hk.com.ahlopm.productmanager.domainobject.serializedquotation.SerializedQuotation;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository
public class SerializedQuotationDAOImpl extends DomainObjectDAOImpl<SerializedQuotation>
        implements SerializedQuotationDAO {

    @SuppressWarnings("unchecked")
    @Override
    public List<SerializedQuotation> findByQuotationId(long quotationId) {

        Criteria c = getCurrentSession().createCriteria(SerializedQuotation.class);
        c.add(Restrictions.eq("quotationId", quotationId));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        return c.list();
    }

    @Override
    public SerializedQuotation findPresentingByQuotationId(long quotationId) {

        Criteria c = getCurrentSession().createCriteria(SerializedQuotation.class);
        c.add(Restrictions.eq("quotationId", quotationId));
        c.add(Restrictions.eq("status", QuotationStatus.PRESENTING));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        return (SerializedQuotation) c.uniqueResult();
    }

}
