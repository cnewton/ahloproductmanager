package hk.com.ahlopm.productmanager.dao.serializedquotation;

import hk.com.ahlopm.productmanager.dao.DomainObjectDAO;
import hk.com.ahlopm.productmanager.domainobject.serializedquotation.SerializedQuotation;

import java.util.List;

public interface SerializedQuotationDAO extends DomainObjectDAO<SerializedQuotation> {

    public List<SerializedQuotation> findByQuotationId(long quotationId);

    public SerializedQuotation findPresentingByQuotationId(long quotationId);

}
