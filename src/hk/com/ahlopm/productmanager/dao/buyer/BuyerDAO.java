package hk.com.ahlopm.productmanager.dao.buyer;

import hk.com.ahlopm.productmanager.dao.DomainObjectDAO;
import hk.com.ahlopm.productmanager.domainobject.buyer.Buyer;

public interface BuyerDAO extends DomainObjectDAO<Buyer> {

}
