package hk.com.ahlopm.productmanager.dao.buyer;

import hk.com.ahlopm.productmanager.dao.DomainObjectDAOImpl;
import hk.com.ahlopm.productmanager.domainobject.buyer.Buyer;

import org.springframework.stereotype.Repository;

@Repository
public class BuyerDAOImpl extends DomainObjectDAOImpl<Buyer> implements BuyerDAO {

}
