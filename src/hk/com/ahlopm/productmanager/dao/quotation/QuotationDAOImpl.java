package hk.com.ahlopm.productmanager.dao.quotation;

import hk.com.ahlopm.productmanager.dao.DomainObjectDAOImpl;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;
import hk.com.ahlopm.productmanager.domainobject.quotation.QuotationStatus;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository
public class QuotationDAOImpl extends DomainObjectDAOImpl<Quotation> implements QuotationDAO {

    @Override
    @SuppressWarnings("unchecked")
    public List<Quotation> findAllInReview() {

        Criteria c = getCurrentSession().createCriteria(Quotation.class);
        c.add(Restrictions.eq("status", QuotationStatus.REVIEW));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        return c.list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Quotation> findAllInManagement() {

        Object[] manageStates = { QuotationStatus.ACCEPTED, QuotationStatus.DECLINED, QuotationStatus.PRESENTING,
                QuotationStatus.FINALIZING, QuotationStatus.CANCELLED };

        Criteria c = getCurrentSession().createCriteria(Quotation.class);
        c.add(Restrictions.in("status", manageStates));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        return c.list();
    }
}
