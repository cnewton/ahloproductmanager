package hk.com.ahlopm.productmanager.dao.quotation;

import java.util.List;

import hk.com.ahlopm.productmanager.dao.DomainObjectDAO;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;

public interface QuotationDAO extends DomainObjectDAO<Quotation> {

    public List<Quotation> findAllInManagement();

    public List<Quotation> findAllInReview();

}
