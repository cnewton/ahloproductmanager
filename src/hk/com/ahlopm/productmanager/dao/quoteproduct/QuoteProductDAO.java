package hk.com.ahlopm.productmanager.dao.quoteproduct;

import hk.com.ahlopm.productmanager.dao.DomainObjectDAO;
import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;

public interface QuoteProductDAO extends DomainObjectDAO<QuoteProduct> {

}
