package hk.com.ahlopm.productmanager.dao.quoteproduct;

import hk.com.ahlopm.productmanager.dao.DomainObjectDAOImpl;
import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;

import org.springframework.stereotype.Repository;

@Repository
public class QuoteProductDAOImpl extends DomainObjectDAOImpl<QuoteProduct> implements QuoteProductDAO {

}
