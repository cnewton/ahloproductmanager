package hk.com.ahlopm.productmanager.dao.productpicture;

import hk.com.ahlopm.productmanager.dao.DomainObjectDAOImpl;
import hk.com.ahlopm.productmanager.domainobject.productpicture.ProductPicture;

import org.springframework.stereotype.Repository;

@Repository
public class ProductPictureDAOImpl extends DomainObjectDAOImpl<ProductPicture> implements ProductPictureDAO {

}
