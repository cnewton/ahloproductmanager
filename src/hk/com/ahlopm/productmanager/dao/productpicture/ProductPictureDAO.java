package hk.com.ahlopm.productmanager.dao.productpicture;

import hk.com.ahlopm.productmanager.dao.DomainObjectDAO;
import hk.com.ahlopm.productmanager.domainobject.productpicture.ProductPicture;

public interface ProductPictureDAO extends DomainObjectDAO<ProductPicture> {

}
