package hk.com.ahlopm.productmanager.dao.user;

import hk.com.ahlopm.productmanager.dao.DomainObjectDAOImpl;
import hk.com.ahlopm.productmanager.domainobject.user.User;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAOImpl extends DomainObjectDAOImpl<User> implements UserDAO {

    @Override
    public User findByUsername(String username) {

        Criteria c = getCurrentSession().createCriteria(User.class);
        c.add(Restrictions.eq("username", username));
        c.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        return (User) c.uniqueResult();
    }
}
