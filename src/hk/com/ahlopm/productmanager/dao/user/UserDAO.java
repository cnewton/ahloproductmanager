package hk.com.ahlopm.productmanager.dao.user;

import hk.com.ahlopm.productmanager.dao.DomainObjectDAO;
import hk.com.ahlopm.productmanager.domainobject.user.User;

public interface UserDAO extends DomainObjectDAO<User> {

    public User findByUsername(String username);

}
