package hk.com.ahlopm.productmanager.amazons3;

public interface S3RetrievableObject {

    public String getS3Bucket();

    public String getS3FileKey();

    public byte[] getObjectData();

    public void setS3Bucket(String s3Bucket);

    public void setS3FileKey(String s3FileKey);

    public void setObjectData(byte[] data);

}
