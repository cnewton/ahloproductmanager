package hk.com.ahlopm.productmanager.amazons3;

import hk.com.ahlopm.productmanager.exception.S3ResolutionFailureException;
import hk.com.ahlopm.productmanager.service.configuration.ConfigurationService;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.util.IOUtils;

@Component
public class AmazonS3Gateway {

    protected static final Logger logger = LogManager.getLogger(AmazonS3Gateway.class);

    @Autowired
    private ConfigurationService configService;

    private AmazonS3 s3Client = null;

    public void resolveS3Object(S3RetrievableObject object) throws S3ResolutionFailureException {

        if (s3Client == null) {
            s3Client = new AmazonS3Client(new BasicAWSCredentials(configService.get_AWS_S3_ACCESS_KEY(),
                    configService.get_AWS_S3_SECRET_KEY()));
        }

        try {
            S3Object s3object = s3Client.getObject(new GetObjectRequest(object.getS3Bucket(), object.getS3FileKey()));

            InputStream input = s3object.getObjectContent();

            object.setObjectData(IOUtils.toByteArray(input));

            s3object.close();

        } catch (AmazonServiceException e) {
            logger.warn("::::AmazonServiceException");
            logger.warn("Error Message:    " + e.getMessage());
            logger.warn("HTTP Status Code: " + e.getStatusCode());
            logger.warn("AWS Error Code:   " + e.getErrorCode());
            logger.warn("Error Type:       " + e.getErrorType());
            logger.warn("Request ID:       " + e.getRequestId());
            logger.warn("AmazonServiceException::::");

            s3Client = null;
            throw new S3ResolutionFailureException(e);

        } catch (AmazonClientException e) {
            logger.warn(":::::AmazonClientException");
            logger.warn("Error Message: " + e.getMessage());
            logger.warn("AmazonClientException:::::");

            s3Client = null;
            throw new S3ResolutionFailureException(e);

        } catch (IOException e) {
            logger.error("Severe error while reading from s3 stream", e);

            s3Client = null;
            throw new S3ResolutionFailureException(e);
        }

    }

    public void createS3Object(S3RetrievableObject object) throws S3ResolutionFailureException {

        if (s3Client == null) {
            s3Client = new AmazonS3Client(new BasicAWSCredentials(configService.get_AWS_S3_ACCESS_KEY(),
                    configService.get_AWS_S3_SECRET_KEY()));
        }

        if (object.getObjectData() == null) {
            throw new S3ResolutionFailureException("No object data for upload.");
        }

        try {
            ByteArrayInputStream bais = new ByteArrayInputStream(object.getObjectData());
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(object.getObjectData().length);
            s3Client.putObject(object.getS3Bucket(), object.getS3FileKey(), bais, metadata);

        } catch (AmazonServiceException e) {
            logger.warn("::::AmazonServiceException");
            logger.warn("Error Message:    " + e.getMessage());
            logger.warn("HTTP Status Code: " + e.getStatusCode());
            logger.warn("AWS Error Code:   " + e.getErrorCode());
            logger.warn("Error Type:       " + e.getErrorType());
            logger.warn("Request ID:       " + e.getRequestId());
            logger.warn("AmazonServiceException::::");

            s3Client = null;
            throw new S3ResolutionFailureException(e);

        } catch (AmazonClientException e) {
            logger.warn(":::::AmazonClientException");
            logger.warn("Error Message: " + e.getMessage());
            logger.warn("AmazonClientException:::::");

            s3Client = null;
            throw new S3ResolutionFailureException(e);
        }
    }
}
