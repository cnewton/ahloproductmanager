package hk.com.ahlopm.productmanager.domainobject.quotation;

/**
 * The QuotationStatus represents the last known status of a Quotation domain object. As a quotation is progressed
 * through its lifecycle the status will need to be kept up to date.
 * 
 * @author Cameron Newton
 *
 */
public enum QuotationStatus {

    /** The Quotation is being created and/or is in review. */
    REVIEW("Review"),

    /** The Quotation is finished being modified and is finalizing */
    FINALIZING("Finalizing"),

    /** The Quotation has been presented to the client, awaiting acceptance */
    PRESENTING("Being Presented to client"),

    /** The Quotation has been accepted by the client in its current state. */
    ACCEPTED("Accepted by Client"),

    /** The Quotation has been declined by the client in its current state. */
    DECLINED("Declined by Client"),

    /** The Quotation presentation has been cancelled. */
    CANCELLED("Declined by Client");

    private String status;

    private QuotationStatus(String status) {

        this.status = status;
    }

    public String getStatus() {

        return status;
    }

}
