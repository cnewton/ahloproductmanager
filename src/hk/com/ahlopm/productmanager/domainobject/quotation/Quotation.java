package hk.com.ahlopm.productmanager.domainobject.quotation;

import hk.com.ahlopm.productmanager.domainobject.IdBasedDomainObject;
import hk.com.ahlopm.productmanager.domainobject.buyer.Buyer;
import hk.com.ahlopm.productmanager.domainobject.invoice.Invoice;
import hk.com.ahlopm.productmanager.domainobject.quoteproduct.QuoteProduct;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * The Quotation domain object represents a sales quotation. This object stores the list of products and quantities as
 * well as the buyer snapshot and total price. The state of the quotation is represented by the String enum
 * QuotationStatus.
 * 
 * @version 1.0
 * @since 1.0
 * @author Cameron
 *
 */
@Entity
@Table(name = "Quotations")
public class Quotation implements IdBasedDomainObject {

    @Id
    @Min(value = 0, message = "ID must not be less than 0")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id = 0;

    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "buyerId", nullable = true)
    private Buyer buyer;

    @Cascade({ CascadeType.SAVE_UPDATE, CascadeType.DELETE })
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "quotation", orphanRemoval = true)
    @JsonManagedReference
    private Set<QuoteProduct> products;

    private BigDecimal price = BigDecimal.ZERO;

    @Enumerated(EnumType.STRING)
    private QuotationStatus status = QuotationStatus.REVIEW;

    private BigDecimal discount = BigDecimal.ZERO;

    private int revisionNo = 0;

    private String remarks;

    private LocalDateTime lastModified;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "invoiceId", nullable = true)
    private Invoice invoice;

    public long getId() {

        return id;
    }

    public String getName() {

        return name;
    }

    public Buyer getBuyer() {

        return buyer;
    }

    public Set<QuoteProduct> getProducts() {

        return products;
    }

    public BigDecimal getPrice() {

        return price;
    }

    public QuotationStatus getStatus() {

        return status;
    }

    public BigDecimal getDiscount() {

        return discount;
    }

    public int getRevisionNo() {

        return revisionNo;
    }

    public String getRemarks() {

        return remarks;
    }

    public LocalDateTime getLastModified() {

        return lastModified;
    }

    public Invoice getInvoice() {

        return invoice;
    }

    public void setId(long id) {

        this.id = id;
    }

    public void setName(String name) {

        this.name = name;
    }

    public void setBuyer(Buyer buyer) {

        this.buyer = buyer;
    }

    public void setProducts(Set<QuoteProduct> products) {

        this.products = products;
    }

    public void setPrice(BigDecimal price) {

        this.price = price;
    }

    public void setStatus(QuotationStatus status) {

        this.status = status;
    }

    public void setDiscount(BigDecimal discount) {

        this.discount = discount;
    }

    public void setRevisionNo(int revisionNo) {

        this.revisionNo = revisionNo;
    }

    public void setRemarks(String remarks) {

        this.remarks = remarks;
    }

    public void setLastModified(LocalDateTime lastModified) {

        this.lastModified = lastModified;
    }

    public void setInvoice(Invoice invoice) {

        this.invoice = invoice;
    }

    @Override
    public boolean equals(Object quotation) {

        return EqualsBuilder.reflectionEquals(this, quotation);
    }

    @Override
    public String toString() {

        return ToStringBuilder.reflectionToString(this);
    }

}
