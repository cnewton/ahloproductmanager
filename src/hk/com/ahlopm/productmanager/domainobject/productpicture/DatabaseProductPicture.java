package hk.com.ahlopm.productmanager.domainobject.productpicture;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Entity
@Table(name = "DatabaseProductPictures")
@JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class DatabaseProductPicture extends ProductPicture {

    // Denotes the maximum data size of medium blob (IN MYSQL)
    public static final long MAXIMUM_SIZE = 1670000;

    @Column(name = "picture", columnDefinition = "mediumblob")
    private byte[] picture;

    public DatabaseProductPicture() {

        this.setId(0);
    }

    public DatabaseProductPicture(ProductPicture base) {

        this.setPicture(base.getPicture());
        this.setEncodeType(base.getEncodeType());
        this.setId(base.getId());
        this.setProduct(base.getProduct());
    }

    public byte[] getPicture() {

        return this.picture;
    }

    public void setPicture(byte[] picture) {

        this.picture = picture;
    }

    @Override
    public void resolveImage() {

        // Database pictures are already resolved.
    }

    @Override
    public boolean equals(Object picture) {

        return EqualsBuilder.reflectionEquals(this, picture);
    }

    @Override
    public String toString() {

        return ToStringBuilder.reflectionToString(this);
    }

}
