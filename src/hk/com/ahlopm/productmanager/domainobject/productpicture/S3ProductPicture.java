package hk.com.ahlopm.productmanager.domainobject.productpicture;

import hk.com.ahlopm.productmanager.amazons3.S3RetrievableObject;
import hk.com.ahlopm.productmanager.common.ApplicationContextProvider;
import hk.com.ahlopm.productmanager.exception.S3ResolutionFailureException;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Entity
@Table(name = "S3ProductPictures")
@JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class S3ProductPicture extends ProductPicture implements S3RetrievableObject {

    private String s3Bucket;

    private String s3FileKey;

    public S3ProductPicture() {

        this.setId(0);
    }

    public S3ProductPicture(ProductPicture base) {

        this.setPicture(base.getPicture());
        this.setEncodeType(base.getEncodeType());
        this.setId(base.getId());
        this.setProduct(base.getProduct());
    }

    @Override
    public String getS3Bucket() {

        return s3Bucket;
    }

    @Override
    public String getS3FileKey() {

        return s3FileKey;
    }

    @Override
    public void setS3Bucket(String s3Bucket) {

        this.s3Bucket = s3Bucket;
    }

    @Override
    public void setS3FileKey(String s3FileKey) {

        this.s3FileKey = s3FileKey;
    }

    @Override
    public void resolveImage() throws S3ResolutionFailureException {

        ApplicationContextProvider.getS3PictureResolver().resolveS3Object(this);
    }

    @Override
    public boolean equals(Object picture) {

        return EqualsBuilder.reflectionEquals(this, picture);
    }

    @Override
    public String toString() {

        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public void setObjectData(byte[] data) {

        super.setPicture(data);
    }

    @Override
    public byte[] getObjectData() {

        return super.getPicture();
    }

}
