package hk.com.ahlopm.productmanager.domainobject.productpicture;

import hk.com.ahlopm.productmanager.domainobject.IdBasedDomainObject;
import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.exception.ResolutionFailureException;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "ProductPictures")
@JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class ProductPicture implements IdBasedDomainObject {

    @Id
    @Min(value = 0, message = "ID must not be less than 0")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id = 0;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "productId", nullable = false)
    @JsonBackReference
    private Product product;

    private String encodeType;

    @Transient
    // Not database related.
    protected byte[] picture;

    public long getId() {

        return id;
    }

    public Product getProduct() {

        return product;
    }

    public String getEncodeType() {

        return encodeType;
    }

    public byte[] getPicture() {

        return this.picture;
    }

    public void setId(long id) {

        this.id = id;
    }

    public void setProduct(Product product) {

        this.product = product;
    }

    public void setEncodeType(String name) {

        this.encodeType = name;
    }

    public void setPicture(byte[] picture) {

        this.picture = picture;
    }

    public void resolveImage() throws ResolutionFailureException {

        throw new ResolutionFailureException("Unable to resolve base image type");
    }

    @Override
    public boolean equals(Object picture) {

        return EqualsBuilder.reflectionEquals(this, picture);
    }

    @Override
    public String toString() {

        return ToStringBuilder.reflectionToString(this);
    }

}
