package hk.com.ahlopm.productmanager.domainobject.serializedquotation;

import hk.com.ahlopm.productmanager.domainobject.IdBasedDomainObject;
import hk.com.ahlopm.productmanager.domainobject.quotation.QuotationStatus;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;

/**
 * The QuotationBlob domain object represents a sales quotation in fully serialized form as it was when it was moved to
 * a state that requires persisting.
 * 
 * @author Cameron
 *
 */
@Entity
@Table(name = "SerializedQuotations")
public class SerializedQuotation implements IdBasedDomainObject {

    @Id
    @Min(value = 0, message = "ID must not be less than 0")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id = 0;

    @Enumerated(EnumType.STRING)
    private QuotationStatus status;

    private long quotationId;

    @Column(name = "quotation", columnDefinition = "blob")
    private String quotation;

    public long getId() {

        return id;
    }

    public QuotationStatus getStatus() {

        return status;
    }

    public long getQuotationId() {

        return quotationId;
    }

    public String getQuotation() {

        return quotation;
    }

    public void setId(long id) {

        this.id = id;
    }

    public void setStatus(QuotationStatus status) {

        this.status = status;
    }

    public void setQuotationId(long quotationId) {

        this.quotationId = quotationId;
    }

    public void setQuotation(String quotation) {

        this.quotation = quotation;
    }

}
