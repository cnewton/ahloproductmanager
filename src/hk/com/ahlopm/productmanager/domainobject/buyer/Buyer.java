package hk.com.ahlopm.productmanager.domainobject.buyer;

import hk.com.ahlopm.productmanager.domainobject.IdBasedDomainObject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * The Buyer domain object represents a snapshot of a buyers information and contact details at the time of a quotation
 * being made.
 * 
 * @version 1.0
 * @since 1.0
 * @author Cameron
 *
 */
@Entity
@Table(name = "Buyers")
public class Buyer implements IdBasedDomainObject {

    @Id
    @Min(value = 0, message = "ID must not be less than 0")
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id = 0;

    private String name;

    private String phone;

    private String email;

    private String address;

    private String company;

    public long getId() {

        return id;
    }

    public String getName() {

        return name;
    }

    public String getPhone() {

        return phone;
    }

    public String getEmail() {

        return email;
    }

    public String getAddress() {

        return address;
    }

    public String getCompany() {

        return company;
    }

    public void setId(long id) {

        this.id = id;
    }

    public void setName(String name) {

        this.name = name;
    }

    public void setPhone(String phone) {

        this.phone = phone;
    }

    public void setEmail(String email) {

        this.email = email;
    }

    public void setAddress(String address) {

        this.address = address;
    }

    public void setCompany(String company) {

        this.company = company;
    }

    @Override
    public boolean equals(Object buyer) {

        return EqualsBuilder.reflectionEquals(this, buyer);
    }

    @Override
    public String toString() {

        return ToStringBuilder.reflectionToString(this);
    }
}
