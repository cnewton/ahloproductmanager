package hk.com.ahlopm.productmanager.domainobject.quoteproduct;

import hk.com.ahlopm.productmanager.domainobject.IdBasedDomainObject;
import hk.com.ahlopm.productmanager.domainobject.product.Product;
import hk.com.ahlopm.productmanager.domainobject.quotation.Quotation;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * A QuoteProduct represents a single relation of a quotation to a product. Each Quotation stores a list of
 * QuoteProducts as this is a Many Products to One Quotation relation. This relational object also stores the quantity
 * of the given product to be included in the quotation as well as any applicable discount to the preset product price.
 * Discount may be negative to indicate an increase in price.
 * 
 * @author Cameron Newton
 *
 */
@Entity
@Table(name = "QuoteProducts")
public class QuoteProduct implements IdBasedDomainObject {

    @Id
    @Min(value = 0, message = "ID must not be less than 0")
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id = 0;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "quotationId", nullable = false)
    @JsonBackReference
    private Quotation quotation;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "productId", nullable = false)
    private Product product;

    @Column(name = "quantity")
    @Min(value = 0, message = "Quantity must not be less than 0")
    private int quantity = 0;

    @Column(name = "discount")
    private BigDecimal discount = BigDecimal.ZERO;

    public long getId() {

        return id;
    }

    public Quotation getQuotation() {

        return quotation;
    }

    public Product getProduct() {

        return product;
    }

    public int getQuantity() {

        return quantity;
    }

    public BigDecimal getDiscount() {

        return discount;
    }

    public void setId(long id) {

        this.id = id;
    }

    public void setQuotation(Quotation quotation) {

        this.quotation = quotation;
    }

    public void setProduct(Product product) {

        this.product = product;
    }

    public void setQuantity(int quantity) {

        this.quantity = quantity;
    }

    public void setDiscount(BigDecimal discount) {

        this.discount = discount;
    }

    @Override
    public boolean equals(Object product) {

        return EqualsBuilder.reflectionEquals(this, product);
    }

    @Override
    public String toString() {

        return ToStringBuilder.reflectionToString(this);
    }
}
