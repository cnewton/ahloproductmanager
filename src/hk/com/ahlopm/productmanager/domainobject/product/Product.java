package hk.com.ahlopm.productmanager.domainobject.product;

import hk.com.ahlopm.productmanager.domainobject.IdBasedDomainObject;
import hk.com.ahlopm.productmanager.domainobject.productpicture.ProductPicture;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * The Product domain object represents a product that is or once was part of the inventory.
 * 
 * @author Cameron
 *
 */
@Entity
@Table(name = "Products")
public class Product implements IdBasedDomainObject {

    @Id
    @Min(value = 0, message = "ID must not be less than 0")
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id = 0;

    private String description;

    private String style;

    private String finish;

    private BigDecimal price = BigDecimal.ZERO;

    private String productCode;

    private String quantity;

    private String stockPosition;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonManagedReference
    @JoinColumn(name = "primaryPictureId", nullable = true)
    private ProductPicture primaryPicture;

    @Cascade({ CascadeType.SAVE_UPDATE, CascadeType.DELETE })
    @JsonManagedReference
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product", orphanRemoval = true)
    private Set<ProductPicture> pictures;

    public long getId() {

        return id;
    }

    public String getDescription() {

        return description;
    }

    public String getStyle() {

        return style;
    }

    public String getFinish() {

        return finish;
    }

    public BigDecimal getPrice() {

        return price;
    }

    public String getProductCode() {

        return productCode;
    }

    public String getQuantity() {

        return quantity;
    }

    public String getStockPosition() {

        return stockPosition;
    }

    public ProductPicture getPrimaryPicture() {

        return primaryPicture;
    }

    public Set<ProductPicture> getPictures() {

        return pictures;
    }

    public void setId(long id) {

        this.id = id;
    }

    public void setDescription(String description) {

        this.description = description;
    }

    public void setStyle(String style) {

        this.style = style;
    }

    public void setFinish(String finish) {

        this.finish = finish;
    }

    public void setPrice(BigDecimal price) {

        this.price = price;
    }

    public void setProductCode(String productCode) {

        this.productCode = productCode;
    }

    public void setQuantity(String quantity) {

        this.quantity = quantity;
    }

    public void setStockPosition(String stockPosition) {

        this.stockPosition = stockPosition;
    }

    public void setPrimaryPicture(ProductPicture primaryPicture) {

        this.primaryPicture = primaryPicture;
    }

    public void setPictures(Set<ProductPicture> pictures) {

        this.pictures = pictures;
    }

    @Override
    public boolean equals(Object product) {

        return EqualsBuilder.reflectionEquals(this, product);
    }

    @Override
    public String toString() {

        return ToStringBuilder.reflectionToString(this);
    }

}
