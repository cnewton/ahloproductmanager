package hk.com.ahlopm.productmanager.domainobject.user;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "UserRoles")
public class UserRole {

    @Id
    @Min(value = 0, message = "ID must not be less than 0")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId", nullable = false)
    @JsonBackReference
    private User user;

    private String role;

    public long getId() {

        return id;
    }

    public User getUser() {

        return user;
    }

    public String getRole() {

        return role;
    }

    public void setId(long id) {

        this.id = id;
    }

    public void setUser(User user) {

        this.user = user;
    }

    public void setRole(String role) {

        this.role = role;
    }

}
