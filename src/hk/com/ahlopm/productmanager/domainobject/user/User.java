package hk.com.ahlopm.productmanager.domainobject.user;

import hk.com.ahlopm.productmanager.domainobject.IdBasedDomainObject;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Min;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table(name = "Users")
public class User implements IdBasedDomainObject {

    @Id
    @Min(value = 0, message = "ID must not be less than 0")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "username", unique = true, nullable = false)
    private String username;

    @JsonProperty(access = Access.WRITE_ONLY)
    private String password = "DefaultPassword";

    private String email;

    private String name;

    @Cascade({ CascadeType.SAVE_UPDATE, CascadeType.DELETE })
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user", orphanRemoval = true)
    @JsonManagedReference
    private Set<UserRole> roles;

    private boolean enabled;

    public long getId() {

        return id;
    }

    public String getUsername() {

        return username;
    }

    public String getPassword() {

        return password;
    }

    public String getEmail() {

        return email;
    }

    public String getName() {

        return name;
    }

    public Set<UserRole> getRoles() {

        return roles;
    }

    public void setId(long id) {

        this.id = id;
    }

    public void setUsername(String username) {

        this.username = username;
    }

    public void setPassword(String password) {

        this.password = password;
    }

    public void setEmail(String email) {

        this.email = email;
    }

    public void setName(String name) {

        this.name = name;
    }

    public void setRoles(Set<UserRole> roles) {

        this.roles = roles;
    }

    public boolean isEnabled() {

        return enabled;
    }

    public void setEnabled(boolean enabled) {

        this.enabled = enabled;
    }

}
