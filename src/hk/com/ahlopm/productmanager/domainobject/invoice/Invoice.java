package hk.com.ahlopm.productmanager.domainobject.invoice;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "Invoices")
@JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class Invoice {

    @Id
    @Min(value = 0, message = "ID must not be less than 0")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Transient
    private byte[] invoice;

    public long getId() {

        return id;
    }

    public byte[] getInvoice() {

        return invoice;
    }

    public void setId(long id) {

        this.id = id;
    }

    public void setInvoice(byte[] invoice) {

        this.invoice = invoice;
    }

    @Override
    public boolean equals(Object invoice) {

        return EqualsBuilder.reflectionEquals(this, invoice);
    }

    @Override
    public String toString() {

        return ToStringBuilder.reflectionToString(this);
    }

}
