package hk.com.ahlopm.productmanager.domainobject.invoice;

import hk.com.ahlopm.productmanager.amazons3.S3RetrievableObject;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Entity
@Table(name = "S3Invoices")
@JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public class S3Invoice extends Invoice implements S3RetrievableObject {

    private String s3Bucket;

    private String s3FileKey;

    @Override
    public String getS3Bucket() {

        return s3Bucket;
    }

    @Override
    public String getS3FileKey() {

        return s3FileKey;
    }

    @Override
    public byte[] getObjectData() {

        return this.getInvoice();
    }

    @Override
    public void setS3Bucket(String s3Bucket) {

        this.s3Bucket = s3Bucket;
    }

    @Override
    public void setS3FileKey(String s3FileKey) {

        this.s3FileKey = s3FileKey;
    }

    @Override
    public void setObjectData(byte[] data) {

        this.setInvoice(data);
    }

}
