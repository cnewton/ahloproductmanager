package hk.com.ahlopm.productmanager.domainobject;

public interface IdBasedDomainObject {

    public long getId();

    public void setId(long id);

}
