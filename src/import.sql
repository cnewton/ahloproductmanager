

 
#This File will be run after a new database has been created by hibernate's auto create system.

#This script creates the root admin user as well as the configuration table.

#The password for the root user will be 'password16'

#This file is for MySQL databases.

#Note that entries must be all in one line.

INSERT INTO Users (id, name, email, enabled, username, password) VALUES (1, "Root User", "root@email.com", true, "rootuser", "$2a$10$7kYAWhRHc5yRlAH8psQoWOMptkgQp5JkDe.TQXdXwTchi5UXXE5WW");

INSERT INTO UserRoles(id, userId, role) VALUES (1, 1, "ROLE_ADMIN");

INSERT INTO UserRoles(id, userId, role) VALUES (2, 1, "ROLE_USER");

INSERT INTO Configuration_Settings (variable, configuration) VALUES ("INVALID_HEADER_NAME", "X-Invalid-Data");

INSERT INTO Configuration_Settings (variable, configuration) VALUES ("AWS_S3_ACCESS_KEY", "AKIAJ5DRPCI77O6WAOMA");

INSERT INTO Configuration_Settings (variable, configuration) VALUES ("AWS_S3_SECRET_KEY", "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");

INSERT INTO Configuration_Settings (variable, configuration) VALUES ("DEFAULT_S3_BUCKET_NAME", "ahloproductmanager");

INSERT INTO Configuration_Settings (variable, configuration) VALUES ("HMAC_SECRET_KEY", "5489bb5b17c182793d7aae442197e9eace37d3c48304ff10b47b4fb83dbbff09");

INSERT INTO Configuration_Settings (variable, configuration) VALUES ("AUTH_HEADER_NAME", "X-AUTH-TOKEN");

INSERT INTO Configuration_Settings (variable, configuration) VALUES ("AUTH_TOKEN_LIFE_SECONDS", "28800");
